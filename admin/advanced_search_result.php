<?php
require('includes/application_top.php');
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH);
require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div class="boxCatTitle">
	<button class="filter-button" id="hide" >-</button>
	<button class="filter-button" id="show">+</button>
	<?php echo HEADING_TITLE_1; ?></div>

<?php
if ($messageStack->size('search') > 0) {
	echo $messageStack->output('search');
}
echo tep_draw_form('advanced_search', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get', 'onsubmit="return check_form(this);"') . tep_hide_session_id();
?>
<div class="contentContainer">
	<script>
		$(document).ready(function () {
			$("#hide").click(function () {
				$("#filters").hide();
			});
			$("#show").click(function () {
				$("#filters").show();
			});
		});
	</script>
	<?php if ($HTTP_GET_VARS['search_in_posters'] == 1) { ?>
		<div class="contentText" id="filters"><table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td colspan="2">      <?php echo tep_draw_input_field('keywords', '', 'style="width: 100%" class="searchadvanced" placeholder="إبحث عن ملصق" ', 'text', false) . tep_draw_hidden_field('search_in_description', '1'); ?>
						<input type="hidden" name="actors_id"  id="actor_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_ACTORS; ?>"/>
						<input type="hidden" name="directors_id"  id="director_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_DIRECTORS; ?>"/>
				</tr>
				<tr><td colspan="2">
						<?php echo tep_draw_input_field('pfrom', '', ' class="searchadvanced-sm" placeholder="السعر من" ', 'text', false); ?>
						<?php echo tep_draw_input_field('pto', '', ' class="searchadvanced-sm" placeholder="السعر الى" ', 'text', false); ?>
						<input type="hidden" name="products_date_released"  id="products_date_released" class="selectadvanced" placeholder="<?php echo TEXT_RELEASE_YEAR; ?>"/>
						<input type="hidden" name="products_size"  id="products_size" class="selectadvanced" placeholder="<?php echo TEXT_PRODUCTS_SIZE; ?>"/>
						<input type="hidden" name="search_in_posters"  id="search_in_posters" value="<?php echo $HTTP_GET_VARS['search_in_posters']; ?>"/>
					</td></tr>
			</table>
			<div>
				<span style="float: left;">

					<?php echo tep_draw_button(IMAGE_BUTTON_SEARCH, 'search', null, 'primary'); ?></span>
			</div>
		</div>
		<div id="directorsArr" class="hide"><?php echo json_encode(tep_get_directors()); ?></div>
		<div id="actorsArr" class="hide"><?php echo json_encode(tep_get_actors()); ?></div>
		<div id="yearsArr" class="hide"><?php echo json_encode(tep_get_posters_date_released()); ?></div>
		<div id="postersSizeArr" class="hide"><?php echo json_encode(tep_get_posters_size()); ?></div>
	<?php } else {
		?>
		<div class="contentText" id="filters"><table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td colspan="2">      <?php echo tep_draw_input_field('keywords', '', 'style="width: 100%" class="searchadvanced" placeholder="إبحث عن كتاب" ', 'text', false) . tep_draw_hidden_field('search_in_description', '1'); ?>
						<input type="hidden" name="categories_id"  id="categories_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_CATEGORIES; ?>"/>
						<?php //echo tep_draw_pull_down_menu('categories_id', tep_get_categories(array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES))), 'no', 'class="selectadvanced" id="categories_id"');  ?> 
						<input type="hidden" name="countries_id"  id="countries_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_COUNTRIES; ?>"/>
						<?php //echo tep_draw_pull_down_menu('countries_id', tep_get_product_countries(array(array('id' => '', 'text' => TEXT_ALL_COUNTRIES)), '1'), 'no', 'id="countries_id" class="selectadvanced selectcoutries"');   ?></td>
				</tr>
				<tr><td colspan="2">
						<input type="hidden" name="publishers_id"  id="publisher_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_MANUFACTURERS; ?>"/>
						<input type="hidden" name="author_id"  id="author_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_AUTHORS; ?>"/>
						<input type="hidden" name="translator_id"  id="translator_id" class="selectadvanced" placeholder="<?php echo TEXT_ALL_TRANSLATORS; ?>"/>
					</td></tr>
				<tr><td colspan="2">
						<input type="hidden" name="products_date_released"  id="products_date_released" class="selectadvanced" placeholder="<?php echo TEXT_RELEASE_YEAR; ?>"/>
						<?php //echo tep_draw_pull_down_menu('products_date_released', tep_get_products_date_released(array(array('id' => '', 'text' => TEXT_RELEASE_YEAR)), '3'), 'no', 'id="products_date_released" class="selectadvanced"');   ?>
					</td></tr>
	<!--			<tr>
					<td colspan="2">      <?php //echo tep_draw_input_field('term', '', 'style="width: 466px!important" class="searchadvanced" placeholder="إبحث عبر الناشر, المؤلف أو المترجم " ');                   ?>
					</td>

				</tr>-->
			</table>
			<div>
				<span style="float: left;">

					<?php echo tep_draw_button(IMAGE_BUTTON_SEARCH, 'search', null, 'primary'); ?></span>
			</div>
		</div>
		<div id="publishersArr" class="hide"><?php echo json_encode(tep_get_publishers(array(), '1')); ?></div>
		<div id="authorsArr" class="hide"><?php echo json_encode(tep_get_authors(array(), '2')); ?></div>
		<div id="translatorsArr" class="hide"><?php echo json_encode(tep_get_translators(array(), '3')); ?></div>
		<div id="categoriesArr" class="hide"><?php echo json_encode(tep_get_categories(array(), 'no')); ?></div>
		<div id="countriesArr" class="hide"><?php echo json_encode(tep_get_product_countries(array(), '1')); ?></div>
		<div id="yearsArr" class="hide"><?php echo json_encode(tep_get_products_date_released(array(), '3')); ?></div>
	<?php } ?>
</div>
</form>
<div class="boxCatTitle marginTop0"><?php echo HEADING_TITLE_2; ?></div>
<div class="clear clearfix"></div>
<div class="contentContainer">
	<?php
	// create column list
	$define_list = array('PRODUCT_LIST_MODEL' => PRODUCT_LIST_MODEL,
		'PRODUCT_LIST_NAME' => PRODUCT_LIST_NAME,
		'PRODUCT_LIST_MANUFACTURER' => PRODUCT_LIST_MANUFACTURER,
		'PRODUCT_LIST_PRICE' => PRODUCT_LIST_PRICE,
		'PRODUCT_LIST_QUANTITY' => PRODUCT_LIST_QUANTITY,
		'PRODUCT_LIST_WEIGHT' => PRODUCT_LIST_WEIGHT,
		'PRODUCT_LIST_IMAGE' => PRODUCT_LIST_IMAGE,
		'PRODUCT_LIST_BUY_NOW' => PRODUCT_LIST_BUY_NOW);

	asort($define_list);
	$keyword = str_replace("  ", " ", $HTTP_GET_VARS['keywords']);
	//clean $keyword
	$keyword = str_replace("إ", "ا", str_replace("أ", "ا", $keyword));
	if ($HTTP_GET_VARS['search_in_posters'] == 1) {  
		$column_list = array();
		reset($define_list);
		while (list($key, $value) = each($define_list)) {
			if ($value > 0)
				$column_list[] = $key;
		}

		$select_column_list = '';

		for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
			switch ($column_list[$i]) {
				case 'PRODUCT_LIST_MODEL':
					$select_column_list .= 'p.products_model, ';
					break;
				case 'PRODUCT_LIST_MANUFACTURER':
					//$select_column_list .= 'm.manufacturers_name, ';
					break;
				case 'PRODUCT_LIST_QUANTITY':
					$select_column_list .= 'p.products_quantity, ';
					break;
				case 'PRODUCT_LIST_IMAGE':
					$select_column_list .= 'p.products_image, ';
					break;
				case 'PRODUCT_LIST_WEIGHT':
					$select_column_list .= 'p.products_weight, ';
					break;
			}
		}
		$filtered = 0;
		$select_str = "select distinct " . $select_column_list . "  GROUP_CONCAT(DISTINCT pub.name  SEPARATOR '&#1548; ') as directors"
				. ",  p.products_id, pd.products_name, p.products_price, p.products_tax_class_id, "
				. "IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.products_price) as final_price ";


		$from_str = "from " . TABLE_PRODUCTS . " p "
				. " left join " . TABLE_PRODUCTS_TO_DIRECTORS . " m on p.products_id = m.products_id "
				. " left join " . TABLE_DIRECTORS . " pub on pub.id = m.directors_id "
				//. " left join " . TABLE_PRODUCTS_TO_COUNTRIES . " cp on p.products_id = cp.products_id "
				//. " left join " . TABLE_COUNTRIES_OF_PRODUCTS . " cou on cou.countries_id = cp.countries_id "
				. " left join " . TABLE_PRODUCTS_TO_ACTORS . " a on  p.products_id = a.products_id "
				. " left join " . TABLE_ACTORS . " auth on  a.actors_id = auth.id "
				//. "left join " . TABLE_PRODUCTS_TO_TRANSLATORS . " t on p.products_id = t.products_id "
				//. "left join " . TABLE_MANUFACTURERS . " trans on t.manufacturers_id = trans.manufacturers_id "
				. " left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id";



		$from_str .= ", " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c";

		$where_str = " where p.products_status = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int) $languages_id . "' and p.products_id = p2c.products_id and p2c.categories_id >=" . POSTERS_CATEGORY_ID . " and p2c.categories_id != " . MAGAZINES_CATEGORY_ID . " ";



		if (isset($HTTP_GET_VARS['directors_id']) && tep_not_null($HTTP_GET_VARS['directors_id'])) {
			$filtered++;
			$where_str .= " and m.directors_id = '" . (int) $HTTP_GET_VARS['directors_id'] . "'";
		}

		if (isset($HTTP_GET_VARS['actors_id']) && tep_not_null($HTTP_GET_VARS['actors_id'])) {
			$filtered++;
			$where_str .= " and a.actors_id = '" . (int) $HTTP_GET_VARS['actors_id'] . "'";
		}

		if (isset($HTTP_GET_VARS['products_date_released']) && tep_not_null($HTTP_GET_VARS['products_date_released'])) {
			$filtered++;
			$where_str .= " and EXTRACT(YEAR FROM `products_date_released`) = '" . (int) $HTTP_GET_VARS['products_date_released'] . "'";
		}


		if (tep_not_null($keyword)) {
			$filtered++;
			$where_str .= "and pd.products_name like '%" . tep_db_input($keyword) . "%' ";
		}
		$products_size = $HTTP_GET_VARS['products_size'];
		if (tep_not_null($products_size)) {
			$filtered++;
			$where_str .= "and p.products_size = '" . tep_db_input($products_size) . "' ";
		}
		$where_str .= '';

		if (tep_not_null($dfrom)) {
			$where_str .= " and p.products_date_added >= '" . tep_date_raw($dfrom) . "'";
		}

		if (tep_not_null($dto)) {
			$where_str .= " and p.products_date_added <= '" . tep_date_raw($dto) . "'";
		}
		if (isset($HTTP_GET_VARS['pfrom'])) {
			$pfrom = $HTTP_GET_VARS['pfrom'];
			$filtered++;
		}

		if (isset($HTTP_GET_VARS['pto'])) {
			$pto = $HTTP_GET_VARS['pto'];
			$filtered++;
		}
		$price_check_error = false;
		if (tep_not_null($pfrom)) {
			if (!settype($pfrom, 'double')) {
				$error = true;
				$price_check_error = true;

				$messageStack->add_session('search', ERROR_PRICE_FROM_MUST_BE_NUM);
			}
		}

		if (tep_not_null($pto)) {
			if (!settype($pto, 'double')) {
				$error = true;
				$price_check_error = true;

				$messageStack->add_session('search', ERROR_PRICE_TO_MUST_BE_NUM);
			}
		}

		if (($price_check_error == false) && is_float($pfrom) && is_float($pto)) {
			if ($pfrom >= $pto) {
				$error = true;

				$messageStack->add_session('search', ERROR_PRICE_TO_LESS_THAN_PRICE_FROM);
			}
		}

		if (tep_not_null($pfrom)) {
			if ($currencies->is_set($currency)) {
				$rate = $currencies->get_value($currency);

				$pfrom = $pfrom / $rate;
			}
		}

		if (tep_not_null($pto)) {
			if (isset($rate)) {
				$pto = $pto / $rate;
			}
		}

		if (DISPLAY_PRICE_WITH_TAX == 'true') {
			if ($pfrom > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) >= " . (double) $pfrom . ")";
			if ($pto > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) <= " . (double) $pto . ")";
		} else {
			if ($pfrom > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) >= " . (double) $pfrom . ")";
			if ($pto > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) <= " . (double) $pto . ")";
		}

		if ((DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto))) {
			$where_str .= " group by p.products_id, tr.tax_priority";
		}



		if ((!isset($HTTP_GET_VARS['sort'])) || (!preg_match('/^[1-8][ad]$/', $HTTP_GET_VARS['sort'])) || (substr($HTTP_GET_VARS['sort'], 0, 1) > sizeof($column_list))) {
			for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
				if ($column_list[$i] == 'PRODUCT_LIST_NAME') {
					$HTTP_GET_VARS['sort'] = $i + 1 . 'a';
					$order_str = " order by pd.products_name";
					break;
				}
			}
		} else {
			$sort_col = substr($HTTP_GET_VARS['sort'], 0, 1);
			$sort_order = substr($HTTP_GET_VARS['sort'], 1);

			switch ($column_list[$sort_col - 1]) {
				case 'PRODUCT_LIST_MODEL':
					$order_str = " order by p.products_model " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_NAME':
					$order_str = " order by pd.products_name " . ($sort_order == 'd' ? 'desc' : '');
					break;
				case 'PRODUCT_LIST_MANUFACTURER':
					$order_str = " order by publishers " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_QUANTITY':
					$order_str = " order by p.products_quantity " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_IMAGE':
					$order_str = " order by pd.products_name";
					break;
				case 'PRODUCT_LIST_WEIGHT':
					$order_str = " order by p.products_weight " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_PRICE':
					$order_str = " order by final_price " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
			}
		}
	} else {
		$column_list = array();
		reset($define_list);
		while (list($key, $value) = each($define_list)) {
			if ($value > 0)
				$column_list[] = $key;
		}

		$select_column_list = '';

		for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
			switch ($column_list[$i]) {
				case 'PRODUCT_LIST_MODEL':
					$select_column_list .= 'p.products_model, ';
					break;
				case 'PRODUCT_LIST_MANUFACTURER':
					//$select_column_list .= 'm.manufacturers_name, ';
					break;
				case 'PRODUCT_LIST_QUANTITY':
					$select_column_list .= 'p.products_quantity, ';
					break;
				case 'PRODUCT_LIST_IMAGE':
					$select_column_list .= 'p.products_image, ';
					break;
				case 'PRODUCT_LIST_WEIGHT':
					$select_column_list .= 'p.products_weight, ';
					break;
			}
		}
		$filtered = 0;
		$select_str = "select distinct " . $select_column_list . "  GROUP_CONCAT(DISTINCT pub.publishers_name  SEPARATOR '&#1548; ') as publishers,GROUP_CONCAT(DISTINCT cou.countries_name  SEPARATOR '&#1548; ') as countries, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids, p.products_id, pd.products_name, p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.products_price) as final_price,p.products_available ";

		if ((DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto))) {
			$select_str .= ", SUM(tr.tax_rate) as tax_rate ";
		}

		$from_str = "from " . TABLE_PRODUCTS . " p "
				. " left join " . TABLE_PRODUCTS_TO_PUBLISHERS . " m on p.products_id = m.products_id "
				. " left join " . TABLE_PUBLISHERS . " pub on pub.publishers_id = m.publishers_id "
				. " left join " . TABLE_PRODUCTS_TO_COUNTRIES . " cp on p.products_id = cp.products_id "
				. " left join " . TABLE_COUNTRIES_OF_PRODUCTS . " cou on cou.countries_id = cp.countries_id "
				. " left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id "
				. " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id "
				. "left join " . TABLE_PRODUCTS_TO_TRANSLATORS . " t on p.products_id = t.products_id "
				. "left join " . TABLE_MANUFACTURERS . " trans on t.manufacturers_id = trans.manufacturers_id "
				. " left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id";

		if ((DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto))) {
			if (!tep_session_is_registered('customer_country_id')) {
				$customer_country_id = STORE_COUNTRY;
				$customer_zone_id = STORE_ZONE;
			}
			$from_str .= " left join " . TABLE_TAX_RATES . " tr on p.products_tax_class_id = tr.tax_class_id left join " . TABLE_ZONES_TO_GEO_ZONES . " gz on tr.tax_zone_id = gz.geo_zone_id and (gz.zone_country_id is null or gz.zone_country_id = '0' or gz.zone_country_id = '" . (int) $customer_country_id . "') and (gz.zone_id is null or gz.zone_id = '0' or gz.zone_id = '" . (int) $customer_zone_id . "')";
		}

		$from_str .= ", " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_CATEGORIES . " c, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c";

		$where_str = " where p.products_status = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int) $languages_id . "' and p.products_id = p2c.products_id and p2c.categories_id = c.categories_id and c.categories_id < " . POSTERS_CATEGORY_ID . " ";

		if (isset($HTTP_GET_VARS['categories_id']) && tep_not_null($HTTP_GET_VARS['categories_id'])) {
			$filtered++;
			//if (isset($HTTP_GET_VARS['inc_subcat']) && ($HTTP_GET_VARS['inc_subcat'] == '1')) {
			$subcategories_array = array();
			tep_get_subcategories($subcategories_array, $HTTP_GET_VARS['categories_id']);

			$where_str .= " and p2c.products_id = p.products_id and p2c.products_id = pd.products_id and (p2c.categories_id = '" . (int) $HTTP_GET_VARS['categories_id'] . "'";

			for ($i = 0, $n = sizeof($subcategories_array); $i < $n; $i++) {
				$where_str .= " or p2c.categories_id = '" . (int) $subcategories_array[$i] . "'";
			}

			$where_str .= ")";
			//} 
		}

		if (isset($HTTP_GET_VARS['publishers_id']) && tep_not_null($HTTP_GET_VARS['publishers_id'])) {
			$filtered++;
			$where_str .= " and m.publishers_id = '" . (int) $HTTP_GET_VARS['publishers_id'] . "'";
		}
		if (isset($HTTP_GET_VARS['countries_id']) && tep_not_null($HTTP_GET_VARS['countries_id'])) {
			$filtered++;
			$where_str .= " and cp.countries_id = '" . (int) $HTTP_GET_VARS['countries_id'] . "'";
		}
		if (isset($HTTP_GET_VARS['author_id']) && tep_not_null($HTTP_GET_VARS['author_id'])) {
			$filtered++;
			$where_str .= " and a.manufacturers_id = '" . (int) $HTTP_GET_VARS['author_id'] . "'";
		}
		if (isset($HTTP_GET_VARS['translator_id']) && tep_not_null($HTTP_GET_VARS['translator_id'])) {
			$filtered++;
			$where_str .= " and t.manufacturers_id = '" . (int) $HTTP_GET_VARS['translator_id'] . "'";
		}
		if (isset($HTTP_GET_VARS['products_date_released']) && tep_not_null($HTTP_GET_VARS['products_date_released'])) {
			$filtered++;
			$where_str .= " and EXTRACT(YEAR FROM `products_date_released`) = '" . (int) $HTTP_GET_VARS['products_date_released'] . "'";
		}


		if (tep_not_null($keyword)) {
			$filtered++;
			$where_str .= "and pd.products_search_words like '%" . tep_db_input($keyword) . "%' ";
		}
		$term = $HTTP_GET_VARS['term'];
		if (tep_not_null($term)) {
			$filtered++;
			$where_str .= "and ( pub.publishers_name like '%" . tep_db_input($term) . "%' or auth.manufacturers_name like '%" . tep_db_input($term) . "%' or trans.manufacturers_name like '%" . tep_db_input($term) . "%')  ";
		}
		$where_str .= '';

		if (tep_not_null($dfrom)) {
			$where_str .= " and p.products_date_added >= '" . tep_date_raw($dfrom) . "'";
		}

		if (tep_not_null($dto)) {
			$where_str .= " and p.products_date_added <= '" . tep_date_raw($dto) . "'";
		}

		if (tep_not_null($pfrom)) {
			if ($currencies->is_set($currency)) {
				$rate = $currencies->get_value($currency);

				$pfrom = $pfrom / $rate;
			}
		}

		if (tep_not_null($pto)) {
			if (isset($rate)) {
				$pto = $pto / $rate;
			}
		}

		if (DISPLAY_PRICE_WITH_TAX == 'true') {
			if ($pfrom > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) >= " . (double) $pfrom . ")";
			if ($pto > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) * if(gz.geo_zone_id is null, 1, 1 + (tr.tax_rate / 100) ) <= " . (double) $pto . ")";
		} else {
			if ($pfrom > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) >= " . (double) $pfrom . ")";
			if ($pto > 0)
				$where_str .= " and (IF(s.status, s.specials_new_products_price, p.products_price) <= " . (double) $pto . ")";
		}

		if ((DISPLAY_PRICE_WITH_TAX == 'true') && (tep_not_null($pfrom) || tep_not_null($pto))) {
			$where_str .= " group by p.products_id, tr.tax_priority";
		}

		if ((!isset($HTTP_GET_VARS['sort'])) || (!preg_match('/^[1-8][ad]$/', $HTTP_GET_VARS['sort'])) || (substr($HTTP_GET_VARS['sort'], 0, 1) > sizeof($column_list))) {
			for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
				if ($column_list[$i] == 'PRODUCT_LIST_NAME') {
					$HTTP_GET_VARS['sort'] = $i + 1 . 'a';
					$order_str = " order by p.products_date_released desc, pd.products_name";
					break;
				}
			}
		} else {
			$sort_col = substr($HTTP_GET_VARS['sort'], 0, 1);
			$sort_order = substr($HTTP_GET_VARS['sort'], 1);

			switch ($column_list[$sort_col - 1]) {
				case 'PRODUCT_LIST_MODEL':
					$order_str = " order by p.products_model " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_NAME':
					$order_str = " order by pd.products_name " . ($sort_order == 'd' ? 'desc' : '');
					break;
				case 'PRODUCT_LIST_MANUFACTURER':
					$order_str = " order by publishers " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_QUANTITY':
					$order_str = " order by p.products_quantity " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_IMAGE':
					$order_str = " order by pd.products_name";
					break;
				case 'PRODUCT_LIST_WEIGHT':
					$order_str = " order by p.products_weight " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
				case 'PRODUCT_LIST_PRICE':
					$order_str = " order by final_price " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
					break;
			}
		}
	}
	$listing_sql = ($filtered > 0) ? $select_str . $from_str . $where_str . ' group by p.products_id ' . $order_str : '';
	require(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING);
	?>

</div>
<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>