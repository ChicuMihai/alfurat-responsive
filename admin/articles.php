<?php
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');

function tep_check_featured_exist() {
	return tep_db_num_rows(tep_db_query("select * from " . TABLE_ARTICLES . " where articles_of_month = '1'"));
}

function tep_set_in_featured_column($products_id, $status) {
	if ($status == '1') {
		return tep_db_query("update " . TABLE_ARTICLES . " set articles_of_month = '1' where articles_id = '" . $products_id . "'");
	} elseif ($status == '0') {
		return tep_db_query("update " . TABLE_ARTICLES . " set articles_of_month = '0' where articles_id = '" . $products_id . "'");
	} else {
		return -1;
	}
}

if (tep_not_null($action)) {
	switch ($action) {
		case 'setflag':
			if (($HTTP_GET_VARS['flag'] == '0') || ($HTTP_GET_VARS['flag'] == '1')) {
				if (isset($HTTP_GET_VARS['pID'])) {
					tep_set_article_status($HTTP_GET_VARS['pID'], $HTTP_GET_VARS['flag']);
				}
		
			}
			break;
		case 'setfeature':
			tep_set_in_featured_column($HTTP_GET_VARS['pID'], $HTTP_GET_VARS['flag']);
			break;
			tep_redirect(tep_href_link(FILENAME_ARTICLES, 'pID=' . $HTTP_GET_VARS['pID']));
			break;
		case 'delete_product_confirm':
			if (isset($HTTP_POST_VARS['articles_id'])) {
				$product_id = tep_db_prepare_input($HTTP_POST_VARS['articles_id']);
				tep_remove_article($product_id);
			}

			if (USE_CACHE == 'true') {
				tep_reset_cache_block('categories');
				tep_reset_cache_block('also_purchased');
			}

			tep_redirect(tep_href_link(FILENAME_ARTICLES));
			break;


		case 'insert_product':
		case 'update_product':
			if (isset($HTTP_GET_VARS['pID']))
				$articles_id = tep_db_prepare_input($HTTP_GET_VARS['pID']);

			$sql_data_array = array(
				'articles_status' => tep_db_prepare_input($HTTP_POST_VARS['articles_status'])
			);
			$products_image = new upload('products_image');
			$products_image->set_destination(DIR_FS_CATALOG_IMAGES);
			if ($products_image->parse() && $products_image->save()) {
				$sql_data_array['articles_image'] = tep_db_prepare_input($products_image->filename);
			}

			if ($action == 'insert_product') {
				$insert_sql_data = array('articles_date_added' => 'now()');

				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_ARTICLES, $sql_data_array);
				$articles_id = tep_db_insert_id();
			} elseif ($action == 'update_product') {
				$update_sql_data = array('articles_last_modified' => 'now()');

				$sql_data_array = array_merge($sql_data_array, $update_sql_data);

				tep_db_perform(TABLE_ARTICLES, $sql_data_array, 'update', "articles_id = '" . (int) $articles_id . "'");
			}



			$languages = tep_get_languages();
			for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
				$language_id = $languages[$i]['id'];
				$articles_name = tep_db_prepare_input($HTTP_POST_VARS['articles_name'][$language_id]);
				$sql_data_array = array('articles_name' => $articles_name,
					'articles_description' => tep_db_prepare_input($HTTP_POST_VARS['products_description'][$language_id]),
					'articles_url' => tep_db_prepare_input($HTTP_POST_VARS['products_url'][$language_id]));

				if ($action == 'insert_product') {
					$insert_sql_data = array('articles_id' => $articles_id,
						'language_id' => $language_id);

					$sql_data_array = array_merge($sql_data_array, $insert_sql_data);

					tep_db_perform(TABLE_ARTICLES_DESCRIPTION, $sql_data_array);
				} elseif ($action == 'update_product') {
					tep_db_perform(TABLE_ARTICLES_DESCRIPTION, $sql_data_array, 'update', "articles_id = '" . (int) $articles_id . "' and language_id = '" . (int) $language_id . "'");
				}
			}


			$pi_sort_order = 0;
			$piArray = array(0);

			foreach ($HTTP_POST_FILES as $key => $value) {
// Update existing large product images
				if (preg_match('/^products_image_large_([0-9]+)$/', $key, $matches)) {
					$pi_sort_order++;

					$sql_data_array = array('htmlcontent' => tep_db_prepare_input($HTTP_POST_VARS['products_image_htmlcontent_' . $matches[1]]),
						'sort_order' => $pi_sort_order);

					$t = new upload($key);
					$t->set_destination(DIR_FS_CATALOG_ARTICLE_IMAGES);
					if ($t->parse() && $t->save()) {
						$sql_data_array['image'] = tep_db_prepare_input($t->filename);
					}

					tep_db_perform(TABLE_ARTICLES_IMAGES, $sql_data_array, 'update', "articles_id = '" . (int) $articles_id . "' and id = '" . (int) $matches[1] . "'");

					$piArray[] = (int) $matches[1];
				} elseif (preg_match('/^products_image_large_new_([0-9]+)$/', $key, $matches)) {
// Insert new large product images
					$sql_data_array = array('articles_id' => (int) $articles_id,
						'htmlcontent' => tep_db_prepare_input($HTTP_POST_VARS['products_image_htmlcontent_new_' . $matches[1]]));

					$t = new upload($key);
					$t->set_destination(DIR_FS_CATALOG_ARTICLE_IMAGES);
					if ($t->parse() && $t->save()) {
						$pi_sort_order++;

						$sql_data_array['image'] = tep_db_prepare_input($t->filename);
						$sql_data_array['sort_order'] = $pi_sort_order;

						tep_db_perform(TABLE_ARTICLES_IMAGES, $sql_data_array);

						$piArray[] = tep_db_insert_id();
					}
				}
			}

			$product_images_query = tep_db_query("select image from " . TABLE_ARTICLES_IMAGES . " where articles_id = '" . (int) $articles_id . "' and id not in (" . implode(',', $piArray) . ")");
			if (tep_db_num_rows($product_images_query)) {
				while ($product_images = tep_db_fetch_array($product_images_query)) {
					$duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_ARTICLES_IMAGES . " where image = '" . tep_db_input($product_images['image']) . "'");
					$duplicate_image = tep_db_fetch_array($duplicate_image_query);

					if ($duplicate_image['total'] < 2) {
						if (file_exists(DIR_FS_CATALOG_ARTICLE_IMAGES . $product_images['image'])) {
							@unlink(DIR_FS_CATALOG_ARTICLE_IMAGES . $product_images['image']);
						}
					}
				}

				tep_db_query("delete from " . TABLE_ARTICLES_IMAGES . " where articles_id = '" . (int) $articles_id . "' and id not in (" . implode(',', $piArray) . ")");
			}

			if (USE_CACHE == 'true') {
				tep_reset_cache_block('categories');
				tep_reset_cache_block('also_purchased');
			}

			tep_redirect(tep_href_link(FILENAME_ARTICLES, 'page=' . $_GET['page'] . '&pID=' . $articles_id));
			break;
	}
}

// check if the catalog image directory exists
if (is_dir(DIR_FS_CATALOG_ARTICLE_IMAGES)) {
	if (!tep_is_writable(DIR_FS_CATALOG_ARTICLE_IMAGES))
		$messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
	$messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}

require(DIR_WS_INCLUDES . 'template_top.php');

if ($action == 'new_product') {
	$parameters = array('articles_name' => '',
		'articles_description' => '',
		'articles_url' => '',
		'articles_id' => '',
		'articles_image' => '',
		'articles_larger_images' => array(),
		'articles_date_added' => '',
		'articles_last_modified' => '',
		'articles_status' => '',
		'articles_of_month' => ''
	);

	$pInfo = new objectInfo($parameters);

	if (isset($HTTP_GET_VARS['pID']) && empty($HTTP_POST_VARS)) {
		$product_query = tep_db_query("select pd.articles_name,pd.articles_description, pd.articles_url, p.articles_id,p.articles_of_month, p.articles_image, p.articles_date_added, p.articles_last_modified"
				. ","
				. " p.articles_status from " . TABLE_ARTICLES . " p, " . TABLE_ARTICLES_DESCRIPTION . " pd where p.articles_id = '" . (int) $HTTP_GET_VARS['pID'] . "' "
				. "and p.articles_id = pd.articles_id  and pd.language_id = '" . (int) $languages_id . "'");
		$product = tep_db_fetch_array($product_query);
		$pInfo->objectInfo($product);
		$product_images_query = tep_db_query("select id, image, htmlcontent, sort_order from " . TABLE_ARTICLES_IMAGES . " where articles_id = '" . (int) $product['articles_id'] . "' order by sort_order");
		while ($product_images = tep_db_fetch_array($product_images_query)) {
			$pInfo->products_larger_images[] = array('id' => $product_images['id'],
				'image' => $product_images['image'],
				'htmlcontent' => $product_images['htmlcontent'],
				'sort_order' => $product_images['sort_order']);
		}
	}


	$languages = tep_get_languages();

	if (!isset($pInfo->articles_status))
		$pInfo->articles_status = '1';
	switch ($pInfo->articles_status) {
		case '0': $in_status = false;
			$out_status = true;
			break;
		case '1':
		default: $in_status = true;
			$out_status = false;
	}



	$form_action = (isset($HTTP_GET_VARS['pID'])) ? 'update_product' : 'insert_product';
	?>

	<?php
	echo tep_draw_form('new_product', FILENAME_ARTICLES, 'page=' . $_GET['page'] . (isset($HTTP_GET_VARS['pID']) ? '&pID=' . $HTTP_GET_VARS['pID'] : '')
			. '&action=' . $form_action, 'post', 'enctype="multipart/form-data"');
	?>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="pageHeading"><?php echo TEXT_NEW_PRODUCT; ?></td>
						<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
					</tr>
				</table></td>
		</tr>

		<tr>
			<td><table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<?php
					for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
						?>
						<tr>
							<td class="main"><?php if ($i == 0) echo TEXT_PRODUCTS_NAME; ?></td>
							<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), '') . tep_draw_input_field('articles_name[' . $languages[$i]['id'] . ']', (empty($pInfo->articles_id) ? '' : tep_get_articles_name($pInfo->articles_id, $languages[$i]['id']))); ?></td>
						</tr>
						<?php
					}
					?>
					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_STATUS; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_radio_field('articles_status', '1', $in_status) . '&nbsp;' . TEXT_PRODUCT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('articles_status', '0', $out_status) . '&nbsp;' . TEXT_PRODUCT_NOT_AVAILABLE; ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>

					<?php
					for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
						?>
						<tr>
							<td class="main" valign="top"><?php if ($i == 0) echo TEXT_PRODUCTS_DESCRIPTION; ?></td>
							<td><table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="main" valign="top"><?php echo tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), ''); ?>&nbsp;</td>
										<td class="main"><?php echo tep_draw_textarea_field_ckeditor('products_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (empty($pInfo->articles_id) ? '' :
								tep_get_articles_description($pInfo->articles_id, $languages[$i]['id'])));
				?></td>
									</tr>
								</table></td>
						</tr>
						<?php
					}
					?>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>




					<tr>
						<td class="main" valign="top"><?php echo TEXT_PRODUCTS_IMAGE; ?></td>
						<td class="main" style="padding-left: 30px;">
							<div><?php echo '<strong>' . TEXT_PRODUCTS_MAIN_IMAGE . ' <small>(' . SMALL_IMAGE_WIDTH . ' x ' . SMALL_IMAGE_HEIGHT . 'px)</small></strong><br />' . (tep_not_null($pInfo->products_image) ? '<a href="' . HTTP_CATALOG_SERVER . DIR_WS_CATALOG_ARTICLES_IMAGES . $pInfo->products_image . '" target="_blank">' . $pInfo->products_image . '</a> &#124; ' : '') . tep_draw_file_field('products_image'); ?></div>

							<ul id="piList">
								<?php
								$pi_counter = 0;

								foreach ($pInfo->products_larger_images as $pi) {
									$pi_counter++;

									echo '                <li id="piId' . $pi_counter . '" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: right;"></span><a href="#" onclick="showPiDelConfirm(' . $pi_counter . ');return false;" class="ui-icon ui-icon-trash" style="float: right;"></a><strong>' . TEXT_PRODUCTS_LARGE_IMAGE . '</strong><br />' . tep_draw_file_field('products_image_large_' . $pi['id']) . '<br /><a href="' . HTTP_CATALOG_SERVER . DIR_WS_CATALOG_ARTICLES_IMAGES . $pi['image'] . '" target="_blank">' . $pi['image'] . '</a><br /><br />' . TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT . '<br />' . tep_draw_textarea_field('products_image_htmlcontent_' . $pi['id'], 'soft', '70', '3', $pi['htmlcontent']) . '</li>';
								}
								?>
							</ul>

							<a  href="#" onclick="addNewPiForm();
									return false;" ><span class="ui-icon ui-icon-plus" style="float: left;"></span><?php echo TEXT_PRODUCTS_ADD_LARGE_IMAGE; ?></a>

							<div id="piDelConfirm" title="<?php echo TEXT_PRODUCTS_LARGE_IMAGE_DELETE_TITLE; ?>">
								<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><?php echo TEXT_PRODUCTS_LARGE_IMAGE_CONFIRM_DELETE; ?></p>
							</div>

							<style type="text/css">
								#piList { list-style-type: none; margin: 0; padding: 0; }
								#piList li { margin: 5px 0; padding: 2px; }
							</style>

							<script type="text/javascript">
								$('#piList').sortable({
									containment: 'parent'
								});

								var piSize = <?php echo $pi_counter; ?>;

								function addNewPiForm() {
									piSize++;

									$('#piList').append('<li id="piId' + piSize + '" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: right;"></span><a href="#" onclick="showPiDelConfirm(' + piSize + ');return false;" class="ui-icon ui-icon-trash" style="float: right;"></a><strong><?php echo TEXT_PRODUCTS_LARGE_IMAGE; ?></strong><br /><input type="file" name="products_image_large_new_' + piSize + '" /><br /><br /><?php echo TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT; ?><br /><textarea name="products_image_htmlcontent_new_' + piSize + '" wrap="soft" cols="70" rows="3"></textarea></li>');
								}

								var piDelConfirmId = 0;

								$('#piDelConfirm').dialog({
									autoOpen: false,
									resizable: false,
									draggable: false,
									modal: true,
									buttons: {
										'Delete': function () {
											$('#piId' + piDelConfirmId).effect('blind').remove();
											$(this).dialog('close');
										},
										Cancel: function () {
											$(this).dialog('close');
										}
									}
								});

								function showPiDelConfirm(piId) {
									piDelConfirmId = piId;

									$('#piDelConfirm').dialog('open');
								}
							</script>

						</td>
					</tr>




				</table></td>
		</tr>
		<tr>
			<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
		</tr>
		<tr>
			<td class="smallText" align="right"><?php
				echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')))
				. tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_ARTICLES, 'page=' . $_GET['page'] . (isset($HTTP_GET_VARS['pID']) ? '&pID=' . $HTTP_GET_VARS['pID'] : '')));
				?></td>
		</tr>
	</table>

	<script type="text/javascript">
		$('#products_date_available').datepicker({
			dateFormat: 'yy-mm-dd'
		});
		$('#products_date_released').datepicker({
			dateFormat: 'yy-mm-dd'
		});
	</script>

	</form>
	<?php
} elseif ($action == 'new_product_preview') { 
	$product_query = tep_db_query("select p.articles_id, pd.language_id, pd.articles_name, pd.articles_description, pd.articles_url,"
			. " p.articles_image,  "
			. "p.articles_date_added, p.articles_last_modified, p.articles_status,p.articles_of_month "
			. " from " . TABLE_ARTICLES . " p, " . TABLE_ARTICLES_DESCRIPTION . " pd "
			. " where p.articles_id = pd.articles_id and p.articles_id = '" . (int) $HTTP_GET_VARS['pID'] . "'");
	$product = tep_db_fetch_array($product_query);

	$pInfo = new objectInfo($product);
	$products_image_name = $pInfo->articles_image;

	$languages = tep_get_languages();
	for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		$pInfo->articles_name = tep_get_articles_name($pInfo->articles_id, $languages[$i]['id']);
		$pInfo->products_description = tep_get_products_description($pInfo->articles_id, $languages[$i]['id']);
		$pInfo->products_url = tep_get_products_url($pInfo->articles_id, $languages[$i]['id']);
		?>
		<table border="0" width="100%" cellspacing="0" cellpadding="2" style="margin-top:25px;">
			<tr>
				<td width="60%">
					<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr><td class="pageHeading"><?php echo sprintf(TEXT_NEW_PRODUCT, tep_output_generated_category_path($current_category_id)); ?></td></tr>
						<tr>
							<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td class="pageHeading" align="left"><?php
											?></td>
										<td class="pageHeading" align="right"><?php //echo tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), $languages[$i]['name']) .
									echo $pInfo->articles_name; ?></td>

									</tr>
								</table></td>
						</tr>
						<tr>
							<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td class="main" align="right"><?php echo tep_image(HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $products_image_name, $pInfo->articles_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="right" hspace="5" vspace="5"') . $pInfo->products_description; ?></td>
						</tr>
						<?php
						if ($pInfo->products_url) {
							?>
							<tr>
								<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
							</tr>
							<tr>
								<td class="main"><?php echo sprintf(TEXT_PRODUCT_MORE_INFORMATION, $pInfo->products_url); ?></td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>

						<tr>
							<td align="left" class="smallText"><?php //echo sprintf(TEXT_PRODUCTS_DATE_RELEASED, '') . '' . tep_date_long($product['products_date_released']); ?></td>
						</tr>
						<?php
					}

					if (isset($HTTP_GET_VARS['origin'])) {
						$pos_params = strpos($HTTP_GET_VARS['origin'], '?', 0);
						if ($pos_params != false) {
							$back_url = substr($HTTP_GET_VARS['origin'], 0, $pos_params);
							$back_url_params = substr($HTTP_GET_VARS['origin'], $pos_params + 1);
						} else {
							$back_url = $HTTP_GET_VARS['origin'];
							$back_url_params = '';
						}
					} else {
						$back_url = FILENAME_ARTICLES;
						$back_url_params = 'pID=' . $pInfo->articles_id;
					}
					?>
					<tr>
						<td align="right" class="smallText"><?php echo tep_draw_button(IMAGE_BACK, 'triangle-1-w', tep_href_link($back_url, $back_url_params)); ?></td>
					</tr>
				</table></td><td width="5%"></td>
			<td align="left" class="smallText" valign="top" width="35%">
				
			</td>
		</tr></table>
	<?php
} else {
	?>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td></td>
		</tr>
		<tr>
			<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
								<tr class="dataTableHeadingRow">
									<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CATEGORIES_PRODUCTS; ?></td>
									<td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
									<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_FEATURED; ?></td>
									<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
								</tr>
								<?php
								$categories_count = 0;
								$rows = 0;



								$products_count = 0;
								$products_query_raw = "select p.articles_id, pd.articles_name, p.articles_image, p.articles_date_added, p.articles_last_modified,"
										. "  p.articles_status, p.articles_of_month from " . TABLE_ARTICLES . " p"
										. ", " . TABLE_ARTICLES_DESCRIPTION . " pd "
										. "where p.articles_id = pd.articles_id and pd.language_id = '" . (int) $languages_id . "'"
										. "  order by pd.articles_name";
								$products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $products_query_raw, $products_query_numrows);
								$products_query = tep_db_query($products_query_raw);
								while ($products = tep_db_fetch_array($products_query)) {

									$products_count++;
									$rows++;
									if ((!isset($HTTP_GET_VARS['pID']) || (isset($HTTP_GET_VARS['pID']) && ($HTTP_GET_VARS['pID'] == $products['articles_id']))) && !isset($pInfo) && (substr($action, 0, 3) != 'new')) {
// find out the rating average from customer reviews
										$pInfo = new objectInfo($products);
									}



									if (isset($pInfo) && is_object($pInfo) && ($products['articles_id'] == $pInfo->articles_id)) { //var_dump($pInfo);exit;
										echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLES, 'pID=' . $products['articles_id'] . '&action=new_product_preview') . '\'">' . "\n";
									} else {
										echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_ARTICLES, 'pID=' . $products['articles_id']) . '\'">' . "\n";
									}
									?>
									<td class="dataTableContent arabicTd"><?php echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'pID=' . $products['articles_id'] . '&action=new_product_preview') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $products['articles_name']; ?></td>
									<td class="dataTableContent" align="center">
										<?php
										if ($products['articles_status'] == '1') {
											echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_ARTICLES, 'action=setflag&flag=0&pID=' . $products['articles_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
										} else {
											echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'action=setflag&flag=1&pID=' . $products['articles_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
										}
										?></td>
									<td  class="dataTableContent" align="right">
										<?php
										if ($products['articles_of_month'] == '1') {
											echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'action=setfeature&flag=0&pID=' . $products['articles_id'], 'NONSSL') . '">'
											. '' . tep_image(DIR_WS_IMAGES . 'icon_remove.png', '', 10, 11) . '</a>';
										} else {
											if (tep_check_featured_exist() == 0)
												echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'action=setfeature&flag=1&pID=' . $products['articles_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_add.png', '', 10, 10) . '</a>';
										}
										?></td>  
									<td class="dataTableContent" align="right"><?php
										if (isset($pInfo) && is_object($pInfo) && ($products['articles_id'] == $pInfo->articles_id)) {
											echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', '');
										} else {
											echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'pID=' . $products['articles_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>';
										}
										?>&nbsp;</td>
						</tr>
						<?php
					}

					$cPath_back = '';
					if (sizeof($cPath_array) > 0) {
						for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
							if (empty($cPath_back)) {
								$cPath_back .= $cPath_array[$i];
							} else {
								$cPath_back .= '_' . $cPath_array[$i];
							}
						}
					}

					$cPath_back = (tep_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
					?>
					<tr>
						<td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
								<tr>
									<td class="smallText"><?php echo TEXT_CATEGORIES . '&nbsp;' . $categories_count; ?></td>
									<td align="right" class="smallText"><?php
										echo tep_draw_button(IMAGE_NEW_ARTICLE, 'plus', tep_href_link(FILENAME_ARTICLES, 'action=new_product'));
										?>&nbsp;</td>
								</tr>
							</table></td>
					</tr>
				</table></td>
			<?php
			$heading = array();
			$contents = array();
			switch ($action) {
				case 'delete_product':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</strong>');

					$contents = array('form' => tep_draw_form('products', FILENAME_ARTICLES, 'action=delete_product_confirm') . tep_draw_hidden_field('articles_id', $pInfo->articles_id));
					$contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);
					$contents[] = array('text' => '<br /><strong>' . $pInfo->articles_name . '</strong>');
					$contents[] = array('text' => '<br />' . $product_categories_string);
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_ARTICLES, 'pID=' . $pInfo->articles_id)));
					break;
				case 'move_product':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_PRODUCT . '</strong>');

					$contents = array('form' => tep_draw_form('products', FILENAME_ARTICLES, 'action=move_product_confirm') . tep_draw_hidden_field('articles_id', $pInfo->articles_id));
					$contents[] = array('text' => sprintf(TEXT_MOVE_PRODUCTS_INTRO, $pInfo->articles_name));
					$contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . tep_output_generated_category_path($pInfo->articles_id, 'product') . '</strong>');
					$contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $pInfo->articles_name) . '<br />' . tep_draw_pull_down_menu('move_to_category_id', tep_get_category_tree(), $current_category_id));
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_MOVE, 'arrow-4', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_ARTICLES, 'pID=' . $pInfo->articles_id)));
					break;
				case 'copy_to':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');

					$contents = array('form' => tep_draw_form('copy_to', FILENAME_ARTICLES, 'action=copy_to_confirm') . tep_draw_hidden_field('articles_id', $pInfo->articles_id));
					$contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
					$contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . tep_output_generated_category_path($pInfo->articles_id, 'product') . '</strong>');
					$contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . tep_draw_pull_down_menu('categories_id', tep_get_category_tree(), $current_category_id));
					$contents[] = array('text' => '<br />' . TEXT_HOW_TO_COPY . '<br />' . tep_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_COPY_AS_LINK . '<br />' . tep_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE);
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_COPY, 'copy', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_ARTICLES, 'pID=' . $pInfo->articles_id)));
					break;
				default:
					if ($rows > 0) {
						if (isset($pInfo) && is_object($pInfo)) {
							$heading[] = array('text' => '<strong>' . tep_get_articles_name($pInfo->articles_id, $languages_id) . '</strong>');

							$contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_ARTICLES, 'pID=' . $pInfo->articles_id . '&action=new_product'))
								. tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_ARTICLES, 'pID=' . $pInfo->articles_id . '&action=delete_product'))
							);
							$contents[] = array('text' => '<br />' . TEXT_DATE_ADDED . ' ' . tep_date_short($pInfo->articles_date_added));
							if (tep_not_null($pInfo->articles_last_modified))
								$contents[] = array('text' => TEXT_LAST_MODIFIED . ' ' . tep_date_short($pInfo->articles_last_modified));

							$contents[] = array('text' => '<br />' . tep_info_image($pInfo->articles_image, $pInfo->articles_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT));
						}
					}
					break;
			}

			if ((tep_not_null($heading)) && (tep_not_null($contents))) {
				echo '            <td width="25%" valign="top">' . "\n";

				$box = new box;
				echo $box->infoBox($heading, $contents);

				echo '            </td>' . "\n";
			}
			?>
		</tr>
		<tr>
			<td class="smallText" valign="top"><?php
				echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_FEATURED);
				//$param = (!empty($search)) ? 'search=' . $search : 'cPath=' . $cPath;
				?></td>
			<td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], $param); ?></td>
		</tr>
	</table></td>
	</tr>
	</table>
	<?php
}
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>