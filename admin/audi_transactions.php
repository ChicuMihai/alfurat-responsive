<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
					<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
				</tr>
			</table></td>
	</tr>
	<tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
							<tr class="dataTableHeadingRow">
								<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NUMBER; ?></td>
								<td class="dataTableHeadingContent"><?php echo TABLE_DATE_ADDED; ?></td>
								<td class="dataTableHeadingContent"><?php echo TABLE_CUSTOMER; ?></td>
								<td class="dataTableHeadingContent">Order ID </td>
								<td class="dataTableHeadingContent">amount </td>
								<td class="dataTableHeadingContent">Trans. Status </td>
								<td class="dataTableHeadingContent">Command</td>
								<td class="dataTableHeadingContent">Message</td>
								<td class="dataTableHeadingContent">Card Type </td>
								<td class="dataTableHeadingContent">Order Info </td>
								<td class="dataTableHeadingContent">Receipt No </td>
								<td class="dataTableHeadingContent">Trans. No</td>
								<td class="dataTableHeadingContent">Response Code </td>
								<td class="dataTableHeadingContent">Response Url</td>
							</tr>
							<?php
							if (isset($HTTP_GET_VARS['page']) && ($HTTP_GET_VARS['page'] > 1))
								$rows = $HTTP_GET_VARS['page'] * MAX_DISPLAY_SEARCH_RESULTS - MAX_DISPLAY_SEARCH_RESULTS;
							$products_query_raw = "select p.date_added,p.customers_id,pd.customers_firstname,pd.customers_lastname,amount,order_id,transaction_status,command,message,cardType,orderInfo,receiptNo,transactionNo,txnResponseCode,responseUrl"
									. " from " . TABLE_AUDI_TRANSACTIONS . " p, " . TABLE_CUSTOMERS . " pd where pd.customers_id = p.customers_id"
									. " order by p.transaction_id DESC";
							$products_split = new splitPageResults($HTTP_GET_VARS['page'], MAX_DISPLAY_SEARCH_RESULTS, $products_query_raw, $products_query_numrows);

							$rows = 0;
							$products_query = tep_db_query($products_query_raw);
							while ($products = tep_db_fetch_array($products_query)) {
								$rows++;
								if (strlen($rows) < 2) {
									$rows = '0' . $rows;
								}
								?>
								<tr class="dataTableRow" >
									<td class="dataTableContent"><?php echo $rows; ?>.</td>
									<td class="dataTableContent" ><?php echo $products['date_added']; ?></td>
									<td class="dataTableContent" ><?php echo $products['customers_firstname'], ' ', $products['customers_lastname']; ?></td>
									<td class="dataTableContent"><?php
										if ($products['order_id']) {
											echo '<a href="' . tep_href_link(FILENAME_ORDERS_INVOICE, 'oID=' . $products['order_id']) . '">' . $products['order_id'] . '</a>';
										}
										?></td>
									<td class="dataTableContent" ><?php echo $products['amount']; ?></td>
									<td class="dataTableContent" ><?php echo $products['transaction_status']; ?></td>
									<td class="dataTableContent" ><?php echo $products['command']; ?></td>
									<td class="dataTableContent" ><?php echo $products['message']; ?></td>
									<td class="dataTableContent" ><?php echo $products['cardType']; ?></td>
									<td class="dataTableContent" ><?php echo $products['orderInfo']; ?></td>
									<td class="dataTableContent" ><?php echo $products['receiptNo']; ?></td>
									<td class="dataTableContent" ><?php echo $products['transactionNo']; ?></td>
									<td class="dataTableContent" ><?php echo $products['txnResponseCode']; ?></td>
									<td class="dataTableContent" ><?php echo $products['responseUrl']; ?></td>
								</tr>
								<?php
							}
							?>
						</table></td>
				</tr>
				<tr>
					<td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
							<tr>
								<td class="smallText" valign="top"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
								<td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page']); ?>&nbsp;</td>
							</tr>
						</table></td>
				</tr>
			</table></td>
	</tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
