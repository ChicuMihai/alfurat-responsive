<?php
define('EP_CURRENT_VERSION', '2.77a');
require('includes/application_top.php');
require_once('includes/database_tables.php');
error_reporting(E_ALL);
require('easypopulate_functions.php');

$system = tep_get_system_information();

define('EP_TEMP_DIRECTORY', DIR_FS_CATALOG . 'temp/');
define('IMAGES_DIRECTORY', DIR_FS_CATALOG . 'images/');


$ep_separator = "\t"; // tab is default

define('EP_EXCEL_SAFE_OUTPUT', true); // default is: true

if (EP_EXCEL_SAFE_OUTPUT == true) {
	if ($language == 'english') {
		$ep_separator = ',';  // comma
	} elseif ($language == 'german') {
		$ep_separator = ';';  // semi-colon
	} else {
		$ep_separator = ',';  // comma  // default for all others.
	}
}
define('EP_PDF_UPLOAD_SUPPORT', false);  // default is false
// Enable Quick Backup Button for oscommerce backup.php
define('EP_QUICK_BACKUP', true);  // default is true



if (!empty($languages_id) && !empty($language)) {
	define('EP_DEFAULT_LANGUAGE_ID', $languages_id);
	define('EP_DEFAULT_LANGUAGE_NAME', $language);
} else {
	//elari check default language_id from configuration table DEFAULT_LANGUAGE
	$epdlanguage_query = tep_db_query("select languages_id, name from " . TABLE_LANGUAGES . " where code = '" . DEFAULT_LANGUAGE . "'");
	if (tep_db_num_rows($epdlanguage_query) > 0) {
		$epdlanguage = tep_db_fetch_array($epdlanguage_query);
		define('EP_DEFAULT_LANGUAGE_ID', $epdlanguage['languages_id']);
		define('EP_DEFAULT_LANGUAGE_NAME', $epdlanguage['name']);
	} else {
		echo 'Strange but there is no default language to work... That may not happen, just in case... ';
	}
}

$languages = tep_get_languages();
$filelayout = '';
$filelayout_count = '';
$filelayout_sql = '';
$fileheaders = '';
if (isset($_GET['action']) && $_GET['action'] == 'zipped') {
//	zipped
	$file = tep_get_uploaded_file('zipped');
	if (is_uploaded_file($file['tmp_name'])) {
		tep_copy_uploaded_file($file, EP_TEMP_DIRECTORY);
		$zip = new ZipArchive;
		$res = $zip->open(EP_TEMP_DIRECTORY . $file['name']);
		if ($res === TRUE) {
			$zip->extractTo(EP_TEMP_DIRECTORY);
			$zip->close();
			$dataInCSV = parse_img_list(EP_TEMP_DIRECTORY . 'list.csv');
			//delete zipped
			unlink(EP_TEMP_DIRECTORY . $file['name']);
			tep_redirect(tep_href_link('easypopulate.php?' . http_build_query(array('message' => 'Images uploaded'), '', '&amp;')));
		} else {
			tep_redirect(tep_href_link('easypopulate.php?' . http_build_query(array('errors' => 'file is not zipped file'), '', '&amp;')));
		}
	}
}
if (!empty($_POST['localfile']) || (isset($_FILES['usrfl']) && isset($_GET['split']) && $_GET['split'] == 0)) {
	$selectedLangID = EP_DEFAULT_LANGUAGE_ID;
	$maxAuthors = 3;
	if (isset($_FILES['usrfl'])) {
		// move the file to where we can work with it
		$file = tep_get_uploaded_file('usrfl');
		if (is_uploaded_file($file['tmp_name'])) {
			tep_copy_uploaded_file($file, EP_TEMP_DIRECTORY);
		}

		$dataInCSV = easy_import_parse_csv_to_array(EP_TEMP_DIRECTORY . $file['name']); 
	}

	// Create Tables and Clean MySQL Tables
	eaim_create_tables();
	eaim_analyze_tables();
	eaim_truncate_tables();
	// Fill in the parsed CSV data into the Database
	eaim_insert_parsed_data($dataInCSV);
	define('LANGUAGE_ID', $selectedLangID);
	define('SECOND_LANGUAGE_ID', 9);
	// now start copying data into os commerce
	$feedback = array('products_imported' => count($dataInCSV['products']));
	$feedback['update_authors'] = eaim_update_manufacturers();
	$feedback['add_authors'] = eaim_add_new_manufacturers();
	$feedback['update_publishers'] = eaim_update_publishers();
	$feedback['add_publishers'] = eaim_add_new_publishers();
	$feedback['update_countries'] = eaim_update_countries();
	$feedback['add_countries'] = eaim_add_new_countries();
	$feedback['update_categories'] = eaim_update_categories();
	$feedback['add_categories'] = eaim_add_new_categories();
	$feedback['update_products'] = eaim_update_products();
	$feedback['add_products'] = eaim_add_new_products();
	$feedback['update_products_publishers'] = eaim_add_update_products_publishers($dataInCSV['products_publishers']);
	$feedback['update_products_countries'] = eaim_add_update_products_countries($dataInCSV['products_countries']);
	$feedback['update_products_authors'] = eaim_add_update_products_authors($dataInCSV['products_authors']);
	$feedback['update_products_translators'] = eaim_add_update_products_translators($dataInCSV['products_translators']);
	//$feedback['update_products_countries'] = eaim_add_update_products_countries($dataInCSV['products_countries']);
	eaim_archive_not_imported_products();
	eaim_analyze_os_tables();
	tep_redirect(tep_href_link('easypopulate.php?' . http_build_query($feedback, '', '&amp;')));
} else {
	$feedback = count($_GET) ?
			array_intersect_key($_GET, array_fill_keys(array('errors', 'message', 'products_imported', 'update_authors', 'add_authors', 'update_publishers', 'add_publishers', 'update_categories', 'add_categories', 'update_products', 'add_products', 'update_products_publishers', 'update_products_authors', 'update_products_translators','update_products_countries'), '')) :
			FALSE;
}
require(DIR_WS_INCLUDES . 'template_top.php');
?>
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	<tr><td class="pageHeading" valign="top">Data Migration tool for BookStore POS by SYSCOM Technologies</td></tr>
	<tr>
		<td>
			<table width="75%" cellpadding="5" cellspacing="0" style="border-collapse:collapse;">
				<tr class="dataTableHeadingRow">
					<td class="dataTableHeadingContent">It is recommended that you back up your Database before you start</td>
				</tr>
				<tr class="dataTableHeadingRow">
					<td class="dataTableHeadingContent">
						<a class="backup-manage-link" target="__BLANK" href="backup.php<?php echo (defined('SID') && tep_not_null(SID)) ? ('?' . tep_session_name() . '=' . tep_session_id()) : '' ?>" ><strong>Backup your database</strong></a>
						<?php
						// Quick Backup Button
						if (EP_QUICK_BACKUP == true) {
							echo 'OR <form name="quick_backup" action="backup.php?action=backupnow', (defined('SID') && tep_not_null(SID) ?
									('&' . tep_session_name() . '=' . tep_session_id()) : ''), '" method="post" id="quick_backup">',
							'<input type="submit" name="quick_backup_button" value="Quick Backup" class="ui-button ui-widget ui-state-highlight ui-priority-primary"/></form>';
						}
						?>
					</td>
				</tr><?php if (FALSE != $feedback) { ?>
					<tr class="dataTableRow">
						<td class="dataTableContent">
							<?php
							echo '<div class="ui-widget ui-corner-all"><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><ul>';
							foreach ($feedback as $k => $v) {
								echo '<li>', $k, ': <strong>', (is_numeric($v) ? number_format($v, 0) : $v), '</strong></li>';
							}
							echo '</ul></div>';
							?>
						</td>
					</tr><?php } ?>
				<tr class="dataTableRow">
					<td class="dataTableContent">
						<?php
						echo '<form enctype="multipart/form-data" action="easypopulate.php?split=0', (defined('SID') && tep_not_null(SID) ?
								('&' . tep_session_name() . '=' . tep_session_id()) : ''), '" method="post">';
						if (defined('SID') && tep_not_null(SID)) {
							echo tep_draw_hidden_field(tep_session_name(), tep_session_id());
						}
						?>
						<h3>Upload and Import CSV File</h3>
						<input type="hidden" name="MAX_FILE_SIZE" value="100000000">
						<input name="usrfl" type="file" size="50" class="ui-button ui-widget ui-priority-secondary"/>
						<input type="submit" name="buttoninsert" class="ui-button ui-widget ui-state-error ui-priority-primary" value="Upload CSV File"/>
						<?php echo '</form>' ?>
					</td>
				</tr>
				<tr class="dataTableRow">
					<td class="dataTableContent">
						<?php
						echo '<form enctype="multipart/form-data" action="easypopulate.php?action=zipped" method="post">';
						if (defined('SID') && tep_not_null(SID)) {
							echo tep_draw_hidden_field(tep_session_name(), tep_session_id());
						}
						?>
						<h3>Upload and Import Images Zipped File</h3>
						<input name="zipped" type="file" size="50" class="ui-button ui-widget ui-priority-secondary"/>
						<input type="submit" name="buttoninsert" class="ui-button ui-widget ui-state-error ui-priority-primary" value="Upload Zipped File"/>
						<?php echo '</form>' ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
	$(function () {
		$("a.backup-manage-link").button();
	});
</script>
<?php
require(DIR_WS_INCLUDES . 'footer.php');
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');