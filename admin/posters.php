<?php
require('includes/application_top.php');
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
$is_poster = ($cPath >= POSTERS_CATEGORY_ID && $cPath != MAGAZINES_CATEGORY_ID) ? TRUE : FALSE;
//$cPath = POSTERS_CATEGORY_ID;
if (tep_not_null($action)) {
	switch ($action) {
		case 'setflag':
			if (($HTTP_GET_VARS['flag'] == '0') || ($HTTP_GET_VARS['flag'] == '1')) {
				if (isset($HTTP_GET_VARS['pID'])) {
					tep_set_product_status($HTTP_GET_VARS['pID'], $HTTP_GET_VARS['flag']);
				}
				if (USE_CACHE == 'true') {
					tep_reset_cache_block('categories');
					tep_reset_cache_block('also_purchased');
				}
			}
		case 'delete_product_confirm':
			if (isset($HTTP_POST_VARS['products_id']) && isset($HTTP_POST_VARS['product_categories']) && is_array($HTTP_POST_VARS['product_categories'])) {
				$product_id = tep_db_prepare_input($HTTP_POST_VARS['products_id']);
				$product_categories = $HTTP_POST_VARS['product_categories'];

				for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
					tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $product_id . "' and categories_id = '" . (int) $product_categories[$i] . "'");
				}

				$product_categories_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $product_id . "'");
				$product_categories = tep_db_fetch_array($product_categories_query);

				if ($product_categories['total'] == '0') {
					tep_remove_product($product_id);
				}
			}

			if (USE_CACHE == 'true') {
				tep_reset_cache_block('categories');
				tep_reset_cache_block('also_purchased');
			}

			tep_redirect(tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath));
			break;


		case 'insert_product':
		case 'update_product':
			if (isset($HTTP_GET_VARS['pID']))
				$products_id = tep_db_prepare_input($HTTP_GET_VARS['pID']);
			$products_date_available = tep_db_prepare_input($HTTP_POST_VARS['products_date_available']);
			$actors = tep_db_prepare_input($HTTP_POST_VARS['actors']);   //var_dump($HTTP_POST_VARS['products_date_released']);exit;
			$products_date_released = tep_db_prepare_input($HTTP_POST_VARS['products_date_released'] . '-01-01');
			$new_director = tep_db_prepare_input($HTTP_POST_VARS['new_director']);
			$directors_id = tep_db_prepare_input($HTTP_POST_VARS['directors_id']);
			$new_category = tep_db_prepare_input($HTTP_POST_VARS['new_category']);
			$categories_id = tep_db_prepare_input($HTTP_POST_VARS['categories_id']);
			$products_wrapped = tep_db_prepare_input($HTTP_POST_VARS['products_wrapped']);
			$products_tubbed = tep_db_prepare_input($HTTP_POST_VARS['products_tubbed']);
			$products_date_released = (!empty($products_date_released)) ? $products_date_released : 'null';
			$products_date_available = (date('Y-m-d') < $products_date_available) ? $products_date_available : 'null';
			$sql_data_array = array('products_quantity' => (int) tep_db_prepare_input($HTTP_POST_VARS['products_quantity']),
				'products_model' => tep_db_prepare_input((empty($HTTP_POST_VARS['products_model'])) ? 'P-' . rand() : $HTTP_POST_VARS['products_model']),
				'products_isbn' => tep_db_prepare_input($HTTP_POST_VARS['products_isbn']),
				'products_edition' => tep_db_prepare_input($HTTP_POST_VARS['products_edition']),
				'products_price' => (float) tep_db_prepare_input($HTTP_POST_VARS['products_price']),
				'products_discount' => (int) tep_db_prepare_input($HTTP_POST_VARS['products_discount']),
				'products_date_available' => $products_date_available,
				'products_date_released' => $products_date_released,
				'products_size' => tep_db_prepare_input($HTTP_POST_VARS['products_size']),
				'products_weight' => (float) tep_db_prepare_input($HTTP_POST_VARS['products_weight']),
				'products_page_nb' => (float) tep_db_prepare_input($HTTP_POST_VARS['products_page_nb']),
				'products_status' => tep_db_prepare_input($HTTP_POST_VARS['products_status']),
				'products_tubbed' => tep_db_prepare_input($HTTP_POST_VARS['products_tubbed']),
				'products_wrapped' => tep_db_prepare_input($HTTP_POST_VARS['products_wrapped']),
				'products_of_week' => (int) tep_db_prepare_input($HTTP_POST_VARS['products_of_week']),
				'products_cover_type' => tep_db_prepare_input($HTTP_POST_VARS['products_cover_type']),
				'products_tax_class_id' => tep_db_prepare_input($HTTP_POST_VARS['products_tax_class_id'])
			);

			$products_image = new upload('products_image');
			$products_image->set_destination(DIR_FS_CATALOG_IMAGES);
			if ($products_image->parse() && $products_image->save()) {
				$sql_data_array['products_image'] = tep_db_prepare_input($products_image->filename);
			}

			if ($action == 'insert_product') {
				$insert_sql_data = array('products_date_added' => 'now()');

				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_PRODUCTS, $sql_data_array);
				$products_id = tep_db_insert_id();
				tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int) $products_id . "', '" . (int) $current_category_id . "')");
				$categories_id = $current_category_id;
			} elseif ($action == 'update_product') {
				$update_sql_data = array('products_last_modified' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $update_sql_data);
				tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int) $products_id . "'");
				$categories_id = $current_category_id;
			}
			if (!empty($new_director)) {
				//add new director
				tep_db_query("insert into " . TABLE_DIRECTORS . " (id, name,date_added) values ('', '" . $new_director . "', '" . date('Y-m-d H:i:s ') . "')");
				$directors_id = tep_db_insert_id();
				tep_db_query("delete from " . TABLE_PRODUCTS_TO_DIRECTORS . " where products_id = '" . (int) $products_id . "' ");
				tep_db_query("insert into " . TABLE_PRODUCTS_TO_DIRECTORS . " (products_id, directors_id) values ('" . (int) $products_id . "', '" . (int) $directors_id . "')");
			} else {
				tep_db_query("delete from " . TABLE_PRODUCTS_TO_DIRECTORS . " where products_id = '" . (int) $products_id . "' ");
				tep_db_query("insert into " . TABLE_PRODUCTS_TO_DIRECTORS . " (products_id, directors_id) values ('" . (int) $products_id . "', '" . (int) $directors_id . "')");
			}
			if (!empty($new_category)) {
				$sql_data_array = array('sort_order' => 1);
				$insert_sql_data = array('parent_id' => POSTERS_CATEGORY_ID,
					'date_added' => 'now()');
				$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
				tep_db_perform(TABLE_CATEGORIES, $sql_data_array);
				$categories_id = tep_db_insert_id();
				$languages = tep_get_languages();
				for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
					$language_id = $languages[$i]['id'];
					$sql_data_array = array('categories_name' => tep_db_prepare_input($new_category));
					$insert_sql_data = array('categories_id' => $categories_id,
						'language_id' => $languages[$i]['id']);
					$sql_data_array = array_merge($sql_data_array, $insert_sql_data);
					tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
				}
			}
			//tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int) $products_id . "' ");
			//tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int) $products_id . "', '" . (int) $categories_id . "')");
			if (!empty($actors)) {
				tep_db_query("delete from " . TABLE_PRODUCTS_TO_ACTORS . " where products_id = '" . (int) $products_id . "' ");
				foreach ($actors as $actor) {
					tep_db_query("insert into " . TABLE_PRODUCTS_TO_ACTORS . " (products_id, actors_id) values ('" . (int) $products_id . "', '" . (int) $actor . "')");
				}
			} else {
				tep_db_query("delete from " . TABLE_PRODUCTS_TO_ACTORS . " where products_id = '" . (int) $products_id . "' ");
			}
			if ($products_wrapped == 1) {
				tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int) $products_id . "' and options_id=1");
				if (SHIPPING_WRAPPED_COST > 0) {
					tep_db_query("insert into " . TABLE_PRODUCTS_ATTRIBUTES . " (products_attributes_id, products_id,options_id,options_values_id,options_values_price,price_prefix) "
							. "values ('', '" . $products_id . "',1,1, '" . SHIPPING_WRAPPED_COST . "','+')");
				}
			} else {
				tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int) $products_id . "' and options_id=1");
			}
			if ($products_tubbed == 1) {
				tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int) $products_id . "' and options_id=2");
				if (SHIPPING_TUB_COST > 0) {
					tep_db_query("insert into " . TABLE_PRODUCTS_ATTRIBUTES . " (products_attributes_id, products_id,options_id,options_values_id,options_values_price,price_prefix) "
							. "values ('', '" . $products_id . "',2,2, '" . SHIPPING_TUB_COST . "','+')");
				}
			} else {
				tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int) $products_id . "' and options_id=2");
			}
			$languages = tep_get_languages();
			for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
				$language_id = $languages[$i]['id'];
				$products_name = tep_db_prepare_input($HTTP_POST_VARS['products_name'][$language_id]);
				$sql_data_array = array('products_name' => $products_name,
					'products_description' => tep_db_prepare_input($HTTP_POST_VARS['products_description'][$language_id]),
					'products_search_words' => str_replace("إ", "ا", str_replace("أ", "ا", $products_name)),
					'products_url' => tep_db_prepare_input($HTTP_POST_VARS['products_url'][$language_id]));

				if ($action == 'insert_product') {
					$insert_sql_data = array('products_id' => $products_id,
						'language_id' => $language_id);

					$sql_data_array = array_merge($sql_data_array, $insert_sql_data);

					tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array);
				} elseif ($action == 'update_product') {
					tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array, 'update', "products_id = '" . (int) $products_id . "' and language_id = '" . (int) $language_id . "'");
				}
			}


			$pi_sort_order = 0;
			$piArray = array(0);

			foreach ($HTTP_POST_FILES as $key => $value) {
// Update existing large product images
				if (preg_match('/^products_image_large_([0-9]+)$/', $key, $matches)) {
					$pi_sort_order++;

					$sql_data_array = array('htmlcontent' => tep_db_prepare_input($HTTP_POST_VARS['products_image_htmlcontent_' . $matches[1]]),
						'sort_order' => $pi_sort_order);

					$t = new upload($key);
					$t->set_destination(DIR_FS_CATALOG_IMAGES);
					if ($t->parse() && $t->save()) {
						$sql_data_array['image'] = tep_db_prepare_input($t->filename);
					}

					tep_db_perform(TABLE_PRODUCTS_IMAGES, $sql_data_array, 'update', "products_id = '" . (int) $products_id . "' and id = '" . (int) $matches[1] . "'");

					$piArray[] = (int) $matches[1];
				} elseif (preg_match('/^products_image_large_new_([0-9]+)$/', $key, $matches)) {
// Insert new large product images
					$sql_data_array = array('products_id' => (int) $products_id,
						'htmlcontent' => tep_db_prepare_input($HTTP_POST_VARS['products_image_htmlcontent_new_' . $matches[1]]));

					$t = new upload($key);
					$t->set_destination(DIR_FS_CATALOG_IMAGES);
					if ($t->parse() && $t->save()) {
						$pi_sort_order++;

						$sql_data_array['image'] = tep_db_prepare_input($t->filename);
						$sql_data_array['sort_order'] = $pi_sort_order;

						tep_db_perform(TABLE_PRODUCTS_IMAGES, $sql_data_array);

						$piArray[] = tep_db_insert_id();
					}
				}
			}

			$product_images_query = tep_db_query("select image from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $products_id . "' and id not in (" . implode(',', $piArray) . ")");
			if (tep_db_num_rows($product_images_query)) {
				while ($product_images = tep_db_fetch_array($product_images_query)) {
					$duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_IMAGES . " where image = '" . tep_db_input($product_images['image']) . "'");
					$duplicate_image = tep_db_fetch_array($duplicate_image_query);

					if ($duplicate_image['total'] < 2) {
						if (file_exists(DIR_FS_CATALOG_IMAGES . $product_images['image'])) {
							@unlink(DIR_FS_CATALOG_IMAGES . $product_images['image']);
						}
					}
				}

				tep_db_query("delete from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $products_id . "' and id not in (" . implode(',', $piArray) . ")");
			}

			if (USE_CACHE == 'true') {
				tep_reset_cache_block('categories');
				tep_reset_cache_block('also_purchased');
			}
			tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'page=' . $_GET['page'] . '&cPath=' . $categories_id . '&pID=' . $products_id));
			break;
	}
}

// check if the catalog image directory exists
if (is_dir(DIR_FS_CATALOG_IMAGES)) {
	if (!tep_is_writable(DIR_FS_CATALOG_IMAGES))
		$messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
	$messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}

require(DIR_WS_INCLUDES . 'template_top.php');

if ($action == 'new_product') {
	$parameters = array('products_name' => '',
		'categories_id' => '',
		'products_description' => '',
		'products_url' => '',
		'products_id' => '',
		'products_quantity' => '',
		'products_model' => '',
		'products_isbn' => '',
		'products_edition' => '',
		'products_image' => '',
		'products_larger_images' => array(),
		'products_price' => '',
		'products_discount' => '',
		'products_size' => '',
		'products_weight' => '',
		'products_page_nb' => '',
		'products_date_added' => '',
		'products_last_modified' => '',
		'products_date_available' => '',
		'products_date_released' => '',
		'products_status' => '',
		'products_of_week' => '',
		'products_cover_type' => '',
		'products_tax_class_id' => '',
		'products_tubbed' => '',
		'products_wrapped' => '',
		'directors_id' => '',
	);

	$pInfo = new objectInfo($parameters);

	if (isset($HTTP_GET_VARS['pID']) && empty($HTTP_POST_VARS)) {
		$product_query = tep_db_query("select pd.products_name, c.categories_id,pd.products_description, pd.products_url, p.products_id, p.products_quantity, p.products_model,p.products_isbn,p.products_edition, p.products_image, p.products_price, p.products_discount, p.products_size, p.products_weight,p.products_page_nb, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available,date_format(p.products_date_released, '%Y-%m-%d') as products_date_released, p.products_status, p.products_tubbed, p.products_wrapped,p.products_of_week,p.products_cover_type, p.products_tax_class_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd , " . TABLE_PRODUCTS_TO_CATEGORIES . " c  where p.products_id = '" . (int) $HTTP_GET_VARS['pID'] . "' and p.products_id = pd.products_id and p.products_id = c.products_id  and pd.language_id = '" . (int) $languages_id . "'");
		$product = tep_db_fetch_array($product_query);

		$pInfo->objectInfo($product);

		$product_images_query = tep_db_query("select id, image, htmlcontent, sort_order from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $product['products_id'] . "' order by sort_order");
		while ($product_images = tep_db_fetch_array($product_images_query)) {
			$pInfo->products_larger_images[] = array('id' => $product_images['id'],
				'image' => $product_images['image'],
				'htmlcontent' => $product_images['htmlcontent'],
				'sort_order' => $product_images['sort_order']);
		}
	}
	$manufacturers_query = tep_db_query("select directors_id from " . TABLE_PRODUCTS_TO_DIRECTORS . "  where products_id = '" . (int) $product['products_id'] . "' ");
	$director = tep_db_fetch_array($manufacturers_query);
	$pInfo->directors_id = $director['directors_id'];
	$directors_array = array(array('id' => '', 'text' => TEXT_NONE));
	$manufacturers_query = tep_db_query("select id, name from " . TABLE_DIRECTORS . "  order by name");
	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
		$directors_array[] = array('id' => $manufacturers['id'],
			'text' => $manufacturers['name']);
	}
	$manufacturers_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . "  where products_id = '" . (int) $product['products_id'] . "' ");
	$category = tep_db_fetch_array($manufacturers_query);
	$pInfo->categories_id = $category['categories_id'];
	$categories_array = array(array('id' => '', 'text' => TEXT_NONE));
	$manufacturers_query = tep_db_query("select " . TABLE_CATEGORIES . ".categories_id, categories_name from " . TABLE_CATEGORIES_DESCRIPTION . "  inner join " . TABLE_CATEGORIES . " on " . TABLE_CATEGORIES_DESCRIPTION . ".categories_id= " . TABLE_CATEGORIES . ".categories_id where  parent_id = '" . (int) POSTERS_CATEGORY_ID . "' order by categories_name");
	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
		$categories_array[] = array('id' => $manufacturers['categories_id'],
			'text' => $manufacturers['categories_name']);
	}
	$actors_array = array(array('id' => '', 'text' => TEXT_NONE));
	$manufacturers_query = tep_db_query("select id, name from " . TABLE_ACTORS . "  order by name");
	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
		$actors_array[] = array('id' => $manufacturers['id'],
			'text' => $manufacturers['name']);
	}
	$manufacturers_query = tep_db_query("select actors.id, name from " . TABLE_ACTORS . " inner join " . TABLE_PRODUCTS_TO_ACTORS . " on " . TABLE_ACTORS . ".id= " . TABLE_PRODUCTS_TO_ACTORS . ".actors_id  where products_id = '" . (int) $product['products_id'] . "'   order by name");
	$pactors = tep_db_fetch_all($manufacturers_query);

//	$authors_array = array(array('id' => '', 'text' => TEXT_NONE));
//	$manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name");
//	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
//		$authors_array[] = array('id' => $manufacturers['manufacturers_id'],
//			'text' => $manufacturers['manufacturers_name']);
//	}
//	$translators_array = array(array('id' => '', 'text' => TEXT_NONE));
//	$manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . "  order by manufacturers_name");
//	while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
//		$translators_array[] = array('id' => $manufacturers['manufacturers_id'],
//			'text' => $manufacturers['manufacturers_name']);
//	}

	$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
	$tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
	while ($tax_class = tep_db_fetch_array($tax_class_query)) {
		$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
			'text' => $tax_class['tax_class_title']);
	}

	$languages = tep_get_languages();

	if (!isset($pInfo->products_status))
		$pInfo->products_status = '1';
	switch ($pInfo->products_status) {
		case '0': $in_status = false;
			$out_status = true;
			break;
		case '1':
		default: $in_status = true;
			$out_status = false;
	}
	if (!isset($pInfo->products_tubbed))
		$pInfo->products_tubbed = '1';
	switch ($pInfo->products_tubbed) {
		case '0': $yes_tubbed = false;
			$no_tubbed = true;
			break;
		case '1':
		default: $yes_tubbed = true;
			$no_tubbed = false;
	}
	if (!isset($pInfo->products_wrapped))
		$pInfo->products_wrapped = '1';
	switch ($pInfo->products_wrapped) {
		case '0': $yes_wrapped = false;
			$no_wrapped = true;
			break;
		case '1':
		default: $yes_wrapped = true;
			$no_wrapped = false;
	}
	if (!isset($pInfo->products_cover_type))
		$pInfo->products_cover_type = 'normal';
	switch ($pInfo->products_cover_type) {
		case 'normal': $normal_status = true;
			$art_status = false;
			break;
		case 'artistic':$normal_status = false;
			$art_status = true;
			break;
		default: $normal_status = true;
			$art_status = false;
	}

	$form_action = (isset($HTTP_GET_VARS['pID'])) ? 'update_product' : 'insert_product';
	?>
	<script type="text/javascript"><!--
	var tax_rates = new Array();
	<?php
	for ($i = 0, $n = sizeof($tax_class_array); $i < $n; $i++) {
		if ($tax_class_array[$i]['id'] > 0) {
			echo 'tax_rates["' . $tax_class_array[$i]['id'] . '"] = ' . tep_get_tax_rate_value($tax_class_array[$i]['id']) . ';' . "\n";
		}
	}
	?>

		function doRound(x, places) {
			return Math.round(x * Math.pow(10, places)) / Math.pow(10, places);
		}

		function getTaxRate() {
			var selected_value = document.forms["new_product"].products_tax_class_id.selectedIndex;
			var parameterVal = document.forms["new_product"].products_tax_class_id[selected_value].value;

			if ((parameterVal > 0) && (tax_rates[parameterVal] > 0)) {
				return tax_rates[parameterVal];
			} else {
				return 0;
			}
		}
		function discount() {
			var products_discount = document.forms["new_product"].products_discount.value;

			if (products_discount > 100) {
				alert('Discount should be less than 100');
				document.forms["new_product"].products_discount.value = '100';
			}

			updateGross();
		}
		function isNumber(event) {
			if (event) {
				var charCode = (event.which) ? event.which : event.keyCode;
				if (charCode != 190 && charCode > 31 &&
						(charCode < 48 || charCode > 57) &&
						(charCode < 96 || charCode > 105) &&
						(charCode < 37 || charCode > 40) &&
						charCode != 110 && charCode != 8 && charCode != 46)
					return false;
			}
			return true;
		}
		function updateGross() {
			var taxRate = getTaxRate();
			var grossValue = document.forms["new_product"].products_price.value;
			var discountValue = document.forms["new_product"].products_discount.value;
			if (discountValue > 0) {
				grossValue = grossValue - (grossValue * discountValue / 100);
			}
			if (taxRate > 0) {

				grossValue = grossValue * ((taxRate / 100) + 1);
			}

			document.forms["new_product"].products_price_gross.value = doRound(grossValue, 4);
		}

		function updateNet() {
			var taxRate = getTaxRate();
			var netValue = document.forms["new_product"].products_price_gross.value;

			if (taxRate > 0) {
				netValue = netValue / ((taxRate / 100) + 1);
			}

			document.forms["new_product"].products_price.value = doRound(netValue, 4);
		}
		//--></script>
	<script type="text/javascript" src="ext/jquery/add_actor.js"></script>
	<?php echo tep_draw_form('new_product', FILENAME_POSTERS, 'page=' . $_GET['page'] . '&cPath=' . $cPath . (isset($HTTP_GET_VARS['pID']) ? '&pID=' . $HTTP_GET_VARS['pID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="pageHeading"><?php echo sprintf(TEXT_NEW_PRODUCT, tep_output_generated_category_path($current_category_id)); ?></td>
						<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
					</tr>
				</table></td>
		</tr>

		<tr>
			<td><table border="0" cellspacing="0" cellpadding="2">
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<?php
					for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
						?>
						<tr>
							<td class="main"><?php if ($i == 0) echo TEXT_PRODUCTS_NAME; ?></td>
							<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), '') . tep_draw_input_field('products_name[' . $languages[$i]['id'] . ']', (empty($pInfo->products_id) ? '' : tep_get_products_name($pInfo->products_id, $languages[$i]['id']))); ?></td>
						</tr>
						<?php
					}
					?>
					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_STATUS; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_radio_field('products_status', '1', $in_status) . '&nbsp;' . TEXT_PRODUCT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('products_status', '0', $out_status) . '&nbsp;' . TEXT_PRODUCT_NOT_AVAILABLE; ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_DATE_AVAILABLE; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_date_available', $pInfo->products_date_available, 'id="products_date_available"') . ' <small>(YYYY-MM-DD)</small>'; ?></td>
					</tr>
					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_DATE_RELEASED; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_date_released', $pInfo->products_date_released, 'id="products_date_released"') . ' <small>(YYYY-MM-DD)</small>'; ?></td>
					</tr>
					<?php if ($is_poster) { ?>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr bgcolor="#ebebff">
							<td class="main"><?php echo TEXT_PRODUCTS_COUNTRY; ?></td>
							<td class="main"><?php
								echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('categories_id', $categories_array, $pInfo->categories_id, 'id="categories_id" style="width:250px;"');
								echo '&nbsp;', TEXT_NEW_COUNTRY, tep_draw_input_field('new_category', '', 'class="formInput"');
								?></td>
						</tr>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr bgcolor="#ebebff">
							<td class="main"><?php echo TEXT_PRODUCTS_DIRECTOR; ?></td>
							<td class="main"><?php
								echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('directors_id', $directors_array, $pInfo->directors_id, 'id="directors_id" style="width:250px;"');
								echo '&nbsp;', TEXT_NEW_DIRECTOR, tep_draw_input_field('new_director', '', 'class="formInput"');
								?></td>
						</tr>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr bgcolor="#ebebff">
							<td class="main"><?php echo TEXT_PRODUCTS_ACTORS; ?></td>
							<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('actors_id', $actors_array, '', 'id="actor" style="width:250px;"'); ?>
								<a href="javascript:;" onclick="selectAuthor();" class="btn btn-sm btn-success">
									<?php echo tep_image('images/icons/add.png'); ?>
								</a>
								&nbsp;<a  id="addNewAuthor" href="javascript:;" onclick="addAuthor();" >
									<?php echo tep_image('images/icons/add_actors.png'); ?>
								</a>
								<span id="newActor"></span>
							</td>
						</tr>
						<tr><td colspan="2"><div class="clearfix">&nbsp;</div>
								<div  id="authorsDiv"><?php
									foreach ($pactors as $artauthor) {
										if (!empty($artauthor['id'])) {
											echo '<span id="auth', $artauthor['id'], '" class="label">', $artauthor['name'],
											'<input type="hidden" name="actors[]" value="', $artauthor['id'], '" />&nbsp<a href="javascript:;" class="glyphicon-remove" onclick="jQuery(this.parentNode).remove();">x</a></span>&nbsp&nbsp';
										}
									}
									?></div></td></tr>
					<?php } ?>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<tr bgcolor="#ebebff" class="hide">
						<td class="main"><?php echo TEXT_PRODUCTS_TAX_CLASS; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('products_tax_class_id', $tax_class_array, $pInfo->products_tax_class_id, 'onchange="updateGross()"'); ?></td>
					</tr>
					<tr bgcolor="#ebebff">
						<td class="main"><?php echo TEXT_PRODUCTS_PRICE_NET; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_price', $pInfo->products_price, 'onkeyup="updateGross()" onKeyDown="return isNumber(event);"'); ?></td>
					</tr>
					<tr bgcolor="#ebebff">
						<td class="main"><?php echo TEXT_PRODUCTS_PRICE_DISCOUNT; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_discount', $pInfo->products_discount, 'onkeyup="discount();" onKeyDown="return isNumber(event);"  ') . '%'; ?></td>
					</tr>
					<tr bgcolor="#ebebff">
						<td class="main"><?php echo TEXT_PRODUCTS_PRICE_GROSS; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_price_gross', $pInfo->products_price, ' readonly="true"'); ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<script type="text/javascript"><!--
		updateGross();
						//--></script>
					<?php
					for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
						?>
						<tr>
							<td class="main" valign="top"><?php if ($i == 0) echo TEXT_PRODUCTS_DESCRIPTION; ?></td>
							<td><table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="main" valign="top"><?php echo tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), ''); ?>&nbsp;</td>
										<td class="main"><?php echo tep_draw_textarea_field_ckeditor('products_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (empty($pInfo->products_id) ? '' : tep_get_products_description($pInfo->products_id, $languages[$i]['id']))); ?></td>
									</tr>
								</table></td>
						</tr>
						<?php
					}
					?>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_QUANTITY; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_quantity', $pInfo->products_quantity); ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>
					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_MODEL; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_model', $pInfo->products_model, 'readonly style="background-color:#ccc;"'); ?></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
					</tr>



					<tr>
						<td class="main" valign="top"><?php echo TEXT_PRODUCTS_IMAGE; ?></td>
						<td class="main" style="padding-left: 30px;">
							<div><?php echo '<strong>' . TEXT_PRODUCTS_MAIN_IMAGE . ' <small>(' . SMALL_IMAGE_WIDTH . ' x ' . SMALL_IMAGE_HEIGHT . 'px)</small></strong><br />' . (tep_not_null($pInfo->products_image) ? '<a href="' . HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $pInfo->products_image . '" target="_blank">' . $pInfo->products_image . '</a> &#124; ' : '') . tep_draw_file_field('products_image'); ?></div>

							<ul id="piList">
								<?php
								$pi_counter = 0;

								foreach ($pInfo->products_larger_images as $pi) {
									$pi_counter++;

									echo '                <li id="piId' . $pi_counter . '" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: right;"></span><a href="#" onclick="showPiDelConfirm(' . $pi_counter . ');return false;" class="ui-icon ui-icon-trash" style="float: right;"></a><strong>' . TEXT_PRODUCTS_LARGE_IMAGE . '</strong><br />' . tep_draw_file_field('products_image_large_' . $pi['id']) . '<br /><a href="' . HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $pi['image'] . '" target="_blank">' . $pi['image'] . '</a><br /><br />' . TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT . '<br />' . tep_draw_textarea_field('products_image_htmlcontent_' . $pi['id'], 'soft', '70', '3', $pi['htmlcontent']) . '</li>';
								}
								?>
							</ul>

							<a  href="#" onclick="addNewPiForm();
										return false;" ><span class="ui-icon ui-icon-plus" style="float: left;"></span><?php echo TEXT_PRODUCTS_ADD_LARGE_IMAGE; ?></a>

							<div id="piDelConfirm" title="<?php echo TEXT_PRODUCTS_LARGE_IMAGE_DELETE_TITLE; ?>">
								<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><?php echo TEXT_PRODUCTS_LARGE_IMAGE_CONFIRM_DELETE; ?></p>
							</div>

							<style type="text/css">
								#piList { list-style-type: none; margin: 0; padding: 0; }
								#piList li { margin: 5px 0; padding: 2px; }
							</style>

							<script type="text/javascript">
								$('#piList').sortable({
									containment: 'parent'
								});

								var piSize = <?php echo $pi_counter; ?>;

								function addNewPiForm() {
									piSize++;

									$('#piList').append('<li id="piId' + piSize + '" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: right;"></span><a href="#" onclick="showPiDelConfirm(' + piSize + ');return false;" class="ui-icon ui-icon-trash" style="float: right;"></a><strong><?php echo TEXT_PRODUCTS_LARGE_IMAGE; ?></strong><br /><input type="file" name="products_image_large_new_' + piSize + '" /><br /><br /><?php echo TEXT_PRODUCTS_LARGE_IMAGE_HTML_CONTENT; ?><br /><textarea name="products_image_htmlcontent_new_' + piSize + '" wrap="soft" cols="70" rows="3"></textarea></li>');
								}

								var piDelConfirmId = 0;

								$('#piDelConfirm').dialog({
									autoOpen: false,
									resizable: false,
									draggable: false,
									modal: true,
									buttons: {
										'Delete': function () {
											$('#piId' + piDelConfirmId).effect('blind').remove();
											$(this).dialog('close');
										},
										Cancel: function () {
											$(this).dialog('close');
										}
									}
								});

								function showPiDelConfirm(piId) {
									piDelConfirmId = piId;

									$('#piDelConfirm').dialog('open');
								}
							</script>

						</td>
					</tr>


					<tr>
						<td class="main"><?php echo TEXT_PRODUCTS_WEIGHT; ?></td>
						<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_weight', $pInfo->products_weight, 'onKeyDown="return isNumber(event);"') . ' g'; ?></td>
					</tr>
					<?php if ($is_poster) { ?>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td class="main"><?php echo TEXT_PRODUCTS_SIZE; ?></td>
							<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_size', $pInfo->products_size); ?></td>
						</tr>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td class="main"><?php echo TEXT_PRODUCTS_TUBBED; ?></td>
							<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_radio_field('products_tubbed', '1', $yes_tubbed) . '&nbsp;' . TEXT_YES . '&nbsp;' . tep_draw_radio_field('products_tubbed', '0', $no_tubbed) . '&nbsp;' . TEXT_NO; ?></td>
						</tr>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td class="main"><?php echo TEXT_PRODUCTS_WRAPPED; ?></td>
							<td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_radio_field('products_wrapped', '1', $yes_wrapped) . '&nbsp;' . TEXT_YES . '&nbsp;' . tep_draw_radio_field('products_wrapped', '0', $no_wrapped) . '&nbsp;' . TEXT_NO; ?></td>
						</tr>
					<?php } ?>
				</table></td>
		</tr>
		<tr>
			<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
		</tr>
		<tr>
			<td class="smallText" align="right"><?php
				echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d')))
				. tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_CATEGORIES, 'page=' . $_GET['page'] . '&cPath=' . $cPath . (isset($HTTP_GET_VARS['pID']) ? '&pID=' . $HTTP_GET_VARS['pID'] : '')));
				?></td>
		</tr>
	</table>

	<script type="text/javascript">
		$('#products_date_available').datepicker({
			dateFormat: 'yy-mm-dd'
		});
		$('#products_date_released').datepicker({
			dateFormat: 'yy-mm-dd'
		});
	</script>

	</form>
	<?php
} elseif ($action == 'new_product_preview') {
	$product_query = tep_db_query("select p.products_id,c.categories_id, pd.language_id, pd.products_name, pd.products_description, pd.products_url, p.products_quantity, p.products_model,p.products_isbn,p.products_edition, p.products_image, p.products_price,p.products_discount, p.products_size, p.products_weight,p.products_page_nb, p.products_date_added, p.products_last_modified, p.products_date_available,p.products_date_released, p.products_status,p.products_of_week, p.products_cover_type  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd , " . TABLE_PRODUCTS_TO_CATEGORIES . " c where p.products_id = pd.products_id and p.products_id = c.products_id and p.products_id = '" . (int) $HTTP_GET_VARS['pID'] . "'");
	$product = tep_db_fetch_array($product_query);

	$pInfo = new objectInfo($product);
	$products_image_name = $pInfo->products_image;

	$languages = tep_get_languages();
	for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
		$pInfo->products_name = tep_get_products_name($pInfo->products_id, $languages[$i]['id']);
		$pInfo->products_description = tep_get_products_description($pInfo->products_id, $languages[$i]['id']);
		$pInfo->products_url = tep_get_products_url($pInfo->products_id, $languages[$i]['id']);
		?>
		<table border="0" width="100%" cellspacing="0" cellpadding="2" style="margin-top:25px;">
			<tr>
				<td width="60%">
					<table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr><td class="pageHeading"><?php echo sprintf(TEXT_NEW_PRODUCT, tep_output_generated_category_path($current_category_id)); ?></td></tr>
						<tr>
							<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr>
										<td class="pageHeading" align="left"><?php
											echo $currencies->format($pInfo->products_price);
											if ($pInfo->products_discount > 0)
												echo ' <br><span class="discount">(Discount:' . $pInfo->products_discount . '%)</span>';
											?></td>
										<td class="pageHeading" align="right"><?php echo tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), $languages[$i]['name']) . '&nbsp;' . $pInfo->products_name; ?></td>

									</tr>
								</table></td>
						</tr>
						<tr>
							<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td class="main" align="right"><?php echo tep_image(HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $products_image_name, $pInfo->products_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT, 'align="right" hspace="5" vspace="5"') . $pInfo->products_description; ?></td>
						</tr>
						<?php
						if ($pInfo->products_url) {
							?>
							<tr>
								<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
							</tr>
							<tr>
								<td class="main"><?php echo sprintf(TEXT_PRODUCT_MORE_INFORMATION, $pInfo->products_url); ?></td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<?php
						if ($pInfo->products_date_available > date('Y-m-d')) {
							?>
							<tr>
								<td align="left" class="smallText"><?php echo sprintf(TEXT_PRODUCT_DATE_AVAILABLE, tep_date_long($pInfo->products_date_available)); ?></td>
							</tr>
							<?php
						} else {
							?>
							<tr>
								<td align="left" class="smallText"><?php echo sprintf(TEXT_PRODUCT_DATE_ADDED, tep_date_long($pInfo->products_date_added)); ?></td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td align="left" class="smallText"><?php echo sprintf(TEXT_PRODUCTS_DATE_RELEASED, '') . '' . tep_date_long($product['products_date_released']); ?></td>
						</tr>
						<?php
					}

					if (isset($HTTP_GET_VARS['origin'])) {
						$pos_params = strpos($HTTP_GET_VARS['origin'], '?', 0);
						if ($pos_params != false) {
							$back_url = substr($HTTP_GET_VARS['origin'], 0, $pos_params);
							$back_url_params = substr($HTTP_GET_VARS['origin'], $pos_params + 1);
						} else {
							$back_url = $HTTP_GET_VARS['origin'];
							$back_url_params = '';
						}
					} else {
						$back_url = FILENAME_POSTERS;
						$back_url_params = 'cPath=' . $cPath . '&pID=' . $pInfo->products_id;
					}
					?>
					<tr>
						<td align="right" class="smallText"><?php echo tep_draw_button(IMAGE_BACK, 'triangle-1-w', tep_href_link($back_url, $back_url_params)); ?></td>
					</tr>
				</table></td><td width="5%"></td>
			<td align="left" class="smallText" valign="top" width="35%">
				<table width="100%"><tr>
						<td width="50%" valign="top"><?php echo 'Product Details: <br><br>' . 'Quantity:' . $product['products_quantity'] . '<br><br>' . 'ISBN:' . $product['products_isbn'] . '<br><br>' . 'Edition:' . $product['products_edition'] . '<br><br>' . 'Page nb:' . $product['products_page_nb']; ?></td>
						<td width="50%" valign="top"><?php echo '<br><br>Model:' . $product['products_model'] . '<br><br>' . 'Size:' . $product['products_size'] . '<br><br>' . 'Weight:' . $product['products_weight'] . ' g' . '<br><br> Cover Type:' . $product['products_cover_type']; ?></td>
					</tr></table>
			</td>
		</tr></table>
	<?php
} else {
	?>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td></td>
		</tr>
		<tr>
			<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
								<tr class="dataTableHeadingRow">
									<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CATEGORIES_PRODUCTS; ?></td>
									<td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>

									<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
								</tr>
								<?php
								$categories_count = 0;
								$rows = 0;
								$current_category_id = POSTERS_CATEGORY_ID;


								$products_count = 0;
								$products_query_raw = "select p.products_id, pd.products_name, p2c.categories_id, p.products_quantity, p.products_image, p.products_price, p.products_date_added, p.products_last_modified, p.products_date_available,p.products_date_released, p.products_status, p.products_of_week from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = pd.products_id and pd.language_id = '" . (int) $languages_id . "' and p.products_id = p2c.products_id and p2c.categories_id = '" . (int) $current_category_id . "' order by pd.products_name";
								$products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $products_query_raw, $products_query_numrows);
								$products_query = tep_db_query($products_query_raw);
								while ($products = tep_db_fetch_array($products_query)) {

									$products_count++;
									$rows++;

// Get categories_id for product if search
									if (isset($HTTP_GET_VARS['search']))
										$cPath = $products['categories_id'];

									if ((!isset($HTTP_GET_VARS['pID']) && !isset($HTTP_GET_VARS['cID']) || (isset($HTTP_GET_VARS['pID']) && ($HTTP_GET_VARS['pID'] == $products['products_id']))) && !isset($pInfo) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
// find out the rating average from customer reviews
										$reviews_query = tep_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating from " . TABLE_REVIEWS . " where products_id = '" . (int) $products['products_id'] . "'");
										$reviews = tep_db_fetch_array($reviews_query);
										$pInfo_array = array_merge($products, $reviews);
										$pInfo = new objectInfo($pInfo_array);
									}

									if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id)) {
										echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=new_product_preview') . '\'">' . "\n";
									} else {
										echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $products['products_id']) . '\'">' . "\n";
									}
									?>
									<td class="dataTableContent arabicTd"><?php echo '<a href="' . tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=new_product_preview') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $products['products_name']; ?></td>
									<td class="dataTableContent" align="center">
										<?php
										if ($products['products_status'] == '1') {
											echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_POSTERS, 'action=setflag&flag=0&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
										} else {
											echo '<a href="' . tep_href_link(FILENAME_POSTERS, 'action=setflag&flag=1&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
										}
										?></td>

									<td class="dataTableContent" align="right"><?php
										if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id)) {
											echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', '');
										} else {
											echo '<a href="' . tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $products['products_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>';
										}
										?>&nbsp;</td>
						</tr>
						<?php
					}

					$cPath_back = '';
					if (sizeof($cPath_array) > 0) {
						for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
							if (empty($cPath_back)) {
								$cPath_back .= $cPath_array[$i];
							} else {
								$cPath_back .= '_' . $cPath_array[$i];
							}
						}
					}

					$cPath_back = (tep_not_null($cPath_back)) ? 'cPath=' . $cPath_back . '&' : '';
					?>
					<tr>
						<td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
								<tr>
									<td class="smallText"><?php echo TEXT_CATEGORIES . '&nbsp;' . $categories_count; ?></td>
									<td align="right" class="smallText"><?php
										echo tep_draw_button(IMAGE_NEW_POSTER, 'plus', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&action=new_product'));
										?>&nbsp;</td>
								</tr>
							</table></td>
					</tr>
				</table></td>
			<?php
			$heading = array();
			$contents = array();
			switch ($action) {
				case 'new_category':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_CATEGORY . '</strong>');

					$contents = array('form' => tep_draw_form('newcategory', FILENAME_POSTERS, 'action=insert_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"'));
					$contents[] = array('text' => TEXT_NEW_CATEGORY_INTRO);

					$category_inputs_string = '';
					$languages = tep_get_languages();
					for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
						$category_inputs_string .= '<br />' . tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']');
					}

					$contents[] = array('text' => '<br />' . TEXT_CATEGORIES_NAME . $category_inputs_string);
					$contents[] = array('text' => '<br />' . TEXT_CATEGORIES_IMAGE . '<br />' . tep_draw_file_field('categories_image'));
					$contents[] = array('text' => '<br />' . TEXT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', '', 'size="2"'));
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath)));
					break;
				case 'edit_category':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_CATEGORY . '</strong>');

					$contents = array('form' => tep_draw_form('categories', FILENAME_POSTERS, 'action=update_category&cPath=' . $cPath, 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('categories_id', $cInfo->categories_id));
					$contents[] = array('text' => TEXT_EDIT_INTRO);

					$category_inputs_string = '';
					$languages = tep_get_languages();
					for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
						$category_inputs_string .= '<br />' . tep_image(tep_catalog_href_link(DIR_WS_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], '', 'SSL'), $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', tep_get_category_name($cInfo->categories_id, $languages[$i]['id']));
					}

					$contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_NAME . $category_inputs_string);
					$contents[] = array('text' => '<br />' . tep_image(HTTP_CATALOG_SERVER . DIR_WS_CATALOG_IMAGES . $cInfo->categories_image, $cInfo->categories_name) . '<br />' . DIR_WS_CATALOG_IMAGES . '<br /><strong>' . $cInfo->categories_image . '</strong>');
					$contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_IMAGE . '<br />' . tep_draw_file_field('categories_image'));
					$contents[] = array('text' => '<br />' . TEXT_EDIT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', $cInfo->sort_order, 'size="2"'));
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id)));
					break;
				case 'delete_category':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CATEGORY . '</strong>');

					$contents = array('form' => tep_draw_form('categories', FILENAME_POSTERS, 'action=delete_category_confirm&cPath=' . $cPath) . tep_draw_hidden_field('categories_id', $cInfo->categories_id));
					$contents[] = array('text' => TEXT_DELETE_CATEGORY_INTRO);
					$contents[] = array('text' => '<br /><strong>' . $cInfo->categories_name . '</strong>');
					if ($cInfo->childs_count > 0)
						$contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_CHILDS, $cInfo->childs_count));
					if ($cInfo->products_count > 0)
						$contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_PRODUCTS, $cInfo->products_count));
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id)));
					break;
				case 'move_category':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_CATEGORY . '</strong>');

					$contents = array('form' => tep_draw_form('categories', FILENAME_POSTERS, 'action=move_category_confirm&cPath=' . $cPath) . tep_draw_hidden_field('categories_id', $cInfo->categories_id));
					$contents[] = array('text' => sprintf(TEXT_MOVE_CATEGORIES_INTRO, $cInfo->categories_name));
					$contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $cInfo->categories_name) . '<br />' . tep_draw_pull_down_menu('move_to_category_id', tep_get_category_tree(), $current_category_id));
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_MOVE, 'arrow-4', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&cID=' . $cInfo->categories_id)));
					break;
				case 'delete_product':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</strong>');

					$contents = array('form' => tep_draw_form('products', FILENAME_POSTERS, 'action=delete_product_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
					$contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);
					$contents[] = array('text' => '<br /><strong>' . $pInfo->products_name . '</strong>');

					$product_categories_string = '';
					$product_categories = tep_generate_category_path($pInfo->products_id, 'product');
					for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
						$category_path = '';
						for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
							$category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
						}
						$category_path = substr($category_path, 0, -16);
						$product_categories_string .= tep_draw_checkbox_field('product_categories[]', $product_categories[$i][sizeof($product_categories[$i]) - 1]['id'], true) . '&nbsp;' . $category_path . '<br />';
					}
					$product_categories_string = substr($product_categories_string, 0, -4);

					$contents[] = array('text' => '<br />' . $product_categories_string);
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id)));
					break;
				case 'move_product':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_MOVE_PRODUCT . '</strong>');

					$contents = array('form' => tep_draw_form('products', FILENAME_POSTERS, 'action=move_product_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
					$contents[] = array('text' => sprintf(TEXT_MOVE_PRODUCTS_INTRO, $pInfo->products_name));
					$contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . tep_output_generated_category_path($pInfo->products_id, 'product') . '</strong>');
					$contents[] = array('text' => '<br />' . sprintf(TEXT_MOVE, $pInfo->products_name) . '<br />' . tep_draw_pull_down_menu('move_to_category_id', tep_get_category_tree(), $current_category_id));
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_MOVE, 'arrow-4', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id)));
					break;
				case 'copy_to':
					$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');

					$contents = array('form' => tep_draw_form('copy_to', FILENAME_POSTERS, 'action=copy_to_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
					$contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
					$contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . tep_output_generated_category_path($pInfo->products_id, 'product') . '</strong>');
					$contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . tep_draw_pull_down_menu('categories_id', tep_get_category_tree(), $current_category_id));
					$contents[] = array('text' => '<br />' . TEXT_HOW_TO_COPY . '<br />' . tep_draw_radio_field('copy_as', 'link', true) . ' ' . TEXT_COPY_AS_LINK . '<br />' . tep_draw_radio_field('copy_as', 'duplicate') . ' ' . TEXT_COPY_AS_DUPLICATE);
					$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_COPY, 'copy', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id)));
					break;
				default:
					if ($rows > 0) {
						if (isset($cInfo) && is_object($cInfo)) { // category info box contents
							$category_path_string = '';
							$category_path = tep_generate_category_path($cInfo->categories_id);
							for ($i = (sizeof($category_path[0]) - 1); $i > 0; $i--) {
								$category_path_string .= $category_path[0][$i]['id'] . '_';
							}
							$category_path_string = substr($category_path_string, 0, -1);

							$heading[] = array('text' => '<strong>' . $cInfo->categories_name . '</strong>');

							$contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_POSTERS, 'cPath=' . $category_path_string . '&cID=' . $cInfo->categories_id . '&action=edit_category')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_POSTERS, 'cPath=' . $category_path_string . '&cID=' . $cInfo->categories_id . '&action=delete_category')) . tep_draw_button(IMAGE_MOVE, 'arrow-4', tep_href_link(FILENAME_POSTERS, 'cPath=' . $category_path_string . '&cID=' . $cInfo->categories_id . '&action=move_category')));
							$contents[] = array('text' => '<br />' . TEXT_DATE_ADDED . ' ' . tep_date_short($cInfo->date_added));
							if (tep_not_null($cInfo->last_modified))
								$contents[] = array('text' => TEXT_LAST_MODIFIED . ' ' . tep_date_short($cInfo->last_modified));
							$contents[] = array('text' => '<br />' . tep_info_image($cInfo->categories_image, $cInfo->categories_name, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT) . '<br />' . $cInfo->categories_image);
							$contents[] = array('text' => '<br />' . TEXT_SUBCATEGORIES . ' ' . $cInfo->childs_count . '<br />' . TEXT_PRODUCTS . ' ' . $cInfo->products_count);
						} elseif (isset($pInfo) && is_object($pInfo)) { // product info box contents
							$heading[] = array('text' => '<strong>' . tep_get_products_name($pInfo->products_id, $languages_id) . '</strong>');

							$contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . '&action=new_product')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_POSTERS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . '&action=delete_product'))
							);
							$contents[] = array('text' => '<br />' . TEXT_DATE_ADDED . ' ' . tep_date_short($pInfo->products_date_added));
							$contents[] = array('text' => '<br />' . sprintf(TEXT_PRODUCT, tep_output_generated_category_path($pInfo->categories_id)));
							if (tep_not_null($pInfo->products_last_modified))
								$contents[] = array('text' => TEXT_LAST_MODIFIED . ' ' . tep_date_short($pInfo->products_last_modified));
							if (date('Y-m-d') < $pInfo->products_date_available)
								$contents[] = array('text' => TEXT_DATE_AVAILABLE . ' ' . tep_date_short($pInfo->products_date_available));
							$contents[] = array('text' => TEXT_DATE_RELEASED . ' ' . tep_date_short($pInfo->products_date_released));
							$contents[] = array('text' => '<br />' . tep_info_image($pInfo->products_image, $pInfo->products_name, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '<br />' . $pInfo->products_image);
							$contents[] = array('text' => '<br />' . TEXT_PRODUCTS_PRICE_INFO . ' ' . $currencies->format($pInfo->products_price) . '<br />' . TEXT_PRODUCTS_QUANTITY_INFO . ' ' . $pInfo->products_quantity);
							$contents[] = array('text' => '<br />' . TEXT_PRODUCTS_AVERAGE_RATING . ' ' . number_format($pInfo->average_rating, 2) . '%');
						}
					} else { // create category/product info
						$heading[] = array('text' => '<strong>' . EMPTY_CATEGORY . '</strong>');

						$contents[] = array('text' => TEXT_NO_CHILD_CATEGORIES_OR_PRODUCTS);
					}
					break;
			}

			if ((tep_not_null($heading)) && (tep_not_null($contents))) {
				echo '            <td width="25%" valign="top">' . "\n";

				$box = new box;
				echo $box->infoBox($heading, $contents);

				echo '            </td>' . "\n";
			}
			?>
		</tr>
		<tr>
			<td class="smallText" valign="top"><?php
				echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_FEATURED);
				$param = (!empty($search)) ? 'search=' . $search : 'cPath=' . $cPath;
				?></td>
			<td class="smallText" align="right"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], $param); ?></td>
		</tr>
	</table></td>
	</tr>
	</table>
	<?php
}
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>