<?php

if (!function_exists('tep_get_uploaded_file')) {

	function tep_get_uploaded_file($filename) {
		if (isset($_FILES[$filename])) {
			$uploaded_file = array('name' => $_FILES[$filename]['name'],
				'type' => $_FILES[$filename]['type'],
				'size' => $_FILES[$filename]['size'],
				'tmp_name' => $_FILES[$filename]['tmp_name']);
		} elseif (isset($_FILES[$filename])) {
			$uploaded_file = array('name' => $_FILES[$filename]['name'],
				'type' => $_FILES[$filename]['type'],
				'size' => $_FILES[$filename]['size'],
				'tmp_name' => $_FILES[$filename]['tmp_name']);
		} else {
			$uploaded_file = array('name' => $GLOBALS[$filename . '_name'],
				'type' => $GLOBALS[$filename . '_type'],
				'size' => $GLOBALS[$filename . '_size'],
				'tmp_name' => $GLOBALS[$filename]);
		}
		return $uploaded_file;
	}

}
if (!function_exists('tep_copy_uploaded_file')) {

	function tep_copy_uploaded_file($filename, $target) {
		if (!is_dir($target)) {
			mkdir($target, 0755);
		}
		if (substr($target, -1) != '/')
			$target .= '/';
		$target .= $filename['name'];
		move_uploaded_file($filename['tmp_name'], $target);
	}

}

function parse_img_list($fullPathToFile) {
	if (!is_file($fullPathToFile)) {
		tep_redirect(tep_href_link('easypopulate.php?' . http_build_query(array('errors' => 'no file was selected'), '', '&amp;')));
	}
	$handle = fopen($fullPathToFile, 'r');

	while (($row = fgetcsv($handle)) !== FALSE) {
		if (is_file(EP_TEMP_DIRECTORY . $row[1])) {
			rename(EP_TEMP_DIRECTORY . $row[1], IMAGES_DIRECTORY . $row[1]);
			tep_db_query("UPDATE `" . TABLE_PRODUCTS . "`  SET products_image = '" . $row[1] . "' where products_model='" . $row[0] . "'  ");
		}
	}
	fclose($handle);
}

if (!function_exists('easy_import_parse_csv')) {

	function easy_import_parse_csv_to_array($fullPathToFile) {
		if (!is_file($fullPathToFile)) {
			tep_redirect(tep_href_link('easypopulate.php?' . http_build_query(array('errors' => 'no file was selected'), '', '&amp;')));
		}
		$handle = fopen($fullPathToFile, 'r');
		$products = array();
		$products_authors = array();
		$products_translators = array();
		$products_countries = array();
		$products_publishers = array();
		$authors = array();
		$countries = array();
		$publishers = array();
		$colNames = fgetcsv($handle); // get firset line
		if (FALSE === $colNames) {
			return FALSE; // file is empty
		}
		$colNames = array('products_id', 'products_model', 'products_name', 'products_sub_title', 'products_description', 'products_quantity',
			'products_isbn', 'products_edition', 'products_price', 'products_discount',
			'products_date_released', 'products_size', 'products_weight', 'products_page_nb', 'products_cover_type', 'products_series', 'products_nb_in_series', 'product_available_volumes',
			'publisher_ids', 'publisher_names', 'author_ids', 'author_names', 'translator_ids',
			'translator_names', 'countryIds', 'countryNames', 'categories_id_1', 'categories_name_1', 'categories_id_2', 'categories_name_2',
			'categories_id_3', 'categories_name_3');

		while (($row = fgetcsv($handle)) !== FALSE) {

			$products[$row[0]] = array_combine($colNames, $row);
			if (!empty($row[18])) {
				$ids = explode('|', $row[18]);
				foreach ($ids as $id) {
					$products_publishers[] = array('products_model' => $row[1], 'publishers_id' => $id);
				}
				$publishers += array_combine($ids, explode('|', $row[19]));
			}
			if (!empty($row[20])) {
				$ids = explode('|', $row[20]);
				foreach ($ids as $id) {
					$products_authors[] = array('products_model' => $row[1], 'manufacturers_id' => $id);
				}
				$authors += array_combine($ids, explode('|', $row[21]));
			}
			if (!empty($row[22])) {
				$ids = explode('|', $row[22]);
				foreach ($ids as $id) {
					$products_translators[] = array('products_model' => $row[1], 'manufacturers_id' => $id);
				}
				$authors += array_combine($ids, explode('|', $row[23]));
			}
			if (!empty($row[24])) {
				$ids = explode('|', $row[24]);
				foreach ($ids as $id) {
					$products_countries[] = array('products_model' => $row[1], 'countries_id' => $id);
				}
				$countries += array_combine($ids, explode('|', $row[25]));
			}
		}
		fclose($handle);
		if (0 < count($products)) {
			ksort($products);
			ksort($publishers);
			ksort($authors);
			ksort($countries);
			return compact('products', 'publishers', 'authors', 'countries', 'products_publishers', 'products_authors', 'products_translators', 'products_countries');
		}
		return array();
	}

}

if (!function_exists('eaim_create_tables')) {

	function eaim_create_tables() {
		$sqlProducts = "CREATE TABLE IF NOT EXISTS `eaim_" . TABLE_PRODUCTS . "` (
  `products_id` int(10) unsigned NOT NULL PRIMARY KEY,
  `products_model` int(10) unsigned NOT NULL,
  `products_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `products_sub_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `products_description` text CHARACTER SET utf8 NOT NULL,
  `products_quantity` int(4) NOT NULL,
  `products_isbn` char(13) CHARACTER SET utf8 NOT NULL,
  `products_edition` varchar(25) CHARACTER SET utf8 DEFAULT NULL,

  `products_price` decimal(15,4) NOT NULL,
  `products_discount` int(4) unsigned DEFAULT '0',
  `products_date_released` datetime DEFAULT NULL,
  `products_size` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_weight` int(4) unsigned NOT NULL,
  `products_page_nb` int(11) DEFAULT NULL,
  `products_cover_type` enum('artistic','normal') COLLATE utf8_unicode_ci DEFAULT NULL,
  `products_series` varchar(255) CHARACTER SET utf8 NOT NULL,
  `products_nb_in_series` varchar(50) CHARACTER SET utf8 NOT NULL,
  `product_available_volumes` varchar(100) CHARACTER SET utf8 NOT NULL,
  `publisher_ids` int(11) DEFAULT NULL,
  `publisher_names` varchar(64) CHARACTER SET utf8 NOT NULL,
  `author_ids` int(11) DEFAULT NULL,
  `author_names` varchar(64) CHARACTER SET utf8 NOT NULL,
  `translator_ids` int(11) DEFAULT NULL,
  `translator_names` varchar(64) CHARACTER SET utf8 NOT NULL,
  `categories_id_1` int(11) unsigned NOT NULL,
  `categories_name_1` varchar(64) CHARACTER SET utf8 NOT NULL,
  `categories_id_2` int(11) unsigned NOT NULL,
  `categories_name_2` varchar(64) CHARACTER SET utf8 NOT NULL,
  `categories_id_3` int(11) unsigned NOT NULL,
  `categories_name_3` varchar(64) CHARACTER SET utf8 NOT NULL,
  INDEX `idx_eaim_" . TABLE_PRODUCTS . "_categories_id_1` (`categories_id_1`), 
  INDEX `idx_eaim_" . TABLE_PRODUCTS . "_categories_name_1` (`categories_name_1`), 
  INDEX `idx_eaim_" . TABLE_PRODUCTS . "_categories_id_2` (`categories_id_2`), 
  INDEX `idx_eaim_" . TABLE_PRODUCTS . "_categories_name_2` (`categories_name_2`), 
  INDEX `idx_eaim_" . TABLE_PRODUCTS . "_categories_id_3` (`categories_id_3`), 
  INDEX `idx_eaim_" . TABLE_PRODUCTS . "_categories_name_3` (`categories_name_3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$sqlAuthors = "CREATE TABLE IF NOT EXISTS `eaim_" . TABLE_MANUFACTURERS . "` (
  `manufacturers_id` int(10) unsigned NOT NULL PRIMARY KEY, 
  `manufacturers_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  INDEX `idx_eaim_" . TABLE_MANUFACTURERS . "_name` (`manufacturers_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$sqlPublishers = "CREATE TABLE IF NOT EXISTS `eaim_" . TABLE_PUBLISHERS . "` (
  `publishers_id` int(10) unsigned NOT NULL PRIMARY KEY, 
  `publishers_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  INDEX `idx_eaim_" . TABLE_PUBLISHERS . "_name` (`publishers_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

		$sqlCountries = "CREATE TABLE IF NOT EXISTS `eaim_" . TABLE_COUNTRIES . "` (
  `countries_id` int(10) unsigned NOT NULL PRIMARY KEY, 
  `countries_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
		tep_db_query($sqlProducts);
		tep_db_query($sqlAuthors);
		tep_db_query($sqlPublishers);
		tep_db_query($sqlCountries);
	}

}

if (!function_exists('eaim_analyze_tables')) {

	function eaim_analyze_tables() {
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS_DESCRIPTION . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_CATEGORIES . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_CATEGORIES_DESCRIPTION . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS_TO_CATEGORIES . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PUBLISHERS . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS_TO_PUBLISHERS . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_COUNTRIES . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS_TO_COUNTRIES . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_MANUFACTURERS . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS_TO_TRANSLATORS . "`;");
		tep_db_query("ANALYZE TABLE `eaim_" . TABLE_PRODUCTS_TO_AUTHORS . "`;");
	}

}

if (!function_exists('eaim_analyze_os_tables')) {

	function eaim_analyze_os_tables() {
		tep_db_query("ANALYZE TABLE `" . TABLE_PRODUCTS . "`;");
		tep_db_query("ANALYZE TABLE `" . TABLE_MANUFACTURERS . "`;");
		tep_db_query("ANALYZE TABLE `" . TABLE_PUBLISHERS . "`;");
		tep_db_query("ANALYZE TABLE `" . TABLE_COUNTRIES_OF_PRODUCTS . "`;");
	}

}

if (!function_exists('eaim_truncate_tables')) {

	function eaim_truncate_tables() {
		tep_db_query("TRUNCATE `eaim_" . TABLE_PRODUCTS . "`;");
		tep_db_query("TRUNCATE `eaim_" . TABLE_MANUFACTURERS . "`;");
		tep_db_query("TRUNCATE `eaim_" . TABLE_PUBLISHERS . "`;");
		tep_db_query("TRUNCATE `eaim_" . TABLE_COUNTRIES . "`;");
	}

}

if (!function_exists('eaim_escape_values')) {

	function eaim_escape_values(&$item, $key) {
		if (empty($item) && "0" !== $item) {
			$item = 'NULL';
		} elseif (!is_numeric($item)) {
			$item = "'" . addslashes($item) . "'";
		}
	}

}

if (!function_exists('eaim_insert_parsed_data')) {

	function eaim_insert_parsed_data($data) {
		if (array_key_exists('products', $data) && count($data['products'])) {
			reset($data['products']);
			ksort($data['products']);
			$colNames = array_keys(current($data['products']));
			$sql = "INSERT INTO `eaim_" . TABLE_PRODUCTS . "` (`" . implode("`, `", $colNames) . "`) VALUES";
			$values = "";
			$r = 0;
			foreach ($data['products'] as $product) {
				array_walk($product, 'eaim_escape_values');
				$values .= ", (" . implode(", ", $product) . ")";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$values = "";
			}
		}
		if (array_key_exists('authors', $data) && count($data['authors'])) {
			reset($data['authors']);
			ksort($data['authors']);
			$sql = "INSERT INTO `eaim_" . TABLE_MANUFACTURERS . "` VALUES";
			$values = "";
			$r = 0;
			foreach ($data['authors'] as $id => $name) {
				$values .= ", ('" . addslashes($id) . "', '" . addslashes($name) . "')";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$values = "";
			}
		}
		if (array_key_exists('publishers', $data) && count($data['publishers'])) {
			reset($data['publishers']);
			ksort($data['publishers']);
			$sql = "INSERT INTO `eaim_" . TABLE_PUBLISHERS . "` VALUES";
			$values = "";
			$r = 0;
			foreach ($data['publishers'] as $id => $name) {
				$values .= ", ('" . addslashes($id) . "', '" . addslashes($name) . "')";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$values = "";
			}
		}
		if (array_key_exists('countries', $data) && count($data['publishers'])) {
			reset($data['countries']);
			ksort($data['countries']);
			$sql = "INSERT INTO `eaim_" . TABLE_COUNTRIES . "` VALUES";
			$values = "";
			$r = 0;
			foreach ($data['countries'] as $id => $name) {
				$values .= ", ('" . addslashes($id) . "', '" . addslashes($name) . "')";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$values = "";
			}
		}
	}

}

if (!function_exists('eaim_update_manufacturers')) {

	function eaim_update_manufacturers() {
		tep_db_query("UPDATE `" . TABLE_MANUFACTURERS . "` AS m INNER JOIN `eaim_" . TABLE_MANUFACTURERS . "` AS em ON em.manufacturers_id = m.manufacturers_id
SET m.manufacturers_name = em.manufacturers_name,m.manufacturers_search_words = REPLACE(REPLACE(em.manufacturers_name, 'أ', 'ا'),'إ','ا'), m.last_modified = CURRENT_TIMESTAMP");
		return tep_db_affected_rows();
	}

}

if (!function_exists('eaim_add_new_manufacturers')) {

	function eaim_add_new_manufacturers() {
		tep_db_query("INSERT INTO `" . TABLE_MANUFACTURERS . "` (`manufacturers_id`, `manufacturers_name`,`manufacturers_search_words`, `date_added`) 
        (SELECT `manufacturers_id`, `manufacturers_name`,REPLACE(REPLACE(manufacturers_name, 'أ', 'ا'),'إ','ا'), CURRENT_TIMESTAMP FROM `eaim_" . TABLE_MANUFACTURERS . "`
        WHERE `manufacturers_id` NOT IN ( SELECT `manufacturers_id` FROM `" . TABLE_MANUFACTURERS . "` ))");
		return tep_db_affected_rows();
	}

}

if (!function_exists('eaim_update_publishers')) {

	function eaim_update_publishers() {
		tep_db_query("UPDATE `" . TABLE_PUBLISHERS . "` AS p INNER JOIN `eaim_" . TABLE_PUBLISHERS . "` AS ep ON ep.publishers_id = p.publishers_id
SET p.publishers_name = ep.publishers_name, p.last_modified = CURRENT_TIMESTAMP");
		return tep_db_affected_rows();
	}

}

if (!function_exists('eaim_add_new_publishers')) {

	function eaim_add_new_publishers() {
		tep_db_query("INSERT INTO `" . TABLE_PUBLISHERS . "` (`publishers_id`, `publishers_name`, `date_added`) 
        (SELECT `publishers_id`, `publishers_name`, CURRENT_TIMESTAMP FROM `eaim_" . TABLE_PUBLISHERS . "`
        WHERE `publishers_id` NOT IN ( SELECT `publishers_id` FROM `" . TABLE_PUBLISHERS . "` ))");
		return tep_db_affected_rows();
	}

}
if (!function_exists('eaim_update_countries')) {

	function eaim_update_countries() {
		tep_db_query("UPDATE `" . TABLE_COUNTRIES_OF_PRODUCTS . "` AS p INNER JOIN `eaim_" . TABLE_COUNTRIES . "` AS ep ON ep.countries_id = p.countries_id
SET p.countries_name = ep.countries_name, p.last_modified = CURRENT_TIMESTAMP");
		return tep_db_affected_rows();
	}

}

if (!function_exists('eaim_add_new_countries')) {

	function eaim_add_new_countries() {
		tep_db_query("INSERT INTO `" . TABLE_COUNTRIES_OF_PRODUCTS . "` (`countries_id`, `countries_name`, `date_added`) 
        (SELECT `countries_id`, `countries_name`, CURRENT_TIMESTAMP FROM `eaim_" . TABLE_COUNTRIES . "`
        WHERE `countries_id` NOT IN ( SELECT `countries_id` FROM `" . TABLE_COUNTRIES_OF_PRODUCTS . "` ))");
		return tep_db_affected_rows();
	}

}

if (!function_exists('eaim_update_categories')) {

	function eaim_update_categories() {
		// level 1
		tep_db_query("UPDATE `" . TABLE_CATEGORIES . "` AS c INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ec ON ec.categories_id_1 = c.categories_id
SET c.categories_image = NULL, c.parent_id = 0, c.sort_order = 1, c.last_modified = CURRENT_TIMESTAMP
WHERE ec.categories_id_1 IS NOT NULL AND ec.categories_id_1 > 0;");
		tep_db_query("UPDATE `" . TABLE_CATEGORIES_DESCRIPTION . "` AS cd
INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ecd ON ecd.categories_id_1 = cd.categories_id AND cd.language_id = " . (1 * LANGUAGE_ID) . "
SET cd.categories_name = ecd.categories_name_1 WHERE ecd.categories_id_1 IS NOT NULL AND ecd.categories_id_1 > 0;");
		$affected = tep_db_affected_rows();
		// level 2
		tep_db_query("UPDATE `" . TABLE_CATEGORIES . "` AS c INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ec ON ec.categories_id_2 = c.categories_id
SET c.categories_image = NULL, c.parent_id = ec.categories_id_1, c.sort_order = 1, c.last_modified = CURRENT_TIMESTAMP
WHERE ec.categories_id_1 IS NOT NULL AND ec.categories_id_2 IS NOT NULL AND ec.categories_id_1 > 0 AND ec.categories_id_2 > 0;");
		tep_db_query("UPDATE `" . TABLE_CATEGORIES_DESCRIPTION . "` AS cd
INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ecd ON ecd.categories_id_2 = cd.categories_id AND cd.language_id = " . (1 * LANGUAGE_ID) . "
SET cd.categories_name = ecd.categories_name_2 WHERE ecd.categories_id_1 IS NOT NULL AND ecd.categories_id_2 IS NOT NULL
AND ecd.categories_id_1 > 0 AND ecd.categories_id_2 > 0;");
		$affected += tep_db_affected_rows();
		// level 3
		tep_db_query("UPDATE `" . TABLE_CATEGORIES . "` AS c INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ec ON ec.categories_id_3 = c.categories_id
SET c.categories_image = NULL, c.parent_id = ec.categories_id_2, c.sort_order = 1, c.last_modified = CURRENT_TIMESTAMP
WHERE ec.categories_id_1 IS NOT NULL AND ec.categories_id_2 IS NOT NULL AND ec.categories_id_3 IS NOT NULL 
AND ec.categories_id_1 > 0 AND ec.categories_id_2 > 0 AND ec.categories_id_3 > 0;");
		tep_db_query("UPDATE `" . TABLE_CATEGORIES_DESCRIPTION . "` AS cd
INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ecd ON ecd.categories_id_3 = cd.categories_id AND cd.language_id = " . (1 * LANGUAGE_ID) . "
SET cd.categories_name = ecd.categories_name_3 WHERE ecd.categories_id_1 IS NOT NULL AND ecd.categories_id_2 IS NOT NULL AND ecd.categories_id_3 IS NOT NULL 
AND ecd.categories_id_1 > 0 AND ecd.categories_id_2 > 0 AND ecd.categories_id_3 > 0;");
		$affected += tep_db_affected_rows();
		return $affected;
	}

}

if (!function_exists('eaim_add_categories')) {

	function eaim_add_new_categories() {
		// level 1
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES . "` (`categories_id`, `parent_id`, `sort_order`, `date_added`) 
    (SELECT `categories_id_1`, 0, 0, CURRENT_TIMESTAMP FROM `eaim_" . TABLE_PRODUCTS . "`
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_1` NOT IN (SELECT `categories_id` FROM `" . TABLE_CATEGORIES . "`)
    GROUP BY `categories_id_1`);");
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES_DESCRIPTION . "` 
    (`categories_id`, `language_id`, `categories_name`) (SELECT `categories_id_1`, " . (1 * LANGUAGE_ID) . ", `categories_name_1`
    FROM `eaim_" . TABLE_PRODUCTS . "` 
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_1` NOT IN (SELECT `categories_id` FROM `" . TABLE_CATEGORIES_DESCRIPTION . "`)
    GROUP BY `categories_id_1`);");

		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES_DESCRIPTION . "` 
    (`categories_id`, `language_id`, `categories_name`) (SELECT `categories_id_1`, " . (1 * SECOND_LANGUAGE_ID) . ", `categories_name_1`
    FROM `eaim_" . TABLE_PRODUCTS . "` 
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_1` NOT IN (SELECT `categories_id` FROM `" . TABLE_CATEGORIES_DESCRIPTION . "` where language_id=" . (1 * SECOND_LANGUAGE_ID) . ")
    GROUP BY `categories_id_1`);");
		$affected = tep_db_affected_rows();
		// level 2
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES . "` (`categories_id`, `parent_id`, `sort_order`, `date_added`) 
    (SELECT `categories_id_2`, `categories_id_1`, 0, CURRENT_TIMESTAMP FROM `eaim_" . TABLE_PRODUCTS . "`
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 AND `categories_id_2` NOT IN 
	(SELECT `categories_id` FROM `" . TABLE_CATEGORIES . "`)
    GROUP BY `categories_id_2`);");
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES_DESCRIPTION . "` 
    (`categories_id`, `language_id`, `categories_name`) (SELECT `categories_id_2`, " . (1 * LANGUAGE_ID) . ", `categories_name_2`
    FROM `eaim_" . TABLE_PRODUCTS . "` 
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 AND `categories_id_2` NOT IN 
	(SELECT `categories_id` FROM `" . TABLE_CATEGORIES_DESCRIPTION . "`)
    GROUP BY `categories_id_2`);");
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES_DESCRIPTION . "` 
    (`categories_id`, `language_id`, `categories_name`) (SELECT `categories_id_2`, " . (1 * SECOND_LANGUAGE_ID) . ", `categories_name_2`
    FROM `eaim_" . TABLE_PRODUCTS . "` 
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 AND `categories_id_2` NOT IN 
	(SELECT `categories_id` FROM `" . TABLE_CATEGORIES_DESCRIPTION . "` where language_id=" . (1 * SECOND_LANGUAGE_ID) . ")
    GROUP BY `categories_id_2`);");
		$affected += tep_db_affected_rows();
		// level 3
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES . "` (`categories_id`, `parent_id`, `sort_order`, `date_added`) 
    (SELECT `categories_id_3`, `categories_id_2`, 0, CURRENT_TIMESTAMP FROM `eaim_" . TABLE_PRODUCTS . "`
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 
	AND `categories_id_3` IS NOT NULL AND `categories_id_3` > 0 AND `categories_id_3` NOT IN (SELECT `categories_id` FROM `" . TABLE_CATEGORIES . "`)
    GROUP BY `categories_id_3`);");
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES_DESCRIPTION . "` 
    (`categories_id`, `language_id`, `categories_name`) (SELECT `categories_id_3`, " . (1 * LANGUAGE_ID) . ", `categories_name_3`
    FROM `eaim_" . TABLE_PRODUCTS . "` 
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 
	AND `categories_id_3` IS NOT NULL AND `categories_id_3` > 0 AND `categories_id_3` NOT IN (SELECT `categories_id` FROM `" . TABLE_CATEGORIES_DESCRIPTION . "`)
    GROUP BY `categories_id_3`);");
		tep_db_query("INSERT INTO `" . TABLE_CATEGORIES_DESCRIPTION . "` 
    (`categories_id`, `language_id`, `categories_name`) (SELECT `categories_id_3`, " . (1 * SECOND_LANGUAGE_ID) . ", `categories_name_3`
    FROM `eaim_" . TABLE_PRODUCTS . "` 
    WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 
	AND `categories_id_3` IS NOT NULL AND `categories_id_3` > 0 AND `categories_id_3` NOT IN (SELECT `categories_id` FROM `" . TABLE_CATEGORIES_DESCRIPTION . "` where language_id=" . (1 * SECOND_LANGUAGE_ID) . ")
    GROUP BY `categories_id_3`);");
		$affected += tep_db_affected_rows();

		tep_db_query("SET @myDispOrder = 0;");
		tep_db_query("UPDATE `" . TABLE_CATEGORIES . "` c INNER JOIN (
        SELECT cc.`categories_id` FROM `" . TABLE_CATEGORIES . "` cc INNER JOIN `" . TABLE_CATEGORIES_DESCRIPTION . "` cd ON cd.`categories_id` = cc.`categories_id`
			WHERE cc.`parent_id` = 0 
			ORDER BY cd.`categories_name` ASC
		) AS `ccc` ON `ccc`.`categories_id` = `c`.`categories_id`
        SET c.`sort_order` = @myDispOrder:= @myDispOrder + 1;");
		return $affected;
	}

}

if (!function_exists('eaim_update_products')) {

	function eaim_update_products() {
		tep_db_query("UPDATE `" . TABLE_PRODUCTS . "` AS p INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ep ON ep.products_model = p.products_model
		SET
			p.products_quantity = ep.products_quantity,
			p.products_isbn = ep.products_isbn,
			p.products_edition = ep.products_edition,
			p.products_price = ep.products_price,
			p.products_status = 1,
			p.products_discount = ep.products_discount,
			p.products_date_released = ep.products_date_released,
			p.products_size = ep.products_size,
			p.products_weight = ep.products_weight,
			p.products_page_nb = ep.products_page_nb,
			p.products_cover_type = ep.products_cover_type,
			p.products_series = ep.products_series,
			p.products_nb_in_series = ep.products_nb_in_series,
			p.products_last_modified = '" . date('Y-m-d H:i:s') . "',
			p.products_available = 1,
			p.product_available_volumes = ep.product_available_volumes
			;");
		tep_db_query("UPDATE `" . TABLE_PRODUCTS_DESCRIPTION . "` AS p "
				. "INNER JOIN `" . TABLE_PRODUCTS . "` pr on  pr.products_id=p.products_id INNER JOIN `eaim_" . TABLE_PRODUCTS . "` as ep ON ep.products_model = pr.products_model
		SET
			p.products_name = ep.products_name,
			p.products_search_words = CONCAT (REPLACE(REPLACE(ep.products_name, 'أ', 'ا'),'إ','ا'),' ',REPLACE(REPLACE( COALESCE( ep.products_sub_title,  '' ), 'أ', 'ا'),'إ','ا')),
			p.products_sub_title = ep.products_sub_title,
			p.products_description = ep.products_description
		WHERE p.language_id = " . (1 * LANGUAGE_ID));
		return tep_db_affected_rows();
	}

}
if (!function_exists('eaim_archive_not_imported_products')) {

	function eaim_archive_not_imported_products() {
		tep_db_query("UPDATE `" . TABLE_PRODUCTS . "` AS p 
		SET p.products_available = 0 where p.products_model NOT IN ( 
		SELECT products_model FROM (
		(SELECT products_model FROM `eaim_" . TABLE_PRODUCTS . "`) UNION  (SELECT products_model FROM `" . TABLE_PRODUCTS_TO_CATEGORIES . "` p INNER JOIN `" . TABLE_PRODUCTS . "` pr on  pr.products_id=p.products_id "
				. " WHERE categories_id >='" . (int) POSTERS_CATEGORY_ID . "' OR categories_id ='" . (int) MAGAZINES_CATEGORY_ID . "')"
				. ") As `All` "
				. " )");
		return tep_db_affected_rows();
	}

}

if (!function_exists('eaim_add_new_products')) {

	function eaim_add_new_products() {
		tep_db_query("INSERT INTO `" . TABLE_PRODUCTS . "` (
			`products_id`, `products_model`, `products_quantity`, `products_isbn`, `products_edition`, `products_image`, `products_price`, `products_discount`, 
			`products_date_added`,`products_last_modified`, `products_date_available`, `products_date_released`, `products_size`, `products_weight`, `products_page_nb`, `products_cover_type`, `products_series`, `products_nb_in_series`, `product_available_volumes`, `products_status`, 
			`products_ordered`) 
			(SELECT 
			NULL AS `products_id`, `products_model`, `products_quantity`, `products_isbn`, `products_edition`, '' as `products_image`, `products_price`, `products_discount`, 
			CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,CURRENT_TIMESTAMP, `products_date_released`, `products_size`, `products_weight`, `products_page_nb`, `products_cover_type`, `products_series`, `products_nb_in_series`, `product_available_volumes`, 1, 0
			FROM `eaim_" . TABLE_PRODUCTS . "` WHERE `products_model` NOT IN (SELECT `products_model` FROM `" . TABLE_PRODUCTS . "` ORDER BY `products_model` ASC));");

		tep_db_query("INSERT INTO `" . TABLE_PRODUCTS_DESCRIPTION . "` (`products_id`, `language_id`, `products_name`,`products_search_words`,`products_sub_title`, `products_description`)
			(SELECT p.products_id, " . (1 * LANGUAGE_ID) . ", `products_name`,CONCAT (REPLACE(REPLACE(products_name, 'أ', 'ا'),'إ','ا'),' ',REPLACE(REPLACE( COALESCE( products_sub_title,  '' ), 'أ', 'ا'),'إ','ا')),`products_sub_title`, `products_description` 
			  FROM `eaim_" . TABLE_PRODUCTS . "`ea_p   INNER JOIN  " . TABLE_PRODUCTS . " p on  ea_p.products_model=p.products_model "
				. "WHERE p.products_id NOT IN (SELECT `products_id` FROM `" . TABLE_PRODUCTS_DESCRIPTION . "` ORDER BY `products_id` ASC));");
		$affected = tep_db_affected_rows();

		tep_db_query("DELETE FROM `" . TABLE_PRODUCTS_TO_CATEGORIES . "` WHERE `products_id` IN (SELECT P.`products_id` FROM `eaim_" . TABLE_PRODUCTS . "` ea_p INNER JOIN " . TABLE_PRODUCTS . " P on  ea_p.products_model=P.products_model);");
		tep_db_query("INSERT INTO `" . TABLE_PRODUCTS_TO_CATEGORIES . "`(`products_id`, `categories_id`) 
SELECT `products_id`, `categories_id_1` FROM (SELECT P.products_id, `categories_id_1` FROM `eaim_" . TABLE_PRODUCTS . "` ea_p INNER JOIN " . TABLE_PRODUCTS . " P on  ea_p.products_model=P.products_model WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0
UNION ALL
SELECT P2.products_id, `categories_id_2` FROM `eaim_" . TABLE_PRODUCTS . "` ea_p2 INNER JOIN " . TABLE_PRODUCTS . " P2 on  ea_p2.products_model=P2.products_model WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0
UNION ALL
SELECT P3.products_id, `categories_id_3` FROM `eaim_" . TABLE_PRODUCTS . "` ea_p3 INNER JOIN " . TABLE_PRODUCTS . " P3 on  ea_p3.products_model=P3.products_model WHERE `categories_id_1` IS NOT NULL AND `categories_id_1` > 0 AND `categories_id_2` IS NOT NULL AND `categories_id_2` > 0 AND `categories_id_3` IS NOT NULL AND `categories_id_3` > 0) AS `source_ids1`;");
		return $affected;
	}

}

if (!function_exists('eaim_add_update_products_publishers')) {

	function eaim_add_update_products_publishers($data) {
		$affected = 0;
		if (count($data)) {
			tep_db_query("DELETE FROM `" . TABLE_PRODUCTS_TO_PUBLISHERS . "` WHERE `products_id` IN ("
					. "SELECT P.products_id FROM `eaim_" . TABLE_PRODUCTS . "`  ea_p INNER JOIN " . TABLE_PRODUCTS . " P on  ea_p.products_model=P.products_model);");
			reset($data);
			ksort($data);
			$colNames = array_keys(current($data));
			$sql = "INSERT INTO `" . TABLE_PRODUCTS_TO_PUBLISHERS . "` (`products_id`, `publishers_id`) VALUES";
			$values = "";
			$r = 0;
			foreach ($data as $row) {
				//bring products_id 
				$val = tep_db_fetch_array(tep_db_query("select products_id from " . TABLE_PRODUCTS . " where products_model = '" . intval($row['products_model']) . "' "));
				$values .= ", (" . $val['products_id'] . "," . $row['publishers_id'] . ")";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$affected += tep_db_affected_rows();
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$affected += tep_db_affected_rows();
				$values = "";
			}
		}
		return $affected;
	}

}
if (!function_exists('eaim_add_update_products_countries')) {

	function eaim_add_update_products_countries($data) {
		$affected = 0;
		if (count($data)) {
			tep_db_query("DELETE FROM `" . TABLE_PRODUCTS_TO_COUNTRIES . "` WHERE `products_id` IN ("
					. "SELECT P.products_id FROM `eaim_" . TABLE_PRODUCTS . "`  ea_p INNER JOIN " . TABLE_PRODUCTS . " P on  ea_p.products_model=P.products_model);");
			reset($data);
			ksort($data);
			$colNames = array_keys(current($data));
			$sql = "INSERT INTO `" . TABLE_PRODUCTS_TO_COUNTRIES . "` (`products_id`, `countries_id`)  VALUES";
			$values = "";
			$r = 0;
			foreach ($data as $row) {
				$val = tep_db_fetch_array(tep_db_query("select products_id from " . TABLE_PRODUCTS . " where products_model = '" . intval($row['products_model']) . "' "));
				$values .= ", (" . $val['products_id'] . "," . $row['countries_id'] . ")";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$affected += tep_db_affected_rows();
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$affected += tep_db_affected_rows();
				$values = "";
			}
		}
		return $affected;
	}

}

if (!function_exists('eaim_add_update_products_translators')) {

	function eaim_add_update_products_translators($data) {
		$affected = 0;
		if (count($data)) {
			tep_db_query("DELETE FROM `" . TABLE_PRODUCTS_TO_TRANSLATORS . "` WHERE `products_id` IN ("
					. "SELECT P.products_id FROM `eaim_" . TABLE_PRODUCTS . "`  ea_p INNER JOIN " . TABLE_PRODUCTS . " P on  ea_p.products_model=P.products_model);");
			reset($data);
			ksort($data);
			$colNames = array_keys(current($data));
			$sql = "INSERT INTO `" . TABLE_PRODUCTS_TO_TRANSLATORS . "` (`products_id`, `manufacturers_id`) VALUES";
			$values = "";
			$r = 0;
			foreach ($data as $row) {
				$val = tep_db_fetch_array(tep_db_query("select products_id from " . TABLE_PRODUCTS . " where products_model = '" . intval($row['products_model']) . "' "));
				$values .= ", (" . $val['products_id'] . "," . $row['manufacturers_id'] . ")";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$affected += tep_db_affected_rows();
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$affected += tep_db_affected_rows();
				$values = "";
			}
		}
		return $affected;
	}

}

if (!function_exists('eaim_add_update_products_authors')) {

	function eaim_add_update_products_authors($data) {
		$affected = 0;
		if (count($data)) {
			tep_db_query("DELETE FROM `" . TABLE_PRODUCTS_TO_AUTHORS . "` WHERE `products_id` IN ("
					. "SELECT P.products_id FROM `eaim_" . TABLE_PRODUCTS . "`  ea_p INNER JOIN " . TABLE_PRODUCTS . " P on  ea_p.products_model=P.products_model);");
			reset($data);
			ksort($data);
			$colNames = array_keys(current($data));
			$sql = "INSERT INTO `" . TABLE_PRODUCTS_TO_AUTHORS . "` (`products_id`, `manufacturers_id`)VALUES";
			$values = "";
			$r = 0;
			foreach ($data as $row) {
				$val = tep_db_fetch_array(tep_db_query("select products_id from " . TABLE_PRODUCTS . " where products_model = '" . intval($row['products_model']) . "' "));
				$values .= ", (" . $val['products_id'] . "," . $row['manufacturers_id'] . ")";
				$r++;
				if (0 === $r % 500) {
					tep_db_query($sql . substr($values, 1));
					$affected += tep_db_affected_rows();
					$values = "";
				}
			}
			if (!empty($values)) {
				tep_db_query($sql . substr($values, 1));
				$affected += tep_db_affected_rows();
				$values = "";
			}
		}
		return $affected;
	}

}