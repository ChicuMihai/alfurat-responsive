<?php
/*
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License

  Featured Products admin
 */

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
require(DIR_WS_INCLUDES . 'template_top.php');

function tep_check_featured_exist($products_id) {
	return tep_db_num_rows(tep_db_query("select * from " . TABLE_FEATURED . " where products_id = '" . $products_id . "'"));
}

switch ($_GET['action']) {
	case 'insert':
		$expires_date = '';
		if ($_POST['day'] && $_POST['month'] && $_POST['year']) {
			$expires_date = $_POST['year'];
			$expires_date .= (strlen($_POST['month']) == 1) ? '0' . $_POST['month'] : $_POST['month'];
			$expires_date .= (strlen($_POST['day']) == 1) ? '0' . $_POST['day'] : $_POST['day'];
		}
		if (tep_check_featured_exist($_GET['products_id']) == 0) {
			tep_db_query("insert into " . TABLE_FEATURED . " (products_id, featured_date_added, expires_date, status) values ('" . $_GET['products_id'] . "', now(), '" . $expires_date . "', '1')");
			$messageStack->reset();
			$messageStack->add(TEXT_FEATURED_PRODUCT_ADDED . ' with Product Code: ' . $_GET['products_id'], 'success');
			echo $messageStack->output();
		}

		break;
}
?>

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
	<tr>
		<td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
								<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
							</tr>
						</table></td>
				</tr>

				<tr>
					<td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class='main'  align='right'>
									<?php echo'<a target="_blank" class="ui-button ui-widget ui-state-highlight ui-priority-primary"  style="padding:10px;" href="' . tep_href_link(FILENAME_FEATURED) . '">' . 'Featured Products List' . '</a>'; ?>	
								</td>
							</tr>
							<tr>
								<td class="main"><br/>

									<?php echo tep_draw_form('search_featured', FILENAME_SELECT_FEATURED, '', 'post'); ?>
									<table border='0'>
										<tr>
											<td class='main'><?php echo '<span class="cat-select">', TEXT_FEATURED_PRODUCT, '</span>'; ?></td>
											<td class='main'><?php echo tep_draw_input_field('products_name', '', 'class="cat-select"') ?></td>
										</tr>

										<tr>
											<td class='main'><?php echo '<span class="cat-select">', TEXT_FEATURED_MODEL, '</span>'; ?></td>
											<td class='main'><?php echo tep_draw_input_field('products_model', '', 'class="cat-select"') ?></td>
										</tr>

										<tr>
											<td class='main' colspan='2' align='right'><input type='submit' class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary ui-priority-primary" value='<?php echo SUBMIT_SEARCH; ?>'/></td>
										</tr>

									</table>
									</form>

									<?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?>

									<table border="0" width="100%" cellspacing="0" cellpadding="2">
										<tr class="dataTableHeadingRow">
											<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
											<td class="dataTableHeadingContent" align="left"><?php echo TABLE_HEADING_MODEL; ?></td>
											<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRODUCTS_PRICE; ?></td>
											<td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MANUFACTURERS; ?></td>
											<td class="dataTableHeadingContent" align="center"><?php echo TEXT_PRODUCTS_AUTHORS; ?></td>
											<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?></td>
											<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?></td>
										</tr>

										<?php
										$name = (isset($HTTP_GET_VARS['products_name'])) ? $HTTP_GET_VARS['products_name'] : $HTTP_POST_VARS['products_name'];
										$model = (isset($HTTP_GET_VARS['products_model'])) ? $HTTP_GET_VARS['products_model'] : $HTTP_POST_VARS['products_model'];

										if (tep_not_null($name) || tep_not_null($model)) {

											$sql = "select d.products_id, d.products_name, p.products_model, p.products_price, p.products_status ,GROUP_CONCAT(DISTINCT pub.publishers_name  SEPARATOR ',')  as publishers, GROUP_CONCAT(DISTINCT author.manufacturers_name  SEPARATOR ',')  as authors  from  " . TABLE_PRODUCTS . " p   "
											;
											$sql.= " inner join " . TABLE_PRODUCTS_DESCRIPTION . " d on p.products_id = d.products_id";
											$sql .= "  left join " . TABLE_PRODUCTS_TO_PUBLISHERS . " a on  p.products_id = a.products_id"
													. "  left join " . TABLE_PUBLISHERS . " pub on a.publishers_id = pub.publishers_id"
													. "  left join " . TABLE_PRODUCTS_TO_AUTHORS . " auth on p.products_id = auth.products_id "
													. "  left join " . TABLE_MANUFACTURERS . " author on author.manufacturers_id = auth.manufacturers_id ";
											$sql.=" where d.language_id = '" . (int) $languages_id . "'  and p.products_id NOT IN (select products_id from " . TABLE_FEATURED . " ) ";
											if (tep_not_null($name))
												$sql .= " and d.products_name like '%" . tep_db_prepare_input($name) . "%'";
											if (tep_not_null($model))
												$sql .= " and p.products_model like '%" . tep_db_prepare_input($model) . "%'"; 
												$sql .= " group by p.products_id "; 
											$search_query = tep_db_query($sql);
											while ($result = tep_db_fetch_array($search_query)) {
												if ($result['products_status'] == '1')
													$icon = tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', '', 10, 10) . '&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', '', 10, 10);
												else
													$icon = tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', '', 10, 10) . '&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
												?>
												<tr class="dataTableRow formAreaTitle" >
													<td  class="dataTableContent arabicTd">
													<a href="<?php echo tep_href_link(FILENAME_SELECT_FEATURED, '&products_id=' . $result['products_id'] . '&action=insert&products_name=' . $name . '&products_model=' . $model); ?>"><?php echo $result['products_name']; ?></a>
													</td>
													<td  class="dataTableContent" align="left"><?php echo $result['products_model']; ?></td>
													<td  class="dataTableContent" align="right"><?php echo $currencies->format($result['products_price']); ?></td>
													<td class="dataTableContent arabicTd" align="right"><?php echo $result['publishers']; ?></td>
													<td class="dataTableContent arabicTd" align="right"><?php echo $result['authors']; ?></td>
													<td  class="dataTableContent" align="right"><?php echo $icon; ?></td>
													<td  class="dataTableContent" align="right"><a href="<?php echo tep_href_link(FILENAME_SELECT_FEATURED, '&products_id=' . $result['products_id'] . '&action=insert&products_name=' . $name . '&products_model=' . $model); ?>"><?php echo tep_draw_button('Save'); ?></a></td>
												</tr>
												<?php
											}
										}
										?>

									</table>


								</td>
								<td class="main" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
							</tr>
						</table></td>
				</tr>
			</table>

		</td>
		<!-- body_text_eof //-->
	</tr>
</table>
<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>