<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_FEATURED_PRODUCTS);

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_FEATURED_PRODUCTS));

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<div class="boxCatTitle"><?php echo HEADING_TITLE; ?></div>

<div class="contentContainer">
	<div class="contentText">
		<?php
		$featured_products_array = array();
		$featured_products_query_raw = "select p2c.categories_id,p.products_available,p.products_id, pd.products_name, p.products_image, p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, p.products_date_added,p.products_date_released,p.products_isbn, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids
   from " . TABLE_PRODUCTS . " p 
	   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p2c.products_id = p.products_id
   left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.products_id = pd.products_id and pd.language_id = '" . $languages_id . "'
   left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id
  		 left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id
				 left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id  
   where p.products_status = '1'   
and YEAR(p.products_date_released)>='" . date("Y") . "'   
	and DATE_ADD( products_last_modified, INTERVAL 60
DAY ) >='" . date('Y-m-d H:i:s') . "' and p2c.categories_id < '" . POSTERS_CATEGORY_ID . "' group by p.products_id 
order by p.products_date_added  DESC ,p.products_date_released  DESC, pd.products_name";

		$featured_products_split = new splitPageResults($featured_products_query_raw, MAX_DISPLAY_FEATURED_PRODUCTS);
		if (($featured_products_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3'))) {
			?>

			<div>
				<span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $featured_products_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

				<span><?php echo $featured_products_split->display_count(TEXT_DISPLAY_NUMBER_OF_FEATURED_PRODUCTS); ?></span>
			</div>

			<br />

			<?php
		}
		?>

		<?php
		if ($featured_products_split->number_of_rows > 0) {
			?>

			<table border="0" width="100%" cellspacing="0" cellpadding="5">

				<?php
				$count = 0;
				$featured_query = tep_db_query($featured_products_split->sql_query);
				while ($featured = tep_db_fetch_array($featured_query)) {
					$count++;
					if ($new_price = tep_get_products_special_price($featured['products_id'])) {
						$products_price = '<del>' . $currencies->display_price($featured['products_price'], tep_get_tax_rate($featured['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($featured['products_tax_class_id'])) . '</span>';
					} else {
						$products_price = $currencies->display_price($featured['products_price'], tep_get_tax_rate($featured['products_tax_class_id']));
					}
					//	$publishers = tep_db_fetch_all(tep_db_query('SELECT `publishers_name` FROM ' . TABLE_PUBLISHERS . " M inner join " . TABLE_PRODUCTS_TO_PUBLISHERS . " P on  P.publishers_id= M.publishers_id WHERE products_id = '" . $featured['products_id'] . "'"));

					($count % 2 == 0) ? $bg = '#eee' : $bg = '#FFF'
					?>
					<tr bgcolor="<?php echo $bg; ?>">
						<td width="<?php echo SMALL_IMAGE_WIDTH + 10; ?>" valign="top" class="main"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $featured['products_id']) . '">' . tep_image(DIR_WS_IMAGES . $featured['products_image'], $featured['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a>'; ?></td>
						<td valign="top" class="main"><?php
							echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $featured['products_id']) . '"><strong>' . $featured['products_name'] . '</strong></a><br /><p>';
							if (!empty($featured['author'])) {
								echo '<span class="green">' . TEXT_AUTHOR . '</span>';
								echo tep_get_authors_with_links($featured['author'], $featured['author_ids']);
//								foreach ($publishers as $key => $value) {
//									echo $value['publishers_name'];
//									if (isset($publishers[$key + 1]))
//										echo',';
//								}
								echo'<br />';
							}
							if (!empty($featured['products_date_released']))
								echo '<span class="green">' . TEXT_RELEASED . '</span> ' . tep_date_get_year($featured['products_date_released']) . '<br />';
							if (!empty($featured['products_isbn']))
								echo '<p class="dir-ltr" style="text-align:right;">' . $featured['products_isbn'] . '<span class="green">:' . TEXT_ISBN . '</span> </p>';
							echo '<br /><br />' . TEXT_PRICE . ' ' . $products_price;
							?></p></td>
						<td align="right" valign="middle" class="smallText" width="100"><?php
							if ($featured['products_available'] * 1 ==1) {
								echo tep_draw_button(IMAGE_BUTTON_IN_CART, 'cart', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action')) . 'action=buy_now&products_id=' . $featured['products_id']));
							}
							?></td>
					</tr>
					<?php
				}
				?>

			</table>

			<?php
		} else {
			?>

			<div>
	<?php echo TEXT_NO_NEW_PRODUCTS; ?>
			</div>

			<?php
		}

		if (($featured_products_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
			?>

			<br />

			<div>
				<span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $featured_products_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

				<span><?php echo $featured_products_split->display_count(TEXT_DISPLAY_NUMBER_OF_FEATURED_PRODUCTS); ?></span>
			</div>
			<br>

			<?php
		}
		?>

	</div>
</div>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>