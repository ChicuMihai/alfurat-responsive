<?php

/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

$cl_box_groups[] = array(
	'heading' => BOX_HEADING_METADATA,
	'apps' => array(
		array(
			'code' => FILENAME_PUBLISHERS,
			'title' => BOX_CATALOG_MANUFACTURERS,
			'link' => tep_href_link(FILENAME_PUBLISHERS)
		),
		array(
			'code' => FILENAME_MANUFACTURERS,
			'title' => BOX_CATALOG_TRANSLATORS,
			'link' => tep_href_link(FILENAME_MANUFACTURERS)
		)
		,
		array(
			'code' => FILENAME_DIRECTORS,
			'title' => BOX_CATALOG_DIRECTORS,
			'link' => tep_href_link(FILENAME_DIRECTORS)
		),
		array(
			'code' => FILENAME_ACTORS,
			'title' => BOX_CATALOG_ACTORS,
			'link' => tep_href_link(FILENAME_ACTORS)
		)
		
	)
);
?>
