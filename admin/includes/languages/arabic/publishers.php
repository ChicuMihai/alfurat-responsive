<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Publisher');

define('TABLE_HEADING', 'Publisher');

define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_MANUFACTURERS', '');

define('TEXT_HEADING_NEW_MANUFACTURER', 'New Publisher');
define('TEXT_HEADING_EDIT_MANUFACTURER', 'Edit Publisher');
define('TEXT_HEADING_DELETE_MANUFACTURER', 'Delete Publisher');


define('TEXT_MANUFACTURERS', 'Publishers:');
define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_PRODUCTS', 'Products:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');

define('TEXT_NEW_INTRO', 'Please fill out the following information for the new ');
define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_MANUFACTURERS_NAME', ' Name:');



define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this  ');
define('TEXT_DELETE_PRODUCTS_1', 'Delete products from this Publisher? (including product reviews, products on special, upcoming products)');
define('TEXT_DELETE_PRODUCTS_2', 'Delete products from this Author? (including product reviews, products on special, upcoming products)');
define('TEXT_DELETE_PRODUCTS_3', 'Delete products from this Translator? (including product reviews, products on special, upcoming products)');
define('TEXT_DELETE_WARNING_PRODUCTS_1', '<strong>WARNING:</strong> There are %s products still linked to this Publisher !');
define('TEXT_DELETE_WARNING_PRODUCTS_2', '<strong>WARNING:</strong> There are %s products still linked to this Author !');
define('TEXT_DELETE_WARNING_PRODUCTS_3', '<strong>WARNING:</strong> There are %s products still linked to this Translator !');

define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: I can not write to this directory. Please set the right user permissions on: %s');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Error: Directory does not exist: %s');
?>
