<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2004 Felix.Schwarz@grede.de

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'New Featured Product');

define('TABLE_HEADING_PRODUCTS', ' Name');
define('TABLE_HEADING_MODEL', ' Model');
define('TABLE_HEADING_PRODUCTS_PRICE', ' Price');
define('TABLE_HEADING_STATUS', ' Status');
define('TABLE_HEADING_ACTION', 'Action');
define('TEXT_FEATURED_PRODUCT', 'Product:');
define('TEXT_FEATURED_MODEL', 'Products Model:');
define('TEXT_FEATURED_PRODUCT_ADDED', 'Featured Product Added');
define('SUBMIT_SEARCH', 'Search');
define('TABLE_HEADING_MANUFACTURERS', ' Publisher');
define('TEXT_PRODUCTS_AUTHORS', ' Author');
?>