<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_COUNT_CUSTOMERS', 'Customers receiving newsletter: %s');
define('TEXT_PRODUCTS', 'Categories');
define('TEXT_SELECTED_PRODUCTS', 'Selected Categories');

define('JS_PLEASE_SELECT_PRODUCTS', 'Please select some categories.');

define('BUTTON_GLOBAL', 'Global');
define('BUTTON_UNSELECT', '>>>');
define('BUTTON_SELECT', '<<<');
define('BUTTON_SUBMIT', 'Submit');
define('BUTTON_CANCEL', 'Cancel');
?>