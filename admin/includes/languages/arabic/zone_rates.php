<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Aramex Zone Rates');


define('TABLE_HEADING', 'Zones');
define('TABLE_RATES', 'Zone Rates');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_HEADING_NEW', 'New Zone');
define('TEXT_HEADING_EDIT', 'Edit Zone');

define('TEXT_HEADING_DELETE', 'Delete Zone');

define('TEXT_MANUFACTURERS', 'Publishers:');
define('TEXT_DATE_ADDED', 'Date Added:');
define('TEXT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_PRODUCTS', 'Zones:');
//define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');

define('TEXT_NEW_INTRO', 'Please fill out the following information for the new ');
define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_MANUFACTURERS_NAME', ' Countries:');
define('TEXT_MANUFACTURERS_RATES', 'RATES:');


define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this  ');

define('TEXT_DELETE_PRODUCTS', 'Delete products from this Publisher? (including product reviews, products on special, upcoming products)');


define('ERROR_DIRECTORY_NOT_WRITEABLE', 'Error: I can not write to this directory. Please set the right user permissions on: %s');
define('ERROR_DIRECTORY_DOES_NOT_EXIST', 'Error: Directory does not exist: %s');
?>
