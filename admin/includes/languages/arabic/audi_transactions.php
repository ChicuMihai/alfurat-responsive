<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Audi Transactions');

define('TABLE_HEADING_NUMBER', 'No.');
define('TABLE_DATE_ADDED', 'Date Added');
define('TABLE_CUSTOMER', 'Customer');
?>