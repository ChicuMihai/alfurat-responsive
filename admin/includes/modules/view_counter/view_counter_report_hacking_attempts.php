<?php
/*
  Copyright (c) 2003 osCommerce
  Portions Copyright 2013 Jack York at http://www.oscommerce-solution.com 
*/
/*
 $db_query = tep_db_query("select bot_name, inet_ntoa(ip_number) as ip from view_counter_storage group by ip_number having count(ip_number) > 10 order by last_date asc  ");

 $iplist = array();
 $iplist[] = array('id' => TEXT_REPORT_PATH_TRACKER_PICK_IP, 'text' => TEXT_REPORT_PATH_TRACKER_PICK_IP);
 while ($db = tep_db_fetch_array($db_query)) {
     $text = ((tep_not_null($db['bot_name']) && $db['bot_name'] != 'bot_name') ? $db['ip'] .'-'.$db['bot_name'] : $db['ip']);
     $iplist[] = array('id' => $db['ip'], 'text' => $text);
 }
 $pathSelectedIP = (! tep_not_null($pathSelectedIP) ? TEXT_REPORT_PATH_TRACKER_PICK_IP : $pathSelectedIP); 
 
 */
 $whereIP = (isset($_POST['current_ip']) ? " and ip_number = inet_aton('" . $_POST['current_ip'] . "')" : '' ); 
?>
   <tr>
     <td><table border="1" cellpadding="0" width="100%" class="BorderedBoxLight">
        
       <!-- BEGIN OF ViewCounter -->
       <?php echo tep_draw_form('view_counter_hackers', FILENAME_VIEW_COUNTER_REPORTS, '', 'post') . tep_draw_hidden_field('action', 'process_path'); ?> 
         <tr class="smallText">
           <td><table border="0" cellpadding="0" width="100%" style="border: thin outset;">
             <tr class="smallText">
               <td>
                 <div style="float:left; vertical-align:bottom; margin-top:3px;"><?php echo TEXT_REPORT_HACKING_ATTENPTS_LIMIT_IP . '&nbsp;'; ?></div>
                 <div style="float:left; padding-left:10px;"><?php echo tep_draw_input_field('current_ip', (($pathSelectedIP != TEXT_REPORT_PATH_TRACKER_PICK_IP) ? $pathSelectedIP : ''), 'maxlength="35" size="14"', false, '', false); ?></div>
               </td>
               <?php echo tep_draw_hidden_field('show_report', $showReport) .
                          tep_draw_hidden_field('sortbyarray', serialize($sortByArray)) .
                          tep_draw_hidden_field('sortby', $sortBy) .
                          tep_draw_hidden_field('last_selected_ip', $pathSelectedIP);
               ?>
             </tr>  
           </table></td>
         </tr>            
       </form>

       <tr class="smallText">
         <?php echo tep_draw_form('view_counter_hackers_ban', FILENAME_VIEW_COUNTER_REPORTS, '', 'post') . tep_draw_hidden_field('action', 'process_hackers'); ?> 
           <td width="100%" ><table border="2" cellpadding="0" width="100%" style="border-width: thin; border-style: outset;" class="smallText">
         
               <?php 
               if (tep_not_null($pathSelectedIP)) {  
                   $db_query = tep_db_query("select inet_ntoa(ip_number) as ip, arg, isbot, last_date from view_counter_storage where arg IS NOT NULL and arg != '' and ip_active=1 " . $whereIP . " order by last_date asc limit 100");
                   
                   while ($db = tep_db_fetch_array($db_query)) { 
                       if (! tep_not_null(CheckForHackerCode($db['arg'])))                  
                           continue;
               ?> 
                    <tr>
                      <td class="smallText" width="10" align="center"><?php echo $db['isbot']; ?></td>
                      <td class="smallText" width="150"><?php echo $db['last_date']; ?></td>
                      <td class="smallText"><?php echo $db['ip']; ?></td>
                      <td class="smallText"><?php echo $db['arg']; ?></td>
                      <td><input type="submit" name="<?php echo $db['ip']; ?>" value="<?php echo TEXT_BUTTON_BAN_IP; ?>" style="font-size:10px; height:14px; width:80px; border:1px solid #000; background:<?php echo $colors[COLOR_BUTTONS]; ?>"></td>
                    </tr>
               <?php } } 
                    echo tep_draw_hidden_field('show_report', $showReport);
               ?> 
       
           </table><td>
         </form>
       </tr>
       
     </table></td>
   </tr>  
