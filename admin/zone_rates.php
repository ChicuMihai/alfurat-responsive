<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
if (tep_not_null($action)) {
	switch ($action) {
		case 'insert':
		case 'save':

			$countries = tep_db_prepare_input($HTTP_POST_VARS['countries']);
			$rates = tep_db_prepare_input($HTTP_POST_VARS['rates']);

			$last_order = tep_db_fetch_array(tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key='MODULE_SHIPPING_ZONES_SORT_ORDER' "));

			$last = $last_order['configuration_value'] + 1;
			$sql_data_array1 = array('configuration_value' => $countries, 'configuration_key' => 'MODULE_SHIPPING_ZONES_COUNTRIES_' . $last);
			$sql_data_array2 = array('configuration_value' => $rates, 'configuration_key' => 'MODULE_SHIPPING_ZONES_COST_' . $last);


			if ($action == 'insert') {
				$sql_data_array3 = array('configuration_value' => $last);
				tep_db_perform(TABLE_CONFIGURATION, $sql_data_array1);
				tep_db_perform(TABLE_CONFIGURATION, $sql_data_array2);
				tep_db_perform(TABLE_CONFIGURATION, $sql_data_array3, 'update', "configuration_key = 'MODULE_SHIPPING_ZONES_SORT_ORDER'");
			} elseif ($action == 'save') {
				$manufacturers_id = tep_db_prepare_input($HTTP_GET_VARS['mID']);
				$last = $manufacturers_id + 1;
				tep_db_perform(TABLE_CONFIGURATION, array('configuration_value' => $countries), 'update', "configuration_id = '" . (int) $manufacturers_id . "'");
				tep_db_perform(TABLE_CONFIGURATION, array('configuration_value' => $rates), 'update', "configuration_id = '" . (int) $last . "'");
			}

			if (USE_CACHE == 'true') {
				tep_reset_cache_block('manufacturers');
			}

			tep_redirect(tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?' . (isset($HTTP_GET_VARS['page']) ? 'page=' . $HTTP_GET_VARS['page'] . '&' : '') . 'mID=' . $manufacturers_id), '');
			break;
		case 'deleteconfirm':
			$manufacturers_id = tep_db_prepare_input($HTTP_GET_VARS['mID']);
			$last = $manufacturers_id + 1;
			tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int) $manufacturers_id . "'");
			tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int) $last . "'");
			if (USE_CACHE == 'true') {
				tep_reset_cache_block('manufacturers');
			}
			tep_redirect(tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'], ''));
			break;
	}
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="pageHeading"><?php echo constant("HEADING_TITLE"); ?></td>
					<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
				</tr>
			</table></td>
	</tr>
	<tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
							<tr class="dataTableHeadingRow">
								<td class="dataTableHeadingContent"><?php echo constant("TABLE_HEADING"); ?> </td>
								<td class="dataTableHeadingContent"><?php echo constant("TABLE_RATES"); ?> </td>
								<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
							</tr>
							<?php
							$manufacturers_query_raw = "select configuration_id, configuration_value from " . TABLE_CONFIGURATION . " where configuration_key like '%MODULE_SHIPPING_ZONES_COUNTRIES%' ";
							$manufacturers_split = new splitPageResults($HTTP_GET_VARS['page'], MAX_DISPLAY_SEARCH_RESULTS, $manufacturers_query_raw, $manufacturers_query_numrows);
							$manufacturers_query = tep_db_query($manufacturers_query_raw);
							while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
								//bring rates of this zone
								$next_id = $manufacturers['configuration_id'] + 1;
								$zone_rates = tep_db_fetch_array(tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_id='" . $next_id . "' "));

								$manufacturers['rates'] = $zone_rates['configuration_value'];
								if ((!isset($HTTP_GET_VARS['mID']) || (isset($HTTP_GET_VARS['mID']) && ($HTTP_GET_VARS['mID'] == $manufacturers['configuration_id']))) && !isset($mInfo) && (substr($action, 0, 3) != 'new')) {
									$manufacturer_products_query = tep_db_query("select count(*) as products_count from " . TABLE_CONFIGURATION . " where configuration_key like '%MODULE_SHIPPING_ZONES_COUNTRIES%' ");
									$manufacturer_products = tep_db_fetch_array($manufacturer_products_query);

									$mInfo_array = array_merge($manufacturers, $manufacturer_products);
									$mInfo = new objectInfo($mInfo_array);
								}

								if (isset($mInfo) && is_object($mInfo) && ($manufacturers['configuration_id'] == $mInfo->configuration_id)) {
									echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $manufacturers['configuration_id'] . '&action=edit', '') . '\'">' . "\n";
								} else {
									echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $manufacturers['configuration_id'], '') . '\'">' . "\n";
								}
								?>
								<td class="dataTableContent"><?php echo $manufacturers['configuration_value']; ?></td>
								<td class="dataTableContent"><?php echo $manufacturers['rates']; ?></td>
								<td class="dataTableContent" align="right"><?php
									if (isset($mInfo) && is_object($mInfo) && ($manufacturers['manufacturers_id'] == $mInfo->configuration_id)) {
										echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif');
									} else {
										echo '<a href="' . tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $manufacturers['configuration_id']), '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>';
									}
									?>&nbsp;</td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
							<tr>
								<td class="smallText" valign="top"><?php echo $manufacturers_split->display_count($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $HTTP_GET_VARS['page'], TEXT_DISPLAY_NUMBER_OF_MANUFACTURERS); ?></td>
								<td class="smallText" align="right"><?php echo $manufacturers_split->display_links($manufacturers_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $HTTP_GET_VARS['page']); ?></td>
							</tr>
						</table></td>
				</tr>
				<?php
				if (empty($action)) {
					?>
					<tr>
						<td align="right" colspan="4" class="smallText"><?php echo tep_draw_button(IMAGE_INSERT, 'plus', tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->products_count . '&action=new', '')); ?></td>
					</tr>
					<?php
				}
				?>
            </table></td>
		<?php
		$heading = array();
		$contents = array();

		switch ($action) {
			case 'new':
				$heading[] = array('text' => '<strong>' . constant("TEXT_HEADING_NEW") . '</strong>');

				$contents = array('form' => tep_draw_form('manufacturers', constant("FILENAME_MODULES_ZONES_RATES") . '?action=insert', '', 'post', 'enctype="multipart/form-data"'));
				$contents[] = array('text' => TEXT_NEW_INTRO . constant("HEADING_TITLE"));
				$contents[] = array('text' => '<br />' . constant("TEXT_MANUFACTURERS_NAME") . '<br />' . tep_draw_input_field('countries'));
				//	$contents[] = array('text' => '<br />' . constant("TEXT_MANUFACTURERS_IMAGE_") . '<br />' . tep_draw_file_field('manufacturers_image'));

				$manufacturer_inputs_string = '';
				$manufacturer_inputs_string .= '<br />' . tep_draw_input_field('rates');


				$contents[] = array('text' => '<br />' . constant("TEXT_MANUFACTURERS_RATES") . $manufacturer_inputs_string);
				$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $HTTP_GET_VARS['mID'], '')));
				break;
			case 'edit':
				$heading[] = array('text' => '<strong>' . constant("TEXT_HEADING_EDIT") . '</strong>');

				$contents = array('form' => tep_draw_form('manufacturers', constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->configuration_id . '&action=save', '', 'post', 'enctype="multipart/form-data"'));
				$contents[] = array('text' => TEXT_EDIT_INTRO);
				$contents[] = array('text' => '<br />' . constant("TEXT_MANUFACTURERS_NAME") . '<br />' . tep_draw_input_field('countries', $mInfo->configuration_value));
				//	$contents[] = array('text' => '<br />' . constant("TEXT_MANUFACTURERS_IMAGE_") . '<br />' . tep_draw_file_field('manufacturers_image') . '<br />' . $mInfo->manufacturers_image);


				$contents[] = array('text' => '<br />' . constant("TEXT_MANUFACTURERS_RATES") . '<br />' . tep_draw_input_field('rates', $mInfo->rates));
				$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->configuration_id, '')));
				break;
			case 'delete':
				$heading[] = array('text' => '<strong>' . constant("TEXT_HEADING_DELETE") . '</strong>');

				$contents = array('form' => tep_draw_form('manufacturers', constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->configuration_id . '&action=deleteconfirm', ''));
				$contents[] = array('text' => TEXT_DELETE_INTRO . constant("HEADING_TITLE"));
				$contents[] = array('text' => '<br /><strong>' . $mInfo->configuration_value . '</strong>');


				$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->configuration_id), ''));
				break;
			default:
				if (isset($mInfo) && is_object($mInfo)) {
					$heading[] = array('text' => '<strong>' . $mInfo->configuration_value . '</strong>');
					$contents[] = array('text' => '<br><strong>' . $mInfo->rates . '</strong><br><br><br>');

					$contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->configuration_id . '&action=edit', '')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(constant("FILENAME_MODULES_ZONES_RATES") . '?page=' . $HTTP_GET_VARS['page'] . '&mID=' . $mInfo->configuration_id . '&action=delete', '')));



					$contents[] = array('text' => '<br />' . TEXT_PRODUCTS . ' ' . $mInfo->products_count);
				}
				break;
		}

		if ((tep_not_null($heading)) && (tep_not_null($contents))) {
			echo '            <td width="25%" valign="top">' . "\n";

			$box = new box;
			echo $box->infoBox($heading, $contents);

			echo '            </td>' . "\n";
		}
		?>
	</tr>
</table></td>
</tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
