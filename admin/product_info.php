<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

if (!isset($HTTP_GET_VARS['products_id'])) {
	tep_redirect(tep_href_link(FILENAME_DEFAULT));
}

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

$product_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id and pd.language_id = '" . (int) $languages_id . "'");
$product_check = tep_db_fetch_array($product_check_query);
if ($product_check['total'] > 0) {
	//bring category id 
	$product_category_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id='" . (int) $HTTP_GET_VARS['products_id'] . "'");
	$category_info = tep_db_fetch_array($product_category_query);
	$is_magazine = 0;
	$is_poster = 0;
	$is_book = 1;
	if ($category_info['categories_id'] == MAGAZINES_CATEGORY_ID) {
		$is_magazine = 1;
		$is_book = 0;
	} else if ($category_info['categories_id'] >= POSTERS_CATEGORY_ID) {
		$is_poster = 1;
		$is_book = 0;
	}

	if ($is_book) {
		$product_info_query = tep_db_query("select GROUP_CONCAT(DISTINCT pub.publishers_name  SEPARATOR '&#1548; ') as publisher, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author, GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ',')  as author_ids,GROUP_CONCAT(DISTINCT trans.manufacturers_name  SEPARATOR ',') as translator, p.products_id, pd.products_name, pd.products_sub_title, pd.products_description, p.products_model, p.products_edition, p.products_isbn, p.products_weight,products_cover_type ,products_series,products_nb_in_series,product_available_volumes,products_page_nb, p.products_size, p.products_quantity,p.products_available, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.products_date_released from " . TABLE_PRODUCTS . " p"
				. " left join  " . TABLE_PRODUCTS_DESCRIPTION . " pd  on pd.products_id = p.products_id "
				. " left join " . TABLE_PRODUCTS_TO_PUBLISHERS . " m on p.products_id = m.products_id "
				. " left join " . TABLE_PUBLISHERS . " pub on pub.publishers_id = m.publishers_id "
				. " left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id "
				. " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id "
				. "left join " . TABLE_PRODUCTS_TO_TRANSLATORS . " t on p.products_id = t.products_id "
				. "left join " . TABLE_MANUFACTURERS . " trans on t.manufacturers_id = trans.manufacturers_id "
				. "  where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "'     and pd.language_id = '" . (int) $languages_id . "'");
		$productType = 'Book';
	} else {
		$product_info_query = tep_db_query("select GROUP_CONCAT(DISTINCT pub.name  SEPARATOR '&#1548; ') as publisher, GROUP_CONCAT(DISTINCT auth.name  SEPARATOR '&#1548; ')  as author, p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_edition, p.products_isbn, p.products_weight,products_cover_type,products_page_nb, p.products_size, p.products_quantity,p.products_available, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.products_date_released from " . TABLE_PRODUCTS . " p"
				. " left join  " . TABLE_PRODUCTS_DESCRIPTION . " pd  on pd.products_id = p.products_id "
				. " left join " . TABLE_PRODUCTS_TO_DIRECTORS . " m on p.products_id = m.products_id "
				. " left join " . TABLE_DIRECTORS . " pub on pub.id = m.directors_id "
				. " left join " . TABLE_PRODUCTS_TO_ACTORS . " a on  p.products_id = a.products_id "
				. " left join " . TABLE_ACTORS . " auth on  a.actors_id = auth.id "
				. "  where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "'     and pd.language_id = '" . (int) $languages_id . "'");
		$productType = 'Poster';
	}
	$product_info = tep_db_fetch_array($product_info_query);

	tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and language_id = '" . (int) $languages_id . "'");

	if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
		$products_price = '<del>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
	} else {
		$products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
	}

	if (tep_not_null($product_info['products_model'])) {
		$products_name = $product_info['products_name'] . '';
	} else {
		$products_name = $product_info['products_name'];
	}
}
require(DIR_WS_INCLUDES . 'template_top.php');

if ($product_check['total'] < 1) {
	?>

	<div class="contentContainer">
		<div class="contentText">
			<?php echo TEXT_PRODUCT_NOT_FOUND; ?>
		</div>

		<?php
		/*		 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
		$rtl = stripos(HTML_PARAMS, 'dir="rtl"');
		if ($rtl !== false) {
			?>
			<div class="left">
			<?php } else { ?>
				<div class="right">
				<?php } /*				 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */ ?>
				<?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?>
			</div>
		</div>

		<?php
	} else {
		echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action')) . 'action=add_product'));
		?>

		<div>
			<div class="boxCatTitle"><?php
				echo $products_name;
				echo (!empty($product_info['products_sub_title'])) ? ' (' . $product_info['products_sub_title'] . ')' : '';
				?></div>
		</div>

		<div class="contentContainer">
			<div class="contentText">
				<?php
				if (tep_not_null($product_info['products_image'])) {
					$photoset_layout = '1';

					$pi_query = tep_db_query("select image, htmlcontent from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $product_info['products_id'] . "' order by sort_order");
					$pi_total = tep_db_num_rows($pi_query);

					if ($pi_total > 0) {
						$pi_sub = $pi_total - 1;

						while ($pi_sub > 5) {
							$photoset_layout .= 5;
							$pi_sub = $pi_sub - 5;
						}

						if ($pi_sub > 0) {
							$photoset_layout .= ($pi_total > 5) ? 5 : $pi_sub;
						}
						?>

						<?php
						/*						 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
						if (stripos(HTML_PARAMS, 'dir="rtl"') !== false) {
							?>
							<div id="piGal" style="float: left ;">
							<?php } else { ?>
								<div id="piGal" style="float: right;">
									<?php
								}
								/*								 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */
								?>

								<?php
								$pi_counter = 0;
								$pi_html = array();

								while ($pi = tep_db_fetch_array($pi_query)) {
									$pi_counter++;

									if (tep_not_null($pi['htmlcontent'])) {
										$pi_html[] = '<div id="piGalDiv_' . $pi_counter . '">' . tep_image(DIR_WS_IMAGES . $pi['image'], '', '', '', 'id="piGalImg_' . $pi_counter . '"') . '<br><h1>' . $pi['htmlcontent'] . '</h1></div>';
									}

									echo tep_image(DIR_WS_IMAGES . $pi['image'], '', '', '', ' id="piGalImg_' . $pi_counter . '"');
								}
								?>

							</div>

							<?php
							if (!empty($pi_html)) {
								echo '    <div style="display: none;">' . implode('', $pi_html) . '</div>';
							}
						} else {
							?>

							<?php
							/*							 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
							if (stripos(HTML_PARAMS, 'dir="rtl"') !== false) {
								?>
								<div class="productImgDiv" <?php echo($is_magazine) ? 'id="piGal"' : ''; ?> style="float: left ;">
								<?php } else { ?>
									<div id="piGal" style="float: right;">
									<?php } /*									 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */ ?>
									<?php echo tep_image(DIR_WS_IMAGES . $product_info['products_image'], addslashes($product_info['products_name']), '', '', 'class="product_img "'); ?>
								</div>

								<?php
							}
						}
						if ($is_magazine) {
							if ($pi_total == 2)
								$photoset_layout = 12;
							?>
							<script type="text/javascript">
								$(function () {
									$('#piGal').css({
										'visibility': 'hidden'
									});

									$('#piGal').photosetGrid({
										layout: '<?php echo $photoset_layout; ?>',
										width: '250px',
										highresLinks: true,
										rel: 'pigallery',
										onComplete: function () {
											$('#piGal').css({'visibility': 'visible'});

											$('#piGal a').colorbox({
												maxHeight: '120%',
												maxWidth: '120%',
												rel: 'pigallery'
											});

											$('#piGal img').each(function () {
												var imgid = $(this).attr('id').substring(9);

												if ($('#piGalDiv_' + imgid).length) {
													$(this).parent().colorbox({inline: true, href: "#piGalDiv_" + imgid});
												}
											});
										}
									});
								});
							</script>
						<?php } ?>
						<div class="productDetailsDiv" id="productDetails">
							<table border="0" width="100%"><tr>
									<td align="right" valign="top">
										<?php
										if (!$is_magazine) {
											if (!empty($product_info['publisher']) ||
													!empty($product_info['products_date_released']) ||
													!empty($product_info['author']) ||
													!empty($product_info['translator']))
												echo'<p class="parBgGray">';
											if (!empty($product_info['publisher'])) {
												echo '<span class="green">';
												echo ($is_book) ? TEXT_PUBLISHEER : TEXT_DIRECTOR;
												echo':</span> ' . $product_info['publisher'];
											}
											if (tep_date_long($product_info['products_date_released'])) {
												echo'<span class="green">';
												echo ($is_poster) ? TEXT_DATE_FILM : TEXT_RELEASED;
												echo':</span> ' . tep_date_get_year($product_info['products_date_released']) . '</p>';
											}
											?>
											<?php
											$authors = ($is_book) ? tep_get_authors_with_links($product_info['author'], $product_info['author_ids']) : $product_info['author'];
											if (!empty($product_info['author'])) {
												echo '<p class="parBgGray"><span class="green">';
												echo ($is_book) ? TEXT_AUTHOR : TEXT_ACTORS;
												echo':</span> ' . $authors . '&nbsp;';
											}
											?>
											<?php if (!empty($product_info['translator'])) echo'<span class="green">' . TEXT_TRANSLATOR . ':</span> ' . $product_info['translator']; ?></p>
											<?php if (!empty($product_info['products_isbn'])) echo'<p class="parBgGray dir-ltr" >' . $product_info['products_isbn'] . '<span class="green">:' . TEXT_ISBN . '</span></p> '; ?>

											<?php
											if (!empty($product_info['products_edition']) || !empty($product_info['products_page_nb']) || !empty($product_info['products_size']) || !empty($product_info['products_cover_type']))
												echo'<p class="parBgGray">';
											if (!empty($product_info['products_edition']))
												echo'<span class="green">' . TEXT_EDITION . ':</span> ' . $product_info['products_edition'] . '&#1548; ';
											?>
											<?php //if (!empty($product_info['products_weight']) && $product_info['products_weight'] > 0) echo'<p class="parBgGray"><span class="green">' . TEXT_WEIGHT . ':</span> ' . $product_info['products_weight'] . TEXT_WEIGHT_GRAM . '</p>'; ?>
											<?php if (!empty($product_info['products_page_nb'])) echo'<span class="green">' . TEXT_PAGE_NB . ':</span> ' . $product_info['products_page_nb'] . '&#1548;'; ?>
											<?php
											if (!empty($product_info['products_size']))
												echo'<span class="green">' . TEXT_SIZE . ':</span> <span dir="ltr">' . $product_info['products_size'] . ' cm </span> &#1548;';
											?>
											<?php
											if (!empty($product_info['products_cover_type'])) {
												echo'<span class="green">' . TEXT_COVER_TYPE . ':</span>';
												echo ($product_info['products_cover_type'] == 'normal') ? TEXT_COVER_TYPE_NORMAL : TEXT_COVER_TYPE_ARTISTIC;
											}
											echo'</p>';
											?>
											<?php
											if (!empty($product_info['products_series']) || !empty($product_info['products_nb_in_series']) || !empty($product_info['product_available_volumes']))
												echo'<p class="parBgGray">';
											if (!empty($product_info['products_series']))
												echo'<span class="green">' . TEXT_SERIES . ':</span> ' . $product_info['products_series'] . '&#1548; ';
											?>
											<?php
											if (!empty($product_info['products_nb_in_series']))
												echo'<span class="green">' . TEXT_SERIES_NUMBER . ':</span> ' . $product_info['products_nb_in_series'] . '&#1548;';
											if (!empty($product_info['product_available_volumes']))
												echo'<span class="green">' . TEXT_AVAILABLE_VOLUME . ':</span> ' . $product_info['product_available_volumes'];
											echo'</p>';
											?>
											</p>
											<?php
										}else {
											echo'<p class="parBgGray">';
											if (tep_date_long($product_info['products_date_released'])) {
												echo'<span class="green">';
												echo TEXT_RELEASED;
												echo':</span> ' . tep_date_get_year($product_info['products_date_released']) . '</p>';
											}
											if (!empty($product_info['products_description'])) {
												echo'	<div><h1> ';
												echo TEXT_MAGAZINES_DESCRIPTION;
												echo'</h1><p>' . stripslashes($product_info['products_description']) . '</p></div>';
											}
										}
										?>
									</td>
								</tr></table></div>
						<div style="clear: both;"></div>	
						<?php
						$products_attributes_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int) $languages_id . "'");
						$products_attributes = tep_db_fetch_array($products_attributes_query);
						if ($products_attributes['total'] > 0) {
							?>

							<p><?php echo '<strong>', TEXT_PRODUCT_OPTIONS, '</strong>'; ?></p>

							<p>
								<?php
								$products_options_name_query = tep_db_query("select distinct popt.products_options_id, popt.products_options_name from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_ATTRIBUTES . " patrib where patrib.products_id='" . (int) $HTTP_GET_VARS['products_id'] . "' and patrib.options_id = popt.products_options_id and popt.language_id = '" . (int) $languages_id . "' order by popt.products_options_name");
								while ($products_options_name = tep_db_fetch_array($products_options_name_query)) {
									$products_options_array = array();
									$products_options_query = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, pa.options_values_price, pa.price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_OPTIONS_VALUES . " pov where pa.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pa.options_id = '" . (int) $products_options_name['products_options_id'] . "' and pa.options_values_id = pov.products_options_values_id and pov.language_id = '" . (int) $languages_id . "'");
									while ($products_options = tep_db_fetch_array($products_options_query)) {

										if ($products_options['options_values_price'] != '0') {
											$products_options_array[] = array('id' => $products_options['products_options_values_id'], 'text' => '<strong>' . $products_options['products_options_values_name'] . ':</strong>');
											$products_options_array[sizeof($products_options_array) - 1]['text'] .= ' (' . $products_options['price_prefix'] . $currencies->display_price($products_options['options_values_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . ') ';
										}
									}
									if (is_string($HTTP_GET_VARS['products_id']) && isset($cart->contents[$HTTP_GET_VARS['products_id']]['attributes'][$products_options_name['products_options_id']])) {
										$selected_attribute = $cart->contents[$HTTP_GET_VARS['products_id']]['attributes'][$products_options_name['products_options_id']];
									} else {
										$selected_attribute = false;
									}
									?>
									<a class="tip" onclick="remove_product(8192);" href="javascript:;"><strong><?php echo $products_options_name['products_options_name'] . ''; ?></strong>
										<span><?php echo constant('option_description_' . $products_options_name['products_options_id']) ?></span>
										<?php
										//	if (count($products_options_array) == 1) {
										echo $products_options_array[0]['text'], '</a>', tep_draw_hidden_field('id[' . $products_options_name['products_options_id'] . ']', $products_options_array[0]['id']);
										//	} else {
										//echo tep_draw_pull_down_menu('id[' . $products_options_name['products_options_id'] . ']', $products_options_array, $selected_attribute);
										//}
										?>
										<?php
									}
									?>
							</p>

							<?php
						}
						?>

						<div style="clear: both;"></div>
						<div >	<?php
							/*							 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
							if ($rtl !== false) {
								?>
								<h1  class="left" >
								<?php } else { ?>
									<h1 class="right" >
										<?php
									}
									/*									 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */
									?><?php echo $products_price; ?></h1></div>

						<?php
						if ($product_info['products_date_available'] > date('Y-m-d H:i:s')) {
							?>

							<p style="text-align: center;"><?php echo sprintf(TEXT_DATE_AVAILABLE, tep_date_long($product_info['products_date_available'])); ?></p>

							<?php
						}
						?>

					</div>
					<?php
					if (!empty($product_info['products_description']) && !$is_magazine) {
						echo'	<div><h1> ';
						echo ($is_book) ? TEXT_DESCRIPTION : TEXT_POSTER_DESCRIPTION;
						echo'</h1><p>' . stripslashes($product_info['products_description']) . '</p></div>';
					}
					?>


					<?php
					$reviews_query = tep_db_query("select count(*) as count from " . TABLE_REVIEWS . " r, " . TABLE_REVIEWS_DESCRIPTION . " rd where r.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and r.reviews_id = rd.reviews_id and rd.languages_id = '" . (int) $languages_id . "' and reviews_status = 1");
					// $reviews = tep_db_fetch_array($reviews_query);
					?>


					<br><br>
					<!-- AddThis Button END -->
					<div class="buttonSet">

						<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
							<a class="addthis_button_facebook"></a>
							<a class="addthis_button_twitter"></a>
							<a class="addthis_button_email"></a>
							<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
						</div>
						<?php if ($product_info['products_available']==1) {
							echo tep_draw_hidden_field('products_id', $product_info['products_id']) . tep_draw_button(IMAGE_BUTTON_IN_CART, 'cart', null, 'primary');
						}else{
							 echo '<p>'.TEXT_BOOK_NOT_AVAILABLE.'</p>';
						}
						?>

					<?php //echo tep_draw_button(IMAGE_BUTTON_REVIEWS . (($reviews['count'] > 0) ? ' (' . $reviews['count'] . ')' : ''), 'comment', tep_href_link(FILENAME_PRODUCT_REVIEWS, tep_get_all_get_params()));      ?>
					</div>
					<?php
					if ((USE_CACHE == 'true') && empty($SID)) {
						echo tep_cache_also_purchased(3600);
					} else {
						include(DIR_WS_MODULES . FILENAME_ALSO_PURCHASED_PRODUCTS);
					}
					?>

				</div>

				</form>
				<script type="text/javascript" src="js/addthis_widget.js"></script>
				<?php
			}

			require(DIR_WS_INCLUDES . 'template_bottom.php');
			require(DIR_WS_INCLUDES . 'application_bottom.php');
			?>
