<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ABOUT_US);

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_ABOUT_US));

require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div>
    <div class="boxTitle"><?php echo HEADING_TITLE; ?></div>
    <div class="conCon">
        <div class="textContent">
            <?php echo ABOUT_US_TEXT; ?>
            <span class="tdbLink" style="margin-bottom: 10px;">
		    <a id="to-en" class="tdb2" href="javascript:void">Translate to English</a>
		</span>
            <div class="buttonSet" style="margin-top: 10px;">
                <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?></span>
            </div>
        </div></div>

    <script>

        var translate = document.getElementById('to-en');
        var lang = "ar";
        var p = document.getElementsByTagName('p')[0];
        var oldText = p.innerHTML;
        var textEnglish = "Alfurat is concerned in the distribution of Arabic books throughout the Arab Word as well as Internationally. We collect, store, and progressively build a database of Arabic words which we make most of it available on our website. Currently, our database consists of 120000 Arabic titles including authors, distributors, publishers, a short description and a cover image. We are also particularly specialized in rare Arabic books, magazines, manuscripts, prints, posters";

        translate.onclick = function(e) {
            e.preventDefault();
            if(lang === "ar") {
                lang = "en";
                p.innerHTML = textEnglish;
                this.innerHTML = "Translate to Arabic";
            } else if (lang === "en") {
                lang = "ar";
                p.innerHTML = oldText;
                this.innerHTML = "Translate to English";
            }
        }

    </script>




    <?php
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
