<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */
require('includes/application_top.php');


// the following cPath references come from application_top.php
$smack_current_id = 0;

?>

<?php
if (isset($HTTP_GET_VARS['categories_id'])) {
	if (empty($HTTP_GET_VARS['categories_id']))
		$HTTP_GET_VARS['categories_id'] = POSTERS_CATEGORY_ID;
	tep_redirect(tep_href_link(FILENAME_DEFAULT, 'cPath=' . $HTTP_GET_VARS['categories_id']));
}

if (isset($HTTP_GET_VARS['author'])) {
	$author = $HTTP_GET_VARS['author'];
	$author_name_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int) $author . "'");
	$author_name = tep_db_fetch_array($author_name_query);
	$oscTemplate->setTitle($author_name['manufacturers_name'] . ', ' . $oscTemplate->getTitle());
} else {
	$author = 0;
}
if (isset($HTTP_GET_VARS['translator'])) {
	$translator = $HTTP_GET_VARS['translator'];
	$author_name_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int) $translator . "'");
	$author_name = tep_db_fetch_array($author_name_query);
	$oscTemplate->setTitle($author_name['manufacturers_name'] . ', ' . $oscTemplate->getTitle());
} else {
	$translator = 0;
}

$category_depth = 'top';
if (isset($cPath) && tep_not_null($cPath) && $cPath != POSTERS_CATEGORY_ID) {
	$categories_products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int) $current_category_id . "'");
	$categories_products = tep_db_fetch_array($categories_products_query);
	if ($categories_products['total'] > 0) {
		$category_depth = 'products'; // display products
	} else {
		$category_parent_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where parent_id = '" . (int) $current_category_id . "'");
		$category_parent = tep_db_fetch_array($category_parent_query);
		if ($category_parent['total'] > 0) {
			$category_depth = 'nested'; // navigate through the categories
		} else {
			$category_depth = 'products'; // category has no products, but display the 'no products' message
		}
	}
}
if ($current_category_id == POSTERS_CATEGORY_ID)
	$category_depth = 'products';

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
$current_page = FILENAME_DEFAULT;   // Nivo Slider

require(DIR_WS_INCLUDES . 'template_top.php');

if ($category_depth == 'nested') {
	$category_query = tep_db_query("select cd.categories_name, c.categories_image from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int) $current_category_id . "' and cd.categories_id = '" . (int) $current_category_id . "' and cd.language_id = '" . (int) $languages_id . "'");
	$category = tep_db_fetch_array($category_query);
	?>


	<?php
} elseif ($category_depth == 'products' || (isset($HTTP_GET_VARS['manufacturers_id']) && !empty($HTTP_GET_VARS['manufacturers_id'])) || $translator > 0 || $author > 0) {
// create column list
	$define_list = array('PRODUCT_LIST_MODEL' => PRODUCT_LIST_MODEL,
		'PRODUCT_LIST_NAME' => PRODUCT_LIST_NAME,
		'PRODUCT_LIST_MANUFACTURER' => PRODUCT_LIST_MANUFACTURER,
		'PRODUCT_LIST_PRICE' => PRODUCT_LIST_PRICE,
		'PRODUCT_LIST_QUANTITY' => PRODUCT_LIST_QUANTITY,
		'PRODUCT_LIST_WEIGHT' => PRODUCT_LIST_WEIGHT,
		'PRODUCT_LIST_IMAGE' => PRODUCT_LIST_IMAGE,
		'PRODUCT_LIST_BUY_NOW' => PRODUCT_LIST_BUY_NOW);

	asort($define_list);

	$column_list = array();
	reset($define_list);
	while (list($key, $value) = each($define_list)) {
		if ($value > 0)
			$column_list[] = $key;
	}

	$select_column_list = '';

	for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
		switch ($column_list[$i]) {
			case 'PRODUCT_LIST_MODEL':
				$select_column_list .= 'p.products_model, ';
				break;
			case 'PRODUCT_LIST_NAME':
				$select_column_list .= 'pd.products_name, ';
				break;
			case 'PRODUCT_LIST_QUANTITY':
				$select_column_list .= 'p.products_quantity, ';
				break;
			case 'PRODUCT_LIST_IMAGE':
				$select_column_list .= 'p.products_image, ';
				break;
			case 'PRODUCT_LIST_WEIGHT':
				$select_column_list .= 'p.products_weight, ';
				break;
		}
	}

// show the products of a specified manufacturer
	if (isset($HTTP_GET_VARS['manufacturers_id']) && !empty($HTTP_GET_VARS['manufacturers_id'])) {
		if (isset($HTTP_GET_VARS['filter_id']) && tep_not_null($HTTP_GET_VARS['filter_id'])) {
// We are asked to show only a specific category
			$listing_sql = "select " . $select_column_list . " p.products_id, p.manufacturers_id, p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.products_price) as final_price from " . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_MANUFACTURERS . " m, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_status = '1' and p.manufacturers_id = m.manufacturers_id and m.manufacturers_id = '" . (int) $HTTP_GET_VARS['manufacturers_id'] . "' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id and pd.language_id = '" . (int) $languages_id . "' and p2c.categories_id = '" . (int) $HTTP_GET_VARS['filter_id'] . "'";
		} else {
// We show them all
			$listing_sql = "select " . $select_column_list . " p.products_id, p.manufacturers_id, p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.products_price) as final_price from " . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_MANUFACTURERS . " m where p.products_status = '1' and pd.products_id = p.products_id and pd.language_id = '" . (int) $languages_id . "' and p.manufacturers_id = m.manufacturers_id and m.manufacturers_id = '" . (int) $HTTP_GET_VARS['manufacturers_id'] . "'";
		}
	} else if ($author > 0) {
		$listing_sql = "select " . $select_column_list . " p.products_id,  p.products_available,p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids"
				. " ,IF(s.status, s.specials_new_products_price, p.products_price) as final_price"
				. " from " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS . " p"
				. "  left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id"
				. " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id  "
				. " left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id  and  auth.manufacturers_id = '" . (int) $author . "' group by p.products_id  ";//and pd.language_id = '" . (int) $languages_id . "'
		$catname = TABLE_HEADING_AUTHOR_PRODUCTS . $author_name['manufacturers_name'];
	} else {
		if ($translator > 0) {
		$listing_sql = "select " . $select_column_list . " p.products_id,  p.products_available,p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids"
				. " ,IF(s.status, s.specials_new_products_price, p.products_price) as final_price"
				. " from " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS . " p"
				. "  left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id"
				. "  left join " . TABLE_PRODUCTS_TO_TRANSLATORS . " trans on  p.products_id = trans.products_id"
				. " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id  "
				. " left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id  and  trans.manufacturers_id = '" . (int) $translator . "' group by p.products_id  ";//and pd.language_id = '" . (int) $languages_id . "'
		$catname = TABLE_HEADING_TRANSLATOR_PRODUCTS . $author_name['manufacturers_name'];
		} else {
			// show the products in a given categorie
		if ((isset($HTTP_GET_VARS['filter_id']) && tep_not_null($HTTP_GET_VARS['filter_id'])) || (isset($HTTP_GET_VARS['authors_id']) && tep_not_null($HTTP_GET_VARS['authors_id'])) || (isset($HTTP_GET_VARS['translators_id']) && tep_not_null($HTTP_GET_VARS['translators_id']))
		) {
			$where = '';
// We are asked to show only specific catgeory
			$listing_sql = "select " . $select_column_list . " p.products_id, p.products_price,GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, IF(s.status, s.specials_new_products_price, p.products_price) as final_price from " . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c , " . TABLE_PRODUCTS_TO_PUBLISHERS . " m,    " . TABLE_PUBLISHERS . " pub ";
			$listing_sql .= "," . TABLE_PRODUCTS_TO_AUTHORS . " a," . TABLE_MANUFACTURERS . " auth ";
			if ((isset($HTTP_GET_VARS['filter_id']) && tep_not_null($HTTP_GET_VARS['filter_id']))) {
				$where.="   and pub.publishers_id = '" . (int) $HTTP_GET_VARS['filter_id'] . "'";
			}
			if ((isset($HTTP_GET_VARS['authors_id']) && tep_not_null($HTTP_GET_VARS['authors_id']))) {

				$where.=" and auth.manufacturers_id = '" . (int) $HTTP_GET_VARS['authors_id'] . "'";
			}
			if ((isset($HTTP_GET_VARS['translators_id']) && tep_not_null($HTTP_GET_VARS['translators_id']))) {
				$listing_sql .= "," . TABLE_PRODUCTS_TO_TRANSLATORS . " t," . TABLE_MANUFACTURERS . " trans ";
				$where.=" and p.products_id = t.products_id and trans.manufacturers_id=t.manufacturers_id  and trans.manufacturers_id = '" . (int) $HTTP_GET_VARS['translators_id'] . "'";
			}
			$listing_sql .= "where p.products_status = '1'  and p.products_id = p2c.products_id  and p.products_id = a.products_id and auth.manufacturers_id=a.manufacturers_id  and pd.products_id = p2c.products_id  and p2c.categories_id = '" . (int) $current_category_id . "' and p.products_id = m.products_id  and pub.publishers_id = m.publishers_id " . $where . " group by p.products_id"; //and pd.language_id = '" . (int) $languages_id . "'
		} else {
// We show them all
			if ($search_in_posters) {
				$listing_sql = "select " . $select_column_list . " p.products_id,  p.products_price, p.products_tax_class_id,"
						. " IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, "
						. "GROUP_CONCAT(DISTINCT pub.name  SEPARATOR '&#1548; ') as directors "
						. ",IF(s.status, s.specials_new_products_price, p.products_price) as final_price from " . TABLE_PRODUCTS_DESCRIPTION . " pd,"
						. " " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_TO_DIRECTORS . " m on p.products_id = m.products_id   "
						. "left join " . TABLE_DIRECTORS . " pub on pub.id = m.directors_id left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c";

				if ($current_category_id == POSTERS_CATEGORY_ID) {
					$listing_sql .=" inner join  " . TABLE_CATEGORIES . " cat  on cat.categories_id=p2c.categories_id ";
				}

				$listing_sql .= " where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id and pd.language_id = '" . (int) $languages_id . "' "
						. " ";
				if ($current_category_id == POSTERS_CATEGORY_ID) {
					$listing_sql .=" and ( p2c.categories_id = '" . (int) $current_category_id . "'  OR cat.parent_id = '" . (int) POSTERS_CATEGORY_ID . "' ) ";
				} else {
					$listing_sql .=" and p2c.categories_id = '" . (int) $current_category_id . "'";
				}
				$listing_sql .= " group by p.products_id  ";
			} else {
				$where = ($is_magazine == 1) ? "and pd.language_id = '" . (int) $languages_id . "'" : '';
				$listing_sql = "select " . $select_column_list . " p.products_id,  p.products_price,  p.products_available, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids"
						. " ,IF(s.status, s.specials_new_products_price, p.products_price) as final_price"
						. " from " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS . " p"
						. "  left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id"
						. " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id  "
						. " left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id"
						. ", " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c "
						. "where p.products_status = '1' and p.products_id = p2c.products_id and pd.products_id = p2c.products_id  and p2c.categories_id = '" . (int) $current_category_id . "' " . $where . " group by p.products_id  ";
			}
		}
		}
		//$translator

	}

	if ((!isset($HTTP_GET_VARS['sort'])) || (!preg_match('/^[1-8][ad]$/', $HTTP_GET_VARS['sort'])) || (substr($HTTP_GET_VARS['sort'], 0, 1) > sizeof($column_list))) {
		for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
			if ($column_list[$i] == 'PRODUCT_LIST_NAME') {
				$HTTP_GET_VARS['sort'] = $i + 1 . 'a';
				$listing_sql .= " , pd.products_name";
				break;
			}
		}
		$listing_sql.=' order by p.products_date_added  DESC ,p.products_date_released  DESC ';
	} else {
		$sort_col = substr($HTTP_GET_VARS['sort'], 0, 1);
		$sort_order = substr($HTTP_GET_VARS['sort'], 1);

		switch ($column_list[$sort_col - 1]) {
			case 'PRODUCT_LIST_MODEL':
				$listing_sql .= " order by p.products_model " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
				break;
			case 'PRODUCT_LIST_NAME':
				$listing_sql .= " order by p.products_date_added  DESC ,p.products_date_released  DESC , pd.products_name " . ($sort_order == 'd' ? 'desc' : '');
				break;
			case 'PRODUCT_LIST_MANUFACTURER':
				$listing_sql .= " order by author " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
				break;
			case 'PRODUCT_LIST_QUANTITY':
				$listing_sql .= " order by p.products_quantity " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
				break;
			case 'PRODUCT_LIST_IMAGE':
				$listing_sql .= " order by pd.products_name";
				break;
			case 'PRODUCT_LIST_WEIGHT':
				$listing_sql .= " order by p.products_weight " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
				break;
			case 'PRODUCT_LIST_PRICE':
				$listing_sql .= " order by final_price " . ($sort_order == 'd' ? 'desc' : '') . ", pd.products_name";
				break;
		}
	}

	//$catname = HEADING_TITLE;
	if (isset($HTTP_GET_VARS['manufacturers_id']) && !empty($HTTP_GET_VARS['manufacturers_id'])) {
		$image = tep_db_query("select manufacturers_image, manufacturers_name as catname from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int) $HTTP_GET_VARS['manufacturers_id'] . "'");
		$image = tep_db_fetch_array($image);
		$catname = $image['catname'];
	} elseif ($current_category_id) {
		$image = tep_db_query("select c.categories_image, cd.categories_name as catname from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int) $current_category_id . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int) $languages_id . "'");
		$image = tep_db_fetch_array($image);
		$catname = $image['catname'];
		$smack_current_id = (int) $current_category_id;
	}
	?>

	<div class="boxTitle">
        <?php echo($search_in_posters == 1) ? tep_output_generated_category_path($current_category_id) : $catname; ?></div>
	<?php
	if ($smack_current_id != '0' && $smack_current_id != POSTERS_CATEGORY_ID) {
		if (tep_session_is_registered('customer_id')) {
			$check_query = tep_db_query("select count(*) as count from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int) $customer_id . "' and global_product_notifications = '1'");
			include(DIR_WS_MODULES . '/boxes/categories_notifications.php');
		}
	}


	?>

		<?php
// optional Product List Filter

		if ($is_poster){

			include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_POSTERS);
        }
		else{

			include(DIR_WS_MODULES . FILENAME_PRODUCT_LIST);
        }
		?>

	<!--</div>-->

	<?php
} else {
        $getBanners = tep_db_query("SELECT banners_id, banners_title, banners_url, banners_image, banners_group, banners_html_text, expires_impressions, expires_date, date_scheduled, date_added, date_status_change FROM banners");


    ?>


    <div class="row">
        <div class="col-md-12">
            <div class="navigation-banner-swiper">
                <div class="swiper-container swiper-container1" data-autoplay="5000" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="1">
                    <div class="swiper-wrapper swiper-wrapper1">
                        <?php
                        $i = 1;
                        while ( $images = tep_db_fetch_array($getBanners) ) {
                        ?>
                            <div class="swiper-slide active" data-val="0">
                                <div class=" slider-nav align-<?=$i;?>" style="height: 500px; background-image: url(<?php  echo 'http://alfurat-responsive:81/images/'. $images['banners_image'];?>);">
                                    <div class="clear"></div>
                                </div>
                            </div>

                        <?php
                        $i++;
                        }
                        ?>
                    </div>
                    <div class="clear"></div>
                    <div class="pagination">

                    </div>
                </div>
            </div>

            <div class="clear"></div>

        </div>

    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="information-blocks products-grid">
                <div class="tabs-container">
                    <div class="swiper-tabs tabs-switch">
                        <div class="title">Products</div>
                        <div class="list">
                            <a class="block-title tab-switcher active">Latest Releases</a>
                            <a class="block-title tab-switcher">Bestsellers</a>
                            <a class="block-title tab-switcher">Posters</a>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div>
                        <div class="tabs-entry">
                            <div class="row">
                            <?php
                            include(DIR_WS_MODULES . FILENAME_FEATURED_LIST);

                            ?>
                            </div>
                        </div>
                        <div class="tabs-entry">
                            <div class="row">
                             <?php   include(DIR_WS_MODULES . FILENAME_BESTSELLERS);
                             ?>
                            </div>
                        </div>
                        <div class="tabs-entry">
                            <div class="row">
                            <?php include(DIR_WS_MODULES . FILENAME_POSTERS);?>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

        <?php
            if ($oscTemplate->hasBlocks('boxes_column_left')) {
            ?>
                <div class="col-md-3">
                <?php echo $oscTemplate->getBlocks('boxes_column_left'); ?>
                </div>

         <?php
         }

          if ($oscTemplate->hasBlocks('boxes_column_right')) {
              ?>

              <div class="col-md-3">
                  <?php echo $oscTemplate->getBlocks('boxes_column_right'); ?>
              </div>

              <?php
          }




          echo pop_up();
?>













<?php
  //  include(DIR_WS_MODULES . FILENAME_BESTSELLERS);
?>


	<?php
	//     }
	?>
	<!-- Nivo Slider -->
	<?php //echo HEADING_TITLE;  //echo tep_customer_greeting();           ?>






</div>
    <?php
}

require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
