<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

// if the customer is not logged on, redirect them to the login page
if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
}

// if there is nothing in the customers cart, redirect them to the shopping cart page
if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}

// if no shipping method has been selected, redirect the customer to the shipping method selection page
if (!tep_session_is_registered('shipping')) {
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
}

// avoid hack attempts during the checkout procedure by checking the internal cartID
if (isset($cart->cartID) && tep_session_is_registered('cartID')) {
    if ($cart->cartID != $cartID) {
        tep_redirect(tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
    }
}

// Stock Check
if ((STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true')) {
    $products = $cart->get_products();
    for ($i = 0, $n = sizeof($products); $i < $n; $i++) {
        if (tep_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
            tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
            break;
        }
    }
}

// if no billing destination address was selected, use the customers own address as default
if (!tep_session_is_registered('billto')) {
    tep_session_register('billto');
    $billto = $customer_default_address_id;
} else {
// verify the selected billing address
    if ((is_array($billto) && empty($billto)) || is_numeric($billto)) {
        $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int) $customer_id . "' and address_book_id = '" . (int) $billto . "'");
        $check_address = tep_db_fetch_array($check_address_query);

        if ($check_address['total'] != '1') {
            $billto = $customer_default_address_id;
            if (tep_session_is_registered('payment'))
                tep_session_unregister('payment');
        }
    }
}

require(DIR_WS_CLASSES . 'order.php');
$order = new order;


if (!tep_session_is_registered('comments'))
    tep_session_register('comments');
if (isset($HTTP_POST_VARS['comments']) && tep_not_null($HTTP_POST_VARS['comments'])) {
    $comments = tep_db_prepare_input($HTTP_POST_VARS['comments']);
}

$total_weight = $cart->show_weight();
$total_count = $cart->count_contents();

// load all enabled payment modules
require(DIR_WS_CLASSES . 'payment.php');
$payment_modules = new payment;
$action = (isset($HTTP_GET_VARS['action']) ? $HTTP_GET_VARS['action'] : '');
if (tep_not_null($action)) {
    switch ($action) {
        case 'audi':
            //insert the transaction info to db and send the id to bank  number_format($order->info['total'],2) * 100
            $insert_sql_data = array(
                'amount' => number_format($order->info['total'],2),
                'customers_id' => (int) $customer_id,
                'date_added' => 'now()',
                'transaction_status' => 'w',
                'merchantID' => MODULE_PAYMENT_CYBS_SA_PROFILE_ID
            );
            tep_db_perform(TABLE_AUDI_TRANSACTIONS, $insert_sql_data);
            $transaction_id = tep_db_insert_id();
            $info_needed = array(
                'accessCode' => MODULE_PAYMENT_CYBS_SA_ACCESS_KEY,
                'merchTxnRef' => time(),
                'merchant' => MODULE_PAYMENT_CYBS_SA_PROFILE_ID,
                'orderInfo' => $transaction_id,
                'amount' =>  number_format($order->info['total'],2) * 100,
                'returnURL' => tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, 'action=paid','SSL',TRUE,FALSE)
            );
            $result = array();
            $result = $payment_modules->prepare_data_bank_audi($info_needed);
            echo "<script language=\"javascript\">alert('".CONFIRMATION_WARNING."');"
                . "top.location.href='https://onlinepayment.areeba.com/TPGWeb/payment/prepayment.action?" . $result['newHash'] . "'</script>";
            break;
    }
}
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_PAYMENT);

$breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<script type="text/javascript"><!--
    var selected;

    function selectRowEffect(object, buttonSelect) {
        if (!selected) {
            if (document.getElementById) {
                selected = document.getElementById('defaultSelected');
            } else {
                selected = document.all['defaultSelected'];
            }
        }

        if (selected)
            selected.className = 'moduleRow';
        object.className = 'moduleRowSelected';
        selected = object;

// one button is not an array
        if (document.checkout_payment.payment[0]) {
            document.checkout_payment.payment[buttonSelect].checked = true;
        } else {
            document.checkout_payment.payment.checked = true;
        }
    }

    function rowOverEffect(object) {
        if (object.className == 'moduleRow')
            object.className = 'moduleRowOver';
    }

    function rowOutEffect(object) {
        if (object.className == 'moduleRowOver')
            object.className = 'moduleRow';
    }
    //--></script>
<?php echo $payment_modules->javascript_validation(); ?>
<div>
    <div class="boxTitle"><?php echo HEADING_TITLE; ?></div>

    <?php echo tep_draw_form('checkout_payment', tep_href_link(FILENAME_CHECKOUT_CONFIRMATION, '', 'SSL'), 'post', 'onsubmit="return check_form();"', true); ?>

    <div class="conCon">

        <?php
        if (isset($HTTP_GET_VARS['payment_error']) && is_object(${$HTTP_GET_VARS['payment_error']}) && ($error = ${$HTTP_GET_VARS['payment_error']}->get_error())) {
            ?>

            <div class="textContent">
                <?php echo '<strong>' . tep_output_string_protected($error['title']) . '</strong>'; ?>

                <p class="messageStackError"><?php echo tep_output_string_protected($error['error']); ?></p>
            </div>

            <?php
        }
        ?>

        <h2><?php echo TABLE_HEADING_BILLING_ADDRESS; ?></h2>

        <div class="textContent">
            <div class="ui-widget infoBoxContainer" >
                <p><?php echo TEXT_SELECTED_BILLING_DESTINATION; ?></p>

                <div class="ui-widget-content infoBoxContents">
                    <?php echo tep_address_label($customer_id, $billto, true, ' ', '<br />'); ?>
                </div>
            </div>


        </div>
        <div style="clear: both;"></div>
        <div class="align-left"><?php echo tep_draw_button(IMAGE_BUTTON_CHANGE_ADDRESS, 'home', tep_href_link(FILENAME_CHECKOUT_PAYMENT_ADDRESS, '', 'SSL')); ?></div>

        <div style="clear: both;"></div>

        <h2><?php echo TABLE_HEADING_PAYMENT_METHOD; ?></h2>

        <?php
        $selection = $payment_modules->selection();

        if (sizeof($selection) > 1) {
            ?>

            <div class="textContent">
                <div >
                    <?php echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?>
                </div>

                <?php echo TEXT_SELECT_PAYMENT_METHOD; ?>
            </div>

            <?php
        } else {
            ?>

            <div class="textContent">
                <?php echo TEXT_ENTER_PAYMENT_INFORMATION; ?>
            </div>

            <?php
        }
        ?>

        <div class="textContent">

            <?php
            $radio_buttons = 0;
            for ($i = 0, $n = sizeof($selection); $i < $n; $i++) {
                ?>

                <table border="0" width="100%" cellspacing="0" cellpadding="2">

                    <?php
                    if (($selection[$i]['id'] == $payment) || ($n == 1)) {
                        echo '      <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
                    } else {
                        echo '      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
                    }
                    ?>

                    <td  align="left" width="95%"><strong><?php echo $selection[$i]['module']; ?></strong></td>
                    <td align="right">

                        <?php
                        if (sizeof($selection) > 1) {
                            echo tep_draw_radio_field('payment', $selection[$i]['id'], ($selection[$i]['id'] == $payment));
                        } else {
                            echo tep_draw_hidden_field('payment', $selection[$i]['id']);
                        }
                        ?>

                    </td>
                    </tr>

                    <?php
                    if (isset($selection[$i]['error'])) {
                        ?>

                        <tr>
                            <td colspan="2"><?php echo $selection[$i]['error']; ?></td>
                        </tr>

                        <?php
                    } elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
                        ?>

                        <tr>
                            <td colspan="2"><table border="0" cellspacing="0" cellpadding="2">

                                    <?php
                                    for ($j = 0, $n2 = sizeof($selection[$i]['fields']); $j < $n2; $j++) {
                                        ?>

                                        <tr>
                                            <td><?php echo $selection[$i]['fields'][$j]['title']; ?></td>
                                            <td><?php echo $selection[$i]['fields'][$j]['field']; ?></td>
                                        </tr>

                                        <?php
                                    }
                                    ?>

                                </table></td>
                        </tr>

                        <?php
                    }
                    ?>

                </table>

                <?php
                $radio_buttons++;
            }
            ?>

        </div>

        <h2><?php echo TABLE_HEADING_COMMENTS; ?></h2>

        <div class="textContent">
            <?php echo tep_draw_textarea_field('comments', 'soft', '60', '5', '', 'class="form-textarea"'); ?>
        </div>

        <div class="textContent">
            <div class="progress_div">
                <div id="coProgressBar" style="height: 5px;"></div>

                <table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                        <td align="center" width="33%" class="checkoutBarFrom"><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '" class="checkoutBarFrom">' . CHECKOUT_BAR_DELIVERY . '</a>'; ?></td>
                        <td align="center" width="33%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
                        <td align="center" width="33%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
                    </tr>
                </table>
            </div>

            <button type="submit" class="btn-green btn btn-primary"><?php echo CONTINUE3;?></button>
        </div>
    </div>

    <script type="text/javascript">
        $('#coProgressBar').progressbar({
            value: 66
        });
    </script>

    </form>

    <?php
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
