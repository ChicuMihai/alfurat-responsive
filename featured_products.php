<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
$current_page = FILENAME_DEFAULT;   // Nivo Slider

require(DIR_WS_INCLUDES . 'template_top.php');

?>


		<?php
		$featured_products_query_raw = "select p2c.categories_id,p.products_available,p.products_id, pd.products_name, p.products_image, p.products_price, p.products_tax_class_id, IF(s.status, s.specials_new_products_price, NULL) as specials_new_products_price, p.products_date_added,p.products_date_released,p.products_isbn, GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author , GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ', ')  as author_ids
   from " . TABLE_PRODUCTS . " p 
	   inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p2c.products_id = p.products_id
   left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.products_id = pd.products_id and pd.language_id = '" . $languages_id . "'
   left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id
  		 left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id
				 left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id  
   where p.products_status = '1'   
and YEAR(p.products_date_released)>='" . date("Y") . "'   
	and DATE_ADD( products_last_modified, INTERVAL 60
DAY ) >='" . date('Y-m-d H:i:s') . "' and p2c.categories_id < '" . POSTERS_CATEGORY_ID . "' group by p.products_id 
order by p.products_date_added  DESC ,p.products_date_released  DESC, pd.products_name";

if (!empty($featured_products_query_raw)){
    $listing_split = new splitPageResults($featured_products_query_raw, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
}



if ($listing_split->number_of_rows > 0) {
    display_books_listening($listing_split, $languages_id,$HTTP_GET_VARS,$current_category_id,$cPath,$is_poster);
} else {
    ?>
    <p><?php echo($filtered > 0) ? TEXT_NO_PRODUCTS : TEXT_CHOOSE_FILTERS; ?></p>
    <?php
}



require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>