<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');
//latest articles
$articles_query = "select p.articles_id,articles_date_added,articles_name  from " . TABLE_ARTICLES . " p, " . TABLE_ARTICLES_DESCRIPTION . " pd where p.articles_status = '1' "
    . " and pd.articles_id = p.articles_id and pd.language_id = '" . (int) $languages_id . "'";
if (isset($HTTP_GET_VARS['articles_id'])) {
    $articles_query .= " and p.articles_id != '" . (int) $HTTP_GET_VARS['articles_id'] . "'";
}
$articles_query .= " limit 6";
$articles_query = tep_db_query($articles_query);
$articles = tep_db_fetch_all($articles_query);
if (!isset($HTTP_GET_VARS['articles_id'])) {
    tep_redirect(tep_href_link(FILENAME_DEFAULT));
}
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ARTICLE_INFO);
$article_check_query = tep_db_query("select count(*) as total from " . TABLE_ARTICLES . " p, " . TABLE_ARTICLES_DESCRIPTION . " pd where p.articles_status = '1' and p.articles_id = '" . (int) $HTTP_GET_VARS['articles_id'] . "' "
    . "and pd.articles_id = p.articles_id and pd.language_id = '" . (int) $languages_id . "'");
$article_check = tep_db_fetch_array($article_check_query);
if ($article_check['total'] > 0) {
    $article_info_query = tep_db_query("select  "
        . "p.articles_id, pd.articles_name, pd.articles_description, p.articles_image, pd.articles_url, p.articles_date_added from " . TABLE_ARTICLES . " p"
        . " left join  " . TABLE_ARTICLES_DESCRIPTION . " pd  on pd.articles_id = p.articles_id  where p.articles_status = '1' and p.articles_id = '" . (int) $HTTP_GET_VARS['articles_id'] . "'     and pd.language_id = '" . (int) $languages_id . "'");

    $article_info = tep_db_fetch_array($article_info_query);
    tep_db_query("update " . TABLE_ARTICLES_DESCRIPTION . " set articles_viewed = articles_viewed+1 where articles_id = '" . (int) $HTTP_GET_VARS['articles_id'] . "' and language_id = '" . (int) $languages_id . "'");
}
require(DIR_WS_INCLUDES . 'template_top.php');
if (count($articles) > 0) {
?>
<div class="article-container style-1">
    <h3><?php echo TEXT_LATEST_ARTICLES; ?></h3>
    <br>
    <?php
    foreach ($articles as $value) {
        echo '<div class="articles "><a class="records" href="', tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $value['articles_id']), '">&#9679; ', $value['articles_name'], ' ', tep_date_short($value['articles_date_added']), '</a></div>';
    }
    ?>



    <div class="clear mb--40"></div>
    <?php
    }
    if ($article_check['total'] < 1) {
    ?>
    <div class="contentContainer">
        <div class="contentText">
            <?php echo TEXT_PRODUCT_NOT_FOUND; ?>
        </div>
        <?php
        /*		 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
        $rtl = stripos(HTML_PARAMS, 'dir="rtl"');
        if ($rtl !== false) {
        ?>
        <div class="left">
            <?php } else { ?>
            <div class="right">
                <?php } /*				 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */ ?>
                <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?>
            </div>
        </div>

        <?php
        } else {
        ?>
        <div>
            <div class="boxCatTitle <?php echo(count($articles) == 0) ? 'mt-0' : '' ?>"><?php
                echo $article_info['articles_name'];
                ?></div>
        </div>
        <div class="contentContainer">
            <?php
            if (tep_not_null($article_info['articles_image'])) {
            $photoset_layout = '1';
            $pi_query = tep_db_query("select image, htmlcontent from " . TABLE_ARTICLES_IMAGES . " where articles_id = '" . (int) $article_info['articles_id'] . "' order by sort_order");
            $pi_total = tep_db_num_rows($pi_query);

            if ($pi_total > 0) {
            $pi_sub = $pi_total - 1;

            while ($pi_sub > 5) {
                $photoset_layout .= 5;
                $pi_sub = $pi_sub - 5;
            }

            if ($pi_sub > 0) {
                $photoset_layout .= ($pi_total > 5) ? 5 : $pi_sub;
            }
            ?>

        <?php
        /*						 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
        if (stripos(HTML_PARAMS, 'dir="rtl"') !== false) {
        ?>
            <div id="piGal" style="float: left ;">
                <?php } else { ?>
                <div id="piGal" style="float: right;">
                    <?php
                    }
                    /*								 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */
                    ?>

                    <?php
                    $pi_counter = 0;
                    $pi_html = array();

                    while ($pi = tep_db_fetch_array($pi_query)) {
                        $pi_counter++;

                        if (tep_not_null($pi['htmlcontent'])) {
                            $pi_html[] = '<div id="piGalDiv_' . $pi_counter . '">' . tep_image(DIR_WS_ARTICLES . $pi['image'], '', '', '', 'id="piGalImg_' . $pi_counter . '"') . '<br><h1>' . $pi['htmlcontent'] . '</h1></div>';
                        }

                        echo tep_image(DIR_WS_ARTICLES . $pi['image'], '', '', '', ' id="piGalImg_' . $pi_counter . '"');
                    }
                    ?>

                </div>

                <?php
                if (!empty($pi_html)) {
                    echo '    <div style="display: none;">' . implode('', $pi_html) . '</div>';
                }
                } else {
                ?>

                <?php
                /*							 * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
                if (stripos(HTML_PARAMS, 'dir="rtl"') !== false) {
                ?>
                <div class="productImgDiv" <?php echo($is_magazine) ? 'id="piGal"' : ''; ?> style="float: left ;">
                    <?php } else { ?>
                    <div id="piGal" style="float: right;">
                        <?php } /*									 * * EOF Arabic for osc2.3.1 Ver.1.0 ** */ ?>
                        <?php echo tep_image(DIR_WS_IMAGES . $article_info['articles_image'], addslashes($article_info['articles_name']), '', '', 'class="mr-15 product_img "'); ?>
                    </div>

                    <?php
                    }
                    }
                    if ($pi_total == 2)
                        $photoset_layout = 12;
                    ?>
                    <?php
                    if (tep_date_long($article_info['articles_date_added'])) {
                        echo'<b><span class="green">';
                        echo TEXT_RELEASED;
                        echo':</span>' . tep_date_short($article_info['articles_date_added']) .'</b>'.'';
                    }
                    if (!empty($article_info['articles_description'])) {
                        echo'	<div><p>' . stripslashes($article_info['articles_description']) . '</p></div>';
                    }
                    $margin = (stripos(HTML_PARAMS, 'dir="rtl"') !== false) ? 'margin-right' : 'margin-left';
                    ?>
                    <script type="text/javascript">
                        $(function () {
                            $('#piGal').css({
                                'visibility': 'hidden',
                                '<?php echo $margin; ?>': '15px'
                            });

                            $('#piGal').photosetGrid({
                                layout: '<?php echo $photoset_layout; ?>',
                                width: '250px',
                                highresLinks: true,
                                rel: 'pigallery',
                                onComplete: function () {
                                    $('#piGal').css({'visibility': 'visible'});

                                    $('#piGal a').colorbox({
                                        maxHeight: '120%',
                                        maxWidth: '120%',
                                        rel: 'pigallery'
                                    });

                                    $('#piGal img').each(function () {
                                        var imgid = $(this).attr('id').substring(9);

                                        if ($('#piGalDiv_' + imgid).length) {
                                            $(this).parent().colorbox({inline: true, href: "#piGalDiv_" + imgid});
                                        }
                                    });
                                }
                            });
                        });
                    </script>

                    <div class="buttonSet left">
                        <a class="button style-17" href="<?php echo tep_href_link(FILENAME_ARTICLES);?>">View More</a>
                    </div>
                    <div style="clear: both;"></div>






                </div>



            </div>



            <?php
            }

            require(DIR_WS_INCLUDES . 'template_bottom.php');
            require(DIR_WS_INCLUDES . 'application_bottom.php');
            ?>
