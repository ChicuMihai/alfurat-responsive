<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONDITIONS);

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CONDITIONS));

require(DIR_WS_INCLUDES . 'template_top.php');
?><div class="content-center " style="padding-top: inherit;">
    <div class="boxTitle"><?php echo HEADING_TITLE; ?></div>
    <div class="conCon"></br>
        <div class="textContent">
            <?php echo CONDITIONS_TEXT; ?>
            <div class="buttonSet">
                <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?></span>
            </div>
        </div></div></div>
<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>