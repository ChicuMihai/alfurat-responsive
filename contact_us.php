<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONTACT_US);
// start modification for reCaptcha
require_once('includes/classes/recaptchalib.php');
// end modification for reCaptcha
if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'send') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    $error = false;

    $name = tep_db_prepare_input($HTTP_POST_VARS['name']);
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email']);
    $enquiry = tep_db_prepare_input($HTTP_POST_VARS['enquiry']);

    $actionRecorder = new actionRecorder('ar_contact_us', (tep_session_is_registered('customer_id') ? $customer_id : null), $name);
    if (!$actionRecorder->canPerform()) {
        $error = true;

        $actionRecorder->record(false);

        $messageStack->add('contact', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES') ? (int) MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES : 15)));
    }
// start modification for reCaptcha
    // the response from reCAPTCHA
    $resp = null;

    // was there a reCAPTCHA response?
    $resp = recaptcha_check_answer(RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

    // temp fix until new template is done.
    // this object overrides the response data
    // that is being fetched by Google's reCaptcha.
    // comment out the line below if you want the captcha to work like normal.

    $resp = (object) array('is_valid' => true);

    if (tep_validate_email($email_address) & ($resp->is_valid)) {
        tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, EMAIL_SUBJECT, $enquiry, $name, $email_address);
        $actionRecorder->record();
        tep_redirect(tep_href_link(FILENAME_CONTACT_US, 'action=success'));
    } else {
        if (!tep_validate_email($email_address)) {
            $error = true;
            $messageStack->add('contact', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
        }
        if (!$resp->is_valid) {
            $error = true;
            $messageStack->add('contact', ENTRY_SECURITY_CHECK_ERROR);
        }
    }

// end modification for reCaptcha
}

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CONTACT_US));

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<div class="information-blocks">
    <div class="map-box type-2">
        <div id="map-canvas"style="overflow: hidden;"></div>
    </div>
    <div class="row">

        <div class="col-md-4 information-entry">
            <h3 class="block-title main-heading">الفرات للنشر والتوزيع </h3>
            <div class="article-container style-1">
                <p>
         <span class="green">
          <strong>الفرات للنشر والتوزيع</strong>
        </span>
                    <br><br>
                    <strong>قسم المبيعات والتوزيع</strong>
                    <br>
                    رأس بيروت  –
                    شارع عبلا
                    <br>
                    بنايـة  بخعازي
                    –  بيـروت  -   لبنـان
                    <br>
                    تلفاكس:   750554 -1-00961
                    <br>
                    للإستفسارات:
                    <a href="mailto:sales@alfurat.com">sales@alfurat.com</a>
                    <br>
                    <br>
                    <strong>الفرات للنشر والتوزيع / المكتبة</strong>
                    <br>
                    شارع الحمراء - بناية رسامني
                    <br>
                    ص.ب: 6435 - 113 بيروت - لبنـان
                    <br>
                    هاتف: 750054   <br>
                    فاكس: 750053 - 1 -00961 <br>
                    للإستفسارات:
                    <a href="mailto:alfurat@alfurat.com">alfurat@alfurat.com</a>
                </p>
            </div>
            <div class="share-box detail-info-entry">
                <div class="title">Share in social media</div>
                <div class="socials-box">
                    <a href="https://www.facebook.com/pg/Alfuratdistributor/community/"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                    <a href="#"><i class="fa fa-linkedin"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <?php
        if ($messageStack->size('contact') > 0) {
            echo $messageStack->output('contact');
        }
        ?>



        <?php
        if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'success')) {
            ?>
            <p><?php echo TEXT_SUCCESS; ?></p>
            <div style="float: right;">
                <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?>
            </div>
            <?php
        } else {
        ?>
        <?php echo tep_draw_form('contact_us', tep_href_link(FILENAME_CONTACT_US, 'action=send'), 'post', '', true); ?>
        <div class="col-md-8 information-entry">
            <h3 class="block-title main-heading">Drop Us A Line</h3>
            <form>
                <div class="row">
                    <div class="col-sm-6">
                        <label>Your Full Name <span>*</span></label>
                        <input class="simple-field" type="text" placeholder="Your name (required)" required="" value="" name="name">
                        <div class="clear"></div>
                    </div>
                    <div class="col-sm-12">
                        <label>Your Email <span>*</span></label>
                        <input class="simple-field" type="email" placeholder="Your email address (required)" required="" value="" name="email">
                        <label>Your Message <span>*</span></label>
                        <textarea class="simple-field" placeholder="Your message content (required)" required=""name="enquiry"></textarea>
                        <div class="button style-10">Send Message<input type="submit" value=""></div>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <?php }
    ?>








    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkUT2kWga2XmklQMvQ4wSHJiv_hEmAJF4 "></script>
    <script src="js/map.js"></script>
    <?php
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
