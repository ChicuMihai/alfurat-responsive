<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

if (!isset($HTTP_GET_VARS['products_id'])) {
    tep_redirect(tep_href_link(FILENAME_DEFAULT));
}

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

$product_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and pd.products_id = p.products_id ");//and pd.language_id = '" . (int) $languages_id . "'
$product_check = tep_db_fetch_array($product_check_query);
if ($product_check['total'] > 0) {
    //bring category id
    $product_category_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id='" . (int) $HTTP_GET_VARS['products_id'] . "'");
    $category_info = tep_db_fetch_array($product_category_query);
    $is_magazine = 0;
    $is_poster = 0;
    $is_book = 1;
    if ($category_info['categories_id'] == MAGAZINES_CATEGORY_ID) {
        $is_magazine = 1;
        $is_book = 0;
    } else if ($category_info['categories_id'] >= POSTERS_CATEGORY_ID) {
        $is_poster = 1;
        $is_book = 0;
    }

    if ($is_book) {
        $product_info_query = tep_db_query("select GROUP_CONCAT(DISTINCT pub.publishers_name  SEPARATOR '&#1548; ') as publisher,"
            . " GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author, GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ',')  as author_ids"
            . ", GROUP_CONCAT(DISTINCT trans.manufacturers_id  SEPARATOR ',')  as translator_ids,GROUP_CONCAT(DISTINCT trans.manufacturers_name  SEPARATOR '&#1548;') as translator, p.products_id, pd.products_name, pd.products_sub_title, pd.products_description, p.products_model, p.products_edition, p.products_isbn, p.products_weight,products_cover_type ,products_series,products_nb_in_series,product_available_volumes,products_page_nb, p.products_size, p.products_quantity,p.products_available, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.products_date_released from " . TABLE_PRODUCTS . " p"
            . " left join  " . TABLE_PRODUCTS_DESCRIPTION . " pd  on pd.products_id = p.products_id "
            . " left join " . TABLE_PRODUCTS_TO_PUBLISHERS . " m on p.products_id = m.products_id "
            . " left join " . TABLE_PUBLISHERS . " pub on pub.publishers_id = m.publishers_id "
            . " left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id "
            . " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id "
            . "left join " . TABLE_PRODUCTS_TO_TRANSLATORS . " t on p.products_id = t.products_id "
            . "left join " . TABLE_MANUFACTURERS . " trans on t.manufacturers_id = trans.manufacturers_id "
            . "  where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' "); //and pd.language_id = '" . (int) $languages_id . "'
        $productType = 'Book';
    } else {
        $product_info_query = tep_db_query("select GROUP_CONCAT(DISTINCT pub.name  SEPARATOR '&#1548; ') as publisher, GROUP_CONCAT(DISTINCT auth.name  SEPARATOR '&#1548; ')  as author, p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_edition, p.products_isbn, p.products_weight,products_cover_type,products_page_nb, p.products_size, p.products_quantity,p.products_available, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.products_date_released from " . TABLE_PRODUCTS . " p"
            . " left join  " . TABLE_PRODUCTS_DESCRIPTION . " pd  on pd.products_id = p.products_id "
            . " left join " . TABLE_PRODUCTS_TO_DIRECTORS . " m on p.products_id = m.products_id "
            . " left join " . TABLE_DIRECTORS . " pub on pub.id = m.directors_id "
            . " left join " . TABLE_PRODUCTS_TO_ACTORS . " a on  p.products_id = a.products_id "
            . " left join " . TABLE_ACTORS . " auth on  a.actors_id = auth.id "
            . "  where p.products_status = '1' and p.products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' "); //and pd.language_id = '" . (int) $languages_id . "'
        $productType = 'Poster';
    }
    $product_info = tep_db_fetch_array($product_info_query);

    tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int) $HTTP_GET_VARS['products_id'] . "' and language_id = '" . (int) $languages_id . "'");

    if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
        $products_price = '<del>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</p></b>';
    } else {
        $products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
    }

    if (tep_not_null($product_info['products_model'])) {
        $products_name = $product_info['products_name'] . '';
    } else {
        $products_name = $product_info['products_name'];
    }
}
require(DIR_WS_INCLUDES . 'template_top.php');

if ($product_check['total'] < 1) {
    ?>


    <div class="contentText">
        <?php echo TEXT_PRODUCT_NOT_FOUND; ?>
    </div>

    <?php
    /*       * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
    $rtl = stripos(HTML_PARAMS, 'dir="rtl"');
    if ($rtl !== false) {
        ?>
        <div class="left">
    <?php } else { ?>
        <div class="right">
    <?php } /*               * * EOF Arabic for osc2.3.1 Ver.1.0 ** */ ?>
    <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link(FILENAME_DEFAULT)); ?>
    </div>
    </div>

    <?php
} else {
    echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_PRODUCT_INFO, tep_get_all_get_params(array('action')) . 'action=add_product'));
    ?>
<div class="row" >
    <div class="col-sm-5 col-md-4 col-lg-5 information-entry">
        <div class="product-preview-box">
            <?php
            if (tep_not_null($product_info['products_image'])) {
            $photoset_layout = '1';

            $pi_query = tep_db_query("select image, htmlcontent from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int) $product_info['products_id'] . "' order by sort_order");
            $pi_total = tep_db_num_rows($pi_query);

            if ($pi_total > 0) {
                $pi_sub = $pi_total - 1;

                while ($pi_sub > 5) {
                    $photoset_layout .= 5;
                    $pi_sub = $pi_sub - 5;
                }

                if ($pi_sub > 0) {
                    $photoset_layout .= ($pi_total > 5) ? 5 : $pi_sub;
                }
                ?>

<div style="height: 400px">
                <?php
                $pi_counter = 0;
                $pi_html = array();

                while ($pi = tep_db_fetch_array($pi_query)) {
                    $pi_counter++;

                    if (tep_not_null($pi['htmlcontent'])) {
                        $pi_html[] =  tep_image(DIR_WS_IMAGES . $pi['image']);
                    }

                    echo tep_image(DIR_WS_IMAGES . $pi['image']. 'style="height: 100%; width:100%; margin-right: auto; margin-left: auto; display: block;"'  );
                }
                ?>



                <?php
                if (!empty($pi_html)) {
                    echo '    <div style="display: none;">' . implode('', $pi_html) . '</div>';
                }
            } else {
            ?>

            <?php
            /*                           * * BOF Arabic for osc2.3.1 Ver.1.0 ** */
            if (stripos(HTML_PARAMS, 'dir="rtl"') !== false) {
                ?>
                
                 <div><?php echo($is_magazine) ? 'id="piGal"' : ''; ?></div></div>
            <?php } else { ?>
            
                <?php } 
                 /*                                   * * EOF Arabic for osc2.3.1 Ver.1.0 ** */ ?>
                 <div style="height: 400px">
                <?php
             
                       if (file_exists(DIR_WS_IMAGES . $product_info['products_image'])) {

                    
                 
             echo tep_image(DIR_WS_IMAGES . $product_info['products_image'], $product_info['products_name'], '', '', $image_size[1] . '  style="height: 100%; margin-right: auto; margin-left: auto; display: block;"'  );   
                }else {

                echo tep_image(DIR_WS_IMAGES .'/default_img.gif', 'DEFAULT', '', '', $image_size[1] . '  style="height: 100%; margin-right: auto; margin-left: auto; display: block;"') ;
                    
                }
                ?>
            </div>
        </div>
        <?php
        }
        }
        if ($is_magazine) {
            if ($pi_total == 2)
                $photoset_layout = 12;
            ?>
            
        <?php } ?>
    </div>
   

    <div class="col-sm-7 col-md-4 information-entry">
        <div class="product-detail-box">
            <div class="product-title"><?php
                echo $products_name;
                echo (!empty($product_info['products_sub_title'])) ? ' (' . $product_info['products_sub_title'] . ')' : '';
                ?></div>
        


        <div class="product-description prod detail-info-entry">

            <?php
            if (!$is_magazine) {
                if (!empty($product_info['publisher']) ||
                    !empty($product_info['products_date_released']) ||
                    !empty($product_info['author']) ||
                    !empty($product_info['translator']))
                    echo'<p>';
                if (!empty($product_info['publisher'])) {
                    echo '<p><b>';
                    echo ($is_book) ? TEXT_PUBLISHEER : TEXT_DIRECTOR;
                    echo':</p></b> ' . $product_info['publisher'];
                }
                if (tep_date_long($product_info['products_date_released'])) {
                    echo'<p><b>';
                    echo ($is_poster) ? TEXT_DATE_FILM : TEXT_RELEASED;
                    echo':</p></b> ' . tep_date_get_year($product_info['products_date_released']) . '</p>';
                }
                ?>
                <?php
                $authors = ($is_book) ? tep_get_authors_with_links($product_info['author'], $product_info['author_ids']) : $product_info['author'];
                if (!empty($product_info['translator']) || !empty($product_info['author'])) {
                    echo '<p>';
                }
                if (!empty($product_info['author'])) {
                    echo '<p><b>';
                    echo ($is_book) ? TEXT_AUTHOR : TEXT_ACTORS;
                    echo':</p></b> ' . $authors . '&nbsp;';
                }
                ?>
                <?php if (!empty($product_info['translator'])){
                    $authors = ($is_book) ? tep_get_translators_with_links($product_info['translator'], $product_info['translator_ids']) : $product_info['translator'];
                    echo'<p><b>' . TEXT_TRANSLATOR . ':</p></b> ' . $authors. '&nbsp;';
                } ?></p>
                <?php if (!empty($product_info['products_isbn'])) echo'<p><b>' . TEXT_ISBN . ':</p></b>' . $product_info['products_isbn']; ?>

                <?php
                if (!empty($product_info['products_edition']) || !empty($product_info['products_page_nb']) || !empty($product_info['products_size']) || !empty($product_info['products_cover_type']))
                    echo'<p>';
                if (!empty($product_info['products_edition']))
                    echo'<b>' . TEXT_EDITION . ':</b> ' . $product_info['products_edition'] . '&#1548; ';
                ?>
                <?php //if (!empty($product_info['products_weight']) && $product_info['products_weight'] > 0) echo'<p class="parBgGray"><p><b>' . TEXT_WEIGHT . ':</p></b> ' . $product_info['products_weight'] . TEXT_WEIGHT_GRAM . '</p>'; ?>
                <?php if (!empty($product_info['products_page_nb'])) echo'<b>' . TEXT_PAGE_NB . ':</b> ' . $product_info['products_page_nb'] . '&#1548;'; ?>
                <?php
                if (!empty($product_info['products_size']))
                    echo'<b>' . TEXT_SIZE . ':</b> <span dir="ltr">' . $product_info['products_size'] . ' cm </b> &#1548;';
                ?>
                <?php
                if (!empty($product_info['products_cover_type'])) {
                    echo'<b>' . TEXT_COVER_TYPE . ':</b>';
                    echo ($product_info['products_cover_type'] == 'normal') ? TEXT_COVER_TYPE_NORMAL : TEXT_COVER_TYPE_ARTISTIC;
                }
                echo'</p>';
                ?>
                <?php
                if (!empty($product_info['products_series']) || !empty($product_info['products_nb_in_series']) || !empty($product_info['product_available_volumes']))
                    echo'<p class="parBgGray">';
                if (!empty($product_info['products_series']))
                    echo'<b>' . TEXT_SERIES . ':</b> ' . $product_info['products_series'] . '&#1548; ';
                ?>
                <?php
                if (!empty($product_info['products_nb_in_series']))
                    echo'<b>' . TEXT_SERIES_NUMBER . ':</b> ' . $product_info['products_nb_in_series'] . '&#1548;';
                if (!empty($product_info['product_available_volumes']))
                    echo'<p><b>' . TEXT_AVAILABLE_VOLUME . ':</b> ' . $product_info['product_available_volumes'].'</p>';
                echo'</p>';
                ?>
                </p>
                <?php
            }else {
                echo'<p class="parBgGray">';
                if (tep_date_long($product_info['products_date_released'])) {
                    echo'<p><b>';
                    echo TEXT_RELEASED;
                    echo':</p></b> ' . tep_date_get_year($product_info['products_date_released']) . '</p>';
                }
                if (!empty($product_info['products_description'])) {
                    echo'   <div><h1 class="text-left"> ';
                    echo TEXT_MAGAZINES_DESCRIPTION;
                    echo'</h1><p>' . stripslashes($product_info['products_description']) . '</p></div>';
                }
            }
            ?>

        </div>
        <div class="price detail-info-entry">
            <div class="current"><?php echo $products_price; ?></div>
            <?php
            if ($product_info['products_date_available'] > date('Y-m-d H:i:s')) {
                ?>

                <p style="text-align: center;"><?php echo sprintf(TEXT_DATE_AVAILABLE, tep_date_long($product_info['products_date_available'])); ?></p>

                <?php
            }
            ?>


        </div>
        <?php
        if ($product_info['products_available'] * 1 == 1) {
            echo tep_draw_hidden_field('products_id', $product_info['products_id']) .'<div class="button style-10">Add To Cart<input type="submit" name="cart"></div>';
        } else {
            echo '<p>' . TEXT_BOOK_NOT_AVAILABLE . '</p>';
        }
        ?>
        </div>

    </div>
   <div class="col-md-4 col-lg-3 information-entry product-sidebar">
                <?php echo $oscTemplate->getBlocks('boxes_column_left'); ?>
  </div>
 </form>
</div>
   <div class="share-box detail-info-entry">
                                <div class="title">Share in social media</div>
                                <div class="socials-box">
                                    <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/pg/Alfuratdistributor/community/"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </div>
                                <div class="clear"></div>
                            </div>


    <?php
}

require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
