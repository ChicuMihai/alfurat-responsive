<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
}

// needs to be included earlier to set the success message in the messageStack
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_EDIT);

if (isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    if (ACCOUNT_GENDER == 'true') $gender = tep_db_prepare_input($HTTP_POST_VARS['gender']);
    $firstname = tep_db_prepare_input($HTTP_POST_VARS['firstname']);
    $lastname = tep_db_prepare_input($HTTP_POST_VARS['lastname']);
    if (ACCOUNT_DOB == 'true') $dob = tep_db_prepare_input($HTTP_POST_VARS['dob']);
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
    $telephone = tep_db_prepare_input($HTTP_POST_VARS['telephone']);
    $fax = tep_db_prepare_input($HTTP_POST_VARS['fax']);

    $error = false;

    if (ACCOUNT_GENDER == 'true') {
        if ( ($gender != 'm') && ($gender != 'f') ) {
            $error = true;

            $messageStack->add('account_edit', ENTRY_GENDER_ERROR);
        }
    }

    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_FIRST_NAME_ERROR);
    }

    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_LAST_NAME_ERROR);
    }

    if (ACCOUNT_DOB == 'true') {
        $dob=date('m-d-Y', strtotime($dob));

        if ((strlen($dob) < ENTRY_DOB_MIN_LENGTH) || (!empty($dob) && (!is_numeric(tep_date_raw($dob)) || !@checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4))))) {
            $error = true;

            $messageStack->add('account_edit', ENTRY_DATE_OF_BIRTH_ERROR);
        }
    }

    if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR);
    }

    if (!tep_validate_email($email_address)) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    }

    $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "' and customers_id != '" . (int)$customer_id . "'");
    $check_email = tep_db_fetch_array($check_email_query);
    if ($check_email['total'] > 0) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
    }

    if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;

        $messageStack->add('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR);
    }

    if ($error == false) {
        $sql_data_array = array('customers_firstname' => $firstname,
            'customers_lastname' => $lastname,
            'customers_email_address' => $email_address,
            'customers_telephone' => $telephone,
            'customers_fax' => $fax);

        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

        $sql_data_array = array('entry_firstname' => $firstname,
            'entry_lastname' => $lastname);

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$customer_default_address_id . "'");

// reset the session variables
        $customer_first_name = $firstname;

        $messageStack->add_session('account', SUCCESS_ACCOUNT_UPDATED, 'success');

        tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
    }
}

$account_query = tep_db_query("select customers_gender, customers_firstname, customers_lastname, customers_dob, customers_email_address, customers_telephone, customers_fax from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
$account = tep_db_fetch_array($account_query);

$breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'));

require(DIR_WS_INCLUDES . 'template_top.php');
require('includes/form_check.js.php');
?>
<div class="boxTitle"><?php echo HEADING_TITLE; ?></div>

<?php
if ($messageStack->size('account_edit') > 0) {
    echo $messageStack->output('account_edit');
}
?>

<?php echo tep_draw_form('account_edit', tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL'), 'post', 'onsubmit="return check_form(account_edit);"', true) . tep_draw_hidden_field('action', 'process'); ?>
<div class="conCon">
    <div>
        <div class="inputRequirement left"><?php echo FORM_REQUIRED_INFORMATION; ?>&nbsp;</div>

        <h2><?php echo MY_ACCOUNT_TITLE; ?></h2>
    </div>
</div>
<div class="information-blocks">
    <div class="row">
        <div class="col-sm-9 information-entry">
            <div class="row">

                <div class="col-xs-6">
                    <div style="margin-bottom: 10px;">
                        <label><?php echo ENTRY_FIRST_NAME; ?> <span>*</span></label>
                        <input type="text" style="width: 100%;" class="form-control" placeholder="<?php echo $account['customers_firstname'];?>" name="firstname">

                    </div>
                </div>

                <div class="col-xs-6">
                    <div style="margin-bottom: 10px;">
                        <label><?php echo ENTRY_LAST_NAME; ?> <span>*</span></label>
                        <input type="text" style="width: 100%;" class="form-control" placeholder="<?php echo $account['customers_lastname'];?>" name="lastname">
                    </div>
                </div>
                <?php
                if (ACCOUNT_DOB == 'true') {
                    ?>
                    <div class="col-xs-6">
                        <div style="margin-bottom: 15px;">
                            <label><?php echo ENTRY_DATE_OF_BIRTH; ?><span>*</span></label>
                            <input type="text" style="width: 100%;" class="form-control" placeholder="<?php echo tep_date_short($account['customers_dob']);?>" name="dob"id="dob">
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-xs-6">
                    <label><?php echo ENTRY_EMAIL_ADDRESS; ?><span>*</span></label>
                    <div style="margin-bottom: 15px;">
                        <input type="text" style="width: 100%;" class="form-control" placeholder="<?php echo $account['customers_email_address'];?>" name="email_address">
                    </div>
                </div>
                <?php
                if (ACCOUNT_GENDER == 'true') {
                    if (isset($gender)) {
                        $male = ($gender == 'm') ? true : false;
                    } else {
                        $male = ($account['customers_gender'] == 'm') ? true : false;
                    }
                    $female = !$male;
                    ?>

                    <div class="col-xs-12">
                        <div style="margin-bottom: 15px;">
                            <h3 style="margin-bottom: 10px;">
                                <strong><?php echo ENTRY_GENDER; ?></strong>
                            </h3>
                            <div class="radio">
                                <?php echo  '<label>'. tep_draw_radio_field('gender', 'm', $male,'style="-webkit-appearance: radio; margin-top: 0px;"')  . MALE .'</label>'.
                                    '<label>'.tep_draw_radio_field('gender', 'f', $female,'style="-webkit-appearance: radio; margin-top: 0px;"') .FEMALE .'</label>'; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-xs-6">
                    <label><?php echo ENTRY_TELEPHONE_NUMBER; ?><span>*</span></label>
                    <div style="margin-bottom: 15px;">
                        <input type="text" style="width: 100%;" class="form-control" placeholder="<?php echo $account['customers_telephone'];?>" name="telephone">
                    </div>
                </div>
                <div class="col-xs-6">
                    <label><?php echo ENTRY_FAX_NUMBER; ?></label>
                    <div style="margin-bottom: 15px;">
                        <input type="text" style="width: 100%;" class="form-control" placeholder="<?php echo $account['customers_fax'];?>" name="fax">
                    </div>
                </div>



            </div>
            <?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(FILENAME_ACCOUNT, '', 'SSL')); ?>
            <button type="submit" class="btn-green btn btn-primary"><?php echo CONTINUE3;?></button>
        </div>
    </div>
    </form>
    <script src="js/pikaday.js"></script>
    <script>
        const currentDate=new Date().getFullYear().toString();
        var picker = new Pikaday({ field: document.getElementById('dob'),
            yearRange: [1900,currentDate],
            toString(date, format = 'MM-DD-YYYY') {
                const day = date.getDate();
                const month = date.getMonth() + 1;
                const year = date.getFullYear();
                const formattedDate = [
                    month < 10 ? '0' + month : month
                    ,day < 10 ? '0' + day : day
                    ,year
                ].join('/')
                return `${formattedDate}`;
            }
        });
    </script>
    <?php
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
