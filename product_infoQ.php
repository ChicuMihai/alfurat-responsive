<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

if (!isset($_POST['id'])) {
	tep_redirect(tep_href_link(FILENAME_DEFAULT));
}

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_INFO);

$product_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = '" . (int) $_POST['id'] . "' and pd.products_id = p.products_id ");//and pd.language_id = '" . (int) $languages_id . "'
$product_check = tep_db_fetch_array($product_check_query);
if ($product_check['total'] > 0) {
	//bring category id 
	$product_category_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id='" . (int) $_POST['id'] . "'");
	$category_info = tep_db_fetch_array($product_category_query);
	$is_magazine = 0;
	$is_poster = 0;
	$is_book = 1;
	if ($category_info['categories_id'] == MAGAZINES_CATEGORY_ID) {
		$is_magazine = 1;
		$is_book = 0;
	} else if ($category_info['categories_id'] >= POSTERS_CATEGORY_ID) {
		$is_poster = 1;
		$is_book = 0;
	}

	if ($is_book) {
		$product_info_query = tep_db_query("select GROUP_CONCAT(DISTINCT pub.publishers_name  SEPARATOR '&#1548; ') as publisher,"
				. " GROUP_CONCAT(DISTINCT auth.manufacturers_name  SEPARATOR '&#1548; ')  as author, GROUP_CONCAT(DISTINCT auth.manufacturers_id  SEPARATOR ',')  as author_ids"
				. ", GROUP_CONCAT(DISTINCT trans.manufacturers_id  SEPARATOR ',')  as translator_ids,GROUP_CONCAT(DISTINCT trans.manufacturers_name  SEPARATOR '&#1548;') as translator, p.products_id, pd.products_name, pd.products_sub_title, pd.products_description, p.products_model, p.products_edition, p.products_isbn, p.products_weight,products_cover_type ,products_series,products_nb_in_series,product_available_volumes,products_page_nb, p.products_size, p.products_quantity,p.products_available, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.products_date_released from " . TABLE_PRODUCTS . " p"
				. " left join  " . TABLE_PRODUCTS_DESCRIPTION . " pd  on pd.products_id = p.products_id "
				. " left join " . TABLE_PRODUCTS_TO_PUBLISHERS . " m on p.products_id = m.products_id "
				. " left join " . TABLE_PUBLISHERS . " pub on pub.publishers_id = m.publishers_id "
				. " left join " . TABLE_PRODUCTS_TO_AUTHORS . " a on  p.products_id = a.products_id "
				. " left join " . TABLE_MANUFACTURERS . " auth on  a.manufacturers_id = auth.manufacturers_id "
				. "left join " . TABLE_PRODUCTS_TO_TRANSLATORS . " t on p.products_id = t.products_id "
				. "left join " . TABLE_MANUFACTURERS . " trans on t.manufacturers_id = trans.manufacturers_id "
				. "  where p.products_status = '1' and p.products_id = '" . (int) $_POST['id'] . "' "); //and pd.language_id = '" . (int) $languages_id . "'
		$productType = 'Book';
	} else {
		$product_info_query = tep_db_query("select GROUP_CONCAT(DISTINCT pub.name  SEPARATOR '&#1548; ') as publisher, GROUP_CONCAT(DISTINCT auth.name  SEPARATOR '&#1548; ')  as author, p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_edition, p.products_isbn, p.products_weight,products_cover_type,products_page_nb, p.products_size, p.products_quantity,p.products_available, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.products_date_released from " . TABLE_PRODUCTS . " p"
				. " left join  " . TABLE_PRODUCTS_DESCRIPTION . " pd  on pd.products_id = p.products_id "
				. " left join " . TABLE_PRODUCTS_TO_DIRECTORS . " m on p.products_id = m.products_id "
				. " left join " . TABLE_DIRECTORS . " pub on pub.id = m.directors_id "
				. " left join " . TABLE_PRODUCTS_TO_ACTORS . " a on  p.products_id = a.products_id "
				. " left join " . TABLE_ACTORS . " auth on  a.actors_id = auth.id "
				. "  where p.products_status = '1' and p.products_id = '" . (int) $_POST['id'] . "' "); //and pd.language_id = '" . (int) $languages_id . "'
		$productType = 'Poster';
	}
	$product_info = tep_db_fetch_array($product_info_query);

	tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int) $_POST['id'] . "' and language_id = '" . (int) $languages_id . "'");


}
$data=$product_info;

if (file_exists(DIR_WS_IMAGES . $product_info['products_image'])){

$data['products_image']=$product_info['products_image'];

}else{

	$data['products_image']='default_img.gif';
}


$data=$product_info;
$data['products_price']=$currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
$data['success']=1;

echo json_encode($data);

?>