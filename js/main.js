function view_product(id){

    var id = id;

    var postData ={id: id};

    $.ajax({
        type: 'POST',
        url:"http://alfurat-responsive:81/product_infoQ.php",
        data: postData,
        dataType: 'json',

        success: function(data){
              //  console.log(data);
            if(data['success']){

                var edition=data['products_edition']+',Page Numbers:'+data['products_page_nb']+',Size:'+data['products_size']+',Cover Type:'+data['products_cover_type'];
                var prod_img='<img style="width: 100%;" src="http://alfurat-responsive:81/images/'+data['products_image']+'"/>';

              $('#product-title').html(data['products_name']);
              $('#product-subtitle').html(data['products_sub_title']);
                $('#product-isbn').html(data['products_isbn']);
               $('#product-edition').html(edition);
                $('#product-volumes').html(data['product_available_volumes']);
                $('#product_image').html(prod_img);
                $('.product_price').html(data['products_price']);

              $('#item_id').html(data['products_id']);

            }

        }


    })

}


function add_to_cart(item_id = false) {
    if(item_id){
        var q= 1;
        var id=item_id;
    }else {
        var q= $('#items_toCart').html();
        var id= $('#item_id').html();
    }
    var postData ={id: id, q:q};
    $.ajax({
        type: 'POST',
        url:"http://alfurat-responsive:81/add_to_cart.php",
        data: postData,
        dataType: 'json',

        success: function(data){

            if(data['success']){
                $('#shopping_cart').html(data['q']);
            }

        }


    })
}

function add_item (id) {
    var price=$('#price_'+id).html().replace(/[$.]+/g, '')/100;
    var subtotal=$('#subtotal_'+id).html().replace(/[$.]+/g, '')/100;
    console.log(price,subtotal);
    var subtotal_uptd=(subtotal+price).toFixed(2);
    console.log(price,subtotal,subtotal_uptd);

    $('#subtotal_'+id).html('$'+subtotal_uptd);
    var q = parseInt($('#items_toCart_'+id).html());
    var rq = q+1;
    var postData ={id: id, q: rq};
    $.ajax({
        type: 'POST',
        url:"http://alfurat-responsive:81/add_to_cart.php",
        data: postData,
        dataType: 'json',

        success: function(data){

            if(data['success']){

                $('#shopping_cart').html(data['q']);
                $('.total').html('$'+data['show_total'].toFixed(2));
            }

        }
    })


}







function remove_item (id) {
    var price=$('#price_'+id).html().replace(/[$.]+/g, '')/100;
    var subtotal=$('#subtotal_'+id).html().replace(/[$.]+/g, '')/100;
    var subtotal_uptd=(subtotal-price).toFixed(2);
    if(subtotal_uptd>0){
    $('#subtotal_'+id).html('$'+subtotal_uptd);
    var q = parseInt($('#items_toCart_'+id).html());
    var rq = q-1;
    var postData ={id: id, q: rq};
    $.ajax({
        type: 'POST',
        url:"http://alfurat-responsive:81/add_to_cart.php",
        data: postData,
        dataType: 'json',

        success: function(data){

            if(data['success']){

                $('#shopping_cart').html(data['q']);
                $('.total').html('$'+data['show_total'].toFixed(2));
            }

        }
    })
    }
}


function remove_from_cart(id) {
    var id=id;
    var postData ={id: id, action: 'remove'};
    $.ajax({
        type: 'POST',
        url:"http://alfurat-responsive:81/add_to_cart.php",
        data: postData,
        dataType: 'json',

        success: function(data){

            if(data['success']){
                $('#bookrow_'+id).remove();
                $('#shopping_cart').html(data['q']);
                $('.total').html('$'+data['show_total'].toFixed(2));
            }

        }
    })

}