<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
if (!tep_session_is_registered('customer_id')) {
	tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}
$transaction_id = ($_GET['transaction_id']) ? $_GET['transaction_id'] : 0;
if ($transaction_id > 0) {
	$transaction_query = tep_db_query("select amount,customers_id,date_added,order_id,receiptNo,transactionNo from " . TABLE_AUDI_TRANSACTIONS . " where transaction_id = '" . (int) $transaction_id . "' ");
	$transaction = tep_db_fetch_array($transaction_query);
	if (date('Y-m-d', strtotime($transaction['date_added'])) != date('Y-m-d') || $customer_id != (int) $transaction['customers_id']) {
		$transaction = array();
	}
}


require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SUCCESS);

$breadcrumb->add(NAVBAR_TITLE_1);
$breadcrumb->add(NAVBAR_TITLE_2);

//require(DIR_WS_INCLUDES . 'template_top.php');
?>
<?php echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'action=update', 'SSL')); ?>
<style>
	@media print {.hidden-print {display: none !important;}}
</style>
<div class="contentContainer">
	<?php if (!empty($transaction)) { ?>
	<div style="border:1px solid #000; margin-top: 5px;padding: 5px; "><table width="100%" border="0" cellspace="0" cellpadding="0" style="font-family:arial; font-size:14px;   line-height: 32px;">
						<tr>
				<td colspan="3" align="center">
					<strong>Receipt No:</strong> <?php echo $transaction['receiptNo']; ?>
				</td>
			</tr>
				<tr>
					<td width="33%" align="left"></td>
					<td width="33%" align="center"></td>
					<td width="33%" align="left"><strong>Date:</strong> <?php echo $transaction['date_added']; ?></td>
				</tr>
				<tr>
					<td align="left" colspan="2"><strong>Customer's Name:</strong> <?php echo tep_customers_name($customer_id); ?></td>
					<td align="left" ><strong>Amount:</strong> <?php echo $transaction['amount'],' USD'; ?></td>
				</tr>
				<tr>
					<td align="left" colspan="2"><strong>Powered By: areeba</strong></td>
					<td align="left" ><strong>Transaction No:</strong> <?php echo $transaction['transactionNo']; ?></td>
				</tr>
			</table>
	</div><br>
	<div class="contentContainer">
	<div class="buttonSet">
		<span class="buttonAction">
			<button class="hidden-print " onclick="window.print(); setTimeout('window.close()', 800)">Print Receipt</button></span>
	</div>
</div>
	<?php }
	?>
</div>
</form>
<?php
//require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
