<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

// if the customer is not logged on, redirect them to the shopping cart page
if (!tep_session_is_registered('customer_id')) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}
$transaction_id = ($_GET['transaction_id']) ? $_GET['transaction_id'] : 0;
if ($transaction_id > 0) {
    $transaction_query = tep_db_query("select amount,customers_id,date_added,order_id,receiptNo,transactionNo from " . TABLE_AUDI_TRANSACTIONS . " where transaction_id = '" . (int) $transaction_id . "' ");
    $transaction = tep_db_fetch_array($transaction_query);
    if (date('Y-m-d', strtotime($transaction['date_added'])) != date('Y-m-d') || $customer_id != (int) $transaction['customers_id']) {
        $transaction = array();
    }
}
$orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where customers_id = '" . (int) $customer_id . "' order by date_purchased desc limit 1");

// redirect to shopping cart page if no orders exist
if (!tep_db_num_rows($orders_query)) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
}

$orders = tep_db_fetch_array($orders_query);
$order_id = $orders['orders_id'];
$page_content = $oscTemplate->getContent('checkout_success');

if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'update')) {
    tep_redirect(tep_href_link(FILENAME_DEFAULT));
}

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SUCCESS);

$breadcrumb->add(NAVBAR_TITLE_1);
$breadcrumb->add(NAVBAR_TITLE_2);

require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div>
    <div class="boxTitle"><?php echo HEADING_TITLE; ?></div>

    <?php echo tep_draw_form('order', tep_href_link(FILENAME_CHECKOUT_SUCCESS, 'action=update', 'SSL')); ?>

    <div class="conCon">
        <?php if (!empty($transaction)) { ?>
            <div class="receipt-border "><table width="100%" border="0" cellspace="0" cellpadding="0" class="receipt-font">
                    <tr>
                        <td colspan="3" align="center">
                            <strong>Receipt No</strong>: <?php echo $transaction['receiptNo']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="33%" align="left"></td>
                        <td width="33%" align="center"></td>
                        <td width="33%" align="left"><strong>Date:</strong> <?php echo $transaction['date_added']; ?></td>
                    </tr>
                    <tr>
                        <td align="left" ><strong>Amount:</strong> <?php echo $transaction['amount'], ' USD'; ?></td>
                        <td align="left" colspan="2"><strong>Customer's Name:</strong> <?php echo tep_customers_name($customer_id); ?></td>
                    </tr>
                    <tr>
                        <td align="left" ><strong>Transaction No:</strong> <?php echo $transaction['transactionNo']; ?></td>
                        <td align="left" colspan="2"><strong>Powered by: areeba</strong></td>
                    </tr>
                </table>
            </div><br>
            <div class="conCon">
                <div class="buttonSet">
				<span class="buttonAction">
					<input type="hidden" id="transaction_id" value="<?php echo $transaction_id; ?>">
                    <?php echo tep_draw_button('Print Receipt', 'triangle-1-e', null, 'primary'); ?></span>
                </div>
            </div>
            <?php
        }
        echo $page_content;
        ?>
    </div>

    <div class="conCon">
        <div class="buttonSet">
            <span class="buttonAction"> <button type="submit" class="btn-green btn btn-primary"><?php echo CONTINUE3;?></button></span>
        </div>
    </div>

    </form>
    <?php
    if ($transaction_id > 0)
        echo'<script type="text/javascript" src="js/print-receipt.js"></script>';
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
