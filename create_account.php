<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2013 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

// needs to be included earlier to set the success message in the messageStack
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CREATE_ACCOUNT);
// start modification for reCaptcha
require_once('includes/classes/recaptchalib.php');
// end modification for reCaptcha
$process = false;
if (isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    $process = true;

    if (ACCOUNT_GENDER == 'true') {
        if (isset($HTTP_POST_VARS['gender'])) {
            $gender = tep_db_prepare_input($HTTP_POST_VARS['gender']);
        } else {
            $gender = false;
        }
    }
    $firstname = tep_db_prepare_input($HTTP_POST_VARS['firstname']);
    $lastname = tep_db_prepare_input($HTTP_POST_VARS['lastname']);
    if (ACCOUNT_DOB == 'true')
        $dob = tep_db_prepare_input($HTTP_POST_VARS['dob']);
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
    if (ACCOUNT_COMPANY == 'true')
        $company = tep_db_prepare_input($HTTP_POST_VARS['company']);
    $street_address = tep_db_prepare_input($HTTP_POST_VARS['street_address']);
    if (ACCOUNT_SUBURB == 'true')
        $suburb = tep_db_prepare_input($HTTP_POST_VARS['suburb']);
    $postcode = tep_db_prepare_input($HTTP_POST_VARS['postcode']);
    $city = tep_db_prepare_input($HTTP_POST_VARS['city']);
    if (ACCOUNT_STATE == 'true') {
        $state = tep_db_prepare_input($HTTP_POST_VARS['state']);
        if (isset($HTTP_POST_VARS['zone_id'])) {
            $zone_id = tep_db_prepare_input($HTTP_POST_VARS['zone_id']);
        } else {
            $zone_id = false;
        }
    }
    $country = tep_db_prepare_input($HTTP_POST_VARS['country']);
    $telephone = tep_db_prepare_input($HTTP_POST_VARS['telephone']);
    $fax = tep_db_prepare_input($HTTP_POST_VARS['fax']);
    if (isset($HTTP_POST_VARS['newsletter'])) {
        $newsletter = tep_db_prepare_input($HTTP_POST_VARS['newsletter']);
    } else {
        $newsletter = false;
    }
    $password = tep_db_prepare_input($HTTP_POST_VARS['password']);
    $confirmation = tep_db_prepare_input($HTTP_POST_VARS['confirmation']);

    $error = false;

    if (ACCOUNT_GENDER == 'true') {
        if (($gender != 'm') && ($gender != 'f')) {
            $error = true;
            $messageStack->add('create_account', ENTRY_GENDER_ERROR);
        }
    }

    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_FIRST_NAME_ERROR);
    }

    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_LAST_NAME_ERROR);
    }


    if (ACCOUNT_DOB == 'true') {
        $dob=date('m-d-Y', strtotime($dob));

        if ((strlen($dob) < ENTRY_DOB_MIN_LENGTH) || (!empty($dob) && (!is_numeric(tep_date_raw($dob)) || !@checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4))))) {
            $error = true;

            $messageStack->add('create_account', ENTRY_DATE_OF_BIRTH_ERROR);
        }
    }

    if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR);
    } elseif (tep_validate_email($email_address) == false) {
        $error = true;

        $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
    } else {
        $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
        $check_email = tep_db_fetch_array($check_email_query);
        if ($check_email['total'] > 0) {
            $error = true;

            $messageStack->add('create_account', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
        }
    }

    if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_STREET_ADDRESS_ERROR);
    }
    if (strlen($suburb) < ENTRY_SUBURB_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_SUBURB_ERROR);
    }

    if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_POST_CODE_ERROR);
    }

    if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_CITY_ERROR);
    }

    if (is_numeric($country) == false) {
        $error = true;

        $messageStack->add('create_account', ENTRY_COUNTRY_ERROR);
    }

    if (ACCOUNT_STATE == 'true') {
        $zone_id = 0;
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int) $country . "'");
        $check = tep_db_fetch_array($check_query);
        $entry_state_has_zones = ($check['total'] > 0);
        if ($entry_state_has_zones == true) {
            $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int) $country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
            if (tep_db_num_rows($zone_query) == 1) {
                $zone = tep_db_fetch_array($zone_query);
                $zone_id = $zone['zone_id'];
            } else {
                $error = true;

                $messageStack->add('create_account', ENTRY_STATE_ERROR_SELECT);
            }
        } else {
            if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
                $error = true;

                $messageStack->add('create_account', ENTRY_STATE_ERROR);
            }
        }
    }

    if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_TELEPHONE_NUMBER_ERROR);
    }


    if (strlen($password) < ENTRY_PASSWORD_MIN_LENGTH) {
        $error = true;

        $messageStack->add('create_account', ENTRY_PASSWORD_ERROR);
    } elseif ($password != $confirmation) {
        $error = true;

        $messageStack->add('create_account', ENTRY_PASSWORD_ERROR_NOT_MATCHING);
    }
    if (test_Password($password, 5) < 30) {
        $error = true;
        $messageStack->add('create_account', ENTRY_PASSWORD_NEW_STRENGTH);
    }
    // start modification for reCaptcha
    // the response from reCAPTCHA
    $resp = null;

    // was there a reCAPTCHA response?
    $resp = recaptcha_check_answer(RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

    // temp fix until new template is done.
    // this object overrides the response data
    // that is being fetched by Google's reCaptcha.
    // comment out the line below if you want the captcha to work like normal.
    $resp = (object) array('is_valid' => true);

    if (!$resp->is_valid) {
        $error = true;
        $messageStack->add('create_account', ENTRY_SECURITY_CHECK_ERROR);
    }
// end modification for reCaptcha
    if ($error == false) {
        $sql_data_array = array('customers_firstname' => $firstname,
            'customers_lastname' => $lastname,
            'customers_email_address' => $email_address,
            'customers_telephone' => $telephone,
            'customers_fax' => $fax,
            'customers_newsletter' => $newsletter,
            'customers_password' => tep_encrypt_password($password));

        if (ACCOUNT_GENDER == 'true')
            $sql_data_array['customers_gender'] = $gender;
        if (ACCOUNT_DOB == 'true')
            $sql_data_array['customers_dob'] = tep_date_raw($dob);

        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);

        $customer_id = tep_db_insert_id();

        $sql_data_array = array('customers_id' => $customer_id,
            'entry_firstname' => $firstname,
            'entry_lastname' => $lastname,
            'entry_telephone' => $telephone,
            'entry_street_address' => $street_address,
            'entry_postcode' => $postcode,
            'entry_city' => $city,
            'entry_country_id' => $country);

        if (ACCOUNT_GENDER == 'true')
            $sql_data_array['entry_gender'] = $gender;
        if (ACCOUNT_COMPANY == 'true')
            $sql_data_array['entry_company'] = $company;
        if (ACCOUNT_SUBURB == 'true')
            $sql_data_array['entry_suburb'] = $suburb;
        if (ACCOUNT_STATE == 'true') {
            if ($zone_id > 0) {
                $sql_data_array['entry_zone_id'] = $zone_id;
                $sql_data_array['entry_state'] = '';
            } else {
                $sql_data_array['entry_zone_id'] = '0';
                $sql_data_array['entry_state'] = $state;
            }
        }

        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

        $address_id = tep_db_insert_id();

        tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int) $address_id . "' where customers_id = '" . (int) $customer_id . "'");

        tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int) $customer_id . "', '0', now())");



        if (SESSION_RECREATE == 'True') {
            tep_session_recreate();
        }

        $customer_first_name = $firstname;
        $customer_default_address_id = $address_id;
        $customer_country_id = $country;
        $customer_zone_id = $zone_id;
        tep_session_register('customer_id');
        tep_session_register('customer_first_name');
        tep_session_register('customer_default_address_id');
        tep_session_register('customer_country_id');
        tep_session_register('customer_zone_id');
        ///kgt
        if (MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true') {
            //kgt - discount coupons
            if (!tep_session_is_registered('coupon'))
                tep_session_register('coupon');
            //this needs to be set before the order object is created, but we must process it after
            $coupon = tep_db_prepare_input($HTTP_POST_VARS['coupon']);

            if (tep_not_null($coupon)) {
                require_once( DIR_WS_CLASSES . 'discount_coupon.php' );
                $discount_coupon->coupon = new discount_coupon($coupon, '');
//if they have entered something in the coupon field
                $discount_coupon->coupon->verify_code();
                if (MODULE_ORDER_TOTAL_DISCOUNT_COUPON_DEBUG != 'true') {
                    if (!$discount_coupon->coupon->is_errors()) { //if we have passed all tests (no error message), make sure we still meet free shipping requirements, if any
                        $discount_coupon->coupon->apply_discount();
                        //$messageStack->add_session('checkout_address', ENTRY_DISCOUNT_COUPON_SUCCESS);
                        //	tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '', 'SSL'));
                    }
                }
            } else { //if the coupon field is empty, unregister the coupon from the session
                if (tep_session_is_registered('coupon')) { //we had a coupon entered before, so we need to unregister it
                    tep_session_unregister('coupon');
                }
            }
            //end kgt - discount coupons
        }
        ///kgt
// reset session token
        $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());

// restore cart contents
        $cart->restore_contents();

// build the message content
        $name = $firstname . ' ' . $lastname;

        if (ACCOUNT_GENDER == 'true') {
            if ($gender == 'm') {
                $email_text = sprintf(EMAIL_GREET_MR, $lastname);
            } else {
                $email_text = sprintf(EMAIL_GREET_MS, $lastname);
            }
        } else {
            $email_text = sprintf(EMAIL_GREET_NONE, $firstname);
        }

        $email_text .= EMAIL_WELCOME . EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
        tep_mail($name, $email_address, EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

        tep_redirect(tep_href_link(FILENAME_CREATE_ACCOUNT_SUCCESS, '', 'SSL'));
    }
}

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'));

require(DIR_WS_INCLUDES . 'template_top.php');
/***** Begin View Counter *****/
$addrArray= array('city','state','country');
if (VIEW_COUNTER_AUTOFILL == 'true') {
    include('includes/functions/view_counter.php');
    GetAutofillArray($addrArray);
}
/***** End View Counter *****/
require('includes/form_check.js.php');
?>
<script type="text/javascript" src="ext/jquery/pstrength/jquery.pstrength-min.1.2.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">


<?php
if ($messageStack->size('create_account') > 0) {
    echo $messageStack->output('create_account');
}
?>
<input type="hidden" value="<?php echo PASSWORD_STRENGTH;?>" name="passwordStrength" id="passwordStrength" />
<input type="hidden" value="<?php echo MINIMUM_LENGTH;?>" name="minimumLength" id="minimumLength" />



<?php echo tep_draw_form('create_account', tep_href_link(FILENAME_CREATE_ACCOUNT, '', 'SSL'), 'post', 'onsubmit="return check_form(create_account);class="form-inline""', true) . tep_draw_hidden_field('action', 'process'); ?>
<div class="information-blocks">
    <div class="row">
        <div class="col-sm-9 information-entry">
            <div class="login-box">
                <div class="article-container style-1">
                    <h3>Register</h3>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div style="margin-bottom: 10px;">
                            <label><?php echo ENTRY_FIRST_NAME; ?> <span>*</span></label>
                            <input type="text" name="firstname"style="width: 100%;" class="form-control" placeholder="First name">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <label><?php echo ENTRY_LAST_NAME; ?> <span>*</span></label>
                        <div style="margin-bottom: 15px;">
                            <input type="text" style="width: 100%;" class="form-control" placeholder="Last name" name="lastname">
                        </div>
                    </div>
                    <?php
                    if (ACCOUNT_DOB == 'true') {
                        ?>
                        <div class="col-xs-6">
                            <div style="margin-bottom: 15px;">
                                <label><?php echo ENTRY_DATE_OF_BIRTH; ?><span>*</span></label>
                                <input type="text" style="width: 100%;" class="form-control" placeholder="Birth date" name="dob"id="dob">
                            </div>
                        </div>
                    <?php }?>
                    <div class="col-xs-6">
                        <label><?php echo ENTRY_EMAIL_ADDRESS; ?><span>*</span></label>
                        <div style="margin-bottom: 15px;">
                            <input type="text" style="width: 100%;" class="form-control" placeholder="E-mail" name="email_address">
                        </div>
                    </div>
                    <?php
                    if (ACCOUNT_GENDER == 'true') {
                        ?>
                        <div class="col-xs-12">
                            <div style="margin-bottom: 15px;">
                                <h3 style="margin-bottom: 10px;">
                                    <strong><?php echo ENTRY_GENDER; ?></strong>
                                </h3>
                                <div class="radio">
                                    <label><input type="radio" style="-webkit-appearance: radio; margin-top: 0px;" name="gender" value="m"><?php echo MALE;?></label>
                                    <label><input  style="-webkit-appearance: radio; margin-top: 0px;" type="radio" name="gender"value="f"><?php echo FEMALE;?></label>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <?php
                    if (ACCOUNT_COMPANY == 'true') {
                        ?>
                        <div class="col-xs-12">
                            <label><?php echo ENTRY_COMPANY; ?></label>
                            <div style="margin-bottom: 15px;">
                                <input type="text" style="width: 100%;" class="form-control" placeholder="Company name" name="company">
                            </div>
                        </div>
                    <?php }?>

                    <div class="col-xs-12">
                        <div style="margin-bottom: 15px;">
                            <label><?php echo ENTRY_COUNTRY; ?><span>*</span></label>
                            <div class="simple-drop-down">
                                <?php echo tep_get_country_list('country',  $addrArray['country']) . '&nbsp;';?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div style="margin-bottom: 15px;">
                            <label><?php echo ENTRY_STATE; ?><span>*</span></label>
                            <?php
                            if (ACCOUNT_STATE == 'true') {
                            ?>
                            <?php
                            if ($process == true) {
                                if ($entry_state_has_zones == true) {
                                    $zones_array = array();
                                    $zones_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int) $country . "' order by zone_name");
                                    while ($zones_values = tep_db_fetch_array($zones_query)) {
                                        $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
                                    }
                                    echo tep_draw_pull_down_menu('state', $zones_array);
                                } else {
                                    echo tep_draw_input_field('state', $addrArray['state'],'class="form-control" placeholder="State"');
                                }
                            } else {
                                echo tep_draw_input_field('state', $addrArray['state'], 'class="form-control"placeholder="State" ');
                            }

                            if (tep_not_null(ENTRY_STATE_TEXT) && ENTRY_STATE_MIN_LENGTH > 0)

                            ?>
                        </div>
                    </div>
                    <?php }?>

                    <div class="col-xs-6">
                        <label><?php echo ENTRY_CITY; ?><span>*</span></label>
                        <div style="margin-bottom: 15px;">
                            <input type="text" style="width: 100%;" class="form-control" placeholder="City" name="city">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <label><?php echo ENTRY_STREET_ADDRESS; ?><span>*</span></label>
                        <div style="margin-bottom: 15px;">
                            <input type="text" style="width: 100%;" class="form-control" placeholder="Street" name="street_address">
                        </div>
                    </div>

                    <?php
                    if (ACCOUNT_SUBURB == 'true') {
                        ?>

                        <div class="col-xs-6">
                            <label><?php echo ENTRY_SUBURB; ?><span>*</span></label>
                            <div style="margin-bottom: 15px;">
                                <input type="text" style="width: 100%;" class="form-control" placeholder="Building" name="suburb">
                            </div>
                        </div>
                    <?php }?>
                    <div class="col-xs-12">
                        <label><?php echo ENTRY_POST_CODE; ?></label>
                        <div style="margin-bottom: 15px;">
                            <input type="text" style="width: 100%;" class="form-control" placeholder="P.O box" name="postcode">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div style="margin-bottom: 15px;">
                            <label><?php echo ENTRY_TELEPHONE_NUMBER; ?><span>*</span></label>
                            <?php echo tep_draw_input_field('telephone', '', 'class="form-control" placeholder="Telephone" style="width: 100%;" ') . '&nbsp;';?>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <label><?php echo ENTRY_FAX_NUMBER; ?></label>
                        <div style="margin-bottom: 15px;">
                            <?php echo tep_draw_input_field('fax', '', 'class="form-control" placeholder="Fax Number"');?>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div style="margin-bottom: 15px;">
                            <h3 style="margin-bottom: 10px;">
                                <strong>Newsletter</strong>
                                <input type="checkbox" name="newsletter"style="-webkit-appearance: radio; margin-top: 0px;">
                            </h3>
                        </div>
                    </div>
                    <?php
                    /* kgt - discount coupons */
                    if (MODULE_ORDER_TOTAL_DISCOUNT_COUPON_STATUS == 'true') {
                        //echo tep_draw_form('discount_coupon', tep_href_link(FILENAME_SHOPPING_CART));
                        ?>
                        <div class="col-xs-12">
                            <div style="margin-bottom: 15px;">
                                <label><?php echo ENTRY_DISCOUNT_COUPON; ?></label>
                                <?php echo tep_draw_input_field('coupon', '', 'class="form-control" placeholder="Coupon Code" '); ?>
                            </div>
                        </div>
                    <?php }?>
                    <div class="col-xs-6">
                        <label><?php echo ENTRY_PASSWORD; ?><span>*</span></label>
                        <div style="margin-bottom: 15px;">
                            <input type="password" class="form-control" name="password" placeholder=" Password">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div style="margin-bottom: 15px;">
                            <label><?php echo ENTRY_PASSWORD_CONFIRMATION; ?><span>*</span></label>
                            <input type="password" class="form-control" name="confirmation" placeholder="Confirm Password">
                        </div>
                    </div>

                </div>
                <input type="submit" class="btn btn-info" value="<?echo SIGN_UP?>" />
            </div>
        </div>
        </form>
    </div>
    <script src="js/pikaday.js"></script>
    <script>
        const currentDate=new Date().getFullYear().toString();
        var picker = new Pikaday({ field: document.getElementById('dob'),
            yearRange: [1900,currentDate],
            toString(date, format = 'MM-DD-YYYY') {
                const day = date.getDate();
                const month = date.getMonth() + 1;
                const year = date.getFullYear();
                const formattedDate = [
                    month < 10 ? '0' + month : month
                    ,day < 10 ? '0' + day : day
                    ,year
                ].join('/')
                return `${formattedDate}`;
            }
        });
    </script>

    <?php
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
