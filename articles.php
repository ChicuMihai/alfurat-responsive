<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */
require('includes/application_top.php');
// the following cPath references come from application_top.php





require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ARTICLES);
require(DIR_WS_INCLUDES . 'template_top.php');


// create column list
$define_list = array(
    'PRODUCT_LIST_IMAGE' => PRODUCT_LIST_IMAGE,
    'PRODUCT_LIST_NAME' => PRODUCT_LIST_NAME,
    'PRODUCT_LIST_DATE' => PRODUCT_LIST_BUY_NOW);

asort($define_list);

$column_list = array();
reset($define_list);
while (list($key, $value) = each($define_list)) {
    if ($value > 0)
        $column_list[] = $key;
}

$select_column_list = '';

for ($i = 0, $n = sizeof($column_list); $i < $n; $i++) {
    switch ($column_list[$i]) {

        case 'PRODUCT_LIST_NAME':
            $select_column_list .= 'pd.articles_name, ';
            break;

        case 'PRODUCT_LIST_IMAGE':
            $select_column_list .= 'p.articles_image, ';
            break;
        case 'PRODUCT_LIST_DATE':
            $select_column_list .= 'p.articles_date_added, ';
            break;
    }
}





$listing_sql = "select " . $select_column_list . " p.articles_id  from " . TABLE_ARTICLES_DESCRIPTION . " pd,"
    . " " . TABLE_ARTICLES . " p where p.articles_status = '1' and p.articles_id = pd.articles_id and pd.language_id = '" . (int) $languages_id . "' ";
?>

<div class="boxTitle"><?php echo ARTICLES; ?></div>
<div class="conCon">
    <?php
    /*
      $Id$

      osCommerce, Open Source E-Commerce Solutions
      http://www.oscommerce.com

      Copyright (c) 2014 osCommerce

      Released under the GNU General Public License
     */

    if (!empty($listing_sql))
        $listing_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.articles_id');
    ?>

    <div class="textContent">

        <?php
        if (($listing_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') )) {
            ?>

            <div>
                <span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

                <span><?php
                    echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_ARTICLES);
                    ?></span>
            </div>

            <br />

            <?php
        }

        $prod_list_contents = '<div class="ui-widget infoBoxContainer">' .
            '  <div class=" ui-corner-top infoBoxHeading" >' .
            '    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="productListingCatHeader" style="background-color: #eee;">' .
            '      <tr>';

        for ($col = 0, $n = sizeof($column_list); $col < $n; $col++) {
            $lc_align = '';

            switch ($column_list[$col]) {
                case 'PRODUCT_LIST_NAME':
                    $lc_text = TABLE_HEADING_PRODUCTS;
                    $lc_align = '';
                    $lc_width = '260';
                    break;
                case 'PRODUCT_LIST_IMAGE':
                    $lc_text = TABLE_HEADING_IMAGE;
                    $lc_align = 'center';
                    $lc_width = '130';
                    break;
                case 'PRODUCT_LIST_DATE':
                    $lc_text = PRODUCT_LIST_DATE;
                    $lc_width = '100';
                    $lc_align = 'center';
                    break;
            }



            $prod_list_contents .= '        <td ' . (tep_not_null($lc_width) ? ' width="' . $lc_width . '"' : '') . ' ' . (tep_not_null($lc_align) ? ' align="' . $lc_align . '"' : '') . '><strong>' . $lc_text . '</strong></td>';
        }

        $prod_list_contents .= '      </tr>' .
            '    </table>' .
            '  </div>';

        if ($listing_split->number_of_rows > 0) {
            $rows = 0;
            $listing_query = tep_db_query($listing_split->sql_query);

            $prod_list_contents .= '  <div class="ui-widget-content ui-corner-bottom productListTable">' .
                '    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="productListingData">';

            while ($listing = tep_db_fetch_array($listing_query)) {
                $rows++;

                $prod_list_contents .= '      <tr>';

                for ($col = 0, $n = sizeof($column_list); $col < $n; $col++) {
                    switch ($column_list[$col]) {
                        case 'PRODUCT_LIST_NAME':
                            $prod_list_contents .= '        <td width="260"><a href="' . tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $listing['articles_id']) . '"'
                                . ' class="productLink">' . $listing['articles_name'] . '</a></td>';

                            break;




                        case 'PRODUCT_LIST_IMAGE':
                            $prod_list_contents .= '        <td width="130" align="right"><a href="' . tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $listing['articles_id']) . '">'
                                . tep_image(DIR_WS_IMAGES . $listing['articles_image'], $listing['articles_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>';

                            break;
                        case 'PRODUCT_LIST_DATE':
                            $prod_list_contents .= '        <td width="100" align="center">' . tep_date_short($listing['articles_date_added']). '</td>';
                            break;
                    }
                }

                $prod_list_contents .= '      </tr>';
            }

            $prod_list_contents .= '    </table>' .
                '  </div>' .
                '</div>';

            echo $prod_list_contents;
        } else {
            ?>

            <p><?php echo($filtered > 0) ? TEXT_NO_PRODUCTS : TEXT_CHOOSE_FILTERS; ?></p>

            <?php
        }

        if (($listing_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
            ?>

            <br />

            <div>
                <span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

                <span><?php
                    //	$text = ($cPath != POSTERS_CATEGORY_ID) ? TEXT_DISPLAY_NUMBER_OF_PRODUCTS : TEXT_DISPLAY_NUMBER_OF_POSTERS;
                    echo $listing_split->display_count($text);
                    ?></span>
            </div>

            <?php
        }
        ?>

    </div>

</div>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
