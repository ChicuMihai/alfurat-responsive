<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

require('includes/application_top.php');

if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
}

// needs to be included earlier to set the success message in the messageStack
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_NOTIFICATIONS_CAT);

if (isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    (array) $products = $HTTP_POST_VARS['products'];

    if (sizeof($products) > 0) {
        $products_parsed = array();
        reset($products);
        while (list(, $value) = each($products)) {
            if (is_numeric($value)) {
                $products_parsed[] = $value;
            }
        }

        if (sizeof($products_parsed) > 0) {
            $check_query = tep_db_query("select count(*) as total from " . TABLE_CAT_NOTIFICATIONS . " where customers_id = '" . (int) $customer_id . "' and categories_id not in (" . implode(',', $products_parsed) . ")");
            $check = tep_db_fetch_array($check_query);

            if ($check['total'] > 0) {
                tep_db_query("delete from " . TABLE_CAT_NOTIFICATIONS . " where customers_id = '" . (int) $customer_id . "' and categories_id not in (" . implode(',', $products_parsed) . ")");
            }
        }
    } else {
        $check_query = tep_db_query("select count(*) as total from " . TABLE_CAT_NOTIFICATIONS . " where customers_id = '" . (int) $customer_id . "'");
        $check = tep_db_fetch_array($check_query);

        if ($check['total'] > 0) {
            tep_db_query("delete from " . TABLE_CAT_NOTIFICATIONS . " where customers_id = '" . (int) $customer_id . "'");
        }
    }

    $messageStack->add_session('account', SUCCESS_NOTIFICATIONS_UPDATED, 'success');

    tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
}

$breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS, '', 'SSL'));

require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div>
    <div class="boxTitle"><?php echo HEADING_TITLE; ?></div>

    <?php echo tep_draw_form('account_notifications', tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS_CAT, '', 'SSL'), 'post', '', true) . tep_draw_hidden_field('action', 'process'); ?>

    <div class="conCon">

        <h2><?php echo NOTIFICATIONS_TITLE; ?></h2>

        <div class="textContent">

            <?php
            $products_check_query = tep_db_query("select count(*) as total from " . TABLE_CAT_NOTIFICATIONS . " where customers_id = '" . (int) $customer_id . "'");
            $products_check = tep_db_fetch_array($products_check_query);
            if ($products_check['total'] > 0) {
                ?>

                <div><?php echo NOTIFICATIONS_DESCRIPTION; ?></div>

                <table border="0" width="100%" cellspacing="0" cellpadding="2">

                    <?php
                    $counter = 0;
                    $products_query = tep_db_query("select pd.categories_id, pd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " pd, " . TABLE_CAT_NOTIFICATIONS . " pn where pn.customers_id = '" . (int) $customer_id . "' and pn.categories_id = pd.categories_id and pd.language_id = '" . (int) $languages_id . "' order by pd.categories_name");
                    while ($products = tep_db_fetch_array($products_query)) {
                        ?>

                        <tr>
                            <td width="30"><?php echo tep_draw_checkbox_field('products[' . $counter . ']', $products['categories_id'], true); ?></td>
                            <td><strong><?php echo $products['categories_name']; ?></strong></td>
                        </tr>

                        <?php
                        $counter++;
                    }
                    ?>

                </table>

                <?php
            } else {
                ?>

                <div>
                    <?php echo NOTIFICATIONS_NON_EXISTING; ?>
                </div>

                <?php
            }
            ?>

        </div>



        <div class="buttonSet">
		<span class="buttonAction">
		<?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(FILENAME_ACCOUNT, '', 'SSL')); ?>
            <button type="submit" class="btn-green btn btn-primary"><?php echo CONTINUE3;?></button></span>
        </div>
    </div>

    </form>

    <?php
    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
