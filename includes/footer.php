

<div class="footer-wrapper style-3">
    <footer class="type-1">
        <div class="footer-columns-entry">
            <div class="row">
                <div class="col-md-2">
                    <img alt="" src="https://alfurat.com/images/store_logo.png" class="footer-logo">
                </div>

                <div class="col-md-2 col-sm-4">
                    <h3 class="column-title"><?php echo SUPPORT;?></h3>
                    <ul class="column">
                        <li><a href="<?php tep_href_link(FILENAME_DEFAULT)?>"><?php echo HOME;?></a></li>
                        <li><a href="<?php echo tep_href_link(FILENAME_ABOUT_US); ?>"><?php echo ABOUT;?></a></a></li>
                        <li><a href="<?php echo tep_href_link(FILENAME_CONTACT_US); ?>"><?php echo CONTACT_US;?></a></a></li>
                        <li><a href="<?php echo tep_href_link(FILENAME_CONDITIONS); ?>"><?php echo TERMS_AND_CONDITIONS; ?></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <h3 class="column-title"><?php echo LOGIN_OR_REGISTER; ?></h3>
                    <ul class="column">
                        <li>

                            <?php if (tep_session_is_registered('customer_id')) { ?>

                                <a href="<?php echo tep_href_link(FILENAME_LOGOFF, '', 'SSL'); ?>"> <?php echo LOG_OUT;?>    </a>

                            <?php } else { ?>

                                <a href="<?php echo tep_href_link(FILENAME_LOGIN); ?>" ><?php echo LOG_IN; ?></a>
                            <?php } ?>
                        </li>
                        <li><a href="<?php echo tep_href_link(FILENAME_CREATE_ACCOUNT); ?>" ><?php echo CREATE_ACCOUNT;?> </a></li>
                        <li><a href="<?php echo tep_href_link(FILENAME_PASSWORD_FORGOTTEN); ?>"><?php echo FORGOT_YOUR_PASSWORD ?></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <h3 class="column-title"><?php echo CONNECT_WITH_US;?></h3>
                    <ul class="column">
                        <li><img src="images/fb.png" border="0" vspace="5" /><a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/pg/Alfuratdistributor/community/"><span class="white">/Alfuratdistributor</span></a></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col-md-2 col-sm-4">
                    <h3 class="column-title"><?php echo PAYMENT_METHOD;?></h3>
                    <div class="cell-view">
                        <div class="payment-methods">
                            <img src="images/visa.png" border="0"  vspace="2"/>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clearfix visible-sm-block"></div>
            </div>
        </div>

        <div class="footer-bottom-navigation">
            <div class="cell-view">
                <div class="copyright">Copyrights &copy; 2018 الفرات للنشر والتوزيع <br/><br/> <span class="text-center">Powered by: <a >Syscom Technologies</a></span></div>
            </div>
        </div>
    </footer>
</div>


</div> <!--content-push-->
</div> <!-- content-center fixed-header-margin //-->
<div class="clear"></div>
</div><!-- content-block //-->















<!--                    <img src="images/in.png" border="0" vspace="5" />-->
<!--                    <img src="images/twitter.png" border="0"  vspace="6" />-->



<!-- Nivo Slider -->



<?php
if ($banner = tep_banner_exists('dynamic', 'footer')) {
    ?>

    <!-- <div class="grid_24" style="text-align: center; padding-bottom: 20px;">
     <?php //echo tep_display_banner('static', $banner); ?>
  </div>
-->
    <?php
}
switch (basename($PHP_SELF)) {
    case 'advanced_search_result.php':
        echo'<script data-require="lodash-underscore@3.7.0" data-semver="3.7.0" src="ext/select2/lodash.min.js"></script>';
        echo'<script type="text/javascript" src="ext/select2/select2.min.js"></script>';
        echo($search_in_posters == 1) ? '<script type="text/javascript" src="js/posters_advanced_search_result.js"></script>' : '<script type="text/javascript" src="js/advanced_search_result.js"></script>';
        break;
    default:
        break;
}
?>

<script src="js/jquery-2.1.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/idangerous.swiper.min.js"></script>
<script src="js/global.js"></script>
<script src="js/main.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/jquery.jscrollpane.min.js"></script>
