<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

if ($messageStack->size('header') > 0) {
    echo '<div class="grid_24">' . $messageStack->output('header') . '</div>';
}
$product_of_week = tep_random_select("select p.products_id from " . TABLE_PRODUCTS . " p  where p.products_status = '1' and p.products_of_week='1'  ");
$articles_of_month = tep_random_select("select p.articles_id from " . TABLE_ARTICLES . " p  where p.articles_status = '1' and p.articles_of_month='1'  ");
?>


<!-- HEADER -->



<div class="header-wrapper style-14">
    <header class="type-4">
        <div class="header-top">
            <div class="header-top-entry">
                <div class="title">
                    <b><? echo $language; ?></b> <i class="fa fa-caret-down"></i>
                </div>
                <div class="list">
                    <?php

                    //Search and Flags START



                    if (substr(basename($PHP_SELF), 0, 8) != 'checkout') {

                        if (!isset($lng) || (isset($lng) && !is_object($lng))) {

                            include(DIR_WS_CLASSES . 'language.php');

                            $lng = new language;

                        }



                        $languages_string = '';

                        reset($lng->catalog_languages);

                        while (list($key, $value) = each($lng->catalog_languages)) {

                            $languages_string .= ' <a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('language', 'currency')) . 'language=' . $key, $request_type) . '"class="list-entry">'.$value['name'].'</a> ';

                        }

                        ?>
                        <?php echo $languages_string; ?>
                    <?php }?>
                </div>
            </div>

            <div class="header-top-entry">
                <div class="title"  >    <b><? echo   CURRENCY;?></b>
                    <!-- &nbsp;&nbsp;   -->
                    <?php
                    echo tep_draw_form('currencies', tep_href_link(basename($PHP_SELF), '', $request_type, false), 'get');
                    reset($currencies->currencies);
                    $currencies_array = array();
                    while (list($key, $value) = each($currencies->currencies)) {
                        $currencies_array[] = array('id' => $key, 'text' => $value['title']);
                    }
                    $hidden_get_variables = '';
                    reset($HTTP_GET_VARS);
                    while (list($key, $value) = each($HTTP_GET_VARS)) {
                        if (($key != 'currency') && ($key != tep_session_name()) && ($key != 'x') && ($key != 'y')) {
                            $hidden_get_variables .= tep_draw_hidden_field($key, $value);
                        }
                    }
                    ?>

                </div>
            </div>
          <div class="header-top-entry" >
              <div id="selcur">
                  <?php
                  echo tep_draw_pull_down_menu('currency', $currencies_array, $currency, 'onChange="this.form.submit();" class="input select"') . $hidden_get_variables . tep_hide_session_id();
                  ?>
              </div>
          </div>
            <?php
            echo '</form>';
            ?>
            <div class="menu-button responsive-menu-toggle-class"><i class="fa fa-reorder"></i></div>
            <div class="clear"></div>
        </div>

        <div class="header-middle">
            <div class="logo-wrapper">
                <div class="row">
                    <div class="col-md-2 col-sm-12 col-lg-2">
                        <div id="storeLogo"><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT) . '">' . tep_image(DIR_WS_IMAGES . 'store_logo.png', STORE_NAME) . '</a>'; ?></div>

                    </div>
                                    <?php
                                    if(!$is_article){
                                        echo '<div class="searchBox"> ';
                                        $category = 0;
                                        $is_magazine = 0;
                                        $is_poster = 0;
                                        if (basename($PHP_SELF) == 'product_info.php') {
                                            $category = tep_get_product_path($HTTP_GET_VARS['products_id']);
                                        } else if (isset($HTTP_GET_VARS['cPath'])) {
                                            $category = $HTTP_GET_VARS['cPath'];
                                        }
                                        $label = SEARCH_FOR_BOOK;
                                        $search_in_posters = 0;
                                        if ($category == MAGAZINES_CATEGORY_ID) {
                                            $is_magazine = 1;
                                        } else
                                            if ((substr_count($category, POSTERS_CATEGORY_ID) > 0 || $category > POSTERS_CATEGORY_ID) || ($HTTP_GET_VARS['search_in_posters'] == 1)) {
                                                $label = SEARCH_FOR_POSTER;
                                                $search_in_posters = 1;
                                                $is_poster = 1;
                                            }

                                        echo'</form>    </div>';
                                    }
                                    ?>
                    <div class="col-md-10 hidden-xs hidden-sm col-lg-10">
                        <div class="search-box">
                            <?=tep_draw_form('quick_find', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get')?>

                            <div class="search-field">



                                <?=
                               tep_draw_hidden_field('search_in_posters', $search_in_posters) . tep_draw_hidden_field('search_in_description', '1') . tep_hide_session_id()   . tep_draw_input_field('keywords', '', 'size="10" maxlength="30" class="searchfield" placeholder="' .  $label . '"') ; ?>
                                </div>

                                <div class="search-button" >
                                 <button type="submit" class="btn-green btn btn-primary" style="width: inherit;height: inherit; padding: inherit;"> <i class="fa fa-search" > </i>  </button>

                                 </div>
                            
                      
                        <div class="search-drop-down">
                            <button  type="button" data-toggle="modal" data-target="#searchbar" class="btn-s title"><span class="advanced"><? echo ADVANCED;?> </span></button>
                        </div>
                          </form>
                          </div>

                    </div>
                  </div>


            </div>

            <div class="right-entries">
                <a class="header-functionality-entry link--hover" href="<?php echo tep_href_link(FILENAME_SHOPPING_CART); ?>"><i class="fa fa-shopping-cart"></i><span id="shopping_cart"><?=$cart->count_contents()?></span></a>
                      <a class="header-functionality-entry link--hover" href="<?php echo tep_href_link(FILENAME_CONTACT_US); ?>"> <i class="fa fa-envelope"></i><span> <?php echo CONTACT_US;?></span></a>



                      <?php if (!tep_session_is_registered('customer_id')) { ?>

                <a class="header-functionality-entry link--hover" href="<?php echo tep_href_link(FILENAME_CREATE_ACCOUNT); ?>"> <i class="fa fa-user"></i><span><?php echo CREATE_ACCOUNT;?></span> </a>
                <a class="header-functionality-entry link--hover" href="<?php echo tep_href_link(FILENAME_LOGIN); ?>">
                <i class="fa fa-sign-in"></i> <span> <?php echo LOG_IN;?></span> </a>
                    <?php } else { ?>
                  <a class="header-functionality-entry link--hover" href="<?php echo tep_href_link(FILENAME_ACCOUNT); ?>"> <i class="fa fa-user"></i><span><?php echo MY_ACCOUNT;?> </span></a>
                  <a class="header-functionality-entry link--hover" href="<?php echo tep_href_link(FILENAME_LOGOFF, '', 'SSL'); ?>">  <i class="fa fa-sign-in"></i> <span> <?php echo LOG_OUT;?></span> </a>
                <?php } ?>



            </div>
        </div>

        <div class="hidden-md hidden-lg">
            <form>

                    <?php
                    if(!$is_article){
                        echo '<div class="searchBox"> ';
                        $category = 0;
                        $is_magazine = 0;
                        $is_poster = 0;
                        if (basename($PHP_SELF) == 'product_info.php') {
                            $category = tep_get_product_path($HTTP_GET_VARS['products_id']);
                        } else if (isset($HTTP_GET_VARS['cPath'])) {
                            $category = $HTTP_GET_VARS['cPath'];
                        }
                        $label = SEARCH_FOR_BOOK;
                        $search_in_posters = 0;
                        if ($category == MAGAZINES_CATEGORY_ID) {
                            $is_magazine = 1;
                        } else
                            if ((substr_count($category, POSTERS_CATEGORY_ID) > 0 || $category > POSTERS_CATEGORY_ID) || ($HTTP_GET_VARS['search_in_posters'] == 1)) {
                                $label = SEARCH_FOR_POSTER;
                                $search_in_posters = 1;
                                $is_poster = 1;
                            }

                        echo'</form>	</div>';
                    }
                    ?>

                 <div class="search-box">

                            <?=tep_draw_form('quick_find', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get')?>

                            <div class="search-field">



                                <?=
                               tep_draw_hidden_field('search_in_posters', $search_in_posters) . tep_draw_hidden_field('search_in_description', '1') . tep_hide_session_id()   . tep_draw_input_field('keywords', '', 'size="10" maxlength="30" class="searchfield" placeholder="' .  $label . '"') ; ?>
                                </div>

                                <div class="search-button" >
                                 <button type="submit" class="btn-green btn btn-primary" style="width: inherit;height: inherit; padding: inherit;"> <i class="fa fa-search" > </i>  </button>

                                 </div>
                            </form>

                <div class="search-drop-down text-center">
                    <a data-toggle="modal" data-target="#searchbar" href="" class="fa fa-gear title gear" ></a>
                </div>   </div>
            </form>
        </div>

        <div class="close-header-layer"></div>
        <div class="navigation">
            <div class="navigation-header responsive-menu-toggle-class">
                <div class="title">Navigation</div>
                <div class="close-menu"></div>
            </div>
            <div class="nav-overflow">
                <nav class="col-md-push-1">
                    <ul class="ul-center" style="width: 100%; margin-bottom: 1px;">
                        <li class="full-width"><a href="<?php echo tep_href_link(FILENAME_FEATURED_PRODUCTS); ?>"><?php echo TABLE_HEADING_FEATURED_PRODUCTS; ?></a></li>
                        <li class="full-width-columns"><a href="index.php?cPath=<?php echo POSTERS_CATEGORY_ID; ?>"><?php echo POSTERS; ?></a></li>
                        <li class="simple-list"><a href="index.php?cPath=<?php echo MAGAZINES_CATEGORY_ID; ?>"> <?php echo MAGAZINES; ?> </a></li>
                        <li class="column-1"><a href="<?php echo (!empty($product_of_week['products_id'])) ? tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product_of_week['products_id']) : 'javascript:;'; ?>"><?php echo BOOK_OF_WEEK; ?></a></li>
                        <li class="column-1"><a href="<?php echo (!empty($articles_of_month['articles_id'])) ? tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $articles_of_month['articles_id']) : 'javascript:;'; ?>"><?php echo ARTICLE_OF_MONTH; ?></a></li>
                        <li class="column-1"><a href="<?php echo tep_href_link(FILENAME_CONDITIONS); ?>"><?php echo TERMS_AND_CONDITIONS; ?></a></li>


                    </ul>
                     <div class="clear"></div>
                    <a class="fixed-header-visible additional-header-logo"><img src="https://alfurat.com/images/store_logo.png" alt=""/></a>
                </nav>
                <div class="navigation-footer responsive-menu-toggle-class">
                    <div class="socials-box">
                      <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/pg/Alfuratdistributor/community/"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-youtube"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="clear"></div>
</div>


<!--advance searcch -->
<div class="modal" id="searchbar" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-green">
                <h3 class="modal-title" id="exampleModalLabel"><? echo ADVANCED;?></h3>
            </div>
            <div class="modal-body">

                <form>
                    <div style="padding-bottom: 10px;">
                        <div class="form-group input-down" >
                            <div class="search-field1" >

                                <div class="form-group input-down">
                                    <?php
                                    if(!$is_article){
                                        echo '<div class="searchBox"> ';
                                        $category = 0;
                                        $is_magazine = 0;
                                        $is_poster = 0;
                                        if (basename($PHP_SELF) == 'product_info.php') {
                                            $category = tep_get_product_path($HTTP_GET_VARS['products_id']);
                                        } else if (isset($HTTP_GET_VARS['cPath'])) {
                                            $category = $HTTP_GET_VARS['cPath'];
                                        }
                                        $label = SEARCH_FOR_BOOK;
                                        $search_in_posters = 0;
                                        if ($category == MAGAZINES_CATEGORY_ID) {
                                            $is_magazine = 1;
                                        } else
                                            if ((substr_count($category, POSTERS_CATEGORY_ID) > 0 || $category > POSTERS_CATEGORY_ID) || ($HTTP_GET_VARS['search_in_posters'] == 1)) {
                                                $label = SEARCH_FOR_POSTER;
                                                $search_in_posters = 1;
                                                $is_poster = 1;
                                            }

                                        echo'</form>	</div>';
                                    }
                                    ?>

                                    <div class="search-field1">
                                        <?php
                                        echo tep_draw_form('quick_find', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get')  . tep_draw_hidden_field('search_in_posters', $search_in_posters) . tep_draw_hidden_field('search_in_description', '1') . tep_hide_session_id()  . tep_draw_input_field('keywords', '', 'size="10" maxlength="30" class="searchfield" placeholder="' . $label . '"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 input-down">
                                <div id="selmod">
                                    <td>
                                        <?php  echo tep_draw_pull_down_menu('categories_id', tep_get_categories(array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES))),'','class="searchadvanced"'); ?>
                                    </td>
                                </div>
                            </div>
                            <div class="col-md-4 input-down">
                                <div id="selmod">
                                    <td>
                                        <?php echo tep_draw_pull_down_menu('author_id', tep_get_manufacturers(array(array('id' => '', 'text' => TEXT_ALL_AUTHORS)),'2'),'','class="selectadvanced"'); ?>
                                    </td>
                                </div>
                            </div>
                            <div class="col-md-4 input-down">
                                <div id="selmod">
                                    <td>
                                        <?php echo tep_draw_pull_down_menu('countries_id', tep_get_product_countries(array(array('id' => '', 'text' => TEXT_ALL_COUNTRIES)),'1'),'','class="selectadvanced"'); ?>
                                    </td>
                                </div>
                            </div>
                        </div>

                    </div>
            </div>

            <div class="modal-footer">
                <button  style="float: left;" type="button" class="btn btn-secondary" data-dismiss="modal"><? echo CLOSE;?></button>

                <button type="submit" class="btn-green btn btn-primary"><? echo SEARCH;?></button>
            </div>





        </div>
        </form>
    </div>
</div>



     <div class="content-push <?php echo $oscTemplate->getGridContainerWidth(); ?> ">

  

    <?php
    if (isset($HTTP_GET_VARS['error_message']) && tep_not_null($HTTP_GET_VARS['error_message'])) {
        ?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="headerError">
                <td class="headerError"><?php echo htmlspecialchars(stripslashes(urldecode($HTTP_GET_VARS['error_message']))); ?></td>
            </tr>
        </table>
        <?php
    }

    if (isset($HTTP_GET_VARS['info_message']) && tep_not_null($HTTP_GET_VARS['info_message'])) {
        ?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="headerInfo"><div class="clear clearfix"></div>
                <td class="headerInfo"><?php echo htmlspecialchars(stripslashes(urldecode($HTTP_GET_VARS['info_message']))); ?></td>
            </tr>
        </table>
        <?php
    }
    ?>