<?php 
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

////
// This function validates a plain text password with a
// salted or phpass password
  function tep_validate_password($plain, $encrypted) {
    if (tep_not_null($plain) && tep_not_null($encrypted)) {
      if (tep_password_type($encrypted) == 'salt') {
        return tep_validate_old_password($plain, $encrypted);
      }

      if (!class_exists('PasswordHash')) {
        include(DIR_WS_CLASSES . 'passwordhash.php');
      }

      $hasher = new PasswordHash(10, true);

      return $hasher->CheckPassword($plain, $encrypted);
    }

    return false;
  }

////
// This function validates a plain text password with a
// salted password
  function tep_validate_old_password($plain, $encrypted) {
    if (tep_not_null($plain) && tep_not_null($encrypted)) {
// split apart the hash / salt
      $stack = explode(':', $encrypted);

      if (sizeof($stack) != 2) return false;

      if (md5($stack[1] . $plain) == $stack[0]) {
        return true;
      }
    }

    return false;
  }

////
// This function encrypts a phpass password from a plaintext
// password.
  function tep_encrypt_password($plain) {
    if (!class_exists('PasswordHash')) {
      include(DIR_WS_CLASSES . 'passwordhash.php');
    }

    $hasher = new PasswordHash(10, true);

    return $hasher->HashPassword($plain);
  }

////
// This function encrypts a salted password from a plaintext
// password.
  function tep_encrypt_old_password($plain) {
    $password = '';

    for ($i=0; $i<10; $i++) {
      $password .= tep_rand();
    }

    $salt = substr(md5($password), 0, 2);

    $password = md5($salt . $plain) . ':' . $salt;

    return $password;
  }

////
// This function returns the type of the encrpyted password
// (phpass or salt)
  function tep_password_type($encrypted) {
    if (preg_match('/^[A-Z0-9]{32}\:[A-Z0-9]{2}$/i', $encrypted) === 1) {
      return 'salt';
    }

    return 'phpass';
  }
   		function test_Password($password,$minimum)
{
			$F = 0;
		
			if ( strlen( $password ) < $minimum) {
				$F = ($F - 100);
			} else {
				if ( strlen( $password ) >= $minimum &&  strlen( $password ) <= ($minimum + 2)) {
					$F = ($F + 6);
				} else {
					if ( strlen( $password ) >= ($minimum + 3) &&  strlen( $password ) <= ($minimum + 4)) {
						$F = ($F + 12);
					} else {
						if ( strlen( $password ) >= ($minimum + 5)) {
							$F = ($F + 18);
						}
					}
				}
			}
			if (preg_match('/[a-z]/',$password)) {
				$F = ($F + 1);
			}
			if (preg_match('/[A-Z]/',$password)) {
				$F = ($F + 5);
			}
			if (preg_match('/\d+/',$password)) {
				$F = ($F + 5);
			}
			if (preg_match('/(.*[0-9].*[0-9].*[0-9])/',$password)) {
				$F = ($F + 7);
			}
			if (preg_match('/.[!,@,#,$,%,^,&,*,?,_,~]/',$password)) {
				$F = ($F + 5);
			}
			if (preg_match('/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/',$password)) {
				$F = ($F + 7);
			}
			if (preg_match('/([a-z].*[A-Z])|([A-Z].*[a-z])/',$password)) {
				$F = ($F + 2);
			}
			if (preg_match('/([a-zA-Z])/',$password) && preg_match('/([0-9])/',$password)) {
				$F = ($F + 3);
			}
			if (preg_match('/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/',$password)) {
				$F = ($F + 3);
			}

		$common=array("password", "sex", "god", "123456", "12345", "1234", "123", "liverpool", "letmein", "qwerty", "monkey"); 
			for ($D = 0; $D < count($common); $D++) {
				if (strtolower($password) == $common[$D]) {
					$F = -200;
				}
			}
			return $F;
		}
?>