<?php


function select_options($languages_id=null,$HTTP_GET_VARS=null,$current_category_id=null,$cPath=null,$is_poster=null){








    if (PRODUCT_LIST_FILTER > 0) {
        if (isset($HTTP_GET_VARS['manufacturers_id']) && !empty($HTTP_GET_VARS['manufacturers_id'])) {
            $filterlist_sql = "select distinct c.categories_id as id, cd.categories_name as name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where p.products_status = '1' and p.products_id = p2c.products_id and p2c.categories_id = c.categories_id and p2c.categories_id = cd.categories_id and cd.language_id = '" . (int) $languages_id . "' and p.manufacturers_id = '" . (int) $HTTP_GET_VARS['manufacturers_id'] . "' order by cd.categories_name";
        } else {
            $filterlist_sql = "select distinct pub.publishers_id as id, pub.publishers_name as name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_PRODUCTS_TO_PUBLISHERS . " m , " . TABLE_PUBLISHERS . " pub where p.products_status = '1' and p.products_id = m.products_id  and pub.publishers_id = m.publishers_id  and p.products_id = p2c.products_id and p2c.categories_id = '" . (int) $current_category_id . "' order by pub.publishers_name";
            $filterlist_sql_author = "select distinct auth.manufacturers_id as id, auth.manufacturers_name as name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_PRODUCTS_TO_AUTHORS . " m , " . TABLE_MANUFACTURERS . " auth where p.products_status = '1'  and p.products_id = m.products_id  and auth.manufacturers_id = m.manufacturers_id and p.products_id = p2c.products_id and p2c.categories_id = '" . (int) $current_category_id . "' order by auth.manufacturers_name";
            $filterlist_sql_translator = "select distinct trans.manufacturers_id as id, trans.manufacturers_name as name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_PRODUCTS_TO_TRANSLATORS . " m , " . TABLE_MANUFACTURERS . " trans where p.products_status = '1' and p.products_id = m.products_id  and trans.manufacturers_id = m.manufacturers_id  and p.products_id = p2c.products_id and p2c.categories_id = '" . (int) $current_category_id . "' order by trans.manufacturers_name";

            $filterlist_query = tep_db_query($filterlist_sql);
            $filterlist_query_author = tep_db_query($filterlist_sql_author);
            $filterlist_query_translator = tep_db_query($filterlist_sql_translator);

            $select='';

            if ($is_poster) {
                if (isset($HTTP_GET_VARS['categories_id']) && !empty($HTTP_GET_VARS['categories_id'])) {
                    $select.= tep_draw_hidden_field('categories_id', $HTTP_GET_VARS['categories_id']);
                    $options_categories = array(array('id' => '', 'text' => TEXT_ALL_COUNTRIES));
                } else {
                    $select.= tep_draw_hidden_field('cPath', $cPath);
                    $options_categories = array(array('id' => '', 'text' => TEXT_ALL_COUNTRIES));
                }
                $filterlist_sql_categories = "select distinct descr.categories_id as id, descr.categories_name as name from   " . TABLE_CATEGORIES . " cat inner join  " . TABLE_CATEGORIES_DESCRIPTION . "  descr"
                    . "  where    cat.categories_id = descr.categories_id and descr.language_id = '" . (int) $languages_id . "' and cat.parent_id = '" . (int) POSTERS_CATEGORY_ID . "' order by descr.categories_name";
                $filterlist_query_categories = tep_db_query($filterlist_sql_categories);
                while ($filterlist = tep_db_fetch_array($filterlist_query_categories)) {
                    $options_categories[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
                }
                $select.= '<div class="simple-drop-down">'.tep_draw_pull_down_menu('categories_id', $options_categories, (isset($HTTP_GET_VARS['cPath']) ? $HTTP_GET_VARS['cPath'] : ''), 'onchange="this.form.submit()" class=""').'</div>';
            } else {
                if (tep_db_num_rows($filterlist_query) > 1) {
                    if (isset($HTTP_GET_VARS['manufacturers_id']) && !empty($HTTP_GET_VARS['manufacturers_id'])) {
                        $select.= tep_draw_hidden_field('manufacturers_id', $HTTP_GET_VARS['manufacturers_id']);
                        $options = array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES));
                    } else {
                        $select.= tep_draw_hidden_field('cPath', $cPath);
                        $options = array(array('id' => '', 'text' => TEXT_ALL_MANUFACTURERS));
                    }
                    if (isset($HTTP_GET_VARS['authors_id']) && !empty($HTTP_GET_VARS['authors_id'])) {
                        $select.= tep_draw_hidden_field('authors_id', $HTTP_GET_VARS['authors_id']);
                        $options_authors = array(array('id' => '', 'text' => TEXT_ALL_AUTHORS));
                    } else {
                        $select.= tep_draw_hidden_field('cPath', $cPath);
                        $options_authors = array(array('id' => '', 'text' => TEXT_ALL_AUTHORS));
                    }
                    if (isset($HTTP_GET_VARS['translators_id']) && !empty($HTTP_GET_VARS['translators_id'])) {
                        $select.= tep_draw_hidden_field('translators_id', $HTTP_GET_VARS['translators_id']);
                        $options_translators = array(array('id' => '', 'text' => TEXT_ALL_TRANSLATORS));
                    } else {
                        $select.= tep_draw_hidden_field('cPath', $cPath);
                        $options_translators = array(array('id' => '', 'text' => TEXT_ALL_TRANSLATORS));
                    }
                    $select.= tep_draw_hidden_field('sort', $HTTP_GET_VARS['sort']);
                    while ($filterlist = tep_db_fetch_array($filterlist_query)) {
                        $options[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
                    }
                    while ($filterlist = tep_db_fetch_array($filterlist_query_author)) {
                        $options_authors[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
                    }
                    while ($filterlist = tep_db_fetch_array($filterlist_query_translator)) {
                        $options_translators[] = array('id' => $filterlist['id'], 'text' => $filterlist['name']);
                    }






                    $select.='<div class="simple-drop-down">'. tep_draw_pull_down_menu('filter_id', $options, (isset($HTTP_GET_VARS['filter_id']) ? $HTTP_GET_VARS['filter_id'] : ''), 'onchange="this.form.submit()" class=""').'</div>';
                    $select.='<div class="simple-drop-down">'. tep_draw_pull_down_menu('authors_id', $options_authors, (isset($HTTP_GET_VARS['authors_id']) ? $HTTP_GET_VARS['authors_id'] : ''), 'onchange="this.form.submit()" class=""').'</div>';
                    $select.='<div class="simple-drop-down">'. tep_draw_pull_down_menu('translators_id', $options_translators, (isset($HTTP_GET_VARS['translators_id']) ? $HTTP_GET_VARS['translators_id'] : ''), 'onchange="this.form.submit()" class=""').'</div>';
                }
            }

            echo   tep_draw_form('filter', FILENAME_DEFAULT, 'get') . '' . TEXT_SHOW . '&nbsp;';

          echo  '<div class="shop-grid-controls">
                                    <div class="entry">
                                        <div class="inline-text">Sorty by</div>
                                        ';
            echo $select;


                                      echo '</div>
                                </div>';





            echo tep_hide_session_id() . '</form>';
        }
    }else{


        echo '';
    }





}





















function display_books($products_query){
    $currencies = new currencies();

    while ($new_products = tep_db_fetch_array($products_query)) {

        echo '<div class="product-entry custom-col-5-in-row">
             <div class="product-slide-entry shift-image">
             <div class="product-image">' . tep_image(DIR_WS_IMAGES . $new_products['products_image'], $new_products['products_name']) . '
             ' . tep_image(DIR_WS_IMAGES . $new_products['products_image'], $new_products['products_name']) . '
             <div class="bottom-line left-attached">
             <a class="bottom-line-a square"  onclick="add_to_cart('.$new_products['products_id'] .'); return true;"><i class="fa fa-shopping-cart"></i></a>
             <a class="bottom-line-a square open-product" onclick="view_product('.$new_products['products_id'].')"><i class="fa fa-info"></i></a>
             </div>
             </div>
             <a class="title" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $new_products["products_id"]) . '">'.$new_products['products_name'].'</a>
             <div class="price">
             <div class="current">'. $currencies->display_price($new_products['products_price'], tep_get_tax_rate($new_products['products_tax_class_id'])) .'</div>
             </div>
             </div>
             <div class="clear"></div>
             </div>';
    }

}






    function getData() {
        global $categories_string, $posters_string, $tree, $languages_id, $cPath, $cPath_array;
        $tree = array();

        $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c,"
            . " " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . POSTERS_CATEGORY_ID . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "'"
            . "  order by sort_order, cd.categories_name");
        $all = tep_db_fetch_all($categories_query);
        /*$posters_string .='<li><a href="#">&nbsp; ' . TEXT_ALL_COUNTRIES . ' &nbsp;</a>';
        $posters_string .='<ul>';
        foreach ($all as $categories) {
            $posters_string .= '<li><a href="index.php?cPath=' . $categories['categories_id'] . '"><img src="ext/jquery/ui/redmond/images/'.$menu_icon.'.png" class="menuBullet"/>' . $categories['categories_name'] . '&nbsp;</a>';
            $posters_string .='</li>';
        }
        $posters_string .='</ul>';
        $posters_string .='</li>';
*/
        $categories_string = '';
        $tree = array();

        $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '0' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "' and c.categories_id !='" . POSTERS_CATEGORY_ID . "' and c.categories_id !='" . MAGAZINES_CATEGORY_ID . "'   order by sort_order, cd.categories_name");
        $all = tep_db_fetch_all($categories_query);
        foreach ($all as $categories) {
//new code
            //  $products_in_category = tep_count_products_in_category($categories['categories_id']);
            //$categories_string .= '<li><a href="#"><img src="ext/jquery/ui/redmond/images/menu-icon.png" class="menuBullet"/>' . $categories['categories_name'] . '&nbsp;(' . $products_in_category . ')</a>';
            $categories_string .= '<div class="accordeon-title">' . $categories['categories_name'] . '</a></div><div class="accordeon-entry">';
            $parent_id = $categories['categories_id'];
            $subcategories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . (int) $parent_id . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "' order by sort_order, cd.categories_name");
            if (tep_db_num_rows($subcategories_query)) {
                $categories_string .='<ul>';
                //2nd level menu
                $sub = tep_db_fetch_all($subcategories_query);
                foreach ($sub as $sub_categories) {
                    //$products_in_sub_category = tep_count_products_in_category($sub_categories['categories_id']);
                    $hasSub = tep_has_category_subcategories($sub_categories['categories_id']);
                    if (!($hasSub)) {
                        $categories_string .= '<li><a href="index.php?cPath=' . $sub_categories['categories_id'] . '">&nbsp' . $sub_categories['categories_name'] . '&nbsp;</a>';
                    } else {
                        $categories_string .='<div class="accordeon-title"><li><a href="#">&nbsp;' . $sub_categories['categories_name'] . '&nbsp;</a></div>';
                    }


                    //3rd level
                    $sub_parent_id = $sub_categories['categories_id'];
                    $third_categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . (int) $sub_parent_id . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "' order by sort_order, cd.categories_name");
                    if (tep_db_num_rows($third_categories_query)) {
                        $categories_string .=' <div class="accordeon-entry"><ul>';
                        $third = tep_db_fetch_all($third_categories_query);
                        foreach ($third as $third_categories) {
                            //  $products_in_third_category = tep_count_products_in_category($third_categories['categories_id']);
                            //if ($products_in_third_category > 0) {
                            $categories_string .= '<li><a href="index.php?cPath=' . $third_categories['categories_id'] . '">&nbsp;&nbsp;' . $third_categories['categories_name'] . '&nbsp;</a>';
//                          } else {
//                              $categories_string .='<li><a href="#">&nbsp;&nbsp;' . $third_categories['categories_name'] . '</a>';
//                          }
                        }
                        $categories_string .='</ul></div>';
                    }
                    $categories_string .='</li>';
                }
                $categories_string .='</ul></div>';
            }
            $categories_string .='</li>';
        }






        //  $this->tep_show_category($first_element);


        $data .=
            '<div class="information-blocks margin-bottom-15 categories-border-wrapper">'.
            '   <div class="block-title size-3 border-bottom">' . MODULE_BOXES_CATEGORIES_BOX_TITLE . '</div>' .
            '<div class="accordeon">'.
            '   
                <div class="article-container style-1">
                <ul>' . $categories_string . '</ul></div>' .
            '</div></div>';



        return $data;
    }



function display_books_listening($listing_sql, $languages_id=null,$HTTP_GET_VARS=null,$current_category_id=null,$cPath=null,$is_poster=null){
    $products_query = tep_db_query($listing_sql->sql_query);
    $currencies = new currencies();
    $text = TEXT_DISPLAY_NUMBER_OF_PRODUCTS;


echo '<div class="information-blocks">
                    <div class="row">
                        <div class="col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                            <div class="page-selector">
                                <div class="pages-box hidden-xs">
                                    '.TEXT_RESULT_PAGE . ' ' . $listing_sql->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))).'
                                </div>';
    select_options($languages_id,$HTTP_GET_VARS,$current_category_id,$cPath, $is_poster);
    echo '<div class="clear"></div>
                            </div>
                            <div class="row shop-grid grid-view">';
    while ($new_products = tep_db_fetch_array($products_query)) {
        echo '<div class="col-md-3 col-sm-4 shop-grid-item">
                                    <div class="product-slide-entry shift-image">
                                        <div class="product-slide-entry shift-image">
                                            <div class="product-image">';
                                           if (file_exists(DIR_WS_IMAGES . $new_products['products_image'])) {

                    
                 
             echo tep_image(DIR_WS_IMAGES . $new_products['products_image'], $new_products['products_name'] ) . ' ' . tep_image(DIR_WS_IMAGES . $new_products['products_image'], $new_products['products_name'] );   
                }else {

                echo tep_image(DIR_WS_IMAGES .'/default_img.gif', 'DEFAULT') . ' ' . tep_image(DIR_WS_IMAGES . '/default_img.gif', 'DEFAULT');
                    
                }
                                                
                                          echo '      <div class="bottom-line left-attached">
                                                     <a class="bottom-line-a square"  onclick="add_to_cart('.$new_products['products_id'] .');"><i class="fa fa-shopping-cart"></i></a>
                                                     <a class="bottom-line-a square open-product" onclick="view_product('.$new_products['products_id'].')"><i class="fa fa-info"></i></a>          
                                                </div>
                                            </div>
                                            <a class="title" href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $new_products["products_id"]) . '">'.$new_products['products_name'].'</a>
                                            <div class="price">
                                                <div class="current">'. $currencies->display_price($new_products['products_price'], tep_get_tax_rate($new_products['products_tax_class_id'])) .'</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>';
    }


                    echo  '</div>
                            <div class="page-selector">
                                <div class="description">'. $listing_sql->display_count($text).'</div>
                                <div class="pages-box">
                                '.TEXT_RESULT_PAGE . ' ' . $listing_sql->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))).'
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>';

                        echo '<div class="col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8 blog-sidebar">'.getData().'</div></div>';




         echo pop_up();


}


function pop_up(){

    return '<div id="product-popup" class="overlay-popup">
            <div class="overflow">
                <div class="table-view">
                    <div class="cell-view">
                        <div class="close-layer"></div>
                        <div class="popup-container">
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-sm-6 information-entry">
                                    <div class="product-preview-box">
                                        <div class="product-preview-swiper">
                                            <div>
                                                <div>
                                                    <div class="product-zoom-image" id="product_image">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pagination"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 information-entry">
                                    <div class="product-detail-box">
                                        <h1 id="product-title" class="title-green product-title" dir="rtl"></h1>
                                        <h3 id="product-subtitle" class="title-product product-subtitle" dir="rtl"></h3>
                                        <h3 class="product-subtitle "><b class="bold">ISBN</b>:<span id="product-isbn"></span> </h3>
                                        <div class="product-description detail-info-entry"><b class="bold">Edition</b>: <span id="product-edition"></span></div>
                                        <h3 class="product-subtitle"><b class="bold">Available Volumes</b>: <span id="product-volumes"></span></h3>
                                        <div class="price detail-info-entry">
                                            <div class="current" dir="ltr"><span class="product_price"></span></div>
                                        </div>
                                        <div class="quantity-selector detail-info-entry">
                                            <div class="detail-info-entry-title"><b class="bold">Quantity</b></div>
                                            <div class="entry number-minus">&nbsp;</div>
                                            <div class="entry number" id="items_toCart">1</div>
                                            <div class="entry number-plus">&nbsp;</div>
                                            <div id="item_id"></div>
                                            
                                        </div>
                                        <div class="detail-info-entry">

                                            <a class="button style-10" onclick="add_to_cart();">Add to cart</a>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="close-popup"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';


}




