<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

if ( (!isset($new_products_category_id)) || ($new_products_category_id == '0') ) {
    $new_products_query = tep_db_query("select p.products_id, p.products_image, p.products_tax_class_id, pd.products_name, if(s.status, s.specials_new_products_price, p.products_price) as products_price from " . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_status = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' order by p.products_date_added desc limit " . MAX_DISPLAY_NEW_PRODUCTS);
} else {
    $new_products_query = tep_db_query("select distinct p.products_id, p.products_image, p.products_tax_class_id, pd.products_name, if(s.status, s.specials_new_products_price, p.products_price) as products_price "
        . "from " . TABLE_PRODUCTS . " p left join " . TABLE_SPECIALS . " s on p.products_id = s.products_id, " . TABLE_PRODUCTS_DESCRIPTION . " pd, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, " . TABLE_CATEGORIES . " c where p.products_id = p2c.products_id and p2c.categories_id = c.categories_id and c.categories_id >= '" . (int)$new_products_category_id . "' and c.categories_id < '" . (int)MAGAZINES_CATEGORY_ID . "' and p.products_status = '1' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' order by p.products_date_added desc limit " . MAX_DISPLAY_NEW_PRODUCTS);
}



while ($new_products = tep_db_fetch_array($new_products_query)) {

    echo '<div class="product-entry custom-col-5-in-row">
             <div class="product-slide-entry shift-image">
             <div class="product-image">' . tep_image(DIR_WS_IMAGES . $new_products['products_image'], $new_products['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '
             ' . tep_image(DIR_WS_IMAGES . $new_products['products_image'], $new_products['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '
             <div class="bottom-line left-attached">
             <a class="bottom-line-a square"><i class="fa fa-shopping-cart"></i></a>
             <a class="bottom-line-a square open-product"><i class="fa fa-info"></i></a>
             </div>
             </div>
             <a class="title" href="product.html">'.$new_products['products_name'].'</a>
             <div class="price">
             <div class="current">'. $currencies->display_price($new_products['products_price'], tep_get_tax_rate($new_products['products_tax_class_id'])) .'</div>
             </div>
             </div>
             <div class="clear"></div>
             </div>';
}


?>

<h2><?php // echo sprintf(TABLE_HEADING_NEW_PRODUCTS, strftime('%B')); ?></h2>












