<?php

/*

 v 0.9 2004/09/19 14:29:56 Artem Marchenko www.smokingplaza.com $

  Based upon region based rates (regions.php), 
   http://www.oscommerce.com/community/contributions,818
  Based upon zones.php by wilt, written for:

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com
  Copyright (c) 2002 osCommerce
  Released under the GNU General Public License

  -----------------------------------------------------------------------------
  Regionally Boxed -  Rates Based on a number of boxes used to send goods and on weight of
  actual goods being sent. Different costs can be set for different regions (composed
  of countries and/or states)

  Region-related part is completely taken from Region Based Rates by Jorge Suarez:
   http://www.oscommerce.com/community/contributions,818
   
  This module allows you create shipping regions by dividing states of the USA
  (or other country) in different groups.
  Each group will then have it's own shipping price which can based on price
  or weight. 

  This module is perfect when for those of you need to charge different when
  shipping to different parts of the country.

  Features
  0..Costs can be specified ONLY as price for box(es) + weight per weight unit
  1..Regions can be composed of US States or of any other country
  2..Order weight or price can be used to calculate shipping price.
  3..Any number of regions
  4..Handling fee can be added.

  Artem
  
  Set the number of regions you need with
  $this->regions = xx;  

  Please note that any country / state that is not in one of the groups
  will not be able to checkout if this the only shipping you provide.
  However it will display a nice message saying so.

  Region entry [single_box_price]:[amount_of_units_to_change_price]:[price_increase]
  E.g. 5:2:6 means 5 per box + 6 per every 2 weight/price/per_item units
  Note, that if weight is e.g. 1.57, shipping cost is 5 + 6 = 11



  Written by Artem Marchenko (region_boxed@dwarf.mailshell.com)

*/

//  class region_boxed {
//    var $code, $title, $description, $enabled, $regions;
	class regionBoxed {
      var $code, $title, $description, $enabled, $regions;


// class constructor
    function regionBoxed() {
      $this->code = 'regionBoxed';
      $this->title = MODULE_SHIPPING_REGION_BOXED_TEXT_TITLE;
      $this->description = MODULE_SHIPPING_REGION_BOXED_TEXT_DESCRIPTION;
      $this->icon = '';
      $this->enabled = MODULE_SHIPPING_REGION_BOXED_STATUS;
      // CUSTOMIZE THIS SETTING FOR THE NUMBER OF States NEEDED
      $this->regions = 3;
    }

// class methods
    function quote($method = '') {
      global $order, $shipping_weight, $cart, $total_count, $shipping_num_boxes;
      
      if (MODULE_SHIPPING_REGION_BOXED_MODE == 'price') {
        $order_total_price = $cart->show_total();
      } 
      if (MODULE_SHIPPING_REGION_BOXED_MODE == 'weight') {
        //$order_total_price = $shipping_weight;
	$order_total_price = $cart->show_weight();
//echo "weight mode: $order_total_price<br>";
      }
      if (MODULE_SHIPPING_REGION_BOXED_MODE == 'per_item') {
        $order_total_price = $total_count;
      }      

      $dest_state = $order->delivery['state'];
      $dest_country = $order->delivery['country']['title'];
      
      $dest_region = 0;
      $error = false;

      for ($i=1; $i<=$this->regions; $i++) {
        $regions_table = constant('MODULE_SHIPPING_REGION_BOXED' . $i);
        $country_states_or_countries = split("[,]", $regions_table);
        if (in_array($dest_state, $country_states_or_countries)) {
          $dest_region = $i;
          break;
        }
      }
      if ($dest_region == 0) {
	      for ($i=1; $i<=$this->regions; $i++) {
	        $regions_table = constant('MODULE_SHIPPING_REGION_BOXED' . $i);
	        $country_states_or_countries = split("[,]", $regions_table);
	        if (in_array($dest_country, $country_states_or_countries)) {
	          $dest_region = $i;
	          break;
	        }
	      }
      }

      if ($dest_region == 0) {
        $error = true;
      } else {
        $shipping = -1;
        $region_cost = constant('MODULE_SHIPPING_REGION_BOXED_COST' . $i);

        $regions_table = split("[:,]" , $region_cost);

//        if ((MODULE_SHIPPING_REGION_BOXED_MODE == 'price') || (MODULE_SHIPPING_REGION_BOXED_MODE == 'weight')) {
//echo "price mode<br>";

	$table_cost = split(":" , $region_cost);
	$boxCost = $table_cost[0];
	$weightStep = $table_cost[1];
	$stepCost = $table_cost[2];
//echo "boxCost: ".$boxCost."<br>";
//echo "weightStep: $weightStep<br>";
//echo "stepCost: $stepCost<br>";
//echo "shipping_num_boxes: $shipping_num_boxes<br>";
//echo "order_total_price: $order_total_price<br>";
	$shipping = $boxCost*$shipping_num_boxes;
	$stepCount = ceil($order_total_price/$weightStep);
//echo "stepCount: $stepCount<br>";
	$shipping += $stepCount*$stepCost;
        $shipping_method = MODULE_SHIPPING_REGION_BOXED_TEXT_WAY . ' ' . "$dest_state, $dest_country" . ' ' . MODULE_SHIPPING_REGION_BOXED_TEXT_UNITS;        
//        }
//        if (MODULE_SHIPPING_REGION_BOXED_MODE == 'per_item') {
//echo "per item mode<br>";
//		 	$shipping = $regions_table[1] * $order_total_price;
//   	         	$shipping_method = MODULE_SHIPPING_REGION_BOXED_ITEM . ' ' . $total_count . ' ' . MODULE_SHIPPING_REGION_BOXED_ITEMS . ' ' . "$dest_state, $dest_country" . ' ' . MODULE_SHIPPING_REGION_BOXED_TEXT_UNITS;
//   	}        
        
        
        if ($shipping == -1) {
          $shipping_cost = 0;
          $shipping_method = MODULE_SHIPPING_REGION_BOXED_UNDEFINED_RATE;
//echo "undefined rate<br>";
        } else {
          $shipping_cost = ($shipping + MODULE_SHIPPING_REGION_BOXED_HANDLING + SHIPPING_HANDLING);
        }
      }

      $this->quotes = array('id' => $this->code,
                            'module' => MODULE_SHIPPING_REGION_BOXED_TEXT_TITLE,
                            'methods' => array(array('id' => $this->code,
                                                     'title' => $shipping_method,
                                                     'cost' => $shipping_cost)));

      if (tep_not_null($this->icon)) $this->quotes['icon'] = tep_image($this->icon, $this->title);

      if ($error == true) $this->quotes['error'] = MODULE_SHIPPING_REGION_BOXED_INVALID_ZONE;
      return $this->quotes;
    }

    function check() {
      if (!isset($this->_check)) {
        $check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_REGION_BOXED_STATUS'");
        $this->_check = tep_db_num_rows($check_query);
      }
      return $this->_check;
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) VALUES ('Enable US Regions Method', 'MODULE_SHIPPING_REGION_BOXED_STATUS', '1', 'Do you want to offer Regions rate shipping?', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Handling Fee', 'MODULE_SHIPPING_REGION_BOXED_HANDLING', '0', 'Handling Fee for this shipping method', '6', '0', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Mode', 'MODULE_SHIPPING_REGION_BOXED_MODE', 'weight', 'Is the shipping table based on total Weight or Total amount of order.', '6', '0', 'tep_cfg_select_option(array(\'weight\', \'price\', \'per_item\'), ', now())");
      for ($i = 1; $i <= $this->regions; $i++) {
        $default_countries = '';
        if ($i == 1) {
          $default_states_or_countries = 'United States,Canada';
          $default_prices = '1:1:4';
        }
        if ($i == 2) {
          $default_states_or_countries = 'Alabama,Arizona,Arkansas,California,Colorado,Connecticut,Delaware,District of Columbia,Florida,Georgia,Idaho,Illinois,Indiana,Iowa,Kansas,Kentucky,Louisiana,Maine,Maryland,Massachusetts,Michigan,Minnesota,Mississippi,Missouri,Montana';
          $default_prices = '1:1:4';
        }
        if ($i == 3) {
          $default_states_or_countries = 'Nebraska,Nevada,New Hampshire,New Jersey,New Mexico,New York,North Carolina,North Dakota,Ohio,Oklahoma,Oregon,Pennsylvania,Rhode Island,South Carolina,South Dakota,Texas,Utah,Vermont,Virginia,Washington,West Virginia,Wisconsin,Wyoming';
          $default_prices = '1:1:4';
        }
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Region " . $i ." States/Countries', 'MODULE_SHIPPING_REGION_BOXED" . $i ."', '" . $default_states_or_countries . "', 'Comma separated list of States and/or Countries', '6', '0', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Region " . $i ." Shipping Table', 'MODULE_SHIPPING_REGION_BOXED_COST" . $i ."', '" . $default_prices . "' , 'Example: 3:8,10.50.....(box cost)<b>:</b>(increase step)', '6', '0', now())");
      }
    }

    function remove() {
      $keys = '';
      $keys_array = $this->keys();
      for ($i=0; $i<sizeof($keys_array); $i++) {
        $keys .= "'" . $keys_array[$i] . "',";
      }
      $keys = substr($keys, 0, -1);

      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in (" . $keys . ")");
    }

    function keys() {
      $keys = array('MODULE_SHIPPING_REGION_BOXED_STATUS', 'MODULE_SHIPPING_REGION_BOXED_HANDLING', 'MODULE_SHIPPING_REGION_BOXED_MODE');

      for ($i=1; $i<=$this->regions; $i++) {
        $keys[] = 'MODULE_SHIPPING_REGION_BOXED' . $i;
        $keys[] = 'MODULE_SHIPPING_REGION_BOXED_COST' . $i;
      }

      return $keys;
    }
  }
?>
