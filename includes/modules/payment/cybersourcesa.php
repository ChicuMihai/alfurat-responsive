<?php

/*
  $Id: cybersourcesa.php,v 1.0 2014/09/15 $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
 */

class cybersourcesa {

	var $code, $title, $description, $sort_order, $enabled;

// class constructor
	function cybersourcesa() {
		global $order;

		$this->code = 'cybersourcesa';
		$this->title = MODULE_PAYMENT_CYBS_SA_TEXT_TITLE;
		$this->description = MODULE_PAYMENT_CYBS_SA_TEXT_DESCRIPTION;
		$this->sort_order = MODULE_PAYMENT_CYBS_SA_SORT_ORDER;
		$this->enabled = ((MODULE_PAYMENT_CYBS_SA_STATUS == 'True') ? true : false);

		if ((int) MODULE_PAYMENT_CYBS_SA_ORDER_STATUS_ID > 0) {
			$this->order_status = MODULE_PAYMENT_CYBS_SA_ORDER_STATUS_ID;
		}

		if (is_object($order)) {
			$this->update_status();
		}
		$this->form_action_url = tep_href_link(FILENAME_CHECKOUT_PAYMENT, 'action=audi','SSL',TRUE,FALSE);

		//Special Functions made for the display of the config properties



		function cybs_sa_select_col_option($select_array, $key_value, $key = '') {

			$string = '<br><table border="0" width="100%" cellspacing="0" cellpadding="0">';
			$n = sizeof($select_array);

			for ($i = 0; $i < $n;) {
				$string .= '<tr>';
				for ($i2 = 0; $i2 < ($n / 2); $i2++, $i++) {
					$string .= '<td class="infoBoxContent">';
					$name = ((tep_not_null($key)) ? 'configuration[' . $key . ']' : 'configuration_value');
					$string .= '<input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';

					if ($key_value == $select_array[$i]) {
						$string .= ' CHECKED';
					}
					$string .= '> ' . $select_array[$i] . '&nbsp;&nbsp;</td>';
				}
				$string .= '</tr>';
			}

			$string .= '</table>';
			return $string;
		}

		function cybs_sa_card_options($select_array, $key_value, $key = '') {

			$string = "\n\n" . '<script language="javascript">' . "\n" .
					'function types(frm) {' . "\n" .
					'  var cards = "";' . "\n" .
					'  for (var i = 0; i < frm.elements.length; i++) {' . "\n" .
					'    if(frm[i].name == \'configuration[MODULE_PAYMENT_CYBS_SA_CARD_TYPES]\')' . "\n" .
					'      var index = i;' . "\n" .
					'  }' . "\n" .
					'  for(var i = 0; i < frm.card_types.length; i++) {' . "\n" .
					'    if(frm.card_types[i].checked)' . "\n" .
					'      cards += 1;' . "\n" .
					'    else' . "\n" .
					'      cards += 0;' . "\n" .
					'  }' . "\n" .
					'  frm[index].value = cards;' . "\n" .
					'}' . "\n" .
					'</script>' . "\n\n" .
					'<input type="hidden" name="configuration[MODULE_PAYMENT_CYBS_SA_CARD_TYPES]" value="' . MODULE_PAYMENT_CYBS_SA_CARD_TYPES . '">' .
					'<br><table border="0" width="100%" cellspacing="0" cellpadding="0">';
			$n = sizeof($select_array);

			for ($i = 0; $i < $n;) {
				$string .= '<tr>';

				for ($i2 = 0; $i2 < ($n / 3); $i2++, $i++) {
					$string .= '<td class="infoBoxContent">';
					$string .= '<input type="checkbox" name="card_types" onClick="types(this.form)"';

					if (cybs_sa_bitmap_value($i)) {
						$string .= ' CHECKED';
					}
					$string .= '> ' . $select_array[$i] . '&nbsp;&nbsp;</td>';
				}
				$string .= '</tr>';
			}

			$string .= '</table>';
			return $string;
		}

		function cybs_sa_bitmap_value($card_number) {
			$bitmap = MODULE_PAYMENT_CYBS_SA_CARD_TYPES;

			return (($bitmap / pow(10, 5 - $card_number)) % 10);
		}

		//End of functions
	}

// class methods
	function update_status() {
		global $order;

		if (($this->enabled == true) && ((int) MODULE_PAYMENT_CYBS_SA_ZONE > 0)) {
			$check_flag = false;
			$check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_PAYMENT_CYBS_SA_ZONE . "' and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id");
			while ($check = tep_db_fetch_array($check_query)) {
				if ($check['zone_id'] < 1) {
					$check_flag = true;
					break;
				} else if ($check['zone_id'] == $order->billing['zone_id']) {
					$check_flag = true;
					break;
				}
			}

			if ($check_flag == false) {
				$this->enabled = false;
			}
		}
	}

	function javascript_validation() {
		
	}

	function selection() {
		global $order;

		for ($i = 1; $i < 13; $i++) {
			$expires_month[] = array('id' => sprintf('%02d', $i), 'text' => strftime('%B', mktime(0, 0, 0, $i, 1, 2000)));
		}

		$today = getdate();
		for ($i = $today['year']; $i < $today['year'] + 10; $i++) {
			$expires_year[] = array('id' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)), 'text' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)));
		}

		$types = array('Visa', 'MasterCard', 'American Express', 'Discover', 'Diners Club', 'JCB');
		for ($i = 0; $i < sizeof($types); $i++) {
			if (cybs_sa_bitmap_value($i)) {
				$card_types[] = array('id' => '00' . ($i + 1), 'text' => $types[$i]);
			}
		}

		$selection = array('id' => $this->code,
			'module' => 'Powered by: areeba',
			'fields' => array());

		return $selection;
	}

	function pre_confirmation_check() {
		return FALSE;
	}

	function confirmation() {
		$confirmation = array(
			'title' => MODULE_PAYMENT_CYBS_SA_TEXT_TITLE,
		);
		if (count($_GET) > 0 && isset($_GET["vpc_TxnResponseCode"])) {
			//the the fields passed from the url to be displayed
			$amount = $this->null2unknown(addslashes($_GET["amount"]) / 100);
//			$locale = null2unknown(addslashes($_GET["vpc_Locale"]));
//			$batchNo = null2unknown(addslashes($_GET["vpc_BatchNo"]));
//			$command = null2unknown(addslashes($_GET["vpc_Command"]));
			$message = $this->null2unknown(addslashes($_GET["vpc_Message"]));
//			$version = null2unknown(addslashes($_GET["vpc_Version"]));
			$cardType = $this->null2unknown(addslashes($_GET["vpc_Card"]));
			//$orderInfo = null2unknown(addslashes($_GET["orderInfo"]));
			//	$receiptNo = null2unknown(addslashes($_GET["vpc_ReceiptNo"]));
			//	$merchantID = null2unknown(addslashes($_GET["merchant"]));
			//	$authorizeID = null2unknown(addslashes($_GET["vpc_AuthorizeId"]));
			//$merchTxnRef = null2unknown(addslashes($_GET["merchTxnRef"]));
//			$transactionNo = null2unknown(addslashes($_GET["vpc_TransactionNo"]));
			//$acqResponseCode = null2unknown(addslashes($_GET["vpc_AcqResponseCode"]));
			$txnResponseCode = $this->null2unknown(addslashes($_GET["vpc_TxnResponseCode"]));
			// Show 'Error' in title if an error condition

			$confirmation['fields'][] = array('title' => 'Purchase Amount', 'field' => $amount . ' ' . LANGUAGE_CURRENCY);
			$confirmation['fields'][] = array('title' => 'Message', 'field' => $message);
			$confirmation['fields'][] = array('title' => 'Card Type', 'field' => $cardType);
			$confirmation['fields'][] = array('title' => 'VPC Transaction Response Code', 'field' => $txnResponseCode);
			$confirmation['fields'][] = array('title' => 'Transaction Response Code Description', 'field' => $this->getResponseDescription($txnResponseCode));
		}
		return $confirmation;
	}

//function to map each response code number to a text message	
	function getResponseDescription($responseCode) {
		switch ($responseCode) {
			case "0" : $result = "Transaction Successful";
				break;
			case "?" : $result = "Transaction status is unknown";
				break;
			case "1" : $result = "Unknown Error";
				break;
			case "2" : $result = "Bank Declined Transaction";
				break;
			case "3" : $result = "No Reply from Bank";
				break;
			case "4" : $result = "Expired Card";
				break;
			case "5" : $result = "Insufficient funds";
				break;
			case "6" : $result = "Error Communicating with Bank";
				break;
			case "7" : $result = "Payment Server System Error";
				break;
			case "8" : $result = "Transaction Type Not Supported";
				break;
			case "9" : $result = "Bank declined transaction (Do not contact Bank)";
				break;
			case "A" : $result = "Transaction Aborted";
				break;
			case "C" : $result = "Transaction Cancelled";
				break;
			case "D" : $result = "Deferred transaction has been received and is awaiting processing";
				break;
			case "E" : $result = "Invalid Credit Card";
				break;
			case "F" : $result = "3D Secure Authentication failed";
				break;
			case "I" : $result = "Card Security Code verification failed";
				break;
			case "G" : $result = "Invalid Merchant";
				break;
			case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)";
				break;
			case "N" : $result = "Cardholder is not enrolled in Authentication scheme";
				break;
			case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed";
				break;
			case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed";
				break;
			case "S" : $result = "Duplicate SessionID (OrderInfo)";
				break;
			case "T" : $result = "Address Verification Failed";
				break;
			case "U" : $result = "Card Security Code Failed";
				break;
			case "V" : $result = "Address Verification and Card Security Code Failed";
				break;
			case "X" : $result = "Credit Card Blocked";
				break;
			case "Y" : $result = "Invalid URL";
				break;
			case "B" : $result = "Transaction was not completed";
				break;
			case "M" : $result = "Please enter all required fields";
				break;
			case "J" : $result = "Transaction already in use";
				break;
			case "BL" : $result = "Card Bin Limit Reached";
				break;
			case "CL" : $result = "Card Limit Reached";
				break;
			case "LM" : $result = "Merchant Amount Limit Reached";
				break;
			case "Q" : $result = "IP Blocked";
				break;
			case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed";
				break;
			case "Z" : $result = "Bin Blocked";
				break;

			default : $result = "Unable to be determined";
		}
		return $result;
	}

	//function to display a No Value Returned message if value of field is empty
	function null2unknown($data) {
		if ($data == "")
			return "No Value Returned";
		else
			return $data;
	}

	function process_button() {
		return false;
	}

	function before_process() {
		
	}

	function after_process() {
		// Nothing to do
	}

	function check() {
		if (!isset($this->_check)) {
			$check_query = tep_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_PAYMENT_CYBS_SA_STATUS'");
			$this->_check = tep_db_num_rows($check_query);
		}
		return $this->_check;
	}

	function install() {
		if (file_exists(DIR_FS_CATALOG . 'includes/local/security.php')) {
			$default_key_value = 'Installed';
		} else {
			$default_key_value = 'File Not Found';
		}

		tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Bank Audi Payment Online Module', 'MODULE_PAYMENT_CYBS_SA_STATUS', 'False', 'Do you want to enable Bank Audi Payment Online Module?', '6', '0', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
		tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Set MerchantID', 'MODULE_PAYMENT_CYBS_SA_PROFILE_ID', '0', 'Set the MerchantID of your  profile ', '6', '0', now())");
		tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Set Merchant AccessCode', 'MODULE_PAYMENT_CYBS_SA_ACCESS_KEY', '0', 'Set the Merchant AccessCode of your profile ', '6', '0', now())");
		tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Set The Secure Hash Secret', 'MODULE_PAYMENT_SECURE_SECRET', '0', 'Set the Secure Hash Secret of your profile ', '6', '0', now())");

		
	}

	function remove() {
		tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}

	function keys() {
		return array('MODULE_PAYMENT_CYBS_SA_STATUS', 'MODULE_PAYMENT_CYBS_SA_PROFILE_ID', 'MODULE_PAYMENT_CYBS_SA_ACCESS_KEY','MODULE_PAYMENT_SECURE_SECRET');
	}

}

?>