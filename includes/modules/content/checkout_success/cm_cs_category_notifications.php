<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

  class cm_cs_category_notifications {
    var $code;
    var $group;
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function cm_cs_category_notifications() { 
      $this->code = get_class($this);
      $this->group = basename(dirname(__FILE__));

      $this->title = MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_TITLE;
      $this->description = MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_DESCRIPTION;

      if ( defined('MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_STATUS') ) { 
        $this->sort_order = MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_SORT_ORDER;
        $this->enabled = (MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_STATUS == 'True');
      }
    }

    function execute() {
      global $HTTP_GET_VARS, $HTTP_POST_VARS, $oscTemplate, $customer_id, $order_id;

      if ( tep_session_is_registered('customer_id') ) {
//        $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
//        $global = tep_db_fetch_array($global_query);

    
          if ( isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'update') ) {
            if ( isset($HTTP_POST_VARS['notify']) && is_array($HTTP_POST_VARS['notify']) && !empty($HTTP_POST_VARS['notify']) ) {
              $notify = array_unique($HTTP_POST_VARS['notify']);

              foreach ( $notify as $n ) {
                if ( is_numeric($n) && ($n > 0) ) {
                  $check_query = tep_db_query("select categories_id from " . TABLE_CAT_NOTIFICATIONS . " where categories_id = '" . (int)$n . "' and customers_id = '" . (int)$customer_id . "' limit 1");

                  if ( !tep_db_num_rows($check_query) ) {
                    tep_db_query("insert into " . TABLE_CAT_NOTIFICATIONS . " (categories_id, customers_id, date_added) values ('" . (int)$n . "', '" . (int)$customer_id . "', now())");
                  }
                }
              }
            }
          }

          $products_displayed = array();

          $products_query = tep_db_query("select C.categories_id, categories_name from " . TABLE_ORDERS_PRODUCTS . " O, " . TABLE_PRODUCTS_TO_CATEGORIES . " C, " . TABLE_CATEGORIES_DESCRIPTION . " D   where orders_id = '" . (int)$order_id . "' and O.products_id = C.products_id and D.categories_id=C.categories_id and C.categories_id not in (select categories_id from " . TABLE_CAT_NOTIFICATIONS . " where customers_id=".$customer_id.")  order by categories_name");
          while ($products = tep_db_fetch_array($products_query)) { 
            if ( !isset($products_displayed[$products['categories_id']]) && ($products['categories_id']!=POSTERS_CATEGORY_ID)) { 
              $products_displayed[$products['categories_id']] = tep_draw_checkbox_field('notify[]', $products['categories_id']) . ' ' . $products['categories_name'];
            }
          }

          $products_notifications = implode('<br />', $products_displayed);

          ob_start();
          include(DIR_WS_MODULES . 'content/' . $this->group . '/templates/category_notifications.php');
          $template = ob_get_clean();

          $oscTemplate->addContent($template, $this->group);
        
      }
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_STATUS');
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Category Notifications Module', 'MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_STATUS', 'True', 'Should the product notifications block be shown on the checkout success page?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '3', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_STATUS','MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_SORT_ORDER');
    }
  }
?>
