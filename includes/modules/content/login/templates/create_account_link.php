
<div class="col-sm-6 information-entry">
    <div class="login-box">
        <div class="article-container style-1">
            <h3><?php echo MODULE_CONTENT_LOGIN_HEADING_NEW_CUSTOMER; ?></h3>
            <p><?php echo MODULE_CONTENT_LOGIN_TEXT_NEW_CUSTOMER; ?></p>
            <p><?php echo MODULE_CONTENT_LOGIN_TEXT_NEW_CUSTOMER_INTRODUCTION; ?></p>
        </div>
        <a href="create_account.php" class="button style-12">Register Account</a>
    </div>
</div>

