<div class="information-blocks">
    <div class="row">
        <div class="col-sm-6 information-entry">
            <div class="login-box">
                <div class="article-container style-1">
                    <h3><?php echo MODULE_CONTENT_LOGIN_HEADING_RETURNING_CUSTOMER; ?></h3>
                    <p><?php echo MODULE_CONTENT_LOGIN_TEXT_RETURNING_CUSTOMER; ?></p>

                    <?php echo tep_draw_form('login', tep_href_link(FILENAME_LOGIN, 'action=process', 'SSL'), 'post', '', true); ?>
                    <label><?php echo ENTRY_EMAIL_ADDRESS; ?></label>
                    <?php echo tep_draw_input_field('email_address', '', 'class="simple-field"'); ?>
                    <label><?php echo ENTRY_PASSWORD; ?></label>
                    <?php echo tep_draw_password_field('password', '', 'class="simple-field"'); ?>
                    <p><?php echo '<a href="' . tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL') . '">' . MODULE_CONTENT_LOGIN_TEXT_PASSWORD_FORGOTTEN . '</a>'; ?></p>
                    <div class="button style-10">Login Page<input type="submit" value=""></div>
                    </form>
                </div>
            </div>
        </div>
  		