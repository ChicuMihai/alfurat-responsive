<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */
if (defined('FEATURED_PRODUCTS_DISPLAY') AND FEATURED_PRODUCTS_DISPLAY == 'true') {

    $featured_products_category_id = (isset($new_products_category_id)) ? $new_products_category_id : '0';
    $cat_name_query = tep_db_query('SELECT `categories_name` FROM ' . TABLE_CATEGORIES_DESCRIPTION . " WHERE `categories_id` = '" . $featured_products_category_id . "' limit 1");
    $cat_name_fetch = tep_db_fetch_array($cat_name_query);
    $cat_name = $cat_name_fetch['categories_name'];
    $info_box_contents = array();

    list($usec, $sec) = explode(' ', microtime());
    srand((float) $sec + ((float) $usec * 100000));
    $mtm = rand();

    if ((!isset($featured_products_category_id)) || ($featured_products_category_id == '0')) {
        $title = '<a href="' . tep_href_link(FILENAME_FEATURED_PRODUCTS) . '" class="boxTitleLink">' . TABLE_HEADING_FEATURED_PRODUCTS . '</a>';

        // Phocea Optimize featured query
        // Ben: Option to only show featured products on sale
        $query = 'SELECT p.products_id, p.products_image, p.products_tax_class_id, IF (s.status, s.specials_new_products_price, NULL) AS specials_new_products_price, p.products_price, pd.products_name, pd.products_description ';

        if (defined('FEATURED_PRODUCTS_SPECIALS_ONLY') AND FEATURED_PRODUCTS_SPECIALS_ONLY == 'true') {
            $query .= 'FROM ' . TABLE_SPECIALS . ' s LEFT JOIN ' . TABLE_PRODUCTS . ' p ON s.products_id = p.products_id ';
        } else {
            $query .= 'FROM ' . TABLE_PRODUCTS . ' p LEFT JOIN ' . TABLE_SPECIALS . ' s ON p.products_id = s.products_id ';
        }

        $query .= 'LEFT JOIN ' . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.products_id = pd.products_id AND pd.language_id = '" . $languages_id . "'
    LEFT JOIN " . TABLE_FEATURED . " f ON p.products_id = f.products_id 
		 
    WHERE p.products_status = '1' AND f.status = '1'   order by p.products_date_added  DESC ,p.products_date_released  DESC limit " . MAX_DISPLAY_FEATURED_PRODUCTS;

        $featured_products_query = tep_db_query($query);
        //bring all authors
    } else {
        $title = sprintf(TABLE_HEADING_FEATURED_PRODUCTS_CATEGORY, $cat_name);
        $subcategories_array = array();
        tep_get_subcategories($subcategories_array, $featured_products_category_id);
        $featured_products_category_id_list = tep_array_values_to_string($subcategories_array);
        if ($featured_products_category_id_list == '') {
            $featured_products_category_id_list .= $featured_products_category_id;
        } else {
            $featured_products_category_id_list .= ',' . $featured_products_category_id;
        }

        if (defined('FEATURED_PRODUCTS_SUB_CATEGORIES') AND FEATURED_PRODUCTS_SUB_CATEGORIES == 'true') {
            // current catID as starting value
            $cats[] = $new_products_category_id;
            // put cat-IDs of all cats nested in current branch into $cats array,
            // go through all subbranches
            for ($i = 0; $i < count($cats); $i++) {
                $categorie_query = tep_db_query('SELECT `categories_id` FROM ' . TABLE_CATEGORIES . " WHERE parent_id = '" . (int) $cats[$i] . "'");
                while ($categorie = tep_db_fetch_array($categorie_query)) {
                    $cats[] = $categorie['categories_id'];
                }
                // sort out doubles
                $cats = array_unique($cats);
            }
            $catIdSql = implode(', ', $cats);
        } else {
            $catIdSql = $featured_products_category_id_list;
        }

        // Phocea Optimize featured query
        $query = 'SELECT distinct p.products_id, p.products_image, p.products_tax_class_id, IF (s.status, s.specials_new_products_price, NULL) AS specials_new_products_price, p.products_price, pd.products_name
    FROM ' . TABLE_PRODUCTS . ' p LEFT JOIN ' . TABLE_PRODUCTS_TO_CATEGORIES . ' p2c using(products_id)
    LEFT JOIN ' . TABLE_CATEGORIES . ' c USING (categories_id)
    LEFT JOIN ' . TABLE_FEATURED . ' f ON p.products_id = f.products_id
    LEFT JOIN ' . TABLE_SPECIALS . ' s ON p.products_id = s.products_id
  
    LEFT JOIN ' . TABLE_PRODUCTS_DESCRIPTION . " pd ON p.products_id = pd.products_id AND pd.language_id = '" . $languages_id . "' 
    where c.categories_id IN(" . $catIdSql . ") AND p.products_status = '1' AND f.status = '1' ";

        if (defined('FEATURED_PRODUCTS_SPECIALS_ONLY') AND FEATURED_PRODUCTS_SPECIALS_ONLY == 'true') {
            $query .= " AND s.status = '1' ";
        }
        $query .= 'ORDER BY  p.products_date_added  DESC ,p.products_date_released  DESC  LIMIT ' . MAX_DISPLAY_FEATURED_PRODUCTS;

        $featured_products_query = tep_db_query($query);
    }

    $num_featured_products = tep_db_num_rows($featured_products_query);

    if ($num_featured_products > 0) {
        $counter = 0;
        $col = 0;

        $featured_prods_content = '<table border="0" width="100%" cellspacing="0" cellpadding="2">';
        while ($featured_products = tep_db_fetch_array($featured_products_query)) {

            $authors = tep_db_fetch_all(tep_db_query('SELECT `manufacturers_name` FROM ' . TABLE_MANUFACTURERS . " M inner join " . TABLE_PRODUCTS_TO_AUTHORS . " A on  A.manufacturers_id= M.manufacturers_id WHERE products_id = '" . $featured_products['products_id'] . "'"));

            $counter++;

            if ($col === 0) {
                $featured_prods_content .= '<tr>';
            }

            $featured_prods_content .= '<td width="20%" align="center" valign="top">';
            if ($new_price = tep_get_products_special_price($featured_products['products_id'])) {
                $products_price = '<del>' . $currencies->display_price($featured_products['products_price'], tep_get_tax_rate($featured_products['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($featured_products['products_tax_class_id'])) . '</span>';
            } else {
                $products_price = $currencies->display_price($featured_products['products_price'], tep_get_tax_rate($featured_products['products_tax_class_id']));
            }

            $featured_prods_content .= '<span class="featuredPrice">' . $products_price . '</span>'
                . '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $featured_products['products_id']) . '" class="tooltip">' . tep_image(DIR_WS_IMAGES . $featured_products['products_image'], $featured_products['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT)
                . '<span> <img class="callout" src="images/callout.gif" /> <strong>' . $featured_products['products_name'] . '</strong><strong style="float:left;">' . $currencies->display_price($featured_products['products_price'], tep_get_tax_rate($featured_products['products_tax_class_id'])) . '</strong>';
            if (!empty($authors))
                $featured_prods_content .= '<br /><br />';
            foreach ($authors as $key => $value) {
                $featured_prods_content .= '' . $value['manufacturers_name'] . '';
                if (isset($authors[$key + 1]))
                    $featured_prods_content .= ',';
            }
            $featured_prods_content .= '  <br />';
            if (!empty($featured_products['products_description']))
                $featured_prods_content .= '<br />' . character_limiter($featured_products['products_description'], 500) . '  ';
            $featured_prods_content .=' </span> </a><br /><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $featured_products['products_id']) . '" class="featuredLink">' . $featured_products['products_name'] . '</a></td>';

            $col ++;

            if (($col > 4) || ($counter == $num_featured_products)) {
                $featured_prods_content .= '</tr><tr><td colspan="5" height="10"></td></tr>';

                $col = 0;
            }
        }

        $featured_prods_content .= '</table>';
        ?>

        <div class="boxTitle"><?php echo $title; ?></div>
        <div class="contentText">
            <?php echo $featured_prods_content; ?>
            <div class="buttonSet left">
                <?php echo tep_draw_button(IMAGE_BUTTON_MORE . '', '', tep_href_link(FILENAME_FEATURED_PRODUCTS)); ?>
            </div>
        </div>

        <div class="clear"></div>
        <?php
    }
}
?>