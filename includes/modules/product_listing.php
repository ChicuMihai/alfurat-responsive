<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

if (!empty($listing_sql))
	$listing_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');



if ($listing_split->number_of_rows > 0) {
    display_books_listening($listing_split,$languages_id,$HTTP_GET_VARS,$current_category_id,$cPath,$is_poster);
} else {
    ?>
    <p><?php echo($filtered > 0) ? TEXT_NO_PRODUCTS : TEXT_CHOOSE_FILTERS; ?></p>
    <?php
}


?>