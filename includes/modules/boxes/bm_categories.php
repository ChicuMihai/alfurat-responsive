<?php

/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
 */

class bm_categories {

    var $code = 'bm_categories';
    var $group = 'boxes';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function bm_categories() {
        $this->title = MODULE_BOXES_CATEGORIES_TITLE;
        $this->description = MODULE_BOXES_CATEGORIES_DESCRIPTION;

        if (defined('MODULE_BOXES_CATEGORIES_STATUS')) {
            $this->sort_order = MODULE_BOXES_CATEGORIES_SORT_ORDER;
            $this->enabled = (MODULE_BOXES_CATEGORIES_STATUS == 'True');

            $this->group = ((MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT == 'Left Column') ? 'boxes_column_left' : 'boxes_column_right');
        }
    }

    function tep_show_category($counter) {
        global $tree, $categories_string, $cPath_array;

        for ($i = 0; $i < $tree[$counter]['level']; $i++) {
            $categories_string .= "&nbsp;&nbsp;";
        }

        $categories_string .= '<div class="accordeon-title"><div class="accordeon-entry"><a href="';

        if ($tree[$counter]['parent'] == 0) {
            $cPath_new = 'cPath=' . $counter;
        } else {
            $cPath_new = 'cPath=' . $tree[$counter]['path'];
        }

        $categories_string .= tep_href_link(FILENAME_DEFAULT, $cPath_new) . '">';

        if (isset($cPath_array) && in_array($counter, $cPath_array)) {
            $categories_string .= '<strong>';
        }

// display category name
        $categories_string .= $tree[$counter]['name'];

        if (isset($cPath_array) && in_array($counter, $cPath_array)) {
            $categories_string .= '</strong>';
        }

        if (tep_has_category_subcategories($counter)) {
            $categories_string .= '-&gt;';
        }

        $categories_string .= '</a>';

        if (SHOW_COUNTS == 'true') {
            $products_in_category = tep_count_products_in_category($counter);
            if ($products_in_category > 0) {
                $categories_string .= '&nbsp;(' . $products_in_category . ')';
            }
        }

        $categories_string .= '<br />';

        if ($tree[$counter]['next_id'] != false) {
            $this->tep_show_category($tree[$counter]['next_id']);
        }
    }

    function getData() {
        global $categories_string, $posters_string, $tree, $languages_id, $cPath, $cPath_array;
        $tree = array();

        $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c,"
            . " " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . POSTERS_CATEGORY_ID . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "'"
            . "  order by sort_order, cd.categories_name");
        $all = tep_db_fetch_all($categories_query);
        /*$posters_string .='<li><a href="#">&nbsp; ' . TEXT_ALL_COUNTRIES . ' &nbsp;</a>';
        $posters_string .='<ul>';
        foreach ($all as $categories) {
            $posters_string .= '<li><a href="index.php?cPath=' . $categories['categories_id'] . '"><img src="ext/jquery/ui/redmond/images/'.$menu_icon.'.png" class="menuBullet"/>' . $categories['categories_name'] . '&nbsp;</a>';
            $posters_string .='</li>';
        }
        $posters_string .='</ul>';
        $posters_string .='</li>';
*/
        $categories_string = '';
        $tree = array();

        $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '0' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "' and c.categories_id !='" . POSTERS_CATEGORY_ID . "' and c.categories_id !='" . MAGAZINES_CATEGORY_ID . "'   order by sort_order, cd.categories_name");
        $all = tep_db_fetch_all($categories_query);
        foreach ($all as $categories) {
//new code
            //	$products_in_category = tep_count_products_in_category($categories['categories_id']);
            //$categories_string .= '<li><a href="#"><img src="ext/jquery/ui/redmond/images/menu-icon.png" class="menuBullet"/>' . $categories['categories_name'] . '&nbsp;(' . $products_in_category . ')</a>';
            $categories_string .= '<div class="accordeon-title">' . $categories['categories_name'] . '</a></div><div class="accordeon-entry">';
            $parent_id = $categories['categories_id'];
            $subcategories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . (int) $parent_id . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "' order by sort_order, cd.categories_name");
            if (tep_db_num_rows($subcategories_query)) {
                $categories_string .='<ul>';
                //2nd level menu
                $sub = tep_db_fetch_all($subcategories_query);
                foreach ($sub as $sub_categories) {
                    //$products_in_sub_category = tep_count_products_in_category($sub_categories['categories_id']);
                    $hasSub = tep_has_category_subcategories($sub_categories['categories_id']);
                    if (!($hasSub)) {
                        $categories_string .= '<li><a href="index.php?cPath=' . $sub_categories['categories_id'] . '">&nbsp' . $sub_categories['categories_name'] . '&nbsp;</a>';
                    } else {
                        $categories_string .='<div class="accordeon-title"><li><a href="#">&nbsp;' . $sub_categories['categories_name'] . '&nbsp;</a></div>';
                    }


                    //3rd level
                    $sub_parent_id = $sub_categories['categories_id'];
                    $third_categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . (int) $sub_parent_id . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int) $languages_id . "' order by sort_order, cd.categories_name");
                    if (tep_db_num_rows($third_categories_query)) {
                        $categories_string .=' <div class="accordeon-entry"><ul>';
                        $third = tep_db_fetch_all($third_categories_query);
                        foreach ($third as $third_categories) {
                            //	$products_in_third_category = tep_count_products_in_category($third_categories['categories_id']);
                            //if ($products_in_third_category > 0) {
                            $categories_string .= '<li><a href="index.php?cPath=' . $third_categories['categories_id'] . '">&nbsp;&nbsp;' . $third_categories['categories_name'] . '&nbsp;</a>';
//							} else {
//								$categories_string .='<li><a href="#">&nbsp;&nbsp;' . $third_categories['categories_name'] . '</a>';
//							}
                        }
                        $categories_string .='</ul></div>';
                    }
                    $categories_string .='</li>';
                }
                $categories_string .='</ul></div>';
            }
            $categories_string .='</li>';
        }






        //  $this->tep_show_category($first_element);


        $data .=
            '<div class="information-blocks margin-bottom-15 categories-border-wrapper">'.
            '   <div class="block-title size-3 border-bottom">' . MODULE_BOXES_CATEGORIES_BOX_TITLE . '</div>' .
            '<div class="accordeon">'.
            '   
                <div class="article-container style-1">
    			<ul>' . $categories_string . '</ul></div>' .
            '</div></div>';



        return $data;
    }

    function execute() {
        global $SID, $oscTemplate;

        if ((USE_CACHE == 'true') && empty($SID)) {
            $output = tep_cache_categories_box();
        } else {
            $output = $this->getData();
        }

        $oscTemplate->addBlock($output, $this->group);
    }

    function isEnabled() {
        return $this->enabled;
    }

    function check() {
        return defined('MODULE_BOXES_CATEGORIES_STATUS');
    }

    function install() {
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Categories Module', 'MODULE_BOXES_CATEGORIES_STATUS', 'True', 'Do you want to add the module to your shop?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Content Placement', 'MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT', 'Left Column', 'Should the module be loaded in the left or right column?', '6', '1', 'tep_cfg_select_option(array(\'Left Column\', \'Right Column\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_BOXES_CATEGORIES_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    function remove() {
        tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
        return array('MODULE_BOXES_CATEGORIES_STATUS', 'MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT', 'MODULE_BOXES_CATEGORIES_SORT_ORDER');
    }

}

?>