<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  class bm_myaccount {
    var $code = 'bm_myaccount';
    var $group = 'boxes';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function bm_myaccount() {
      $this->title = MODULE_BOXES_ACCOUNT_HEADING;
      $this->description = MODULE_BOXES_ACCOUNT_DESCRIPTION;

      if ( defined('MODULE_BOXES_MYACCOUNT_STATUS') ) {
        $this->sort_order = MODULE_BOXES_MYACCOUNT_SORT_ORDER;
        $this->enabled = (MODULE_BOXES_MYACCOUNT_STATUS == 'True');

        $this->group = ((MODULE_BOXES_MYACCOUNT_CONTENT_PLACEMENT == 'Left Column') ? 'boxes_column_left' : 'boxes_column_right');
      }
    }

    function execute() {
      global $oscTemplate;

      if (isset($_SESSION['customer_id'])) {
        $data = '<div class="ui-widget infoBoxContainer">' .
                '  <div class="ui-widget-header infoBoxHeading"><a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_HEADING . '</a></div>' .
                '  <div class="ui-widget-content infoBoxContents">' . 
                '<span class="ui-icon ui-icon-home accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_ADDRESS_BOOK . '</a><br />' .
                '<span class="ui-icon ui-icon-key accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_PASSWORD . '</a><br />' .
                '<span class="ui-icon ui-icon-contact accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_EMAIL_PHONE . '</a><br />' .
                '<span class="ui-icon ui-icon-search accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_ORDERS_VIEW . '</a><br />' .
                '<span class="ui-icon ui-icon-cart accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_SHOPPING_CART, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_CART_VIEW . '</a><br />' .
                '<span class="ui-icon ui-icon-mail-closed accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_ACCOUNT_NEWSLETTERS, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_NEWSLETTERS . '</a><br />' .
                '<span class="ui-icon ui-icon-heart accountLinkListEntry"></span> <a href="' . tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS_CAT, '', 'SSL') . '">' . MODULE_BOXES_ACCOUNT_PRODUCTS . '</a>' .
                '  </div>' .
                '</div>';

        $oscTemplate->addBlock($data, $this->group);
      }
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_BOXES_MYACCOUNT_STATUS');
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable My Account Module', 'MODULE_BOXES_MYACCOUNT_STATUS', 'True', 'Do you want to add the My Account box module to your shop?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Content Placement', 'MODULE_BOXES_MYACCOUNT_CONTENT_PLACEMENT', 'Right Column', 'Should the module be loaded in the left or right column?', '6', '1', 'tep_cfg_select_option(array(\'Left Column\', \'Right Column\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_BOXES_MYACCOUNT_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_BOXES_MYACCOUNT_STATUS', 'MODULE_BOXES_MYACCOUNT_CONTENT_PLACEMENT', 'MODULE_BOXES_MYACCOUNT_SORT_ORDER');
    }
  }
?>
