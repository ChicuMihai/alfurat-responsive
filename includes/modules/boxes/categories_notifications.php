<?php
/*
  $Id: product_notifications.php,v 1.8 2003/06/09 22:19:07 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  if ($smack_current_id != '0' && tep_not_null($smack_current_id)) {
?>
<!-- notifications //-->
<div class="alert-notifyme">
<?php

    $info_box_contents = array();
    $info_box_contents[] = array('text' => BOX_HEADING_NOTIFICATIONS);

 //   new infoBoxHeading($info_box_contents, false, false, tep_href_link(FILENAME_CAT_NOTIFICATIONS, '', 'SSL'));

    if (tep_session_is_registered('customer_id')) {
      $check_query = tep_db_query("select count(*) as count from " . TABLE_CAT_NOTIFICATIONS . " where categories_id = '" . $smack_current_id . "' and customers_id = '" . (int)$customer_id . "'");
      $check = tep_db_fetch_array($check_query);

      $notification_exists = (($check['count'] > 0) ? true : false);
    } else {
      $notification_exists = false;
    }

    $info_box_contents = array();
    if ($notification_exists == true) {
      echo '<a class="notifyme" href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=cat_notify_remove', $request_type) . '">' . sprintf(BOX_CAT_NOTIFICATIONS_NOTIFY_REMOVE, tep_get_categories_name($smack_current_id)) .'</a>';
    } else {
    //  echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=cat_notify', $request_type) . '">' . tep_image(DIR_WS_IMAGES . 'box_products_notifications.gif', IMAGE_BUTTON_NOTIFICATIONS) . '</a>';
			  echo '<a  class="notifyme" href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=cat_notify', $request_type) . '">' . sprintf(BOX_CAT_NOTIFICATIONS_NOTIFY, tep_get_categories_name($smack_current_id)) .'</a>';
    }

   // new infoBox($info_box_contents);
?>
         
</div>
<!-- notifications_eof //-->
<?php
  }
?>
