<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
 */

$oscTemplate->buildBlocks();

if (!$oscTemplate->hasBlocks('boxes_column_left')) {
	$oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());
}

if (!$oscTemplate->hasBlocks('boxes_column_right')) {
	$oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());
}
?>
<!DOCTYPE html>
<html <?php echo HTML_PARAMS; ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>" />
		<title><?php echo tep_output_string_protected($oscTemplate->getTitle());
		echo(isset($product_info['products_isbn']) && !empty($product_info['products_isbn']))?' (ISBN:'.str_replace('-','',$product_info['products_isbn']).')':''; ?></title>
		<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>" />
<!--		<link rel="stylesheet" type="text/css" href="ext/jquery/ui/redmond/jquery-ui-1.10.4.minweb.css" />
		<script type="text/javascript" src="ext/jquery/jquery-2.1.3.min.js"></script>
		<link rel="stylesheet" type="text/css" href="ext/jquery/ui/jquery.navgoco.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/nivo-slider.css" />
		<link rel="stylesheet" type="text/css" href="ext/960gs/<?php /*echo ((stripos(HTML_PARAMS, 'dir="rtl"') !== false) ? 'rtl_' : ''); */?>960_24_col.css" />
		<link rel="stylesheet" type="text/css" href="<?php /*echo ((stripos(HTML_PARAMS, 'dir="ltr"') !== false) ? 'ltr_' : ''); */?>stylesheet.css" />
-->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/idangerous.swiper.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700%7CDancing+Script%7CMontserrat:400,700%7CMerriweather:400,300italic%7CLato:400,700,900' rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Cantata+One' rel='stylesheet' type='text/css' />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link href="css/pikaday.css" rel="stylesheet" type="text/css" />
        
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
			.cursor {
				cursor: pointer;
			}
		</style>
		<?php
		switch (basename($PHP_SELF)) {
			case 'product_info.php':
				?>
				<meta property="og:url"                content="<?php echo tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $product_info['products_id']); ?>" />
				<meta property="og:type"               content="website" />
				<meta property="og:title"              content="<?php echo $products_name; ?>" />
				<meta property="og:description"        content="<?php echo str_replace('"', '', $product_info['products_description']); ?>" />
				<meta property="og:image"              content="<?php echo 'https://alfurat.com/images/' . $product_info['products_image']; ?>" />
				<meta property="og:image:width"  content="180" />
				<meta property="og:image:height"   content="270" />
				<meta property="fb:app_id"   content="1161937953844498" />
				<?php
				break;
			case 'article_info.php':
				?>
				<meta property="og:url"                content="<?php echo tep_href_link(FILENAME_ARTICLE_INFO, 'articles_id=' . $article_info['articles_id']); ?>" />
				<meta property="og:type"               content="website" />
				<meta property="og:title"              content="<?php echo $article_info['articles_name']; ?>" />
				<meta property="og:description"        content="<?php echo str_replace('"', '', $article_info['articles_description']); ?>" />
				<meta property="og:image"              content="<?php echo 'https://alfurat.com/images/' . $article_info['articles_image']; ?>" />
				<meta property="og:image:width"  content="180" />
				<meta property="og:image:height"   content="270" />
				<meta property="fb:app_id"   content="1161937953844498" />
				<?php
				break;
		}
		?>
		
		<?php
		echo $oscTemplate->getBlocks('header_tags');
		$is_article=0;
		switch (basename($PHP_SELF)) {
			case 'advanced_search_result.php':
				echo'<link rel="stylesheet" type="text/css" href="ext/select2/select2.css" />';
				echo'<link rel="stylesheet" type="text/css" href="ext/select2/select2-bootstrap.css" />';
				break;
			case 'product_info.php':
				echo '<script type="text/javascript" src="ext/photoset-grid/jquery.photoset-grid.min.js"></script>';
				echo '<link rel="stylesheet" type="text/css" href="ext/colorbox/colorbox.css" />';
				echo'<script type="text/javascript" src="ext/colorbox/jquery.colorbox-min.js"></script>';
				echo'<script type="text/javascript" src="ext/product-info.js"></script>';
				break;
			case 'article_info.php':
				$is_article=1;
				echo '<script type="text/javascript" src="ext/photoset-grid/jquery.photoset-grid.min.js"></script>';
				echo '<link rel="stylesheet" type="text/css" href="ext/colorbox/colorbox.css" />';
				echo'<script type="text/javascript" src="ext/colorbox/jquery.colorbox-min.js"></script>';
			//	echo'<script type="text/javascript" src="ext/product-info.js"></script>';
				break;
			default:
				break;
		}
		?>

	</head>
	<body class="style-14 loaded">

    <!-- LOADER -->
<!--   <div id="loader-wrapper">
        <div class="bubbles">
            <div class="title">loading</div>
            <span></span>
            <span id="bubble2"></span>
            <span id="bubble3"></span>
        </div>
    </div>-->
    <div id="content-block">
        <div class="content-center fixed-header-margin">




<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
            <div class="content-push">



		<!--<div id="bodyContent" class="grid_<?php /*echo $oscTemplate->getGridContentWidth(); */?> <?php /*echo ($oscTemplate->hasBlocks('boxes_column_left') ? 'push_' . $oscTemplate->getGridColumnWidth() : ''); */?>">-->
			
				<?php/*
				if(!$is_article){
					echo '<div class="searchBox"> ';
				$category = 0;
				$is_magazine = 0;
				$is_poster = 0;
				if (basename($PHP_SELF) == 'product_info.php') {
					$category = tep_get_product_path($HTTP_GET_VARS['products_id']);
				} else if (isset($HTTP_GET_VARS['cPath'])) {
					$category = $HTTP_GET_VARS['cPath'];
				}
				$label = SEARCH_FOR_BOOK;
				$search_in_posters = 0;
				if ($category == MAGAZINES_CATEGORY_ID) {
					$is_magazine = 1;
				} else
				if ((substr_count($category, POSTERS_CATEGORY_ID) > 0 || $category > POSTERS_CATEGORY_ID) || ($HTTP_GET_VARS['search_in_posters'] == 1)) {
					$label = SEARCH_FOR_POSTER;
					$search_in_posters = 1;
					$is_poster = 1;
				}
				echo tep_draw_form('quick_find', tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT, '', 'NONSSL', false), 'get') .
				'  <div class="left">' . tep_draw_hidden_field('search_in_posters', $search_in_posters) . tep_draw_hidden_field('search_in_description', '1') . tep_hide_session_id() . tep_draw_button(ADVANCED_SEARCH, MODULE_BOXES_SEARCH_BOX_TITLE, NULL, NULL, NULL, 'advanced-search cursor') . '</div><div class="left">  ' . tep_draw_input_field('keywords', '', 'size="10" maxlength="30" class="searchfield" placeholder="' . $label . '"') . ' </div>';
			echo'</form>	</div>';
				}*/
				?>
