<?php
/*
  $Id	Arabic Text for osc2.3.1 , 17-Feb-2012. $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'ar_AR'
// on FreeBSD try 'ar_AR.ISO_8859-1'
// on Windows try 'ar', or 'Arabic'
@setlocale(LC_TIME, 'ar_AR');
define('BOX_CAT_NOTIFICATIONS_NOTIFY', 'أبلغني بكتب جديدة في <b>%s</b>');
define('BOX_CAT_NOTIFICATIONS_NOTIFY_REMOVE', 'لا تبلغني بكتب جديدة في <b>%s</b>');
define('DATE_FORMAT_SHORT', '%m/%d/%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
define('DATE_FORMAT', 'm/d/Y'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('JQUERY_DATEPICKER_I18N_CODE', ''); // leave empty for en_US; see http://jqueryui.com/demos/datepicker/#localization
define('JQUERY_DATEPICKER_FORMAT', 'mm/dd/yy'); // see http://docs.jquery.com/UI/Datepicker/formatDate
define('MODULE_BOXES_SEARCH_BOX_TITLE', 'بحث متقدم');
////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
function tep_date_raw($date, $reverse = false) {
  if ($reverse) {
    return substr($date, 3, 2) . substr($date, 0, 2) . substr($date, 6, 4);
  } else {
    return substr($date, 6, 4) . substr($date, 0, 2) . substr($date, 3, 2);
  }
}
define('TEXT_CHOOSE_FILTERS', '');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'USD');
  define('TEXT_ALL_COUNTRIES', ' الدول');
// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="rtl" lang="ar"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', STORE_NAME);

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'إشتراك جديد');
define('HEADER_TITLE_MY_ACCOUNT', 'بياناتي');
define('HEADER_TITLE_CART_CONTENTS', 'محتويات السله');
define('HEADER_TITLE_CHECKOUT', 'المحاسبه');
define('HEADER_TITLE_TOP', 'الرئيسية');
define('HEADER_TITLE_CATALOG', 'السوق');
define('HEADER_TITLE_LOGOFF', 'خروج');
define('HEADER_TITLE_LOGIN', 'دخول');

// footer text in includes/footer.php
define('FOOTER_TEXT_REQUESTS_SINCE', 'عدد الزائرين منذ');

// text for gender
define('MALE', 'ذكر');
define('FEMALE', 'أنثي');
define('MALE_ADDRESS', 'السيد');
define('FEMALE_ADDRESS', 'الآنسه/السيدة');

// text for date of birth example
define('DOB_FORMAT_STRING', 'mm/dd/yyyy');

// categories box text in includes/boxes/categories.php
define('BOX_HEADING_CATEGORIES', 'الأقسام');

// manufacturers box text in includes/boxes/manufacturers.php
define('BOX_HEADING_MANUFACTURERS', ' الناشر');

// whats_new box text in includes/boxes/whats_new.php
define('BOX_HEADING_WHATS_NEW', 'الجديد عندنا؟');

// quick_find box text in includes/boxes/quick_find.php
define('BOX_HEADING_SEARCH', 'بحث سريع');
define('BOX_SEARCH_TEXT', 'إستخدم كلمات البحث الرئيسيه للبحث عن الأصناف التي تريدها');
define('BOX_SEARCH_ADVANCED_SEARCH', 'بحث متقدم');

// specials box text in includes/boxes/specials.php
define('BOX_HEADING_SPECIALS', 'عروض خاصه');

// reviews box text in includes/boxes/reviews.php
define('BOX_HEADING_REVIEWS', 'مراجعات');
define('BOX_REVIEWS_WRITE_REVIEW', 'أكتب مراجعه عن هذا الصنف!');
define('BOX_REVIEWS_NO_REVIEWS', 'حالياً لايوجد مراجعه');
define('BOX_REVIEWS_TEXT_OF_5_STARS', '%s من خمس نجوم!');

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_HEADING_SHOPPING_CART', 'سلة التسوق');
define('BOX_SHOPPING_CART_EMPTY', '..فارغه!');

// order_history box text in includes/boxes/order_history.php
define('BOX_HEADING_CUSTOMER_ORDERS', 'نظرة تأريخ الطلبات');

// best_sellers box text in includes/boxes/best_sellers.php
define('BOX_HEADING_BESTSELLERS', 'المبيعات المُفَضلة');
define('BOX_HEADING_BESTSELLERS_IN', 'المبيعات المُفَضلة في<br>&nbsp;&nbsp;');

// notifications box text in includes/boxes/products_notifications.php
define('BOX_HEADING_NOTIFICATIONS', 'التبليغات');
define('BOX_NOTIFICATIONS_NOTIFY', 'برجاء تبليغي عن كل جديد بخصوص هذا المنتج <b>%s</b>');
define('BOX_NOTIFICATIONS_NOTIFY_REMOVE', 'أرجو عدم تبليغي عن كل ما هو جديد <b>%s</b>');

// manufacturer box text
define('BOX_HEADING_MANUFACTURER_INFO', 'معلومات الوكيل المورد');
define('BOX_MANUFACTURER_INFO_HOMEPAGE', '%s الصفحه الرئيسيه');
define('BOX_MANUFACTURER_INFO_OTHER_PRODUCTS', 'أصناف أخرى');

// languages box text in includes/boxes/languages.php
define('BOX_HEADING_LANGUAGES', 'اللغات');

// currencies box text in includes/boxes/currencies.php
define('BOX_HEADING_CURRENCIES', 'العُملات');

// information box text in includes/boxes/information.php
define('BOX_HEADING_INFORMATION', 'معلومات');
define('BOX_INFORMATION_PRIVACY', 'الخصوصيه');
define('BOX_INFORMATION_CONDITIONS', 'شروط الإستخدام');
define('BOX_INFORMATION_SHIPPING', 'ماذا عن الشحن والإرجاع');
define('BOX_INFORMATION_CONTACT', 'إتصل بنا');

// tell a friend box text in includes/boxes/tell_a_friend.php
define('BOX_HEADING_TELL_A_FRIEND', 'أخبر صديق عن موقعنا');
define('BOX_TELL_A_FRIEND_TEXT', 'أعلم أي شخص عن هذا المنتج');

// checkout procedure text
define('CHECKOUT_BAR_DELIVERY', 'عنوان الإستلام');
define('CHECKOUT_BAR_PAYMENT', 'طريقة الدفع');
define('CHECKOUT_BAR_CONFIRMATION', 'تأكيد');
define('CHECKOUT_BAR_FINISHED', 'إنتهى!');

// pull down default text
define('PULL_DOWN_DEFAULT', 'الرجاء الإختيار');
define('TYPE_BELOW', 'أكتب بالأسفل');

// javascript messages
define('JS_ERROR', 'يوجد أخطاء'
		. ' في النموذج الذي أرسلتموه.'
		. '\nالرجاء التأكد من صحة البيانات التاليه:\n\n<br>');

define('JS_REVIEW_TEXT', 'يجب أن يتضمن تعليقك  ' . REVIEW_TEXT_MIN_LENGTH . ' حرفا" على الأقل  ');
define('JS_REVIEW_RATING', ' تقييم الكتاب');

define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* الرجاء إختيار طريقة الدفع لطلبيتك.\n');

define('JS_ERROR_SUBMITTED', 'من فضلك أضغط/أنقر علي موافق وأنتظر قليلا ..شكرا');

define('JS_ERROR_NO_PAYMENT_MODULE_SELECTED', '* الرجاء إختيار طريقة الدفع لطلبيتك.');

define('CATEGORY_COMPANY', 'تفاصيل الشركة في حال توفرها');
define('CATEGORY_PERSONAL', 'تفاصيل شخصية');
define('CATEGORY_ADDRESS', 'عنوانك');
define('CATEGORY_CONTACT', 'معلومات الإتصال بك');
define('CATEGORY_OPTIONS', 'خيارات');
define('CATEGORY_PASSWORD', 'كلمة السر الخاصة بك');

define('ENTRY_COMPANY', 'إسم الشركة:');
define('ENTRY_COMPANY_ERROR', '&nbsp;<font color="#ff0000">إن أمكن</font>');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER', 'الجنس (Gender):');
define('ENTRY_GENDER_ERROR', 'الرجاء  تحديد الجنس');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME', 'الإسم: ');
define('ENTRY_FIRST_NAME_ERROR', 'يجب أن يحتوي الإسم الأول على ' . ENTRY_FIRST_NAME_MIN_LENGTH .' '.' أحرف كحد أدنى ');
define('ENTRY_FIRST_NAME_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_LAST_NAME', ' الشهرة:');
define('ENTRY_TELEPHONE', ' التلفون:');
define('NOTE', 'ملاحظة');
define('ENTRY_LAST_NAME_ERROR', 'يجب أن يحتوي الإسم الآخر على ' . ENTRY_LAST_NAME_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_LAST_NAME_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_DATE_OF_BIRTH', 'تاريخ الميلاد:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'يجب أن يكون  تاريخ الميلادك  على هذا الشكل: MM / DD / YYYY ');
define('ENTRY_DATE_OF_BIRTH_TEXT', ' (eg. 05/21/1970)');
define('ENTRY_EMAIL_ADDRESS', 'البريد الإلكتروني:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'يجب أن يحتوي البريد الإلكتروني على ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'البريد الإلكتروني غير صحيح!');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'البريد الإلكتروني موجود مُسبقاً!');
define('ENTRY_EMAIL_ADDRESS_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_STREET_ADDRESS', 'الشارع:');
define('ENTRY_STREET_ADDRESS_ERROR', 'يجب أن يحتوي الشارع على ' . ENTRY_STREET_ADDRESS_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_SUBURB_ERROR', 'يجب أن يحتوي البناية على ' . ENTRY_SUBURB_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_STREET_ADDRESS_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_SUBURB', 'البناية:');
//define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_POST_CODE', 'الرمز البريدي');
define('ENTRY_POST_CODE_ERROR', 'يجب أن يحتوي الرمز البريدي على ' . ENTRY_POSTCODE_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_POST_CODE_TEXT', '');
define('ENTRY_CITY', 'المدينة:');
define('ENTRY_CITY_ERROR', 'يجب أن تحتوي المدينة على ' . ENTRY_CITY_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_CITY_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_STATE', 'الولاية أو المحافظة:');
define('ENTRY_STATE_ERROR', 'يجب تحديد الولاية أو المحافظة');
define('ENTRY_STATE_ERROR_SELECT', 'الرجاء تحديد دولة من قائمة الدول');
define('ENTRY_STATE_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_COUNTRY', 'البلد (Country):');
define('ENTRY_COUNTRY_ERROR', 'يجب إختيار البلد');
define('ENTRY_COUNTRY_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_TELEPHONE_NUMBER', 'التلفون:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'يجب أن يحتوي التلفون على ' . ENTRY_TELEPHONE_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_FAX_NUMBER', 'الفاكس:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'قائمة الرسائل الإخبارية   (Newsletter):');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'إنضمام');
define('ENTRY_NEWSLETTER_NO', 'إنفصال');
define('ENTRY_NEWSLETTER_ERROR', '');
define('ENTRY_PASSWORD', 'كلمة السر:');
define('ENTRY_PASSWORD_ERROR', 'يجب أن تحتوي كلمة السر على ' . ENTRY_PASSWORD_MIN_LENGTH .' '. 'أحرف كحد أدنى ');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'تأكيد كلمة السر يجب أن تتطابق كلمة السر .');
define('ENTRY_PASSWORD_TEXT', '&nbsp;<font color="#ff0000">مطلوب</font>');
define('ENTRY_PASSWORD_CONFIRMATION', 'تأكيد كلمة السر:');
define('ENTRY_PASSWORD_CONFIRMATION_TEXT', '&nbsp;<font color="#ffffff">علي الأقل خمسة حروف مطلوب</font>');
define('ENTRY_PASSWORD_CURRENT', '  كلمة السر الحالية');
define('ENTRY_PASSWORD_CURRENT_TEXT', '*');
define('ENTRY_PASSWORD_STRENGTH', 'يجب أن تتضمن كلمة السر أحرف و أرقام ورموز');
define('ENTRY_PASSWORD_CURRENT_ERROR', ' ﻲﺠﺑ ﺄﻧ ﻚﻠﻣﺓ ﺎﻠﺳﺭ ﺖﺤﺗﻮﻳ ﻊﻟﻯ ﻡﺍ ﻻ ﻲﻘﻟ ﻊﻧ' . ENTRY_PASSWORD_MIN_LENGTH . ' ﺄﺣﺮﻓ');
define('ENTRY_PASSWORD_NEW', '  كلمة سر جديدة');
define('ENTRY_PASSWORD_NEW_TEXT', '*');
define('ENTRY_PASSWORD_NEW_ERROR', 'يجب أن تحتوي  كلمة السر الجديدة على ما لا يقل عن ' . ENTRY_PASSWORD_MIN_LENGTH .' '. ' أحرف ');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'تأكيد كلمة السر يجب أن تتطابق كلمة السر الجديدة.');
define('ENTRY_PASSWORD_NEW_STRENGTH', 'كلمة السر الجديدة يجب أن تكون قوية');
define('PASSWORD_HIDDEN', '--مخفي--');
define('SEARCH_BY_AUTHOR', 'البحث بالمؤلف');
define('SEARCH_BY_TRANSLATOR', 'البحث بالمترجم');
define('FORM_REQUIRED_INFORMATION', '* مطلوب تدوينه');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', ' الصفحات:');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> المقالات');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> الكتب');
define('TEXT_DISPLAY_NUMBER_OF_POSTERS', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> الملصقات');
define('TEXT_DISPLAY_NUMBER_OF_MAGAZINES', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> المجلات');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> طلبيه');
define('TEXT_DISPLAY_NUMBER_OF_REVIEWS', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> مراجعه)');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> صنف جديد');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'عرض <b>%d</b> إلى <b>%d</b> من <b>%d</b> العروض الخاصة');

define('PREVNEXT_TITLE_FIRST_PAGE', 'الصفحه الأولى');
define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'الصفحه السابقه');
define('PREVNEXT_TITLE_NEXT_PAGE', 'الصفحه التاليه');
define('PREVNEXT_TITLE_LAST_PAGE', 'الصفحه الأخيره');
define('PREVNEXT_TITLE_PAGE_NO', 'صفحه %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'المجموعه السابقه من %d صفحات');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'المجموعه التاليه من %d صفحات');
define('PREVNEXT_BUTTON_FIRST', '&lt;&lt;أول');
define('PREVNEXT_BUTTON_PREV', '&lt;&lt;&nbsp;');
define('PREVNEXT_BUTTON_NEXT', '&nbsp;&gt;&gt;</a>&nbsp;');
define('PREVNEXT_BUTTON_LAST', '&gt;&gt;أخير');

define('IMAGE_BUTTON_ADD_ADDRESS', 'إضافة عنوان');
define('IMAGE_BUTTON_ADDRESS_BOOK', 'دفتر العناوين');
define('IMAGE_BUTTON_BACK', 'رجوع');
define('IMAGE_BUTTON_BUY_NOW', 'إشتري الآن');
define('IMAGE_BUTTON_CHANGE_ADDRESS', 'تغيير عنوان');
define('IMAGE_BUTTON_CHECKOUT', 'محاسبه');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'تأكيد الطلبيه');
define('IMAGE_BUTTON_CONTINUE', 'متابعة');
define('IMAGE_BUTTON_SEND', 'إرسال');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'استمرار الشراء');
define('IMAGE_BUTTON_DELETE', 'حذف');
define('IMAGE_BUTTON_EDIT_ACCOUNT', 'تحرير الإشتراك');
define('IMAGE_BUTTON_HISTORY', 'محفوظات الطلبيه');
define('IMAGE_BUTTON_LOGIN', 'الدخول');
define('IMAGE_BUTTON_IN_CART', 'إشتري الآن');
define('IMAGE_BUTTON_NOTIFICATIONS', 'التبليغات');
define('IMAGE_BUTTON_QUICK_FIND', 'بحث سريع');
define('IMAGE_BUTTON_REMOVE_NOTIFICATIONS', 'الغاء التبليغات');
define('IMAGE_BUTTON_REVIEWS', 'مراجعه');
define('IMAGE_BUTTON_MORE', 'المزيد');
define('IMAGE_BUTTON_SEARCH', 'البحث');
define('IMAGE_BUTTON_SHIPPING_OPTIONS', 'خيارات الشحن');
define('IMAGE_BUTTON_TELL_A_FRIEND', 'أخبر صديقك');
define('IMAGE_BUTTON_UPDATE', 'تعديل');
define('IMAGE_BUTTON_UPDATE_CART', 'تجديد السله');
define('IMAGE_BUTTON_WRITE_REVIEW', 'كتابة مراجعه');

define('SMALL_IMAGE_BUTTON_DELETE', 'الغاء');
define('SMALL_IMAGE_BUTTON_EDIT', 'تعديل');
define('SMALL_IMAGE_BUTTON_VIEW', 'استعراض');

define('ICON_ARROW_RIGHT', 'أكثر');
define('ICON_CART', 'في العربةِ');
define('ICON_ERROR', 'خطأ');
define('ICON_SUCCESS', 'تم بنجاح');
define('ICON_WARNING', 'التحذير');

define('TEXT_GREETING_PERSONAL', 'مرحباً بكم مره أخرى <span class="greetUser">%s!</span> هل ترغبوا في مشاهدة <a href="%s"><u>الأصناف الجديده</u></a> المتاحه للشراء؟');
define('TEXT_GREETING_PERSONAL_RELOGON', 'إذا أنت لست %s, الرجاء <a href="%s"><u>الدخول بنفسك</u></a> بمعلومات إشتراكك.');
define('TEXT_GREETING_GUEST', 'مرحباً يا <span class="greetUser">ضيفنا!</span> هل ترغب في <a href="%s"><u>الدخول بنفسك</u></a>؟ أو هل تفضل في <a href="%s"><u>إنشاء إشتراك جديد</u></a>?');

define('TEXT_SORT_PRODUCTS', 'فرز الأصناف ');
define('TEXT_DESCENDINGLY', 'تنازلياً');
define('TEXT_ASCENDINGLY', 'تصاعدياً');
define('TEXT_BY', ' بـ ');

define('TEXT_REVIEW_BY', 'بواسطة %s');
define('TEXT_REVIEW_WORD_COUNT', '%s كلمه');
define('TEXT_REVIEW_RATING', 'التقدير: %s [%s]');
define('TEXT_REVIEW_DATE_ADDED', 'تاريخ الإضافه: %s');
define('TEXT_NO_REVIEWS', 'لايوجد حالياً مراجعة الصنف.');

define('TEXT_NO_NEW_PRODUCTS', 'لايوجد حاليا أصناف.');

/*** Begin Header Tags SEO ***/
define('BOX_HEADING_HEADERTAGS_TAGCLOUD', 'ﻊﻤﻠﻳﺎﺗ ﺎﻠﺒﺤﺛ ﺎﻠﺷﺎﺌﻋﺓ');
define('TEXT_SEE_MORE', 'ﺎﻗﺭﺃ ﺎﻠﻣﺰﻳﺩ');
/*** End Header Tags SEO ***/

define('TEXT_UNKNOWN_TAX_RATE', 'نسبة الضرائب غير معروفه');

define('TEXT_REQUIRED', '<span class="errorText">مطلوب</span>');

define('ERROR_TEP_MAIL', '<font face="Verdana, Arial" size="2" color="#ff0000"><strong>TEP ERROR: Cannot send the email through the specified SMTP server. Please check your php.ini setting and correct the SMTP server if necessary.</strong></font>');

define('TEXT_CCVAL_ERROR_INVALID_DATE', 'The expiry date entered for the credit card is invalid. Please check the date and try again.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'The credit card number entered is invalid. Please check the number and try again.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'The first four digits of the number entered are: %s. If that number is correct, we do not accept that type of credit card. If it is wrong, please try again.');

define('FOOTER_TEXT_BODY', 'كافة الحقوق محفوظة ' . date('Y') . ' <a href="' . tep_href_link(FILENAME_DEFAULT) . '" class="footerLinks">' . STORE_NAME . '</a><br />Powered by <a href="http://syscomlb.com" target="_blank" class="footerLinks">Syscom Technologies</a>');
// BOF: Featured Products
define('BOX_CATALOG_FEATURED_PRODUCTS', 'Featured Products');
define('BOX_HEADING_FEATURED', 'Featured');
// osC reCaptcha
define('ENTRY_SECURITY_CHECK', 'أدخل الرمز');
define('ENTRY_SECURITY_CHECK1', 'أدخل الرمز ');
define('ENTRY_SECURITY_CHECK_ERROR', '	يرجى إدخال الرمز في المكان المحدد   ');
//kgt - discount coupons
define('ENTRY_DISCOUNT_COUPON_ERROR', 'رمز القسيمة الذي أدخلته غير صالح.');
define('ENTRY_DISCOUNT_COUPON_SUCCESS', 'رمز القسيمة الذي أدخلته  صالح.');
define('ENTRY_DISCOUNT_COUPON_AVAILABLE_ERROR', 'The coupon code you have entered is no longer valid.');
define('ENTRY_DISCOUNT_COUPON_USE_ERROR', 'سجلاتنا تفيد بأنه تم استخدام هذه القسيمة. لا يجوز لك استخدامها');
define('ENTRY_DISCOUNT_COUPON_MIN_PRICE_ERROR', 'The minimum order total for this coupon is %s');
define('ENTRY_DISCOUNT_COUPON_MIN_QUANTITY_ERROR', 'The minimum number of products required for this coupon is %s');
define('ENTRY_DISCOUNT_COUPON_EXCLUSION_ERROR', 'Some or all of the products in your cart are excluded.' );
define('ENTRY_DISCOUNT_COUPON', 'رمز القسيمة:');
define('ENTRY_DISCOUNT_COUPON_SHIPPING_CALC_ERROR', 'Your calculated shipping charges have changed.');
//end kgt - discount coupons
/**** Begin View Counter ****/ //NOTE: text must be on one line
define('TEXT_VIEW_COUNTER_EMAIL_TRAP_MSG', 'The IP %s attempted access to the directory named admin and is most likely a hacker.' . "\r\n\r\n" . '******* This IP should be banned *******.' . "\r\n\r\n" . 'Click this url, %s, to find out more information about the IP.');
define('TEXT_VIEW_COUNTER_EMAIL_TRAP_MSG_HACKER', 'The IP %s attempted to alter the url in a way that is consistent with hacking attempts.' . "\r\n\r\n" . '******* This IP should be banned *******.' . "\r\n\r\n" . 'Click this url, %s, to find out more information about this IP.');
define('TEXT_VIEW_COUNTER_EMAIL_TRAP_SUBJECT', 'View counter IP Trap warning for IP %s');
define('TEXT_VIEW_COUNTER_EMAIL_TRAP_MSG_PREV', 'The IP %s attempted access to the directory named admin. This IP has already been banned.');
define('TEXT_VIEW_COUNTER_EMAIL_TRAP_MSG_SPOOF', 'The IP %s accessed the bad bot directory. Click this url, %s, to find out more information about the IP.');
define('TEXT_VIEW_COUNTER_EMAIL_TRAP_SUBJECT_PREV', 'View counter PREVIOUS IP Trap warning');
define('TEXT_VIEW_COUNTER_KILL_MESSAGE', 'We carefully monitor our shops traffic and you have been identified as trying to cause problems. We have notified the authorities of your attempts to defraud us.');
define('CONFIRMATION_WARNING', 'الفرات للنشر والتوزيع غير مسؤولة عن الكتب التي تتعرض للمصادرة أو التلف أو الإرجاع من قبل المؤسسات الرقابية في دول العالم العربي.');

/**** End View Counter ****/
?>