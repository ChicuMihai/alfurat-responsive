<?php
/*
  $Id: product_info.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
//define('TOP_BAR_TITLE', 'معلومات الكتاب');
define('TEXT_PRODUCT_NOT_FOUND', 'Article Not Found!');
//define('TEXT_CURRENT_REVIEWS', 'المراجعات الحاليه:');
//define('TEXT_MORE_INFORMATION', 'لمزيد من المعلومات, صفحة الكتاب هذه <a href="%s" target="_blank"><u>الصفحه</u></a>.');
//define('TEXT_DATE_ADDED', 'هذا الكتاب أُضيف في موقعنا التسويقي بتاريخ %s.');
//define('TEXT_DATE_AVAILABLE', '<font color="#ff0000">هذا الكتاب سيكون متوفراً في %s.</font>');
//define('TEXT_ALSO_PURCHASED_PRODUCTS', 'الزبائن الذين أشتروا هذا المنتج أشتروا أيضاً');
//define('TEXT_PRODUCT_OPTIONS', ' طريقة التوضيب:');
//define('option_description_2', ' يوضع الملصق داخل أنبوب');
//define('option_description_1', ' يوضع الملصق داخل ظرف');
//define('TEXT_CLICK_TO_ENLARGE', 'تكبير الصوره');
//define('TEXT_VIEWING', 'الصفحة الحالية');
//define('TEXT_DESCRIPTION', ' نبذة عن الكتاب');
//define('TEXT_POSTER_DESCRIPTION', ' التفاصيل');
//define('TEXT_MAGAZINES_DESCRIPTION', ' الملاحظات');
//define('TEXT_PUBLISHEER', ' الناشر');
//define('TEXT_AUTHOR', ' المؤلف');
//define('TEXT_TRANSLATOR', ' ترجمة');
//define('TEXT_EDITION', ' الطبعة');
//define('TEXT_SERIES', 'السلسلة');
//define('TEXT_SERIES_NUMBER', 'الرقم في السلسلة');
//define('TEXT_AVAILABLE_VOLUME', 'المجلدات المتوفرة');
//define('TEXT_ISBN', '   ISBN');
//define('TEXT_WEIGHT', ' الوزن');
//define('TEXT_WEIGHT_GRAM', ' غرام');
//define('TEXT_PAGE_NB', ' عدد الصفحات');
//define('TEXT_SIZE', 'القياس');
define('TEXT_RELEASED', ' Release Date');
define('TEXT_LATEST_ARTICLES', 'Latest Articles');
//define('TEXT_DATE_FILM', ' تاريخ الفيلم');
//define('TEXT_DIRECTOR', '  المخرج');
//define('TEXT_ACTORS', '  الممثلين');
//define('TEXT_COVER_TYPE', '  النوع');
//define('TEXT_COVER_TYPE_NORMAL', '   غلاف عادي');
//define('TEXT_COVER_TYPE_ARTISTIC', '   غلاف فني ');
?>
