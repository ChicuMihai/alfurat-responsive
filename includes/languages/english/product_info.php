<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('TEXT_PRODUCT_NOT_FOUND', 'Page not found!');
define('TEXT_CURRENT_REVIEWS', 'Current Reviews:');
define('TEXT_MORE_INFORMATION', 'For more information, please visit this products <a href="%s" target="_blank"><u>webpage</u></a>.');
define('TEXT_DATE_ADDED', 'This product was added to our catalog on %s.');
define('TEXT_DATE_AVAILABLE', '<font color="#ff0000">This product will be in stock on %s.</font>');
define('TEXT_ALSO_PURCHASED_PRODUCTS', 'Customers who bought this product also purchased');
define('TEXT_PRODUCT_OPTIONS', 'Available Options:');
define('TEXT_CLICK_TO_ENLARGE', 'Click to enlarge');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('TEXT_PUBLISHEER', ' Publisher');
define('TEXT_RELEASED', 'Date Release');
define('TEXT_AUTHOR', 'Author');
define('TEXT_EDITION', 'Edition');
define('TEXT_PAGE_NB', 'Page Number');
define('TEXT_SIZE', 'Size');
define('TEXT_COVER_TYPE', 'Cover Type');
define('TEXT_COVER_TYPE_NORMAL', 'Normal');
define('TEXT_COVER_TYPE_ARTISTIC', 'Artistic');
define('TEXT_TRANSLATOR', ' Translator');
define('TEXT_SERIES', 'Series');
define('TEXT_SERIES_NUMBER', 'Series Number');
define('TEXT_AVAILABLE_VOLUME', 'Available Volumes');
define('TEXT_ISBN', '   ISBN');
define('TEXT_WEIGHT', 'Weight');
define('TEXT_WEIGHT_GRAM', 'Gram');
define('TEXT_DATE_FILM', 'Film Date');;
define('TEXT_DIRECTOR', 'Director');
define('TEXT_ACTORS', 'Actors');
define('TEXT_DESCRIPTION', 'About the book');
define('TEXT_POSTER_DESCRIPTION', ' Details');
define('TEXT_MAGAZINES_DESCRIPTION', 'Notes');
define('TEXT_BOOK_NOT_AVAILABLE', '<font color="#ff0000">This book is not available, please contact us to see if we can provide it.</font>');
?>
