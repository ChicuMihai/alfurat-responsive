<?php
/*
  $Id: cc.php,v 1.10 2002/11/01 05:14:11 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_CYBS_SA_TEXT_TITLE', 'CyberSource Secure Acceptance');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_DESCRIPTION', 'Credit Card Test Info:<br><br>51230456708901 2346 <br>Expiry: 05/17');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_CREDIT_CARD_TYPE', 'Credit Card Type:');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_CREDIT_CARD_FIRST_NAME', 'First Name:');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_CREDIT_CARD_LAST_NAME', 'Last Name:');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_CREDIT_CARD_CVV_NUMBER', 'Credit Card CVV Number:');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_CREDIT_CARD_EXPIRES', 'Expiration Date:');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_JS_CYBS_FIRST_NAME', '* The owner\'s first name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_JS_CYBS_LAST_NAME', '* The owner\'s last name of the credit card must be at least ' . CC_OWNER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_JS_CYBS_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card. Please try another card or contact customer service.');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_INVALID_FIELD', 'There has been an error processing your credit card. One or more fields is either invalid or missing. Please try again.');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_INVALID_CVV', 'The CVV code supplied is invalid.  Please be sure to check it for accuracy and try again.');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_DECLINED_INSFUNDS_MESSAGE', 'Your credit card was declined due to insufficient funds. Please try another card or contact your bank for more information.');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_DECLINED_MESSAGE', 'Your credit card was declined. Please try another card or contact your bank for more information.');
  define('MODULE_PAYMENT_CYBS_SA_TEXT_ERROR', 'Credit Card Error!');

  define('CYBS_SA_TEXT_CCVAL_ERROR_CARD_TYPE_MISMATCH', 'The card type you have selected does not match the number entered.  Please try again.');
  define('CYBS_SA_TEXT_CCVAL_ERROR_CARD_CVV_MISSING', 'You must supply the CVV code for this credit card.  Please try again.');
  define('CYBS_SA_TEXT_CCVAL_ERROR_CARD_CVV_INVALID', 'The CVV code supplied is invalid.  Please be sure to check it for length and accuracy then try again.');
?>