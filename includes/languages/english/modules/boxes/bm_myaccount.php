<?php
/*
  $Id: bm_myaccount.php 1739 2012-12-28 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_BOXES_ACCOUNT_HEADING', 'My Account');
define('MODULE_BOXES_ACCOUNT_DESCRIPTION', 'My Account Box Links to Account Edits');
define('MODULE_BOXES_ACCOUNT_ADDRESS_BOOK', 'Change  Address Book');
define('MODULE_BOXES_ACCOUNT_PASSWORD', 'Change my account password');
define('MODULE_BOXES_ACCOUNT_EMAIL_PHONE', 'Change My Profile');
define('MODULE_BOXES_ACCOUNT_ORDERS_VIEW', 'View My Orders');
define('MODULE_BOXES_ACCOUNT_NEWSLETTERS', 'Newsletter Subscription');
define('MODULE_BOXES_ACCOUNT_PRODUCTS', 'Categories Notification');
define('MODULE_BOXES_ACCOUNT_CART_VIEW', 'My Shopping Cart ');
?>