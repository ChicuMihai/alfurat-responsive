<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_TITLE', 'Category Notifications');
  define('MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_DESCRIPTION', 'Add category notification to checkout success page');
  define('MODULE_CONTENT_CHECKOUT_SUCCESS_CATEGORY_NOTIFICATIONS_TEXT_NOTIFY_PRODUCTS', 'Please click on the category you want, in order to inform you of any new publications you are interested in:');
?>
