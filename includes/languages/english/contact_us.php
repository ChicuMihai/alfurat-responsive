<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('ADDRESS', 'Address');
define('DROP_LINE', 'Drop Us A Line');
define('ADDRESS_INFO', '<p style="direction:rtl; margin-right:10px;  "><span class="green"><strong>الفرات للنشر والتوزيع</strong></span>
						<br><br> <strong>
							قسم المبيعات والتوزيع
						</strong><br>  
						رأس بيروت  – 
						شارع عبلا
						<br> بنايـة  بخعازي 
						–  بيـروت  -   لبنـان
						<br> 
						تلفاكس:   750554 -1-00961  <br>
						للإستفسارات: <a href="mailto:sales@alfurat.com">sales@alfurat.com</a>
						<br>  <br><strong>
							الفرات للنشر والتوزيع / المكتبة
						</strong><br>
						شارع الحمراء - بناية رسامني  
						<br>
						ص.ب: 6435 - 113 بيروت - لبنـان
						<br>
						هاتف: 750054   <br>
						فاكس: 750053 - 1 -00961 <br>
للإستفسارات: <a href="mailto:alfurat@alfurat.com">alfurat@alfurat.com</a>
					</p> ');
define('CALL_US', 'Call us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('HEADING_TITLE', 'Contact Us');
define('NAVBAR_TITLE', 'Contact Us');
define('TEXT_SUCCESS', 'Your enquiry has been successfully sent to the Store Owner.');
define('EMAIL_SUBJECT', 'Enquiry from ' . STORE_NAME);
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('ENTRY_NAME', 'Full Name:');
define('ENTRY_EMAIL', 'E-Mail Address:');
define('ENTRY_ENQUIRY', 'Enquiry:');
define('SEND_ENQUIRY', 'Send Message');
define('OUR_LOCATION', 'Our Location');
define('ERROR_ACTION_RECORDER', 'Error: An enquiry has already been sent. Please try again in %s minutes.');
?>