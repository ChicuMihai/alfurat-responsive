<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE_1', 'Checkout');
define('NAVBAR_TITLE_2', 'Shipping Method');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('HEADING_TITLE', 'Delivery Information');

define('TABLE_HEADING_SHIPPING_ADDRESS', 'Shipping Address');
define('TEXT_CHOOSE_SHIPPING_DESTINATION', 'Please choose from your address book where you would like the items to be delivered to.');
define('TITLE_SHIPPING_ADDRESS', 'Shipping Address:');

define('TABLE_HEADING_SHIPPING_METHOD', 'Shipping Method');
define('TEXT_CHOOSE_SHIPPING_METHOD', 'Please select the preferred shipping method to use on this order.');
define('TITLE_PLEASE_SELECT', 'Please Select');
define('TEXT_ENTER_SHIPPING_INFORMATION', 'This is currently the only shipping method available to use on this order.');

define('TABLE_HEADING_COMMENTS', 'Add Comments About Your Order');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Continue Checkout Procedure');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'to select the preferred payment method.');

define('ERROR_NO_SHIPPING_AVAILABLE_TO_SHIPPING_ADDRESS', 'Shipping is currently not available for the selected shipping address. Please select or create a new shipping address to use with your purchase.');
?>
