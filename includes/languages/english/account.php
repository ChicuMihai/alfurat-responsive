<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
//Begin JTP - CatNotify
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
define('EMAIL_CAT_NOTIFICATIONS','View or change my TOWN notification list.');
//End JTP - CatNotify
define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
define('SUPPORT', 'Support');
define('HOME','Home');
define('NAVBAR_TITLE', 'My Account');
define('HEADING_TITLE', 'My Account Information');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('OVERVIEW_TITLE', 'Overview');
define('OVERVIEW_SHOW_ALL_ORDERS', '(show all orders)');
define('OVERVIEW_PREVIOUS_ORDERS', 'Previous Orders');

define('MY_ACCOUNT_TITLE', 'My Account');
define('MY_ACCOUNT_INFORMATION', 'View or change my account information.');
define('MY_ACCOUNT_ADDRESS_BOOK', 'View or change entries in my address book.');
define('MY_ACCOUNT_PASSWORD', 'Change my account password.');

define('MY_ORDERS_TITLE', 'My Orders');
define('MY_ORDERS_VIEW', 'View the orders I have made.');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('EMAIL_NOTIFICATIONS_TITLE', 'E-Mail Notifications');
define('EMAIL_NOTIFICATIONS_NEWSLETTERS', 'Subscribe or unsubscribe from newsletters.');
define('EMAIL_NOTIFICATIONS_PRODUCTS', 'View or change my product notification list.');
define('EMAIL_NOTIFICATIONS_CATEGORIES', 'View or change my Categories notification list.');
?>