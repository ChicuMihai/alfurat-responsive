<?php
/*
  $Id: account_notifications.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE_1', 'حسابي');
define('NAVBAR_TITLE_2', 'تبليغات مُنتَجِ');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('HEADING_TITLE', 'Categories Notification list');

//define('MY_NOTIFICATIONS_TITLE', 'تبليغات مُنتَجي');
//define('MY_NOTIFICATIONS_DESCRIPTION', 'تَسْمحُ قائمةُ إخطارِ المُنتَجَ لك لبَقاء حديثِ على الكتبِ تَجِدُ مِنْ الإهتمامِ.<br><br>لِكي يَكُونَ حديثَ على كُلّ المُنتَج يَتغيّرُ، يَختارُ <b>تبليغات المُنتَجِ العالميةِ</b>.');

//define('GLOBAL_NOTIFICATIONS_TITLE', 'تبليغات المُنتَجِ العالميةِ');
//define('GLOBAL_NOTIFICATIONS_DESCRIPTION', 'تبليغات على كُلّ الكتب المتوفرة.');

define('NOTIFICATIONS_TITLE', 'Categories Notifications');
define('NOTIFICATIONS_DESCRIPTION', 'To remove a category notification, uncheck the category and click Continue.');
define('NOTIFICATIONS_NON_EXISTING', 'Currently  there are no categories<br><br>');

define('SUCCESS_NOTIFICATIONS_UPDATED', 'Your categories notifications are successfully renewed.');
?>
