<?php

/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
 */define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE', 'Conditions of Use');
define('HEADING_TITLE', 'Conditions of Use');
define('FOR_HELP', 'For Help');
define('CONDITIONS_TEXT', '
			<h2> Order Process</h2>
			<p><span class="green mr-15" ><strong>	How does the ordering process take place?</strong></span></p>
			<ul>
				<li>The process begins by searching a certain book and choosing it by adding it to the shopping cart through clicking on "Add to Cart"
				</li><li>The shopping cart containing the desired book appears alongside with all the details concerning the order
				</li><li>The process is terminated by clicking on “Checkout” on the bottom of the shopping cart
				</li><li>You are then transferred to the next page containing all the desired details
				</li>
			</ul>
			<p><span class="green mr-15"><strong>How can I check if my order was done successfully?</strong></span></p>
			<ul><li>
					A message of the invoice will be sent to your personal email. </li></ul>
			<p><span class="green mr-15"><strong>I have placed an order but didn’t receive a message on my e-mail, why?</strong></span></p>
			<ul><li>It appears that you have entered a wrong email address, in this case please send an email through ”contact us” containing your name, address and phone number and the email address era will be corrected.</li></ul>
			<p><span class="green mr-15"><strong>How can I edit the contents of the shopping cart before submitting my order?</strong></span></p>
			<ul><li>To change the desired quantity of a book, navigate to the shopping cart and change the number in the quantity field and then click “Checkout”
				</li><li>	To remove a book from the shopping cart, click “X”</li></ul>
			<p><span class="green mr-15"><strong> 	How can I confirm my submitted order?</strong></span></p>
			<ul><li>A confirming message will be sent to your email address as soon as we receive the order, providing you with its number for follow up</li></ul>
			<p><span class="green mr-15"><strong>The book that I want is unavailable, what do I do?</strong></span></p>
			<ul><li>You can order the non-available books on the website through a message on “contact us” containing the name of the book, the author and the publishing house, and we will notify you if the book is available within 48 hours.</li></ul>
			<p><span class="green mr-15"><strong>I found the book that I am searching for but the cover photo differs from the one I know.</strong></span></p>
			<ul><li>In this case the book is printed by a different publishing house, or is a different copy.</li></ul>
			<h2>Payment method</h2>
			<ul>
				<li>Payments made on Alfurat online website through credit cards (Master or VISA card)</li><br>
				<li>The card must be valid and not out of date.</li><br>
				<li>All transactions are in USD. </li><br>
				<li>"Alfurat for Publishing and Distribution" uses SSL System in payments and purchase in order to protect an information regarding the purchase and therefore “Alfurat” doesn’t have any access to see the credit card number, as the process takes place immediately with the bank that verifies the entered data.
				</li><br>
				<li>	Refund:
					<ul>
						<li>The order can be canceled within 24 hours. (The customer handles the refund fees).</li><br>
						<li>If the book was not available. (Alfurat handles the refund fees)
						</li></ul></li>
			</ul>
		');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('TEXT_INFORMATION', 'Put here your Conditions of Use information.');
?>