<?php
/*
  $Id: account_notifications.php,v 1.1 2003/05/19 19:55:45 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Town Notifications');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('HEADING_TITLE', 'Town Notifications');

define('MY_NOTIFICATIONS_TITLE', 'My Town Notifications');
define('MY_NOTIFICATIONS_DESCRIPTION', 'The town notification list allows you to stay up to date on Towns you find of interest.<br><br>To be up to date on all product additions, select <b>Global Town Notifications</b>.');

define('GLOBAL_NOTIFICATIONS_TITLE', 'Global Town Notifications');
define('GLOBAL_NOTIFICATIONS_DESCRIPTION', 'Recieve notifications on all new products.');

define('NOTIFICATIONS_TITLE', 'Town Notifications');
define('NOTIFICATIONS_DESCRIPTION', 'To remove a town notification, clear the towns checkbox and click on Continue.');
define('NOTIFICATIONS_NON_EXISTING', 'There are currently no towns marked to be notified on.<br><br>To add towns to your town notification list, click on the notification link available on the town\'s category page.');

define('SUCCESS_NOTIFICATIONS_UPDATED', 'Your town notifications have been successfully updated.');
?>
