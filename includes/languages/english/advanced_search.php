<?php 
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE_1', 'Advanced Search');
define('NAVBAR_TITLE_2', 'Search Results');
define('TEXT_ALL_ACTORS', 'All Actors');
define('HEADING_TITLE_1', 'Advanced Search');
define('HEADING_TITLE_2', 'Products meeting the search criteria');
define('TEXT_ALL_DIRECTORS', 'All Directors');
define('HEADING_SEARCH_CRITERIA', 'Search Criteria');
define('TEXT_AUTHOR', 'Author');
define('TEXT_SEARCH_IN_DESCRIPTION', 'Search In Product Descriptions');
define('ENTRY_CATEGORIES', 'Categories:');
define('ENTRY_INCLUDE_SUBCATEGORIES', 'Include Subcategories');
define('ENTRY_MANUFACTURERS', 'Manufacturers:');
define('ENTRY_PRICE_FROM', 'Price From:');
define('ENTRY_PRICE_TO', 'Price To:');
define('ENTRY_DATE_FROM', 'Date From:');
define('ENTRY_DATE_TO', 'Date To:');
define('TEXT_ALL_AUTHORS', 'All Authors');
define('TEXT_CHOOSE_SERIES', 'Choose Series');
define('TEXT_SEARCH_HELP_LINK', '<u>Search Help</u> [?]');
define('TEXT_ALL_TRANSLATORS', 'All Translators');
define('TEXT_ALL_CATEGORIES', 'All Categories');
define('TEXT_ALL_MANUFACTURERS', 'All Publishers');
define('TEXT_RELEASE_YEAR', 'Release Year');
define('HEADING_SEARCH_HELP', 'Search Help');
define('TEXT_SEARCH_HELP', 'Keywords may be separated by AND and/or OR statements for greater control of the search results.<br /><br />For example, <u>Microsoft AND mouse</u> would generate a result set that contain both words. However, for <u>mouse OR keyboard</u>, the result set returned would contain both or either words.<br /><br />Exact matches can be searched for by enclosing keywords in double-quotes.<br /><br />For example, <u>"notebook computer"</u> would generate a result set which match the exact string.<br /><br />Brackets can be used for further control on the result set.<br /><br />For example, <u>Microsoft and (keyboard or mouse or "visual basic")</u>.');
define('TEXT_CLOSE_WINDOW', '<u>Close Window</u> [x]');
define('TEXT_CHOOSE_FILTERS', 'Please fill in any of the search fields ');
define('TABLE_HEADING_IMAGE', '');
define('TABLE_HEADING_MODEL', 'Model');
define('TABLE_HEADING_PRODUCTS', 'Book Name');
define('TABLE_HEADING_MANUFACTURER', 'Manufacturer');
define('TABLE_HEADING_QUANTITY', 'Quantity');
define('TABLE_HEADING_PRICE', 'Price');
define('TABLE_HEADING_WEIGHT', 'Weight');
define('TABLE_HEADING_BUY_NOW', 'Buy Now');
define('TEXT_PRODUCTS_SIZE', ' Size');
define('TEXT_NO_PRODUCTS', 'There is no product that matches the search criteria.');
define('PRICE_FROM', 'Price from');
define('PRICE_TO', 'Price till');
define('TABLE_HEADING_POSTERS', 'Film Name');
define('ERROR_AT_LEAST_ONE_INPUT', 'At least one of the fields in the search form must be entered.');
define('ERROR_INVALID_FROM_DATE', 'Invalid From Date.');
define('ERROR_INVALID_TO_DATE', 'Invalid To Date.');
define('ERROR_TO_DATE_LESS_THAN_FROM_DATE', 'To Date must be greater than or equal to From Date.');
define('ERROR_PRICE_FROM_MUST_BE_NUM', 'Price From must be a number.');
define('ERROR_PRICE_TO_MUST_BE_NUM', 'Price To must be a number.');
define('ERROR_PRICE_TO_LESS_THAN_PRICE_FROM', 'Price To must be greater than or equal to Price From.');
define('ERROR_INVALID_KEYWORDS', 'Invalid keywords.');
define('TABLE_HEADING_DIRECTOR', 'Director');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
?>
