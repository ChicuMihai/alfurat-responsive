<?php
/*
  $Id: about_us.php,v 1.0 2011/04/10 01:48:08 dgw_ Exp $

  Copyright (c) 2011 E-trade360.com LTD
*/

define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('ABOUT_US_TEXT', '<p style="direction: rtl;">		الفرات مؤسسة تعنى بتوزيع الكتاب العربي و المجلات الثقافية العربية.
			نحن نقوم برصد إصدارات الكتب في لبنان وبعض البلدان العربية وإعداد قوائم بها  وعرضها على موقعنا الإلكتروني.	<br>	<br>
			لدينا بنك معلومات لـ 120000 ألف كتاب عربي يتضمن: إسم المؤلف، عنوان الكتاب، دار النشر، الموضوع الأساسي و الفرعي للكتاب وصورة الغلاف.	</p>');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE', 'About us');
define('HEADING_TITLE', 'About us');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
?>