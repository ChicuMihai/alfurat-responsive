<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('NAVBAR_TITLE', 'Cart Contents');
define('HEADING_TITLE', 'What\'s In My Cart?');
define('TEXT_CART_EMPTY', 'Your Shopping Cart is empty!');
define('SUB_TITLE_SUB_TOTAL', 'Sub-Total:');
define('SUB_TITLE_TOTAL', 'Total:');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'Products marked with ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' dont exist in desired quantity in our stock.<br />Please alter the quantity of products marked with (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), Thank you');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Products marked with ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' dont exist in desired quantity in our stock.<br />You can buy them anyway and check the quantity we have in stock for immediate deliver in the checkout process.');
define('TEXT_PUBLISHEER', ' Publisher');
define('TEXT_AUTHOR', ' Author');
define('TEXT_TRANSLATOR', ' Translator');
define('TEXT_RELEASED', 'Date Released');
define('TEXT_QUANTITY', ' Quantity');
define('TEXT_TO_REMOVE', 'Do you want to remove ');
define('TEXT_QUESTION_MARK', '?');
define('TABLE_HEADING_REMOVE', 'Remove');
define('TABLE_HEADING_QUANTITY', 'Quantity');
//define('TABLE_HEADING_MODEL', 'الرمز أو النوع');
define('TABLE_HEADING_PRODUCTS', 'Items(s)');
define('TABLE_HEADING_TOTAL', 'Total');

define('TEXT_DATE_FILM', ' Film Date');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'الأصناف المؤشره بـ *** غير متوفر يرجى حذفه من الطلب و مراسلتنا للبحث في إمكانية تأمينه.');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'الأصناف المؤشره بـ *** غير متوفره بالكميه المرغوبه في مخزوننا.<br>يمكنك شرائها على أي حال ولكن تأكد من الكميه المتوفره في مخزوننا حتى تصل إليكم أثناء معالجة الحسابات.');
define('TEXT_ALTERNATIVE_CHECKOUT_METHODS', '- OR -');
define('TEXT_OR', 'or ');
define('TEXT_REMOVE', 'remove');
define('TABLE_HEADING_COUPON', 'Do you have a coupon code?' );
?>