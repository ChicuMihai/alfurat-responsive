<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','Search');
define('CLOSE','Close');
define('CONTINUE3','Continue');
  define('CURRENCY', 'Currency');
 define('ADVANCED', 'Advanced');
  define('SUPPORT', 'Support');
define('HOME','Home');
define('ABOUT', 'About us');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('LOG_IN', 'Log In');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log Out');
define('CONTACT_US', 'Contact us');
define('CREATE_ACCOUNT', 'Create Account');
define('SEARCH_FOR_BOOK', 'Search for a Book');
define('SEARCH_FOR_POSTER', 'Search for a Poster');
define('ADVANCED_SEARCH', 'Advanced Search');
define('LOG_IN', 'Log in');
define('MY_ACCOUNT', 'My Account');
define('LOG_OUT', 'Log out');
define('POSTERS', 'Posters');
define('MAGAZINES', 'Magazines');
define('BOOK_OF_WEEK', 'Book of the Week');
define('ARTICLE_OF_MONTH', 'Article of the Month');
define('TERMS_AND_CONDITIONS', 'Terms and Conditions');
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Latest Releases');
define('HEADING_TITLE', 'Visitors Cart / Members Cart');
define('SUB_HEADING_TITLE_1', 'Visitors Cart');
define('SUB_HEADING_TITLE_2', 'Members Cart');
define('SUB_HEADING_TITLE_3', 'Info');
define('SUB_HEADING_TEXT_1', 'Every visitor to our online shop will be given a \'Visitors Cart\'. This allows the visitor to store their products in a temporary shopping cart. Once the visitor leaves the online shop, so will the contents of their shopping cart.');
define('SUB_HEADING_TEXT_2', 'Every member to our online shop that logs in is given a \'Members Cart\'. This allows the member to add products to their shopping cart, and come back at a later date to finalize their checkout. All products remain in their shopping cart until the member has  checked them out, or removed the products themselves.');
define('SUB_HEADING_TEXT_3', 'If a member adds products to their \'Visitors Cart\' and decides to log in to the online shop to use their \'Members Cart\', the contents of their \'Visitors Cart\' will merge with their \'Members Cart\' contents automatically.');
define('TEXT_CLOSE_WINDOW', '[ close window ]');
define('FOR_HELP', 'For Help');
define('LOGIN_OR_REGISTER', 'Login or Register');
define('FORGOT_YOUR_PASSWORD', 'Forgot Your Password?');
define('PAYMENT_METHOD', 'Payment Method');
define('CONNECT_WITH_US', 'Connect with us');
?>