<?php
/*
  $Id: checkout_payment.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'المحاسبه');
define('NAVBAR_TITLE_2', 'طريقة الدفع');
define('TOP_BAR_TITLE', 'إجراء محاسبه');
define('HEADING_TITLE', 'طريقة الدفع');
define('TABLE_HEADING_COMMENTS', 'أضف ما ترغب أن تقوله عن طلبكم');
define('TABLE_HEADING_METHODS', 'الطُرق');
define('TABLE_HEADING_SELECTION', 'إختيارات');
define('TABLE_HEADING_DELIVERY_ADDRESS', 'عنوان التوصيل أو الإستلام');
define('TABLE_HEADING_SHIPPING_INFO', 'ناقل الشحن');
define('TABLE_HEADING_SHIPPING_QUOTE', 'إختر التسعيره');
define('CHANGE_DELIVERY_ADDRESS', 'تغيير العنوان');
define('CHANGE_SHIPPING_OPTIONS', 'تغيير خيارات الشحن');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TABLE_HEADING_BILLING_ADDRESS', 'عنوان الفاتورة');
define('TEXT_SELECTED_BILLING_DESTINATION', 'من فضلك اختار من دفتر العناوين العنوان المفروض ارسال الفاتورة/المحاسبة اليه وشكرا');
define('TITLE_BILLING_ADDRESS', 'عنوان الفاتورة/المحاسبة:');
define('TABLE_HEADING_PAYMENT_METHOD', 'طريقة الدفع/المحاسبة');
define('TEXT_SELECT_PAYMENT_METHOD', 'من فضلك اختار طريقة الدفع المناسبة لهذه الطلبية');
define('TITLE_PLEASE_SELECT', '');
define('TEXT_ENTER_PAYMENT_INFORMATION', '');
//define('TEXT_ENTER_PAYMENT_INFORMATION', 'للآسف هذه هي طريقة المحاسبة المتاحة لهذه الطلبية الآن فبرجاء المعذرة');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'استكمال العملية');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'لتأكيد هذه الطلبية');

?>
