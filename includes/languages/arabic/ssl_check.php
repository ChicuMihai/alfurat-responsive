<?php
/*
  $Id: ssl_check.php 1739 2007-12-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE', 'الفحص الأمني');
define('HEADING_TITLE', 'الفحص الأمني');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TEXT_INFORMATION', 'إكتشفنا بأن متصفحك قد ولدت مختلف معرف جلسة SSL المستخدمة في صفحاتنا الآمنة. <br /><br />للتدابير الأمنية سوف تحتاج إلى تسجيل الدخول إلى حسابك مرة أخرى لمواصلة التسوق.<br /><br />بعض برامج التصفح مثل Konqueror 3.1 ليس لديها القدرة على توليد آمنة معرف جلسة SSL تلقائيا الذي نطلبه. إذا كنت تستخدم هذا المتصفح، ونحن نوصي التحول إلى متصفح آخر مثل<a href="http://www.microsoft.com/ie/" target="_blank">Microsoft Internet Explorer</a>, <a href="http://channels.netscape.com/ns/browsers/download_other.jsp" target="_blank">Netscape</a>، أو <a href="http://www.mozilla.org/releases/" target="_blank">Mozilla</a>لمواصلة التسوق. اتخذنا هذا المقياس من الأمن لمنفعتك، ونعتذر مقدما للإزعاج.<br /><br />الرجاء الاتصال بالمالك إذا كان لديك أي أسئلة تتعلق لهذه المشكلة، أو للإستمرار بشراء الكتب offline.');

define('BOX_INFORMATION_HEADING', 'الخصوصية والأمن ');
define('BOX_INFORMATION', 'نحن تحقق من صحة معرف جلسة العمل SSL تتولد تلقائيا بواسطة برنامج المتصفح على كل طلب الصفحة الآمنة التي أدخلت على هذا الخادم.<br /><br />المصادقة هذا يؤكد أنه أنت من الذي تبحر في هذا الموقع مع حسابك وليس أي شخص آخر.');
?>
