<?php
/*
  $Id: login.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE', 'طلبيه');
define('HEADING_TITLE', 'مرحبا، الرجاء تسجيل الدخول');
define('TEXT_STEP_BY_STEP', 'نحن معك أينما تكون, خطوه بخطوه.');

define('NAVBAR_TITLE', 'دخول');
define('TOP_BAR_TITLE', 'دخول إلى \'' . STORE_NAME . '\'');
define('HEADING_TITLE', 'أجعلني أدخل!');
define('TEXT_STEP_BY_STEP', ''); // should be empty

define('ENTRY_EMAIL_ADDRESS2', 'أدخل بريدك الإلكتروني:');
define('HEADING_NEW_CUSTOMER', 'زبون/عميل جديد');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'عندما تشترك معنا كعميل تستطيع التجول في المتجر كما تشاء وأيضا تتمتع بجميع الميزات الخاصة كزبون');

define('TEXT_NEW_CUSTOMER', 'أنا زبون جديد.');
define('HEADING_RETURNING_CUSTOMER', 'أنا من زبائن الموقع المعروفين');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TEXT_RETURNING_CUSTOMER', 'أنا من زبائن الموقع المعروفين,<br>&nbsp;وكلمة السر الخاصه بي:');
define('TEXT_COOKIE', 'إحفظ معلومات الدخول في الكوكي؟');
define('TEXT_PASSWORD_FORGOTTEN', 'نسيت أو فقدت كلمة السر الخاصه بك؟ إضغط هنا');
define('TEXT_LOGIN_ERROR', '<font ><b>خطأ:</b></font> \'البريد الإلكتروني\' و/أو \'كلمة السر\' غير متطابقتين.');
define('TEXT_LOGIN_ERROR_EMAIL', '<font color="#ff0000"><b>خطأ:</b></font> \'البريد الإلكتروني\' موجود في قاعدة البيانات لدينا, الرجاء إستخدام \'كلمة السر\' الخاصه بك للدخول.');
define('TEXT_VISITORS_CART', '<font color="#ff0000"><b>ملاحظه:</b></font>&quot;سلة الزائر&quot; التي لديك محتويات محتوياتها إندمجت مع &quot;سلة العضو&quot; عندما دخلت. <a href="javascript:session_win();">[للإستزاده أكثر]</a>');
?>
