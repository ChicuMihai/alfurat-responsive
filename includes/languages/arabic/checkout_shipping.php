<?php
/*
  $Id: checkout_shipping.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'اختيار الشحن');
define('NAVBAR_TITLE_2', 'طريقة الشحن');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('HEADING_TITLE', 'معلومات التسليمِ');

define('TABLE_HEADING_SHIPPING_ADDRESS', 'عنوان الشحن');
define('TEXT_CHOOSE_SHIPPING_DESTINATION', 'هذه هي طريقة الشحن المتاحة لهذه الطلبية');
define('TITLE_SHIPPING_ADDRESS', 'عنوان الشحن:');

define('TABLE_HEADING_SHIPPING_METHOD', 'طريقة الشحن');
define('TEXT_CHOOSE_SHIPPING_METHOD', 'رجاءً إخترْ طريقةَ الشحن المُفَضَّلةِ للإسْتِعْمال على هذا الطلبِ.');
define('TITLE_PLEASE_SELECT', 'رجاءً إخترْ');
define('TEXT_ENTER_SHIPPING_INFORMATION', 'هذه هي طريقة الشحن المتاحة لهذه الطلبية.');

define('TABLE_HEADING_COMMENTS', 'أضف ما ترغب أن تقوله عن طلبكم');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'واصلْ إجراءَ الاختبار');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'لإخْتياَر طريقةِ الدفع المُفَضَّلةِ.');
define('ERROR_NO_SHIPPING_AVAILABLE_TO_SHIPPING_ADDRESS', 'الشحن غير متوفر حاليا" للعنوان المحدد   , '
		. 'يرجى تحديد أو إنشاء عنوان الشحن جديد لاستخدامه مع عملية الشراء.');

?>
