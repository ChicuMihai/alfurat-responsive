<?php
/*
  $Id: checkout_shipping_address.ph for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'أختيار عنوان الشحن');
define('NAVBAR_TITLE_2', 'تغير عنوان الشحن');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('HEADING_TITLE', 'معلومات التسليم');

define('TABLE_HEADING_SHIPPING_ADDRESS', 'عنوان الشحن');
define('TEXT_SELECTED_SHIPPING_DESTINATION', ':هذا عنوان الشحن الذي إخترته لتسلم المواد');
define('TITLE_SHIPPING_ADDRESS', 'عنوان الشحن:');

define('TABLE_HEADING_ADDRESS_BOOK_ENTRIES', 'مداخل دفترِ عناوين');
define('TEXT_SELECT_OTHER_SHIPPING_DESTINATION', 'رجاءً إخترْ عنوانَ الشحن المُفَضَّلِ إذا كانت الموادِ في هذا الطلبِ سَتُسلّمُ في مكان آخر.');
define('TITLE_PLEASE_SELECT', 'رجاءً إخترْ');

define('TABLE_HEADING_NEW_SHIPPING_ADDRESS', 'عنوان الشحن الجديدِ');
define('TEXT_CREATE_NEW_SHIPPING_ADDRESS', 'رجاءً إستخدم الشكلَ التاليَ لتكوين'
		. ' عنوان شحن جديد'
		. 'لتسليم هذا الطلب:');

define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'واصلْ إجراءات الاختبار.');
define('TEXT_CONTINUE_CHECKOUT_PROCEDURE', 'لإخْتياَر طريقةِ الشحن المُفَضَّلةِ.');
?>
