<?php
/*
  $Id: tell_a_friend.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE', 'أخبر صديقك');
define('HEADING_TITLE', 'أخبر صديقك عن \'%s\'');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('FORM_TITLE_CUSTOMER_DETAILS', 'تفاصيلك');
define('FORM_TITLE_FRIEND_DETAILS', 'تفاصيل صديقك');
define('FORM_TITLE_FRIEND_MESSAGE', 'رسالتك');

define('FORM_FIELD_CUSTOMER_NAME', 'إسمك:');
define('FORM_FIELD_CUSTOMER_EMAIL', 'بريدك الإلكتروني:');
define('FORM_FIELD_FRIEND_NAME', 'إسم صديقك:');
define('FORM_FIELD_FRIEND_EMAIL', 'بريد صديقك الإلكتروني:');

define('TEXT_EMAIL_SUCCESSFUL_SENT', 'رسالتك عن <b>%s</b> تم إرسالها بنجاح إلى <b>%s</b>.');

define('TEXT_EMAIL_SUBJECT', 'صديقك %s يوصيك عن هذا الكتاب العظيم من %s');
define('TEXT_EMAIL_INTRO', 'مرحباً %s!' . "\n\n" . 'صديقك, %s, يرغب منك أن تستطلع عن %s من %s.');
define('TEXT_EMAIL_LINK', 'لكي تشاهد الكتاب إضغط على الرابط التالي أو أنسخ الرابط وألصقه في مستعرض المتصفح لديك:' . "\n\n" . '%s');
define('TEXT_EMAIL_SIGNATURE', 'المحترم,' . "\n\n" . '%s');

define('ERROR_TO_NAME', 'من فضلك أكتب عنوان صديقك');
define('ERROR_TO_ADDRESS', 'من فضلك أكتب عنوان بريد اليكتروني صحيح');
define('ERROR_FROM_NAME', 'من فضلك أكتب أسمك');
define('ERROR_FROM_ADDRESS', 'من فضلك أكتب عنوان بريدك الاليكتروني صحيح');
?>
