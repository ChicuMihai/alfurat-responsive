<?php
/*
  $Id: checkout_confirmation.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'اختبار');
define('NAVBAR_TITLE_2', 'التبليغات');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('HEADING_TITLE', ' تأكيد الطلبية');
define('HEADING_SHIPPING_INFORMATION', 'معلومات الشحن');
define('HEADING_DELIVERY_ADDRESS', 'عنوان التسليم');
define('HEADING_SHIPPING_METHOD', 'طريقة الشحن');
define('HEADING_PRODUCTS', 'الكتب');
define('HEADING_TAX', 'الضريبة');
define('HEADING_TOTAL', 'المجموع');
define('HEADING_BILLING_INFORMATION', 'معلومات المحاسبة/الفاتورة');
define('HEADING_BILLING_ADDRESS', 'عنوان المحاسبة/الفاتورة');
define('HEADING_PAYMENT_METHOD', 'طريقة الدفع/المحاسبة');
define('HEADING_PAYMENT_INFORMATION', 'معلومات الدفع/المحاسبة');
define('HEADING_ORDER_COMMENTS', 'التعليقات والاضافات');

define('TEXT_EDIT', 'أضف');
?>
