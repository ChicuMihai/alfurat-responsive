<?php
/*
  $Id: address_book_process.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'إشتراكي');
define('NAVBAR_TITLE_2', 'دفتر العناوين');
define('NAVBAR_TITLE_ADD_ENTRY', 'إدخال جديد');
define('NAVBAR_TITLE_MODIFY_ENTRY', 'تعديل إدخال');
define('NAVBAR_TITLE_DELETE_ENTRY', 'إحذفْ دخولاً ');
define('HEADING_TITLE_ADD_ENTRY', 'إدخال دفتر عناوين جديد');
define('HEADING_TITLE_MODIFY_ENTRY', 'إدخال دفتر عناوين');
define('HEADING_TITLE_DELETE_ENTRY', 'إحذفْ دخولَ دفترِ عناوين');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('DELETE_ADDRESS_TITLE', 'إحذفْ عنواناً');
define('DELETE_ADDRESS_DESCRIPTION', 'هَلْ أنت متأكّد أنت توَدُّ أَنْ تَحْذفَ العنوانَ المختارَ مِنْ دفترِ عناوينكَ؟');

define('NEW_ADDRESS_TITLE', 'دخول دفترِ العناوين الجديدِ');

define('SELECTED_ADDRESS', 'العنوان المختار');
define('SET_AS_PRIMARY', 'أضف كعنوان أساسي.');

define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED', 'العنوان المختار أُزيلَ بنجاح مِنْ دفترِ عناوينكَ.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED', 'دفتر عناوينكَ جُدّدَ بنجاح.');

define('WARNING_PRIMARY_ADDRESS_DELETION', 'العنوان الأساسي لا يُمْكن أنْ يُحْذَفَ. رجاءً ضِعْ عنوانَ آخرَ كالعنوان والمحاولة الأساسية ثانيةً.');

define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY', 'دخول دفترَ العناوين لا يَجِدُ.');
define('ERROR_ADDRESS_BOOK_FULL', 'دفتر عناوينكَ كاملُ. رجاءً إحذفْ عنوانَ غير مطلوبَ لتَوفير  واحد جديد.');
?>
