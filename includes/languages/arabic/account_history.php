<?php
/*
  $Id: account_history.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'إشتراكي');
define('NAVBAR_TITLE_2', 'المحفوظات');
/*define('TOP_BAR_TITLE', 'محفوظات الإشتراك');*/
define('HEADING_TITLE', 'محفوظات مشترياتي');
define('TEXT_ORDER_NUMBER', 'طلبيه رقم');
define('TEXT_ORDER_DATE', 'تاريخ الطلبيه');
define('TEXT_ORDER_COST', 'تكلفة الطلبيه');
define('TEXT_ORDER_STATUS', 'حالة الطلبيه');
define('TEXT_ORDER_SHIPPED_TO', 'الشحن الي:');
define('TEXT_ORDER_BILLED_TO', 'المحاسبة الي:');
define('TEXT_ORDER_PRODUCTS', 'الكتب:');
define('TEXT_VIEW_ORDER', 'مشاهدة الطلبية');
define('TEXT_NO_PURCHASES', 'حتى الآن لم تقم بشراء أي كتاب.');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
?>
