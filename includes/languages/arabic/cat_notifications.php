<?php
/*
  $Id: account_notifications.php,v 1.1 2003/05/19 19:55:45 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Town Notifications');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('HEADING_TITLE', 'Town Notifications');

define('MY_NOTIFICATIONS_TITLE', 'My Town Notifications');
define('MY_NOTIFICATIONS_DESCRIPTION', 'The town notification list allows you to stay up to date on Towns you find of interest.<br><br>To be up to date on all product additions, select <b>Global Town Notifications</b>.');

define('GLOBAL_NOTIFICATIONS_TITLE', 'Global Town Notifications');
define('GLOBAL_NOTIFICATIONS_DESCRIPTION', 'Recieve notifications on all new products.');

define('NOTIFICATIONS_TITLE', 'Town Notifications');
define('NOTIFICATIONS_DESCRIPTION', 'To remove a town notification, clear the towns checkbox and click on Continue.');
define('NOTIFICATIONS_NON_EXISTING', 'There are currently no towns marked to be notified on.<br><br>To add towns to your town notification list, click on the notification link available on the town\'s category page.');

define('SUCCESS_NOTIFICATIONS_UPDATED', 'Your town notifications have been successfully updated.');
?>
