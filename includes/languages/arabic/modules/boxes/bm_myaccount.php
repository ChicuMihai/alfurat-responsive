<?php
/*
  $Id: bm_myaccount.php 1739 2012-12-28 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_BOXES_ACCOUNT_HEADING', 'حسابي');
define('MODULE_BOXES_ACCOUNT_DESCRIPTION', 'My Account Box Links to Account Edits');
define('MODULE_BOXES_ACCOUNT_ADDRESS_BOOK', 'تغيير العنوان ');
define('MODULE_BOXES_ACCOUNT_PASSWORD', 'تغيير كلمة المرور ');
define('MODULE_BOXES_ACCOUNT_EMAIL_PHONE', 'تغيير البيانات');
define('MODULE_BOXES_ACCOUNT_ORDERS_VIEW', 'عرض الطلبات ');
define('MODULE_BOXES_ACCOUNT_NEWSLETTERS', 'تغيير النشرات الإخبارية ');
define('MODULE_BOXES_ACCOUNT_PRODUCTS', 'تغيير البلاغات ');
define('MODULE_BOXES_ACCOUNT_CART_VIEW', 'ماذا في سلتي؟ ');
?>