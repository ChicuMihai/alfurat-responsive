<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_BOXES_HEADER_TAGS_TITLE', 'بطاقة');
  define('MODULE_BOXES_HEADER_TAGS_DESCRIPTION', 'عرض صفحة البطاقة');
  define('MODULE_BOXES_HEADER_TAGS_BOX_TITLE', 'بطاقة');
?>
