<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_BOXES_ORDER_HISTORY_TITLE', 'طلبات السابقة');
  define('MODULE_BOXES_ORDER_HISTORY_DESCRIPTION', 'عرض الكتب المطلوبة من قبل');
  define('MODULE_BOXES_ORDER_HISTORY_BOX_TITLE', 'طلبات السابقة');
?>
