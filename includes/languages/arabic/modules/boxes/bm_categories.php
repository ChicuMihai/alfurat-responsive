<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_BOXES_CATEGORIES_TITLE', 'فئات الكتب');
  define('MODULE_BOXES_CATEGORIES_DESCRIPTION', 'عرض شجرة التنقل للفئات');
  define('MODULE_BOXES_CATEGORIES_BOX_TITLE', 'فئات الكتب');
  define('MODULE_BOXES_POSTERS_BOX_TITLE', 'فئات الملصقات');
?>
