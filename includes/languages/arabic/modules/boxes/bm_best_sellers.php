<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_BOXES_BEST_SELLERS_TITLE', ' الأكثر مبيعا');
  define('MODULE_BOXES_BEST_SELLERS_DESCRIPTION', 'عرض الأكثر مبيعا عالميا');
  define('MODULE_BOXES_BEST_SELLERS_BOX_TITLE', ' الأكثر مبيعا');
?>
