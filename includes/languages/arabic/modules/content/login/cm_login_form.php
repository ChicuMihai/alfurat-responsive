<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_CONTENT_LOGIN_FORM_TITLE', 'Login Form');
  define('MODULE_CONTENT_LOGIN_FORM_DESCRIPTION', 'Show a login form on the login page');

  define('MODULE_CONTENT_LOGIN_HEADING_RETURNING_CUSTOMER', 'زبون عائد ');
  define('MODULE_CONTENT_LOGIN_TEXT_RETURNING_CUSTOMER', '');
  define('MODULE_CONTENT_LOGIN_TEXT_PASSWORD_FORGOTTEN', '
نسيت كلمة المرور؟ انقر هنا.');
  define('MODULE_CONTENT_LOGIN_TEXT_LOGIN_ERROR', '<font ><b>خطأ:</b></font> \'البريد الإلكتروني\' و/أو \'كلمة السر\' غير متطابقتين.');
?>
