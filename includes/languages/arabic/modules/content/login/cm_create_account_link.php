<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_CONTENT_CREATE_ACCOUNT_LINK_TITLE', 'Create Account Link');
  define('MODULE_CONTENT_CREATE_ACCOUNT_LINK_DESCRIPTION', 'Show a create account container on the login page');

  define('MODULE_CONTENT_LOGIN_HEADING_NEW_CUSTOMER', 'زبون جديد');
  define('MODULE_CONTENT_LOGIN_TEXT_NEW_CUSTOMER', '');
  define('MODULE_CONTENT_LOGIN_TEXT_NEW_CUSTOMER_INTRODUCTION', '
عن طريق انشاء حساب على ' . STORE_NAME . ' سوف تكون قادر على الشراء أسرع, 
وتتبع طلبيات  كنت قد قدمت سابقا
<br>');
?>
