<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_CONTENT_CHECKOUT_SUCCESS_THANK_YOU_TITLE', 'شكرا ');
  define('MODULE_CONTENT_CHECKOUT_SUCCESS_THANK_YOU_DESCRIPTION', 'Show thank you block on the checkout success page.');

  define('MODULE_CONTENT_CHECKOUT_SUCCESS_TEXT_SUCCESS', '	سيتم تجهيز طلبكم وستصل الكتب إلى عنوانكم في غضون يومين إلى سبعة أيام');
  define('MODULE_CONTENT_CHECKOUT_SUCCESS_TEXT_SEE_ORDERS', 'يمكنك الإضطلاع على حالة طلبك في أي وقت في صفحة'
		  . ' <a href="%s">عرض طلبات</a>'
		  . ' حسابك.');
  define('MODULE_CONTENT_CHECKOUT_SUCCESS_TEXT_CONTACT_STORE_OWNER', '
يرجى توجيه أي أسئلة تود  طرحها علينا  على صفحة <a href="%s"> اتصل بنا</a>.');
  define('MODULE_CONTENT_CHECKOUT_SUCCESS_TEXT_THANKS_FOR_SHOPPING', '
شكرا للتسوق معنا  !');
?>
