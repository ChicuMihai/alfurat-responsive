<?php
/*
  $Id: cookie_usage.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE', 'إستعمال كوكي');
define('HEADING_TITLE', 'إستعمال كوكي');

define('TEXT_INFORMATION', 'إكتشفنَا بأنّ متصفّحَكَ لا يَدْعمُ الكوكيز، أَو وَضعنَا الكوكيز لكي تُعطّلَ. <br> <br> للإِسْتِمْرار بتَسَوُّق على الإنترنتِ، نُشجّعُك لتَمْكين الكوكيز على متصفّحِكَ. <br> <br> ل<b> إنترنت إكسبلورير </b> متصفّحات، رجاءً نفّذْ هذه التعليماتِ: <br> <ol> <li> نقرة على الأدواتِ menubar، وخيارات الإنترنتِ منتقاةِ </li> <li> تَختارُ سعرَ الأمنَ، وأعادتْ مستوى الأمنَ إلى الوسطِ </li> </ol> أَخذنَا هذا المقياسِ مِنْ الأمنِ لمنفعتِكَ، ويَعتذرونَ مقدماً إذا أيّ إزعاج مُسَبَّب. <br> <br> الرجاء الاتصال بصاحب الدكّان إذا كان لديكم أي سؤال يَتعلّقُ بهذا المتطلبِ، أَو للإِسْتِمْرار بشِراء الكتبِ غير متَّصلةِ.');

define('BOX_INFORMATION_HEADING', 'سرية وأمن كوكي');
define('BOX_INFORMATION', 'الكوكيز يجب أنْ تُمَكّنَ لشِراء على الإنترنتِ على هذا المخزنِ لإعتِناق السريةِ والأمنِ تَعلّقا بقضايا بخصوص زيارتِكَ إلى هذا الموقعِ. <br> <br> بتَمْكين دعمِ الكوكي على متصفّحِكَ، الإتصال بينكم وهذا الموقعِ مُقَوَّى لِكي يَكُونا مُتَأَكِّدةَ أنتِ التي تَجْعلينَ الصفقاتَ على مصلحتِكِ الخاصةِ، ولمَنْع تسربِ معلوماتِ سريتِكَ.');
?>
