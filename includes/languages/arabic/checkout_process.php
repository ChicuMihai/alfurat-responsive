<?php
/*
  $Id: checkout_process.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('EMAIL_TEXT_SUBJECT', 'طلبيه');
define('EMAIL_TEXT_ORDER_NUMBER', 'طلبيه رقم:');
define('EMAIL_TEXT_INVOICE_URL', 'تفاصيل الفاتوره:');
define('EMAIL_TEXT_DATE_ORDERED', 'تاريخ الطلب:');
define('EMAIL_TEXT_PRODUCTS', 'الأصناف');
define('EMAIL_TEXT_SUBTOTAL', 'المجموع الفرعي:');
define('EMAIL_TEXT_TAX', 'ضرائب:        ');
define('EMAIL_TEXT_SHIPPING', 'الشحن: ');
define('EMAIL_TEXT_TOTAL', 'المجموع:    ');
define('EMAIL_TEXT_DELIVERY_ADDRESS', 'عنوان الإستلام');
define('EMAIL_TEXT_PAYMENT_METHOD', 'طريقة الدفع');
define('EMAIL_TEXT_BILLING_ADDRESS', 'عنوان المحاسبة/الفاتورة');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('TEXT_EMAIL_VIA', 'عن طريق');
?>
