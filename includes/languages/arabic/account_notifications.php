<?php
/*
  $Id: account_notifications.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'حسابي');
define('NAVBAR_TITLE_2', 'تبليغات مُنتَجِ');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('HEADING_TITLE', 'تبليغات مُنتَجِ');

define('MY_NOTIFICATIONS_TITLE', 'تبليغات مُنتَجي');
define('MY_NOTIFICATIONS_DESCRIPTION', 'تَسْمحُ قائمةُ إخطارِ المُنتَجَ لك لبَقاء حديثِ على الكتبِ تَجِدُ مِنْ الإهتمامِ.<br><br>لِكي يَكُونَ حديثَ على كُلّ المُنتَج يَتغيّرُ، يَختارُ <b>تبليغات المُنتَجِ العالميةِ</b>.');

define('GLOBAL_NOTIFICATIONS_TITLE', 'تبليغات المُنتَجِ العالميةِ');
define('GLOBAL_NOTIFICATIONS_DESCRIPTION', 'تبليغات على كُلّ الكتب المتوفرة.');

define('NOTIFICATIONS_TITLE', 'تبليغات مُنتَجِ');
define('NOTIFICATIONS_DESCRIPTION', 'لإزالة إخطار مُنتَجِ، يُوضّحُ الكتبَ أختبر ويَنْقرُ على تكملة.');
define('NOTIFICATIONS_NON_EXISTING', 'هناك حالياً لا مُنتَجاتَ أَشّرتْ لكي تُشعَرَ على. <br> <br> لإضافة الكتبِ إلى قائمةِ إخطارِ مُنتَجِكَ، يَنْقرُ على وصلةِ الإخطارَ متوفرَ على صفحةِ معلوماتِ المُنتَجِ المُفصّلةِ.');

define('SUCCESS_NOTIFICATIONS_UPDATED', 'تبليغات مُنتَجكِ جُدّدَ بنجاح.');
?>
