<?php
/*
  $Id: contact_us.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('ADDRESS', 'العنوان ');
define('DROP_LINE', 'ترك لنا خطا');
define('ADDRESS_INFO', '<p><span class="green"><strong>الفرات للنشر والتوزيع</strong></span>
						<br><br> <strong>
							قسم المبيعات والتوزيع
						</strong><br>  
						رأس بيروت  – 
						شارع عبلا
						<br> بنايـة  بخعازي 
						–  بيـروت  -   لبنـان
						<br> 
						تلفاكس:   750554 -1-00961  <br>
						للإستفسارات: <a href="mailto:sales@alfurat.com">sales@alfurat.com</a>
						<br>  <br><strong>
							الفرات للنشر والتوزيع / المكتبة
						</strong><br>
						شارع الحمراء - بناية رسامني  
						<br>
						ص.ب: 6435 - 113 بيروت - لبنـان
						<br>
						هاتف: 750054   <br>
						فاكس: 750053 - 1 -00961 <br>
للإستفسارات: <a href="mailto:alfurat@alfurat.com">alfurat@alfurat.com</a>
					</p> ');
define('CONTACT_US', 'للإتصال بنا');
define('CALL_US', 'إتصل بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('HEADING_TITLE', 'إتصل بنا');
define('NAVBAR_TITLE', 'إتصل بنا');
define('TEXT_SUCCESS', 'لقد تمت عملية الإرسال بنجاح إلى المسؤولين بموقع السوق.');
define('EMAIL_SUBJECT', 'إستفسار من ' . STORE_NAME);
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('ENTRY_NAME', 'الإسم كاملاً:');
define('ENTRY_EMAIL', 'البريد الإلكتروني:');
define('ENTRY_ENQUIRY', 'إستفسار, سؤال :');
define('SEND_ENQUIRY', 'ارسل رسالة');
define('ERROR_ACTION_RECORDER', 'خطأ :تم بالفعل إرسال النموذج , يرجى المحاولة بعد%s دقائق.');
define('OUR_LOCATION', 'موقعنا ');
?>
