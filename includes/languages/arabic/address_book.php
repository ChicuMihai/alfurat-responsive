<?php
/*
  $Id: address_book.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'إشتراكي');
define('NAVBAR_TITLE_2', 'دفتر العناوين');
define('HEADING_TITLE', 'دفتر عناويني الشخصيه');
define('TABLE_HEADING_NUMBER', 'رقم');
define('TABLE_HEADING_NAME', 'الإسم');
define('TABLE_HEADING_CITY_COUNTRY', 'المدينه / البلد');
define('TEXT_NO_ENTRIES_IN_ADDRESS_BOOK', 'لايوجد إدخالات في دفتر العناوين الخاص بك!');
define('TEXT_MAXIMUM_ENTRIES', '<font color="#ff0000"><b>ملاحظه:</b></font> الحد الأقصى المسموح به %s إدخال دفتر عناوين.');
define('TEXT_MAXIMUM_ENTRIES_REACHED', '<font color="#ff0000"><b>ملاحظه:</b></font>من   الحد الأقصى %s إدخال دفتر عناوين قد تعدى ذلك.');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('PRIMARY_ADDRESS_TITLE', 'العنوان الرئيسي');
define('PRIMARY_ADDRESS_DESCRIPTION', 'هذا العنوان سيستخدم في عمليات المحاسبة والشحن والضرائب اذا لم يحدث به تغيرات لاحقه');
define('ADDRESS_BOOK_TITLE', 'دفتر العناوين');
define('PRIMARY_ADDRESS', 'العنوان الرئيسي');
?>
