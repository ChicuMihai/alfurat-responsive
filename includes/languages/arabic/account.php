<?php
/*
  $Id: account.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/
//Begin JTP - CatNotify
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('EMAIL_CAT_NOTIFICATIONS','View or change my TOWN notification list.');
//End JTP - CatNotify
define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
define('NAVBAR_TITLE', 'إشتراكي');
define('HEADING_TITLE', 'حسابي');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('OVERVIEW_TITLE', 'النظرة العامّة');
define('OVERVIEW_SHOW_ALL_ORDERS', '(مشاهدة الطلبيات)');
define('OVERVIEW_PREVIOUS_ORDERS', 'الطلبات السابقة');

define('MY_ACCOUNT_TITLE', 'إشتراكي');
define('MY_ACCOUNT_INFORMATION', 'إنظرْ أَو غيّرْ معلوماتَ حسابِي');
define('MY_ACCOUNT_ADDRESS_BOOK', 'إنظرْ أَو غيّرْ المداخلَ في دفترِ عناويني');
define('MY_ACCOUNT_PASSWORD', 'غيّرْ كلمةَ سر حسابِي.');

define('MY_ORDERS_TITLE', 'طلباتي');
define('MY_ORDERS_VIEW', 'النظر الي الطلبات التي جَعلتُ');

define('EMAIL_NOTIFICATIONS_TITLE', 'تبليغات بريد إلكتروني');
define('EMAIL_NOTIFICATIONS_NEWSLETTERS', 'إشتركْ أَو غيرَ تَشتركُ مِنْ نشراتِ الأخبار');
define('EMAIL_NOTIFICATIONS_PRODUCTS', 'إنظرْ أَو غيّرْ قائمةَ إخطارِ مُنتَجِي');
define('EMAIL_NOTIFICATIONS_CATEGORIES', 'إنظرْ أَو غيّرْ قائمةَ إخطارِ فئات الكتب');
?>
