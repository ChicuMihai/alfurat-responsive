<?php
/*
  $Id: shopping_cart.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('TEXT_CART_EMPTY', 'سلة التسوق الخاصة بك فارغة!');
define('SUB_TITLE_TOTAL', 'Total:');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TEXT_ALTERNATIVE_CHECKOUT_METHODS', '- OR -');
define('TEXT_OR', 'or ');
define('TEXT_REMOVE', '<img src="images/delete.jpg"/>');
define('TEXT_ALTERNATIVE_CHECKOUT_METHODS', '- OR -');
define('TEXT_QUANTITY', ' الكمية');
define('TEXT_TO_REMOVE', 'هل تريد  حذف  ');
define('TEXT_QUESTION_MARK', '؟');
define('TEXT_PUBLISHEER', ' الناشر');
define('TEXT_AUTHOR', ' المؤلف');
define('TEXT_TRANSLATOR', ' ترجمة');
define('TEXT_RELEASED', ' تاريخ النشر');
define('NAVBAR_TITLE', 'محتويات السله');
define('HEADING_TITLE', 'ماذا في سلتي؟');
define('TABLE_HEADING_REMOVE', 'حذف');
define('TABLE_HEADING_QUANTITY', 'الكميه');
define('TABLE_HEADING_MODEL', 'الرمز أو النوع');
define('TABLE_HEADING_PRODUCTS', 'الكتاب');
define('TABLE_HEADING_TOTAL', 'المجموع');
define('TEXT_CART_EMPTY', 'سلة التسوق لديك فارغه!');
define('SUB_TITLE_SUB_TOTAL', 'المجموع الفرعي:');
define('SUB_TITLE_TOTAL', 'المجموع:');
define('TEXT_DATE_FILM', ' تاريخ الفيلم');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'الأصناف المؤشره بـ *** غير متوفر يرجى حذفه من الطلب و مراسلتنا للبحث في إمكانية تأمينه.');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'الأصناف المؤشره بـ *** غير متوفره بالكميه المرغوبه في مخزوننا.<br>يمكنك شرائها على أي حال ولكن تأكد من الكميه المتوفره في مخزوننا حتى تصل إليكم أثناء معالجة الحسابات.');
//kgt - discount coupons
define('TABLE_HEADING_COUPON', 'هل لديك قسيمة خصم؟' );
//end kgt - discount coupons
?>