<?php
/*
  $Id: conditions.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('CONDITIONS_TEXT', '
			<h2> عملية الطلب</h2>
			<p><span class="green mr-15" ><strong>	كيف تتم عملية شراء الكتب؟</strong></span></p>
			<ul>
				<li>	تبدأ عملية الشراء بالبحث عن الكتاب المطلوب وإختياره بإضافته إلى عربة التسوق من خلال النقر على "إشتري الآن". 
				</li><li>	تظهر عربة التسوق المحتوية على الكتاب الذي تم إختياره بالإضافة إلى كافة التفاصيل المتعلقة بالطلب.
				</li><li>	يتم إنهاء عملية الشراء بالنقر على "محاسبة" الموجودة أسفل محتويات عربة التسوق.
				</li><li>	ثم يتم الإنتقال إلى الصفحة التالية لإدخال كافة المعلومات المطلوبة .
				</li>
			</ul>
			<p><span class="green mr-15"><strong> 	كيف يمكنني التأكد من أن طلبي تم بنجاح؟</strong></span></p>
			<ul><li>
					سيتم إرسال رسالة بالفاتورة على البريد الإلكتروني الخاص بك. </li></ul>
			<p><span class="green mr-15"><strong> 	لقد تم إنشاء طلب من قبلي ولكن لم أستلم الفاتورة على الإيميل , لماذا؟</strong></span></p>
			<ul><li>	يبدو أنك أدخلت البريد الإلكتروني بشكل خاطئ وفي هذه الحالة الرجاء إرسال إيميل من خلال "للإتصال بنا" مع تزويدنا بالإسم و العنوان بالكامل ورقم الهاتف وسيتم تصحيح البريد الإلكتروني.</li></ul>
			<p><span class="green mr-15"><strong> 	كيف يمكنني تعديل محتويات عربة التسوق قبل إتمام عملية الشراء؟</strong></span></p>
			<ul><li>	لتغيير الكمية المطلوبة من أي كتاب في صفحة عربة التسوق يرجى تغيير العدد عند حقل الكمية ومن ثم النقر على " محاسبة".
				</li><li>
					لإزالة أي كتاب من عربة التسوق أنقر على زر "X".</li></ul>
			<p><span class="green mr-15"><strong> 	كيف يمكنني التأكد من الطلبية التي أعددتها ؟</strong></span></p>
			<ul><li>	سيتم إرسال رسالة إلى عنوان البريد الإلكتروني فور إستلامنا الطلبية، نؤكد فيها هذه الطلبية ونزودك برقمها للمتابعة.</li></ul>
			<p><span class="green mr-15"><strong>	الكتاب اللذي أريده غير متوفر على الموقع, مالعمل؟!</strong></span></p>
			<ul><li>	بإمكانك طلب الكتب غير المتوفرة على الموقع من خلال رسالة عبر خانة  "للإتصال بنا" تحتوي على إسم الكتاب و إسم المؤلف و دار النشر وخلال 48 ساعة يتم إعلامك بحال  توفر الكتب المطلوبة.</li></ul>
			<p><span class="green mr-15"><strong>	وجدت الكتاب الذي أبحث عنه لكن صورة الغلاف تختلف عما أعرفه؟</strong></span></p>
			<ul><li>	الكتاب في هذه الحالة يكون متوفر لدار نشر أخرى أو هناك إختلاف بالطبعة.</li></ul>
			<h2> طرق الدفع </h2>
			<ul>
				<li>	يتم الدفع على موقع الفرات للنشر والتوزيع بواسطة البطاقات الإئتمانية (الماستركارد والفيزا).</li><br>
				<li>	يجب أن تكون البطاقة فعالة وغير منتهية الصلاحية.</li><br>
				<li>	الدولار الأميريكي هي العملة المعتدمة عند الدفع.</li><br>
				<li>	تعتمد الفرات للنشر والتوزيع على نظام (SSL) في عملية الشراء لحماية جميع المعلومات التي تتعلق بعملية الشراء، وليس لدى الفرات صلاحية للإطلاع على رقم البطاقة فالعملية تتم مباشرة مع المصرف الذي هو من يتأكد من صحة المعلومات المدخلة. 
				</li><br>
				<li>	إعادة المال:
					<ul>
						<li>	إلغاء الطلب خلال أربعة وعشرون ساعة من تسجيله. (يتحمل الزبون العمولة التي يفرضها المصرف) </li><br>
						<li>	في حال لم يتوفر الكتاب المطلوب . (الفرات تتحمل العمولة التي يفرضها المصرف)
						</li></ul></li>
			</ul>');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE', '  شروط الشراء');
define('HEADING_TITLE', 'شروط الشراء');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TEXT_INFORMATION', 'ضع هنا معلوماتك عن شروط الإستخدام لديك.');
?>
