<?php
/*الصفحة الرئيسية
  $Id: index.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TEXT_MAIN', '');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('TABLE_HEADING_UPCOMING_PRODUCTS', 'الإصدارات القادمة');
define('TABLE_HEADING_DATE_EXPECTED', ' ');
define('TABLE_HEADING_NEW_PRODUCTS', 'أحدث  الملصقات');
if ( ($category_depth == 'products') || (isset($HTTP_GET_VARS['manufacturers_id']) || $translator>0 || $author>0) ) {
  define('HEADING_TITLE', 'لنرى لماذا نحن هنا؟');
  define('TABLE_HEADING_IMAGE', '');
  define('TABLE_HEADING_MODEL', 'النوعيه أو الرمز');
  define('TABLE_HEADING_PRODUCTS', 'إسم الكتاب');
  define('TABLE_HEADING_POSTERS', 'إسم الفيلم');
 define('TABLE_HEADING_MAGAZINES', 'إسم المجلة');
  define('TABLE_HEADING_MANUFACTURER', ' الناشر');
  define('TABLE_HEADING_DIRECTOR', ' المخرج');
  define('TEXT_AUTHOR', ' المؤلف');
  define('TABLE_HEADING_QUANTITY', 'الكميه');
  define('TABLE_HEADING_AUTHOR_PRODUCTS', 'كتب المؤلف ');
  define('TABLE_HEADING_TRANSLATOR_PRODUCTS', 'كتب المترجم ');
  define('TABLE_HEADING_PRICE', 'السعر');
  define('TABLE_HEADING_WEIGHT', 'الوزن');
  define('TABLE_HEADING_BUY_NOW', ' <span class="fontGreen bold">اشتري الآن</span>');
  define('TEXT_NO_PRODUCTS', 'لايوجد حالياً أي كتاب في هذا القسم.');
  define('TEXT_NO_PRODUCTS2', 'لايوجد حالياً أي صنف متاح لهؤلاء الوكلاء الموردون.');
  define('TEXT_NUMBER_OF_PRODUCTS', 'رقم الكتاب: ');
  define('TEXT_SHOW', '');
  define('TEXT_BUY', 'شراء 1 \'');
  define('TEXT_NOW', '\' الآن');
  define('TEXT_ALL_CATEGORIES', 'الكل');
  define('TEXT_ALL_MANUFACTURERS', ' كل الناشرين');
  define('TEXT_ALL_AUTHORS', ' كل المؤلفين');
  define('TEXT_CHOOSE_SERIES', 'إختر السلسلة');
  define('TEXT_ALL_TRANSLATORS', ' كل المترجمين');

} elseif ($category_depth == 'top') {
  define('HEADING_TITLE', 'ماهو الجديد عندنا؟');
} elseif ($category_depth == 'nested') {
  define('HEADING_TITLE', 'Categories');
  define('HEADING_TITLE', 'ماهو الجديد هنا؟');
  // BOF: Featured Products
define('TABLE_HEADING_FEATURED_PRODUCTS', 'Featured Products');
define('TABLE_HEADING_FEATURED_PRODUCTS_CATEGORY', 'Featured Products in %s');

}
?>
