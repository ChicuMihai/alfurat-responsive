<?php
/*
  $Id: password_forgotten.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'دخول');
define('NAVBAR_TITLE_2', 'نسيت كلمة السر');
define('TEXT_MAIN', 'إذا نَسيتَ كلمةَ سركَ، أَدْخلُ عنوان بريدكَ الإلكتروني أدناه و'
		. 'نُرسلُ لك رسالةَ بريد إلكتروني تَحتوي كلمةَ سركِ الجديدةَ.');
define('HEADING_TITLE', 'أنا فقدت كلمة السر الخاصه بي!');
define('ENTRY_EMAIL_ADDRESS', 'البريد الإلكتروني:');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'ملاحظه: البريد الإلكتروني غير موجود في سجلاتنا, الرجاء حاول مره أخرى.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - تذكير كلمة السر');
define('EMAIL_PASSWORD_REMINDER_BODY', 'تذكير كلمة السر تم طلبه بواسطة ' . $REMOTE_ADDR . '.' . "\n\n" . 'كلمة السر في \'' . STORE_NAME . '\' هي:' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'تم بنجاح:  كلمة سر جديدة أُرسلتْ إلى عنوان بريدكِ الإلكتروني.');
define('ERROR_ACTION_RECORDER', 'خطأ: تم بالفعل إرسال رابط إعادة تعيين كلمة السر. يرجى المحاولة مرة أخرى في %s دقائق.');
define('EMAIL_PASSWORD_RESET_BODY', 'A new password has been requested for your account at ' . STORE_NAME . '.' . "\n\n" . 'Please follow this personal link to securely change your password:' . "\n\n" . '%s' . "\n\n" . 'This link will be automatically discarded after 24 hours or after your password has been changed.' . "\n\n" . 'For help with any of our online services, please email the store-owner: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('TEXT_PASSWORD_RESET_INITIATED', 'يرجى التحقق من البريد الإلكتروني الخاص بك للحصول على تعليمات حول كيفية تغيير كلمة المرور الخاصة بك. تعليمات تحتوي على وصلة صالحة فقط لمدة 24 ساعة أو حتى تم تحديث كلمة السر الخاصة بك.');
define('EMAIL_PASSWORD_RESET_SUBJECT', STORE_NAME . ' - New Password');
?>