<?php
/*
  $Id: product_info.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('TOP_BAR_TITLE', 'معلومات الكتاب');
define('TEXT_PRODUCT_NOT_FOUND', 'الكتاب غير موجود!');
define('TEXT_CURRENT_REVIEWS', 'المراجعات الحاليه:');
define('TEXT_MORE_INFORMATION', 'لمزيد من المعلومات, صفحة الكتاب هذه <a href="%s" target="_blank"><u>الصفحه</u></a>.');
define('TEXT_DATE_ADDED', 'هذا الكتاب أُضيف في موقعنا التسويقي بتاريخ %s.');
define('TEXT_DATE_AVAILABLE', '<font color="#ff0000">هذا الكتاب سيكون متوفراً في %s.</font>');
define('TEXT_BOOK_NOT_AVAILABLE', '<font color="#ff0000">هذا الكتاب غير متوفر, يرجى مراسلتنا للبحث في إمكانية تأمينه.</font>');
define('TEXT_ALSO_PURCHASED_PRODUCTS', 'الزبائن الذين أشتروا هذا المنتج أشتروا أيضاً');
define('TEXT_PRODUCT_OPTIONS', ' طريقة التوضيب:');
define('option_description_2', ' يوضع الملصق داخل أنبوب');
define('option_description_1', ' يوضع الملصق داخل ظرف');
define('TEXT_CLICK_TO_ENLARGE', 'تكبير الصوره');
define('TEXT_VIEWING', 'الصفحة الحالية');
define('TEXT_DESCRIPTION', ' نبذة عن الكتاب');
define('TEXT_POSTER_DESCRIPTION', ' التفاصيل');
define('TEXT_MAGAZINES_DESCRIPTION', ' الملاحظات');
define('TEXT_PUBLISHEER', ' الناشر');
define('TEXT_AUTHOR', ' المؤلف');
define('TEXT_TRANSLATOR', ' ترجمة');
define('TEXT_EDITION', ' الطبعة');
define('TEXT_SERIES', 'السلسلة');
define('TEXT_SERIES_NUMBER', 'الرقم في السلسلة');
define('TEXT_AVAILABLE_VOLUME', 'المجلدات المتوفرة');
define('TEXT_ISBN', '   ISBN');
define('TEXT_WEIGHT', ' الوزن');
define('TEXT_WEIGHT_GRAM', ' غرام');
define('TEXT_PAGE_NB', ' عدد الصفحات');
define('TEXT_SIZE', 'القياس');
define('TEXT_RELEASED', ' تاريخ النشر');
define('TEXT_DATE_FILM', ' تاريخ الفيلم');
define('TEXT_DIRECTOR', '  المخرج');
define('TEXT_ACTORS', '  الممثلين');
define('TEXT_COVER_TYPE', '  النوع');
define('TEXT_COVER_TYPE_NORMAL', '   غلاف عادي');
define('TEXT_COVER_TYPE_ARTISTIC', '   غلاف فني ');
?>
