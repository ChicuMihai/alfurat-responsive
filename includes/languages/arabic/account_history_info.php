<?php
/*
  $Id: account_history_info.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'إشتراكي');
define('NAVBAR_TITLE_2', 'محفوظات');
define('NAVBAR_TITLE_3', 'معلومات الطلبيه');
define('HEADING_TITLE', 'معلومات الطلبيه');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('HEADING_ORDER_NUMBER', 'الطلبية #%s');
define('HEADING_ORDER_DATE', 'تاريخ طلبِ:');
define('HEADING_ORDER_TOTAL', 'مجموع طلبِ:');

define('HEADING_DELIVERY_ADDRESS', 'عنوان تسليمِ');
define('HEADING_SHIPPING_METHOD', 'طريقةِ الشحن');

define('HEADING_PRODUCTS', 'الكتب');
define('HEADING_TAX', 'الضريبة');
define('HEADING_TOTAL', 'المجموع');

define('HEADING_BILLING_INFORMATION', 'مُحَاسَبَة المعلوماتِ (الفاتورة)');
define('HEADING_BILLING_ADDRESS', 'مُحَاسَبَة العنوانِ');
define('HEADING_PAYMENT_METHOD', 'طريقة الدفع');

define('HEADING_ORDER_HISTORY', 'تاريخ طلبِ');
define('HEADING_COMMENT', 'التعليقات');
define('TEXT_NO_COMMENTS_AVAILABLE', 'لا تعليقاتَ متوفرةَ.');

define('TABLE_HEADING_DOWNLOAD_DATE', 'الوصلة تَنتهي: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' بَقاء البرامجِ');
define('HEADING_DOWNLOAD', 'وصلات التحميل');
?>
