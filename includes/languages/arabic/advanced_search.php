<?php

/*
  $Id: advanced_search.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
 */define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('TEXT_SEARCH_HELP_LINK', '<u>مساعدة في البحث؟</u>');
define('NAVBAR_TITLE', 'بحث متقدم');
define('HEADING_TITLE', 'أدخل معايير البحث');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('PRICE_FROM', 'السعر من');
define('PRICE_TO', 'السعر الى');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'بحث متقدم');
define('NAVBAR_TITLE_2', 'نتيجة البحث');
define('TABLE_HEADING_POSTERS', 'إسم الفيلم');
define('HEADING_TITLE_1', 'بحث متقدم');
define('TABLE_HEADING_DIRECTOR', ' المخرج');
define('HEADING_TITLE_2', 'نتيجة البحث');
define('HEADING_SEARCH_CRITERIA', 'معاير البحث');
define('TEXT_SEARCH_IN_DESCRIPTION', 'البحث بمواصفات المنتج');
define('ENTRY_PRICE_TO', 'السعر الي:');
define('TEXT_AUTHOR', ' المؤلف');
define('TEXT_PRODUCTS_SIZE', ' القياس');
define('ENTRY_CATEGORIES', 'فئات الكتب:');
define('ENTRY_INCLUDE_SUBCATEGORIES', 'تتضمن الفئات  الفرعيه');
define('ENTRY_MANUFACTURERS', ' الناشر:');
define('ENTRY_PRICE_FROM', 'سعر يبدأ من:');
define('ENTRY_DATE_FROM', 'تاريخ الإضافه من:');
define('ENTRY_DATE_TO', 'إلى:');
define('TEXT_ALL_CATEGORIES', 'كل فئات الكتب');
define('TEXT_ALL_COUNTRIES', 'كل الدول');
define('TEXT_ALL_MANUFACTURERS', 'كل  الناشرين');
define('TEXT_ALL_AUTHORS', 'كل  المؤلفين');
define('TEXT_CHOOSE_SERIES', 'إختر السلسلة');
define('TEXT_ALL_ACTORS', 'كل  الممثلين');
define('TEXT_ALL_TRANSLATORS', 'كل  المترجمين');
define('TEXT_ALL_DIRECTORS', 'كل  المخرجين');
define('TEXT_RELEASE_YEAR', 'سنة النشر');
define('TEXT_CATEGORY_NAME', 'إسم القسم');
define('TEXT_MANUFACTURER_NAME', 'إسم الوكيل المورد');
define('TEXT_PRODUCT_NAME', 'إسم الكتاب');
define('TEXT_PRICE', 'السعر');
define('TEXT_PERFORM_ADVANCED_SEARCH', 'إبدأ البحث المتقدم');
define('TEXT_SEARCH_HELP', '&nbsp;<b>تعرف عن البحث المتقدم!؟</b></font>' . '<br><br>محرك البحث يسمح لك بالبحث بكلمات البحث الرئيسيه عن الكتاب من ناحية إسمه, نوعه, المنتجين, وحتى وصفه<br><br>عندما تبحث عن كلمات, فإنك تستطيع فصل الكلمات أو العبارات بـ و / وكذلك تستطيع فصلها بـ أو. على سبيل المثال, تستطيع إدخال <u>جرير و كتاب</u>.لاحظ الفراغ. وبالتالي سوف يعطي هذا البحث نتيجة الكلمتان معاً . وأيضاً, إذا كتبت <u>عطر أو لعبه</u>, سوف تحصل على قائمة الأصناف التي تحتوي على إحدى الكلمتان أو الكلمتان معاً. إذا لم يتم فصل الكلمات بـ و/أو سوف يتحول التشغيل المنطقي لـ ' . strtoupper(ADVANCED_SEARCH_DEFAULT_OPERATOR) . '.<br><br>وتستطيع كذلك البحث عن كلمات متوافقه ومضبوطه وذلك بتضمينها داخل أقواس الإقتباس. على سبيل المثال, إذا كنت تريد البحث عن <u>"عطر باي"</u>, سوف تحصل بقائمة الأصناف التي تحتوي على الكلمه أو الكلمات الموجوده داخل الأقواس المقتبسه متوافقه ومظبوطه بها.<br><br>الأقواس العاديه تستخدم للتحكم بترتيب المشغل المنطقي. على سبيل المثال, تستطيع إدخال <u>نينتيندو و (كلمه أو إكسسوارات أو "عطر باي")</u>.');
define('ERROR_AT_LEAST_ONE_INPUT', '* أحد الحقول التاليه مطلوب إدخاله:\n    كلمات\n    تاريخ الإضافه من\n    تاريخ الإضافه إلى\n    سعر يبدأ من\n    سعر إلى\n');
define('ERROR_INVALID_FROM_DATE', '* تاريخ الإضافه من خاطئ\n');
define('ERROR_INVALID_TO_DATE', '* إلى تاريخ الإضافه خاطئ\n');
define('ERROR_TO_DATE_LESS_THAN_FROM_DATE', '* التاريخ يجب أن يكون متساوي من ناحية التحديث\n');
define('ERROR_PRICE_FROM_MUST_BE_NUM', '* السعر يجب أن يكون رقم\n');
define('ERROR_PRICE_TO_MUST_BE_NUM', '* السعر يجب أن يكون رقم\n');
define('ERROR_PRICE_TO_LESS_THAN_PRICE_FROM', '* السعران لابد أن يكونان متساويان أو أكبر\n');
define('ERROR_INVALID_KEYWORDS', '* كلمات بحث رئيسيه خاطئه\n');
define('HEADING_SEARCH_HELP', '>تعرف عن البحث المتقدم!؟');
define('TEXT_CLOSE_WINDOW', '<u>اغلاق</u> [x]');
define('TABLE_HEADING_IMAGE', '');
define('TABLE_HEADING_MODEL', 'موديل');
define('TABLE_HEADING_PRODUCTS', 'إسم الكتاب');
define('TABLE_HEADING_MANUFACTURER', 'الناشر');
define('TABLE_HEADING_QUANTITY', 'الكمية');
define('TABLE_HEADING_PRICE', 'السعر');
define('TABLE_HEADING_WEIGHT', 'الوزن');
define('TABLE_HEADING_BUY_NOW', '<span class="fontGreen bold">اشتري الآن</span>');
define('TEXT_NO_PRODUCTS', 'ليس لدينا كتب  مطابقة لمواصفات هذا البحث  ');
define('TEXT_CHOOSE_FILTERS', 'الرجاء ملئ أي خانة من خانات البحث ');
define('ERROR_PRICE_TO_LESS_THAN_PRICE_FROM', 'Price To must be greater than or equal to Price From.');
define('ERROR_INVALID_KEYWORDS', 'Invalid keywords.');
?>
