<?php
/*
  $Id: checkout_success.php for Arabic 2012-02-20 00:52:16Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/define ('SEARCH','بحث ');
define('CLOSE','إغلاق ');
define('CONTINUE3','متابعة ');
  define('CURRENCY', 'العملة ');
  define('ADVANCED', 'بحث متقدم ');
  define('SUPPORT', 'الدعم');
define('HOME', 'الصفحة الرئيسية');
define('ABOUT', 'عن الدار');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء ');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('CONTACT_US', 'للإتصال بنا');
define('CREATE_ACCOUNT', 'إنشاء حساب');
define('LOG_IN', 'دخول الأعضاء');
define('MY_ACCOUNT', 'حسابي');
define('LOG_OUT', 'خروج');
define('POSTERS', 'الملصقات');
define('MAGAZINES', 'المجلات');
define('BOOK_OF_WEEK', 'كتاب  الأسبوع ');
define('ARTICLE_OF_MONTH', 'مقالة الشهر ');
define('SEARCH_FOR_BOOK', 'إبحث عن كتاب ');
define('SEARCH_FOR_POSTER', 'إبحث عن ملصق');
define('ADVANCED_SEARCH', 'بحث  متقدم');
define('TERMS_AND_CONDITIONS', 'شروط الشراء');
define('TABLE_HEADING_FEATURED_PRODUCTS', '  أحدث الإصدارات ');
define('NAVBAR_TITLE_1', 'المحاسبه');
define('NAVBAR_TITLE_2', 'نجحت');
define('TOP_BAR_TITLE', 'عملية المحاسبه إكتملت!');
define('HEADING_TITLE', '	لقد تمت العملية الحسابية بنجاح');
define('TEXT_SUCCESS', 'محاسبة مشترياتك تمت معالجتها بنجاح! سوف يتم تجهيز أصناف طلبيتك في خلال أيام العمل وهي 3-5 أيام.<br><br>تستطيع أن تشاهد وتتابع حالة طلبيتك على صفحة : <a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'NONSSL') . '">\'المشتركين\'</a> وذلك بالضغط على <a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'NONSSL') . '">\'محفوظات\'</a>.<br><br>إذا كان لديكم أي أسئله أو إستفسارات عن طلبياتكم, الرجاء الإرسال إلى <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">مسؤول الموقع</a>.<br><br><font size="3">وشكراً على التسوق والشراء من موقعنا!</font>');
define('FOR_HELP', 'للمساعدة');
define('LOGIN_OR_REGISTER', 'دخول أو تسجيل');
define('FORGOT_YOUR_PASSWORD', 'نسيت كلمة المرور؟');
define('PAYMENT_METHOD', 'طرق الدفع ');
define('CONNECT_WITH_US', 'تــواصل معنا ');
define('TEXT_NOTIFY_PRODUCTS', 'من فضلك أعلمني بأي تحديثات لهذا المنتج:');
define('TEXT_SEE_ORDERS', 'ممكن مشاهدة الطلبيات عن طريق الذهاب الي هذه الوصلة <a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '">\'اشتراكي\'</a> للمشاهدة <a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">\'History\'</a>.');
define('TEXT_CONTACT_STORE_OWNER', 'للاستعلامات <a href="' . tep_href_link(FILENAME_CONTACT_US) . '">مالك المتجر/السوق</a>.');
define('TEXT_THANKS_FOR_SHOPPING', 'شكرا للتسوق معنا علي النت');

define('TABLE_HEADING_COMMENTS', 'أضف تعليق علي الطلبية');

define('TABLE_HEADING_DOWNLOAD_DATE', 'تأريخ منتهي: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' التحميل');
define('HEADING_DOWNLOAD', 'تحميل منتجاتك من هنا:');
define('FOOTER_DOWNLOAD', 'ممكن تحميل منتجاتك/طلبياتك في وقت لاحق \'%s\'');
?>
