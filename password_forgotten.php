<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PASSWORD_FORGOTTEN);
// start modification for reCaptcha
require_once('includes/classes/recaptchalib.php');
// end modification for reCaptcha
$password_reset_initiated = false;

if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    $email_address = tep_db_prepare_input($HTTP_POST_VARS['email_address']);

    $check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
    if (tep_db_num_rows($check_customer_query)) {
        $check_customer = tep_db_fetch_array($check_customer_query);

        $actionRecorder = new actionRecorder('ar_reset_password', $check_customer['customers_id'], $email_address);
        // start modification for reCaptcha
        // the response from reCAPTCHA
        $resp = null;

        // was there a reCAPTCHA response?
        $resp = recaptcha_check_answer (RECAPTCHA_PRIVATE_KEY,
            $_SERVER["REMOTE_ADDR"],
            $_POST["recaptcha_challenge_field"],
            $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) {
            $error = true;
            $messageStack->add('password_forgotten', ENTRY_SECURITY_CHECK_ERROR );
        }else
// end modification for reCaptcha
            if ($actionRecorder->canPerform() ) {
                $actionRecorder->record();

                $reset_key = tep_create_random_value(40);

                tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set password_reset_key = '" . tep_db_input($reset_key) . "', password_reset_date = now() where customers_info_id = '" . (int)$check_customer['customers_id'] . "'");

                $reset_key_url = tep_href_link(FILENAME_PASSWORD_RESET, 'account=' . urlencode($email_address) . '&key=' . $reset_key, 'SSL', false);

                if ( strpos($reset_key_url, '&amp;') !== false ) {
                    $reset_key_url = str_replace('&amp;', '&', $reset_key_url);
                }

                tep_mail($check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'], $email_address, EMAIL_PASSWORD_RESET_SUBJECT, sprintf(EMAIL_PASSWORD_RESET_BODY, $reset_key_url), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

                $password_reset_initiated = true;
            } else {
                $actionRecorder->record(false);

                $messageStack->add('password_forgotten', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_RESET_PASSWORD_MINUTES') ? (int)MODULE_ACTION_RECORDER_RESET_PASSWORD_MINUTES : 5)));
            }
    } else {
        $messageStack->add('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
    }
}

$breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_LOGIN, '', 'SSL'));
$breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_PASSWORD_FORGOTTEN, '', 'SSL'));

require(DIR_WS_INCLUDES . 'template_top.php');
?>
<div>
    <div class="boxTitle"><?php echo HEADING_TITLE; ?></div>

    <?php
    if ($messageStack->size('password_forgotten') > 0) {
        echo $messageStack->output('password_forgotten');
    }

    if ($password_reset_initiated == true) {
        ?>

        <div class="conCon">
            <div class="textContent">
                <?php echo TEXT_PASSWORD_RESET_INITIATED; ?>
            </div>
        </div>

        <?php
    } else {
        ?>

        <?php echo tep_draw_form('password_forgotten', tep_href_link(FILENAME_PASSWORD_FORGOTTEN, 'action=process', 'SSL'), 'post', '', true); ?>

        <div class="conCon">
            <div class="textContent">
                <div><?php echo TEXT_MAIN; ?></div>

                <table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('email_address','','class="form-inputs"'); ?></td>
                    </tr>
                    <!-- start modification for reCaptcha -->
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_SECURITY_CHECK.':'; ?></td>

                        <?php
                        $languages_query = tep_db_query("select code from " . TABLE_LANGUAGES . " where directory = '" . $language . "'");
                        $language_id = tep_db_fetch_array($languages_query);
                        ?>
                        <script>
                            var RecaptchaOptions = {
                                theme : 'white',
                                tabindex : 3,
                                lang : '<?php if (in_array($language_id['code'] ,array('en', 'nl', 'fr', 'de', 'pt', 'ru', 'es', 'tr'))) {echo $language_id['code']; } else {echo 'en'; } ?>',
                            };
                        </script>
                        <td><?php echo recaptcha_get_html(RECAPTCHA_PUBLIC_KEY, null, ($request_type == 'SSL')); ?></td>
                    </tr>
                    <!-- end modification for reCaptcha -->
                </table>
            </div>

            <div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(FILENAME_LOGIN, '', 'SSL')); ?>
        <button type="submit" class="btn-green btn btn-primary"><?php echo CONTINUE3;?></button></span>


            </div>
        </div>

        </form>

        <?php
    }

    require(DIR_WS_INCLUDES . 'template_bottom.php');
    require(DIR_WS_INCLUDES . 'application_bottom.php');
    ?>
